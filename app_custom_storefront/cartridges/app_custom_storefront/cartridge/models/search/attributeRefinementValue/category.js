'use strict';

var base = module.superModule;

/**
 * @function getURL
 * @override app_storefront_base/cartridge/models/search/attributeRefinementValue/category.js
 * To change url for search from searchajax to search-show
 */
function getURL(
    productSearch,
    actionEndpoint,
    id,
    value,
    selected) {
    var url = '';

    if (selected) {
        if (productSearch.category && productSearch.category.parent) {
            url = productSearch
                .urlRefineCategory(actionEndpoint, productSearch.category.parent.ID)
                .relative()
                .toString();
        } else {
            url = productSearch.urlRefineCategory(actionEndpoint, id).relative().toString();
        }
    } else {
        url = productSearch.urlRefineCategory(actionEndpoint, id).relative().toString();
    }

    return url;
};


/**
 * @function CategoryRefinementValueWrapper
 * @override app_storefront_base/cartridge/models/search/attributeRefinementValue/category.js
 *
 */
function CategoryRefinementValueWrapper(productSearch, refinementDefinition, refinementValue, selected) {
    base.call(this, productSearch, refinementDefinition, refinementValue, selected);
    this.url = getURL(productSearch, 'Search-Show', this.id, this.value, this.selected);
}

CategoryRefinementValueWrapper.prototype = Object.create(base.prototype);
module.exports = CategoryRefinementValueWrapper;
