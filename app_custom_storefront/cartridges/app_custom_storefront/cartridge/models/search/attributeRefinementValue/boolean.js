'use strict';

var base = module.superModule;

/**
 * @function getURL
 * @override app_storefront_base/cartridge/models/search/attributeRefinementValue/category.js
 * To change url for search from searchajax to search-show
 */
function getURL(productSearch, actionEndpoint, id, value, selected, selectable) {
    var url = '';

    if (selected) {
        url = productSearch.urlRelaxAttributeValue(actionEndpoint, id, value)
            .relative().toString();
    } else if (!selectable) {
        url = '#';
    } else {
        url = productSearch.urlRefineAttributeValue(actionEndpoint, id, value)
            .relative().toString();
    }

    return url;
}

/**
 * @function BooleanRefinementValueWrapper
 * @override app_storefront_base/cartridge/models/search/attributeRefinementValue/boolean.js
 *
 */
function BooleanRefinementValueWrapper(productSearch, refinementDefinition, refinementValue) {
    base.call(this, productSearch, refinementDefinition, refinementValue);
    this.url = getURL(productSearch, 'Search-Show', this.id, refinementValue.value, this.selected, this.selectable);
}

BooleanRefinementValueWrapper.prototype = Object.create(base.prototype);
module.exports = BooleanRefinementValueWrapper;
