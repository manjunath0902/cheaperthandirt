'use strict';
var base = require('base/search/search');
/** 
 * 
 * 
 * 
 * 
*/
function applyFilterBrand() {
    $(document).ready(function () {
         // Handle refinement value selection and reset click
         $('.container').on(
            'click',
            '.refinement-brand li a, .refinement-bar refinement-brand a.reset, .filter-value refinement-brand a, .swatch-filter refinement-brand a',
            function (e) {
                window.location.href = e.currentTarget.href;
            });

            // refinement-category
        $('.container').on(
            'click',
            '.refinement-category li a, .refinement-bar refinement-category a.reset, .filter-value refinement-category a, .swatch-filter refinement-category a',
            function (e) {
                window.location.href = e.currentTarget.href;
            });
    });
}

var exportSearch = $.extend({}, base, { applyFilterBrand: applyFilterBrand });
module.exports = exportSearch;
