'use strict';

/**
*   js
*   The module implements import logic.
*   @module libPRImport
*/
(function() {
    /* API Includes */
    var Transaction = require('dw/system/Transaction');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var Logger = require('dw/system/Logger');
    var File = require('dw/io/File');
    var dwIO = require('dw/io');
    var Site = require('dw/system/Site');

    /**
    *   PowerReviews data is imported to the appropriate Demandware product (locale-specific if needed).
    *   An email is sent if an error occurs.
    *   @param {String} Locale
    *   @return {Object} Status object
    */
    var libPR = null,
        xmlFile = null,
        products = null,
        fileReader = null,
        tempFile = null,
        tempfileWriter = null,
        xmlStreamReader = null;

    var ImportModule = function (tempLocale) {
        libPR = require('~/cartridge/scripts/lib/libPowerReviewsForImport')(tempLocale);
        tempLocale = libPR.getMappedLocale() || tempLocale;

        this.getProducts = function () {
            products = ProductMgr.queryAllSiteProducts();
        };

        this.deleteReviews = function () {
            var chunkSize = 0;

            Transaction.begin();

            // Loop through all site products
            while (!empty(products) && products.hasNext() && chunkSize < 900) {
                // Reset all product review data
                let product = products.next();
                product.custom.StarRatingCount = 0;
                product.custom.StarRatingBase = 0;
                product.custom.StarRating = 0;
                product.custom.PR_ReviewURL = 'N/A';
                product.custom.PR_QandAURL = 'N/A';

                chunkSize++;
            }

            if (chunkSize > 0) {
                Transaction.commit();
            }
        };

        this.closeProductIterator = function () {
            if (!empty(products)) {
                products.close();
            }
        };

        this.productsHasNext = function () {
            return !empty(products) && products.hasNext();
        };

        this.xmlFileHasNext = function () {
            return !empty(xmlStreamReader) && xmlStreamReader.hasNext();
        };

        this.xmlFileExist = function () {

            var filename = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'PowerReviews' + File.SEPARATOR + 'Reviews' + File.SEPARATOR + 'pwr' + File.SEPARATOR + Site.getCurrent().getCustomPreferenceValue('PR_Review_HashCode') + File.SEPARATOR + 'rawdata' + File.SEPARATOR + 'review_data_summary.xml';

            // Create new file from filename
            xmlFile = new File(filename);

            // Ensure file exists and is an actual file
            return !empty(xmlFile) && xmlFile.exists() && xmlFile.isFile();
        };

        var status = true;
        this.openXMLFile = function () {
            try {
                fileReader = new dwIO.FileReader(xmlFile);
                tempFile = new File(File.TEMP + File.SEPARATOR + 'a.txt');
                // Create new filewrite from temp file
                tempfileWriter = new dwIO.FileWriter(tempFile);
                // Create new xmlstreamreader from filereader
                xmlStreamReader = new dwIO.XMLStreamReader(fileReader);
            } catch (e) {
                status = false;
            }

            return status;
        };

        this.importReviews = function (args) {
            var tempLocale = args.Locale;
            var chunkSize = 0;
            while (!empty(xmlStreamReader) && xmlStreamReader.hasNext()) {
                // Ensure start element
                if (xmlStreamReader.next() === dwIO.XMLStreamConstants.START_ELEMENT) {
                    // Ensure node is a product
                    if (xmlStreamReader.localName === 'product') {
                        var product = xmlStreamReader.readXMLObject();
                        // Init ratingproduct var to represent the demandware product obj
                        let ratingProduct = ProductMgr.getProduct(product.pageid.toString());

                        // Ensure product is available
                        if (!empty(ratingProduct)) {
                            // Write pageid attribute to file
                            tempfileWriter.writeLine(product.pageid.toString());

                            // Get current xmlnode locale
                            var nodelocale = product.attribute('locale');
                            var defaultLocale = dw.system.Site.getCurrent().defaultLocale;
                                // Set product review data to demandware product custom attributes
                                let ratingnum = Number(product.averageoverallrating.toString()),
                                    ratingcount = Number(product.fullreviews.toString());

                                if (chunkSize === 0) {
                                    Transaction.begin();
                                }

                                ratingProduct.custom.StarRatingCount = ratingcount;
                                ratingProduct.custom.StarRatingBase = ratingnum;

                                ratingnum *= 1000;
                                ratingnum += ratingcount;

                                ratingProduct.custom.StarRating = ratingnum;
                                //Save review and q&a urls
                                ratingProduct.custom.PR_ReviewURL = product.inlinefiles.children().toString();
                                ratingProduct.custom.PR_QandAURL =  product.inlinequestionfiles.children().toString();

                                chunkSize++;

                                // Write review and q&a urls to file
                                tempfileWriter.writeLine('Found Review URL ' + product.inlinefiles.children().toString() + ' for product ' + ratingProduct.getID());
                                tempfileWriter.writeLine('Found Q and A URL ' + product.inlinequestionfiles.children().toString() + ' for product ' + ratingProduct.getID());
                        } else {
                            // Log case where product was not found
                            Logger.warn('PowerReviews product id [' + product.pageid.toString().toUpperCase() + '] not found');
                            continue;
                        }
                        tempfileWriter.flush();
                    }
                }

            }
            if (chunkSize > 0) {
                Logger.info(chunkSize + ' PowerReviews products committed');

                Transaction.commit();
            }
        };
    };
    this.closeFiles = function () {
        tempfileWriter.close();
        xmlStreamReader.close();
        fileReader.close();
    };

    module.exports = ImportModule;
}());