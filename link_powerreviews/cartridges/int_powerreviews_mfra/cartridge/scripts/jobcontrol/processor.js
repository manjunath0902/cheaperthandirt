'use strict';

var Status = require('dw/system/Status');
var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger').getLogger('powerreviews');

/**
 * importData
 * imports ratings data via SFTP from PowerReviews
 @returns false on error
 */

function importData() {
    var args = {};
    var libImportAssign = require('~/cartridge/scripts/assign/FullProcessImportAssign');
    if (!libImportAssign.execute(args)) {
        return 'false';
    }

    var locales = Site.getCurrent().getAllowedLocales();
    var initMod = require('~/cartridge/scripts/jobs/initmodule');
    args.InModule = '~/cartridge/scripts/impex/libPRImport';
    args.Actions = ['getProducts'];
    var deleteAssign = require('~/cartridge/scripts/assign/FullProcessImportDeleteReviewsAssign');
    var importReviews = require('~/cartridge/scripts/assign/FullProcessImportReviews');
    for (var i = 0; i < locales.length; i++) {
        var locale = locales[i];
        try {
            if (!request.setLocale(locale)) {
                continue;
            }
        }
        catch (e) {
            Logger.error(e);
            return false;
        }
        args.Locale = locale;
        var res = initMod.execute(args);
        if (res.getStatus() === Status.OK) {
            if (!args.OutModule.xmlFileExist()) {
                continue;
            }
            res = deleteAssign.execute(args);
            if (res.getStatus() === Status.OK) {
                args.Module = args.OutModule;
                process(args);
                importReviews.execute(args);
                process(args);
            }
        }
    }
}

/**
 * process
 * Processes data via SFTP from PowerReviews
 */
function process(args) {
    var Transaction = require('dw/system/Transaction');
    var processProducts = require('~/cartridge/scripts/jobs/processProducts');
    args.Module = args.OutModule;
    Transaction.begin();
    var res = processProducts.execute(args);
    if (res.getStatus() === Status.OK) {
        Transaction.commit();
    }
}

exports.ImportData = importData;
exports.Process = process;
