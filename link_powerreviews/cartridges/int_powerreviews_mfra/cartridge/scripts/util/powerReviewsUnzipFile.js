'use strict';

/**
* powerReviewsUnzipFile.ds
* Unzips PR zip file to the appropriate directory.
*
*    @input zipFile : dw.io.File
*
*/

var Status = require('dw/system/Status');

function execute(args) {
    // Variables
    var utils = require('int_powerreviews_mfra/cartridge/scripts/util/powerReviewsUtils'),
        File = require('dw/io/File'),
        unzipTo = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'PowerReviews' + File.SEPARATOR + 'Reviews'),
        dirString = File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'pwr' + File.SEPARATOR + dw.system.Site.getCurrent().getID();

    (new File(dirString)).mkdirs();
    var pwrFile = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'PowerReviews' + File.SEPARATOR + 'Reviews' + File.SEPARATOR + dw.system.Site.getCurrent().getCustomPreferenceValue('PR_Zip_File_Name'));

      // Confirm FTP connection
    try {
        if (pwrFile.exists()) {
            pwrFile.unzip(unzipTo);
        }
    } catch (e) {
        var Logger = require('dw/system/Logger');
        Logger.error(e);
        return false;
    }

    return new Status(Status.OK);
}

exports.execute = execute;