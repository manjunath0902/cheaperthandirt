'use strict';
/* eslint no-cond-assign: 0 */  // --> OFF
/**


*    Script File
*    Library file that includes getter/setters and util functions
*/

/**
*    Function constructor
*    Initialize public variables
*/
var PowerReviewsUtils = function (locale) {
    var Site = require('dw/system/Site');
    this.siteConfig = Site.getCurrent().getCustomPreferenceValue('PR');
    this.username = Site.getCurrent().getCustomPreferenceValue('PR_FTP_Username');
    this.password = Site.getCurrent().getCustomPreferenceValue('PR_FTP_Password');
    this.host = Site.getCurrent().getCustomPreferenceValue('PR_FTP_Host');
    this.groupId = Site.getCurrent().getCustomPreferenceValue('PRID');
    this.merchantId = 'default';
    this.siteId = 1;
    this.categorySeparator = Site.getCurrent().getCustomPreferenceValue('PRProductFeedCategorySeparator');
    this.productFeedFileName = Site.getCurrent().getCustomPreferenceValue('PRProductFeedFileName');
    this.errorEmail = Site.getCurrent().getCustomPreferenceValue('PR_Email');
    this.errorEmailFrom = Site.getCurrent().getCustomPreferenceValue('PR_Email_From');
    this.hashCode = Site.getCurrent().getCustomPreferenceValue('PR_Review_HashCode');
    this.triggerFileName = Site.getCurrent().getCustomPreferenceValue('PR_Trigger_File_Name');
    this.zipFileName = Site.getCurrent().getCustomPreferenceValue('PR_Zip_File_Name');
    this.catalogId = Site.getCurrent().getCustomPreferenceValue('defaultCatalogId');
    this.showVariants = Site.getCurrent().getCustomPreferenceValue('PRProductVariantShow');
    this.styleOverride = Site.getCurrent().getCustomPreferenceValue('PR_Stylesheet_Override');
    this.orderFeedFileName = Site.getCurrent().getCustomPreferenceValue('PROrderFeedFileName');
    this.triggerFileAction = Site.getCurrent().getCustomPreferenceValue('PR_Trigger_File_Action');
    this.defaultLocaleValue = Site.getCurrent().getCustomPreferenceValue('PRDefaultLocale');
    this.onlineStatus = Site.getCurrent().getCustomPreferenceValue('PR_Online_Status');
    this.sftp = null;
    this.customObject = null;
    this.mappedLocale = null;

    if (!empty(locale)) {
        this.initConfig(locale);
    }
};

/**
*    CONSTANTS
*/
PowerReviewsUtils.IMPEX_POWERREVIEWS_PATH = 'powerreviews/';
PowerReviewsUtils.SRC_FOLDER = '/src/';
PowerReviewsUtils.DELIMITER = '\t';
PowerReviewsUtils.IMAGE_SIZE = 'hero';
PowerReviewsUtils.IN_STOCK = 'IN_STOCK';
PowerReviewsUtils.IN_STOCK_TRUE = 'True';
PowerReviewsUtils.IN_STOCK_FALSE = 'False';
PowerReviewsUtils.PRODUCT_ID_PARAM = 'pid';
PowerReviewsUtils.PRODUCT_URL_PIPELINE = 'Product-Show';
PowerReviewsUtils.ADD_TO_CART_PIPELINE = 'Cart-MiniAddProduct';

/**
*    Getter for groupId Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.initConfig = function(locale) {
    let configs = dw.system.Site.getCurrent().getCustomPreferenceValue('PRLocalesConfigs');
    var StringUtils = require('dw/util/StringUtils');
    for (let i = 0; i < configs.length; i++) {
        let config = configs[i].split('|');
        if (config.length == 3) {
            if (StringUtils.trim(config[0]) == locale) {
                this.siteId = StringUtils.trim(config[1]);
                this.merchantId = StringUtils.trim(config[2]);
            }
        } else if (StringUtils.trim(config[0]) == 'default' && config.length == 4) {
            if (locale == 'default') {
                this.siteId = StringUtils.trim(config[1]);
                this.merchantId = StringUtils.trim(config[2]);
                this.mappedLocale = StringUtils.trim(config[3]);
            }
        } else {
            var Logger = require('dw/system/Logger').getLogger('export-job');
            Logger.error('PowerReviews: Locale Config doesn\'t corrent - {0}', config[i].join('|'));
        }
    }
};

/**
*    Getter for groupId Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getGroupId = function() {
    return !empty(this.groupId) ? this.groupId : '';
};

/**
*    Getter for merchantId Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getMerchantId = function() {
    return this.merchantId;
};

/**
*    Getter for separator Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getCategorySeparator = function() {
    return !empty(this.categorySeparator) ? this.categorySeparator : '>';
};

/**
*    Getter for Product Feed name Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getFeedFileName = function() {
    return this.productFeedFileName;
};

/**
*    Getter for Error Email Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getErrorEmail = function() {
    return this.errorEmail;
};

/**
*    Getter for Error Email From Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getErrorEmailFrom = function() {
    return this.errorEmailFrom;
};

/**
*    Getter for Hash Code Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getHashCode = function() {
    return this.hashCode;
};

/**
*    Getter for Site ID Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getSiteId = function() {
    return !empty(this.siteId) ? this.siteId : '';
};

/**
*    Getter for Trigger File Name Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getTriggerFileName = function() {
    return this.triggerFileName;
};

/**
*    Getter for Trigger File Action Site Pref
*    @return Boolean - Site Pref value
*/
PowerReviewsUtils.prototype.getTriggerFileAction = function() {
    return (this.triggerFileAction.getValue() == 'yes');
};

/**
*    Getter for Zip File Name Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getZipFileName = function() {
    return !empty(this.zipFileName) ? this.zipFileName : 'pwr.zip';
};

/**
*    Getter for Catalog ID Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getCatalogId = function() {
    return this.catalogId;
};

/**
*    Getter for Show Variant Site Pref
*    @return Boolean - Site Pref value
*/
PowerReviewsUtils.prototype.getShowVariants = function() {
    return !empty(this.showVariants) ? this.showVariants : false;
};

/**
*    Getter for Show Variant Site Pref
*    @return Boolean - Site Pref value
*/
PowerReviewsUtils.prototype.getOnlineStatus = function() {
    return !empty(this.onlineStatus) ? this.onlineStatus : false;
};

/**
*    Getter for CSS path Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getStyleOverride = function() {
    return !empty(this.styleOverride) ? this.styleOverride : '';
};

/**
*    Getter for property SFTP User Name
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getUserName = function() {
    return this.username;
};

/**
*    Getter for property SFTP Password
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getPassword = function() {
    return this.password;
};

/**
*    Getter for property SFTP Host
*    @return SFTPClient - Site Pref value
*/
PowerReviewsUtils.prototype.getFtpHost = function() {
    return this.host;
};

/**
*    Getter for SFTP instance object
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getSFTP = function() {
    this.sftp = !empty(this.sftp) ? this.sftp : new dw.net.SFTPClient();
    this.sftp.setTimeout(5000);
    return this.sftp;
};

/**
*    Getter for Order File Name Site Pref
*    @return String - Site Pref value
*/
PowerReviewsUtils.prototype.getOrderFeedFileName = function() {
    return this.orderFeedFileName;
};

/**
*    Getter for PowerReviews CO. In case object doesn't exist create new with predefined attributes
*    @return CustomObject - PowerReviews Custom Object
*/
PowerReviewsUtils.prototype.getCustomObject = function() {
    this.customObject = empty(this.customObject) ? dw.object.queryCustomObject('COPowerReviews', '', null) : this.customObject;

    if (empty(this.customObject)) {
        this.customObject = dw.object.createCustomObject('COPowerReviews', dw.util.UUIDUtils.createUUID());
        this.customObject.custom.uuid = 'COPowerReviews' + dw.system.Site.getCurrent().getID();
        this.customObject.custom.PowerReview_Order_Action = 1;
    }

    return this.customObject;
};

/**
*    Function to get correct file path for server include
*    @param String - full path
*    @return String - path for server include
*/
PowerReviewsUtils.prototype.stripAkamai = function (akamaizedUrl) {

    var strippedAkamaiUrl = akamaizedUrl;
    try {
        if (strippedAkamaiUrl.indexOf('/on/') > -1) {
            strippedAkamaiUrl = strippedAkamaiUrl.substring(strippedAkamaiUrl.indexOf('/on/'), strippedAkamaiUrl.length);
        }
    }
    catch (e) {
        strippedAkamaiUrl = akamaizedUrl;
    }
    return strippedAkamaiUrl;
};

/**
*    Function to get correct URL for server include
*    @param String - Server include content
*    @return String - path for server include
*/
PowerReviewsUtils.prototype.getSingleFileUrl = function (prReviewUrlField) {
    var inlinexml = new Object.XML('<fakecontainer>' + prReviewUrlField + '</fakecontainer>');
    return inlinexml.inlinefile[0].toString();
};

/**
*    Getter Order rule from PowerReviews CO
*    @return String - Rule for Order Feed
*/
PowerReviewsUtils.prototype.getOrderRule = function () {
    return this.getCustomObject().custom.PowerReview_Order_Action.getValue();
};

/**
*    Function that connect to SFTP based on credentials
*    @return Boolean - Status of SFTP connection
*/
PowerReviewsUtils.prototype.connectToFTP = function () {
    try {
        return this.getSFTP().connect(this.host, this.username, this.password);
    } catch (e) {
        throw e;
    }
};

/**
*    Function that connect to SFTP based on credentials
*    @param String - path to insert file
*    @param File - file content
*    @return Boolean - Status of sending file to SFTP
*/
PowerReviewsUtils.prototype.sendToFTP = function (path, file) {
    try {
        return this.getSFTP().putBinary(path, file);
    } catch (e) {
        throw e;
    }
};

/**
*    Function that pull file from SFTP
*    @param String - path to insert file
*    @param File - file content
*    @return Boolean - Status of getting file to SFTP
*/
PowerReviewsUtils.prototype.getFromFTP = function (path, file) {
    try {
        return this.getSFTP().getBinary(path, file);
    } catch (e) {
        throw e;
    }
};

/**
*    Function that validates Site Prefs
*    @return Boolean - Status of validation
*/
PowerReviewsUtils.prototype.validatePrefs = function () {

    var value = this.getMerchantId() || false;

    value = this.getGroupId() && value;
    value = this.getCategorySeparator() && value;
    value = this.getFeedFileName() && value;
    value = this.getErrorEmail() && value;
    value = this.getErrorEmailFrom() && value;
    value = this.getUserName() && value;
    value = this.getPassword() && value;
    value = this.getFtpHost() && value;
    value = this.getHashCode() && value;
    value = this.getSiteId() && value;
    value = this.getTriggerFileName() && value;
    value = this.getZipFileName() && value;
    value = this.getCatalogId() && value;

    return value;
};

/**
*    Function that return Array of Product
*    @param Iteratir - Basket products
*    @return Array - [masterID, variantID, '', quantity, price]
*/
PowerReviewsUtils.prototype.getItems = function (lineItems) {
    var items = [];

    while (!empty(lineItems) && lineItems.hasNext()) {
        var item = lineItems.next(),
            product = item.getProduct(),
            line = [];

        if (!empty(product) && !item.bundledProductLineItem) {
            var productsID = this.getMasterAndVariationId(product);

            line[0] = productsID.masterID;
            line[1] = productsID.variationID;
            line[2] = '';
            line[3] = item.getQuantityValue();
            line[4] = product.getPriceModel().getPrice().getValue();
            items.push(line);
        } else if (item.optionProductLineItem) {

            line[0] = item.getOptionID();
            line[1] = '';
            line[2] = '';
            line[3] = item.getQuantityValue();
            line[4] = item.getBasePrice().getValue();
            items.push(line);
        }

    }
    return items;
};

/**
*    Function that get Master and Variant IDs
*    @param Product - Product
*    @return Object - {masterID, variantID}
*/
PowerReviewsUtils.prototype.getMasterAndVariationId = function (product) {
    var masterID = '',
        variationID = '';

    if (product.master || product.bundle || product.productSet || (product.product && !product.variant)) {
        masterID = product.getID();
    } else {
        masterID = product.getVariationModel().getMaster().getID();
        variationID = product.getID();
    }

    return {
        'masterID': masterID,
        'variationID': variationID
    };

};

/**
*    Function that return part of query
*    @param Object - Key : Value parameters
*    @return String - GET query
*/
PowerReviewsUtils.prototype.getQuery = function (input) {
    var query = '?',
        output = [];

    for (var key in input) {
        if (input.hasOwnProperty(key) && !empty(input[key])) {
            output.push(key + '=' + input[key]);
        }
    }

    query += output.join('&');
    return query;
};

/**
*    Getter for Locale
*    @return String - Locale value
*/
PowerReviewsUtils.prototype.getDefaultValue = function () {
    var value = 'en_US';
    if ((new RegExp('([a-z]{2}_[A-Z]{2})','g')).test(this.defaultLocaleValue)) {
        value = this.defaultLocaleValue;
    }
    return value;
};

PowerReviewsUtils.prototype.getFileContent = function(path) {
    var content = [];
    var fileReader = new dw.io.FileReader(new dw.io.File(path), 'UTF-8'),
        item;
    try {
        // 50,000 is current limit of maximum number of characters,
        // which can be read with dw.io.Reader.read(n) in one call
        // https://xchange.demandware.com/docs/DOC-6533
        while (item = fileReader.read(50000)) {
            content.push(item);
        }
    } catch (e) {
        dw.system.Logger.error(e);
    } finally {
        fileReader.close();
    }
    return content.join('');
};