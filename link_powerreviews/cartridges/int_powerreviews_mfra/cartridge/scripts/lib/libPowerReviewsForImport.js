'use strict';

/**
*    libPowerReviewsForImport.js
*    The main library that includes getter/setters for PowerReviews configuration data
*   and exports util methods used by other modules.
*   @module libPowerReviewsForImport
*/
/* Script Modules */

/* API Includes */
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger').getLogger('powerreviews');
var Status = require('dw/system/Status');
var Site = require('dw/system/Site');
var File = require('dw/io/File');
var dwUtil = require('dw/util');

/**
*    Initialize private PowerReviews configuration variables.
*   The data is accessible only via getter/setter methods.
*   @constructor
*   @param {String} Optional locale parameter
*/
function PRSitePrefs(locale) {
    this.siteConfig = Site.getCurrent().getCustomPreferenceValue('PR');
    this.username = Site.getCurrent().getCustomPreferenceValue('PR_FTP_Username');

    this.password = Site.getCurrent().getCustomPreferenceValue('PR_FTP_Password');
    this.host = Site.getCurrent().getCustomPreferenceValue('PR_FTP_Host');
    this.sftpPortNumber = Site.getCurrent().getCustomPreferenceValue('PR_FTP_Port');
    this.groupId = Site.getCurrent().getCustomPreferenceValue('PRID');
    this.siteId = '1';
    this.categorySeparator = '>';
    this.productFeedFileName = Site.getCurrent().getCustomPreferenceValue('PRProductFeedFileName');
    this.errorEmail = Site.getCurrent().getCustomPreferenceValue('PR_Email');
    this.errorEmailFrom = Site.getCurrent().getCustomPreferenceValue('PR_Email_From');
    this.hashCode = Site.getCurrent().getCustomPreferenceValue('PR_Review_HashCode');
    this.triggerFileName = Site.getCurrent().getCustomPreferenceValue('PR_Trigger_File_Name');
    this.zipFileName = Site.getCurrent().getCustomPreferenceValue('PR_Zip_File_Name');
    this.catalogId = Site.getCurrent().getCustomPreferenceValue('defaultCatalogId');
    this.showVariants = Site.getCurrent().getCustomPreferenceValue('PRProductVariantShow');
    this.styleOverride = Site.getCurrent().getCustomPreferenceValue('PR_Stylesheet_Override');
    this.orderFeedFileName = Site.getCurrent().getCustomPreferenceValue('PROrderFeedFileName');
    this.triggerFileAction = Site.getCurrent().getCustomPreferenceValue('PR_Trigger_File_Action');
    this.defaultLocaleValue = Site.getCurrent().getCustomPreferenceValue('PRDefaultLocale');
    this.onlineStatus = Site.getCurrent().getCustomPreferenceValue('PR_Online_Status');
    this.apiKey = Site.getCurrent().getCustomPreferenceValue('PRApiKey');
    this.structuredData = Site.getCurrent().getCustomPreferenceValue('PrIncludeStructuredData');
    this.sftp = null;
    this.customObject = null;
    this.mappedLocale = null;

    if (!empty(locale)) {
        this.initConfig(locale);
    }
}

/**
*    The method updates site preferences based on given locale
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.initConfig = function(locale) {
    var configs = Site.getCurrent().getCustomPreferenceValue('PRLocalesConfigs');

    for (var i = 0; i < configs.length; i++) {
        var config = configs[i].split('|');
        if (config.length === 3) {
            if (dwUtil.StringUtils.trim(config[0]) === locale) {
                this.siteId = dwUtil.StringUtils.trim(config[1]);
                this.merchantId = dwUtil.StringUtils.trim(config[2]);
            }
        } else if (dwUtil.StringUtils.trim(config[0]) === 'default' && config.length === 4) {
            if (locale === 'default') {
                this.siteId = dwUtil.StringUtils.trim(config[1]);
                this.merchantId = dwUtil.StringUtils.trim(config[2]);
                this.mappedLocale = dwUtil.StringUtils.trim(config[3]);
            }
        } else {
            Logger.error('PowerReviews: Locale Config isn\'t corrent - {0}', config[i].join('|'));
        }
    }
};

/**
*    Getter for groupId Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getGroupId = function() {
    return !empty(this.groupId) ? this.groupId : '';
};

/**
*    Getter for merchantId Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getMerchantId = function() {
    return this.merchantId;
};


/**
*   Getter for mappedLocale Site Pref
*   @return {String} Site Pref value
*/
PRSitePrefs.prototype.getMappedLocale = function() {
    return this.mappedLocale;
};

/**
*    Getter for separator Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getCategorySeparator = function() {
    return this.categorySeparator;
};

/**
*    Getter for Product Feed name Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getFeedFileName = function() {
    return this.productFeedFileName;
};

/**
*    Getter for Error Email Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getErrorEmail = function() {
    return this.errorEmail;
};

/**
*    Getter for Error Email From Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getErrorEmailFrom = function() {
    return this.errorEmailFrom;
};

/**
*    Getter for Hash Code Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getHashCode = function() {
    return this.hashCode;
};

/**
*    Getter for Site ID Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getSiteId = function() {
    return !empty(this.siteId) ? this.siteId : '';
};

/**
*    Getter for Trigger File Name Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getTriggerFileName = function() {
    return this.triggerFileName;
};

/**
*    Getter for Trigger File Action Site Pref
*    @return {Boolean} Site Pref value
*/
PRSitePrefs.prototype.getTriggerFileAction = function() {
    return (this.triggerFileAction.getValue() == 'yes');
};

/**
*    Getter for Zip File Name Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getZipFileName = function() {
    return !empty(this.zipFileName) ? this.zipFileName : 'pwr.zip';
};

/**
*    Getter for Catalog ID Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getCatalogId = function() {
    return this.catalogId;
};

/**
*    Getter for Show Variant Site Pref
*    @return {Boolean} Site Pref value
*/
PRSitePrefs.prototype.getShowVariants = function() {
    return !empty(this.showVariants) ? this.showVariants : false;
};

/**
*    Getter for Show Variant Site Pref
*    @return {Boolean} Site Pref value
*/
PRSitePrefs.prototype.getOnlineStatus = function() {
    return !empty(this.onlineStatus) ? this.onlineStatus : false;
};

/**
*    Getter for Show API Key Pref
*    @return {Boolean} Site Pref value
*/
PRSitePrefs.prototype.getApiKey = function() {
    return !empty(this.apiKey) ? this.apiKey : false;
};

/**
*    Getter for Setting Structured Data
*    @return {Boolean} Site Pref value
*/
PRSitePrefs.prototype.getStructuredData = function() {
    return !empty(this.structuredData) ? this.structuredData : false;
};
/**
*    Getter for CSS path Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getStyleOverride = function() {
    return !empty(this.styleOverride) ? this.styleOverride : '';
};

/**
*    Getter for property SFTP User Name
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getUserName = function() {
    return this.username;
};

/**
*    Getter for property SFTP Password
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getPassword = function() {
    return this.password;
};

/**
*    Getter for property SFTP Host
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getFtpHost = function() {
    return this.host;
};

/**
*    Getter for SFTP instance object
*    @return {SFTPClient} Site Pref value
*/
PRSitePrefs.prototype.getSFTP = function() {
    var sftpClient = require('dw/net/SFTPClient')();
    this.sftp = !empty(this.sftp) ? this.sftp : sftpClient;
    this.sftp.setTimeout(5000);
    return this.sftp;
};

/**
*    Getter for Order File Name Site Pref
*    @return {String} Site Pref value
*/
PRSitePrefs.prototype.getOrderFeedFileName = function() {
    return this.orderFeedFileName;
};

/**
*    Getter for PowerReviews CO. In case object doesn't exist create new with predefined attributes
*    @return {CustomObject} PowerReviews Custom Object
*/
PRSitePrefs.prototype.getCustomObject = function() {
    this.customObject = empty(this.customObject) ? CustomObjectMgr.queryCustomObject('COPowerReviews', '', null) : this.customObject;
    var dwUtil = require('dw/util');
    if (empty(this.customObject)) {
        Transaction.wrap(function () {
            this.customObject = CustomObjectMgr.createCustomObject('COPowerReviews', dwUtil.UUIDUtils.createUUID());
            this.customObject.custom.uuid = 'COPowerReviews' + Site.getCurrent().getID();
            this.customObject.custom.PowerReview_Order_Action = 1;
        });
    }

    return this.customObject;
};


/**
*    Getter Order rule from PowerReviews CO
*    @return {String} Rule for Order Feed
*/
PRSitePrefs.prototype.getOrderRule = function () {

    return this.getCustomObject().custom.PowerReview_Order_Action.getValue();
};



/** Exported utility functions. */

/**
*   @return {Object} Constants
*/

function getConstants() {
    return {
        IMPEX_POWERREVIEWS_PATH: 'powerreviews/',
        SRC_FOLDER: '/src/',
        DELIMITER: ',',
        EXTENSION: '.csv',
        IMAGE_SIZE: 'hero',
        IN_STOCK: 'IN_STOCK',
        IN_STOCK_TRUE: 'True',
        IN_STOCK_FALSE: 'False',
        PRODUCT_ID_PARAM: 'pid',
        PRODUCT_URL_PIPELINE: 'Product-Show',
        ADD_TO_CART_PIPELINE: 'Cart-MiniAddProduct'
    };
}


/**
*    Function that connect to SFTP based on credentials
*   @param {Object} Instance of PRSitePrefs
*    @return {Boolean} Status of SFTP connection
*/
function connectToFTP(sitePrefs) {
    try {
        var result = sitePrefs.getSFTP().connect(sitePrefs.host, sitePrefs.sftpPortNumber, sitePrefs.username, sitePrefs.password);
        return  result;
    } catch (e) {
        Logger.error(e);
        return false;
    }
}

/**
*    Function that connect to SFTP based on credentials
*    @param {String} path to insert file
*    @param {File} file content
*   @param {Object} Instance of PRSitePrefs
*    @return {Boolean} Status of sending file to SFTP
*/
function sendToFTP(path , file, sitePrefs) {
    try {
        return sitePrefs.getSFTP().putBinary(path, file);
    } catch (e) {
        Logger.error(e);
        return false;
    }
}

/**
*    Function that pull file from SFTP
*    @param {String} path to insert file
*    @param {File} file content
*   @param {Object} Instance of PRSitePrefs
*    @return {Boolean} Status of getting file to SFTP
*/
function getFromFTP(path , file, sitePrefs) {
    try {
        var sftp = sitePrefs.getSFTP();
        var result = sftp.getBinary(path, file);
        return result;
    } catch (e) {
        Logger.error(e);
        return false;
    }
}

/**
*    Function that validates Site Prefs
*   @param {Object} Instance of PRSitePrefs
*    @return {Object} Status of validation
*/
function validateSitePrefs(sitePrefs) {

    var value = sitePrefs.getMerchantId() || false;

    value = sitePrefs.getGroupId() && value;
    value = sitePrefs.getCategorySeparator() && value;
    value = sitePrefs.getFeedFileName() && value;
    value = sitePrefs.getErrorEmail() && value;
    value = sitePrefs.getErrorEmailFrom() && value;
    value = sitePrefs.getUserName() && value;
    value = sitePrefs.getPassword() && value;
    value = sitePrefs.getFtpHost() && value;
    value = sitePrefs.getHashCode() && value;
    value = sitePrefs.getSiteId() && value;
    value = sitePrefs.getTriggerFileName() && value;
    value = sitePrefs.getZipFileName() && value;
    value = sitePrefs.getCatalogId() && value;

    return {
        'data': value,
        'status': new Status(Status.OK)
    };
}

/**
*    Function that return Array of Product
*    @param {Iteratir} Basket products
*    @return {Array} [masterID, variantID, '', quantity, price]
*/
function getItems(lineItems) {
    var line = '';

    var idx = 0;
    while (!empty(lineItems) && lineItems.hasNext()) {
        var item = lineItems.next();
        var product = item.getProduct();

        if (!empty(product) && !item.bundledProductLineItem) {
            if (idx++ > 0) {
                line += ',';
            }
            var productsID = getMasterAndVariationId(product);
            line += '{\'page_id\': ' + '\'' + productsID.masterID + '\',';
            if (!empty(productsID.variationID)) {
                line += '\'page_id_variant\': ' + '\'' + productsID.variationID + '\',';
            }
            else {
                line += '\'page_id_variant\': ' + '\'' + '' + '\',';
            }
            line += '\'product_name\':' + '\'' + product.getName() + '\',';
            line += '\'quantity\':' + '\'' + item.getQuantityValue() + '\',';
            line += '\'unit_price\':' + '\'' + product.getPriceModel().getPrice().getValue() + '\'' + '}';
        } else if (item.optionProductLineItem) {
            if (idx++ > 0) {
                line += ',';
            }

            line += '{\'page_id\': ' + '\'' + item.getOptionID() + '\',';
            line += '\'page_id_variant\':' + '\'' + '' + '\',';
            line += '\'product_name\':' + '\'' + '' + '\',';
            line += '\'quantity\':' + '\'' + item.getQuantityValue() + '\',';
            line += '\'unit_price\':' + '\'' + item.getBasePrice().getValue() + '\'' + '}';
        }

    }
    return line;
}

/**
*    Function that get Master and Variant IDs
*    @param {Product} Product
*    @return {Object} {masterID, variantID}
*/
function getMasterAndVariationId(product) {
    var masterID = '',
        variationID = '';

    if (product.master || product.bundle || product.productSet || (product.product && !product.variant)) {
        masterID = product.getID();
    } else {
        masterID = product.getVariationModel().getMaster().getID();
        variationID = product.getID();
    }

    return {
        'masterID': masterID,
        'variationID': variationID
    };

}

/**
*    Function that return part of query
*    @param {Object} Key : Value parameters
*    @return {String} GET query
*/
function getQuery(input) {
    var query = '?',
        output = [];

    for (var key in input) {
        if (input.hasOwnProperty(key) && !empty(input[key])) {
            output.push(key + '=' + input[key]);
        }
    }

    query += output.join('&');
    return query;
}


/**
*   Unzips PR zip file to the appropriate directory.
*   @param {File} Key : Value parameters
*   @param {Object} Instance of PRSitePrefs
*   @return {Object} Status object
*/
function unzipFile(zipFile, sitePrefs) {
    // Variables
    var unzipTo = new File(File.CATALOGS + File.SEPARATOR + sitePrefs.getCatalogId() + File.SEPARATOR + 'default' + File.SEPARATOR),
        dirString = File.TEMP + File.SEPARATOR + 'pwr' + File.SEPARATOR + Site.getCurrent().getID();

    (new File(dirString)).mkdirs();
    var pwrFile = zipFile;

      // Confirm FTP connection
    try {
        if (pwrFile.exists()) {
            pwrFile.unzip(unzipTo);
        }
    } catch (e) {
        Logger.error(e);
        return {'status': new Status(Status.ERROR)};
    }
    return {'status': new Status(Status.OK)};
}


/**
*   Used to determine whether or not the PR trigger file (typically done.txt) exists.
*   @param {Object} Instance of PRSitePrefs
*   @return {Object} Status object
*
*/
function triggerExists(sitePrefs) {
    var triggerFilename = !empty(sitePrefs.getTriggerFileName()) ? sitePrefs.getTriggerFileName() : 'done.txt',
        dirPath = File.IMPEX + File.SEPARATOR + 'pwr' + File.SEPARATOR + Site.getCurrent().getID(),
        localFilename = dirPath +  File.SEPARATOR + triggerFilename;

    (new File(dirPath)).mkdirs();
    var triggerExists = true;
    var triggerFile = new File(localFilename);

    try {
        if (connectToFTP(sitePrefs)) {
            triggerExists = getFromFTP(triggerFilename, triggerFile, sitePrefs);
            if (sitePrefs.getTriggerFileAction()) {
                if (triggerExists) {
                    sitePrefs.getSFTP().del(triggerFilename);
                }
                sitePrefs.getSFTP().disconnect();
            } else {
                triggerExists = true;
            }
        } else {
            return {'status': new Status(Status.ERROR)};
        }
    } catch (e) {
        Logger.error(e);
        return {'status': new Status(Status.ERROR)};
    }
    return {
        'data': triggerExists,
        'status': new Status(Status.OK)
    };
}



/**
*   Downloads PR zip file. Sends an email if there is an error.
*   @param {Object} Instance of PRSitePrefs
*   @return {Object} Status object
*
*/
function getFileFtp(sitePrefs) {

    var downloadFilename = sitePrefs.getZipFileName(),
        dirPath = File.IMPEX + File.SEPARATOR + 'pwr' + File.SEPARATOR + Site.getCurrent().getID();
    (new File(dirPath)).mkdirs();
    var localFilename = dirPath + File.SEPARATOR + downloadFilename;

    var pwrFile = new File(localFilename);

    try {
        if (!(connectToFTP(sitePrefs) && getFromFTP(downloadFilename, pwrFile, sitePrefs))) {
            return {'status': new Status(Status.ERROR)};
        }
    } catch (e) {
        Logger.error(e);
        return {'status': new Status(Status.ERROR)};
    }

    return {
        'data': pwrFile,
        'status': new Status(Status.OK)
    };
}


/**
*   Reads file as string and save it into param FileContent
*   @param {Object} product
*    @param {String} Type
*   @return {Object} Status object
*/

function getFileContent(product, type) {
    var content = '',
        path = '',
        sep = File.SEPARATOR,
        fileReader = null,
        file = null,
        item = '';

    if (type === 'reviews' && 'PR_ReviewURL' in product.custom && product.custom.PR_ReviewURL !== 'N/A') {
        path = sanitizePath(product.custom.PR_ReviewURL);
    } else if (type == 'answers' && 'PR_QandAURL' in product.custom && product.custom.PR_QandAURL !== 'N/A') {
        path = sanitizePath(product.custom.PR_QandAURL);
    } else {
        return {'status': new Status(Status.ERROR)};
    }

    path = File.CATALOGS + sep + Site.getCurrent().getCustomPreferenceValue('defaultCatalogId') + sep + 'default' + sep + path;
    file = new File(path);
    if (!file.exists()) {

        return {'status': new Status(Status.ERROR)};
    }

    fileReader = new dw.io.FileReader(file, 'UTF-8');
    try {
        // 50,000 is current limit of maximum number of characters,
        // which can be read with dw.io.Reader.read(n) in one call
        // https://xchange.demandware.com/docs/DOC-6533
        while (fileReader.ready() && (item = fileReader.read(30000))) {
            content += item;
        }
    } catch (e) {
        content = '';
        Logger.error(e);
        return false;
    } finally {
        fileReader.close();
    }

    return {
        'data': content,
        'status': new Status(Status.OK)
    };
}


/**
*   Helper function.
*   @param {String} Path
*   @return {String} Representation of XML object
*/
function sanitizePath(path) {
    var inlinexml = new Object.XML('<fakecontainer>' + path + '</fakecontainer>');
    return inlinexml.inlinefile[0].toString();
}


/**
*   Returns part of a product's url.
*   @param {String} Pipeline to search for in the click stream
*    @param {String} Additional parameters
*    @param {String} Master Product ID
*    @param {String} Variation Product ID
*    @param {String} Questions ID
*    @param {String} Locale ID
*   @return {Object} Status object
*
*/
function findLastClickStream(pipeLineName, pipeLineParams, masterId, variationId, questionId) {
    var urlu = require('dw/web/URLUtils');
    var list = session.clickStream.clicks.iterator(),
        location = '',
        URLUtils = urlu,
        query = getQuery({
            'pid': masterId,
            'vid': variationId,
            'appName': pipeLineParams,
            'questionId': questionId
        }),
        click = (!empty(list) && list.hasNext()) ? list.next() : null;

    while (!empty(list) && list.hasNext() && click.pipelineName !== pipeLineName) {
        click = list.next();
    }

    location = !empty(click) ? URLUtils.http(pipeLineName) + query : URLUtils.httpsHome();

    return {
        'data': location,
        'status': new Status(Status.OK)
    };
}

/**
* Wrapper to getQuery function to be able to call it isset tag
*   @param {Iterator} Product ID's
*   @param {Object} Instance of PRSitePrefs
*   @return {String} GET query
*/

function getQueryWrapper(productIDs, sitePrefs) {
    return getQuery({'pid': productIDs.masterID,
        'vid': sitePrefs.getShowVariants() ? productIDs.variationID : ''});
}


/**
*   Export function which takes optional locale value and initialize instance of PRSitePrefs
*   based on its value and return object interface.
*/
module.exports = function (locale) {

    // Private instance of PowerReviews configuration data.
    var sitePrefs = new PRSitePrefs(locale);

    // Library interface.
    return {
        getFileContent: getFileContent,

        getConstants: getConstants,

        findLastClickStream: findLastClickStream,

        getMasterAndVariationId: getMasterAndVariationId,

        getQuery: getQuery,

        getItems: getItems,

        validateSitePrefs: function() {
            return validateSitePrefs(sitePrefs);
        },

        getQueryWrapper: function(productIDs) {
            return getQueryWrapper(productIDs, sitePrefs);
        },

        triggerExists: function() {
            return triggerExists(sitePrefs);
        },

        getFileFtp: function() {
            return getFileFtp(sitePrefs);
        },

        unzipFile: function(zipFile) {
            return unzipFile(zipFile, sitePrefs);
        },

        connectToFTP: function() {
            return connectToFTP(sitePrefs);
        },

        sendToFTP: function(path , file) {
            return sendToFTP(path , file, sitePrefs);
        },

        getMappedLocale: function() {
            return sitePrefs.getMappedLocale();
        },

        getCatalogId: function() {
            return sitePrefs.getCatalogId();
        },

        getHashCode: function() {
            return sitePrefs.getHashCode();
        },

        getCustomObject: function() {
            return sitePrefs.getCustomObject();
        },

        getOrderRule: function() {
            return sitePrefs.getOrderRule();
        },

        getOrderFeedFileName: function() {
            return sitePrefs.getOrderFeedFileName();
        },

        getFeedFileName: function() {
            return sitePrefs.getFeedFileName();
        },

        getShowVariants: function() {
            return sitePrefs.getShowVariants();
        },

        getCategorySeparator: function() {
            return sitePrefs.getCategorySeparator();
        },

        getOnlineStatus: function() {
            return sitePrefs.getOnlineStatus();
        },

        getApiKey: function() {
            return sitePrefs.getApiKey();
        },

        getSiteId: function() {
            return sitePrefs.getSiteId();
        },

        getStyleOverride: function() {
            return sitePrefs.getStyleOverride();
        },

        getGroupId: function() {
            return sitePrefs.getGroupId();
        },
        getMerchantId: function() {
            return sitePrefs.getMerchantId();
        },
        getStructuredData: function() {
            return sitePrefs.getStructuredData();
        },
        getProductDescription: function(product) {
            var ldesc = product.getShortDescription();
            var desc = '';
            if (ldesc instanceof dw.content.MarkupText) {
                desc = ldesc.getMarkup();
                //remove unwanted carriage returns.
                desc = desc.replace(/[\n\r]+/g, '');
                return desc;
            }
            else if (ldesc instanceof String) {
                desc = ldesc;
            }
            else {
                desc = '';
            }

            try {
                desc = desc.replace(new RegExp(/(\n|\r|\t)/g));
            }
            catch (e) {
                Logger.error('PowerReviews: Product description string  regex error - {0}', e);
            }

            return desc;
        },
        getCategories: function(product) {
            var cats = product.getOnlineCategories();
            var desc = cats.toArray().map(function (category) {
                return category.displayName;
            }).join('>');

            return desc;
        },
        getPrice: function(product) {
            var priceM = product.getPriceModel();
            var price = priceM.getMinPrice().decimalValue;
            return price;
        },
        getManufacturerSKU: function(product) {
            if (!empty(product.getManufacturerSKU())) {
                var foo = product.getManufacturerSKU();
                return foo;
            }

            return '';
        },
        getBrand: function(product) {
            if (!empty(product.getBrand())) {
                return product.getBrand();
            }

            return '';
        },
        getUPC: function(product) {
            if (!empty(product.getUPC())) {
                return product.getUPC();
            }

            return '';
        }
    };
};
