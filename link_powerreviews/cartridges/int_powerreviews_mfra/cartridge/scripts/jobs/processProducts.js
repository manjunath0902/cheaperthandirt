'use strict';

/**
*
*    @input Module : Object
*    @input Actions : Array
*
*/

var Status = require('dw/system/Status');
var Logger = require('dw/system/Logger');

function execute(args) {
    for (let action in args.Actions) {
        if (typeof args.Module[args.Actions[action]] === 'function') {
            try {
                args.Module[args.Actions[action]](args);
            } catch (e) {
                Logger.error(e);
                return Status(Status.ERROR);
            }
        }
    }

    return new Status(Status.OK);
}

exports.execute = execute;
