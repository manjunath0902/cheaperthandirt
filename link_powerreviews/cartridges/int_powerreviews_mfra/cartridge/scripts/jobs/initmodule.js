'use strict';

/**
*
*    @input InModule : String
*    @input Locale : String
*    @input SubModule : String
*    @input Actions : Array
*    @output OutModule : Object
*
*/
var Status = require('dw/system/Status');
var Logger = require('dw/system/Logger');

function execute(args) {

    try {
        var locale = args.Locale;
        var  module = !empty(args.SubModule) ? new (require(args.InModule))[args.SubModule](locale) : new (require(args.InModule))(locale);
        var actions = args.Actions;

        for (let action in actions) {
            try {
                if (typeof module[actions[action]] === 'function') {
                    module[actions[action]]();
                }
            } catch (e) {
                Logger.error(e);
                return new Status(Status.ERROR);
            }
        }

        args.OutModule = module;
    } catch (e) {
        Logger.error(e);
        return new Status(Status.ERROR);
    }
    return new Status(Status.OK);

}

exports.execute = execute;
