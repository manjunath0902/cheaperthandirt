'use strict';

/**
 * Javascript  Script File
 * To define input and output parameters, create entries of the form:
 *
 * @<paramUsageType> <paramName> : <paramDataType> [<paramComment>]
 *
 * where
 *   <paramUsageType> can be either 'input' or 'output'
 *   <paramName> can be any valid parameter name
 *   <paramDataType> identifies the type of the parameter
 *   <paramComment> is an optional comment
 *
 * For example:
 *
 *   @output Actions : Object
 *   @output nextMethod : String
 *
 */

var Status = require('dw/system/Status');

function execute(args)
{
    try {
        args.Actions = ['deleteReviews'];
        args.nextMethod = 'productsHasNext';
    } catch (e) {
        return false;
    }

    return new Status(Status.OK);
}

exports.execute = execute;
