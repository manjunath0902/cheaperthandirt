'use strict';

/**
 * Javascript Script File
 * To define input and output parameters, create entries of the form:
 *
 * @<paramUsageType> <paramName> : <paramDataType> [<paramComment>]
 *
 * where
 *   <paramUsageType> can be either 'input' or 'output'
 *   <paramName> can be any valid parameter name
 *   <paramDataType> identifies the type of the parameter
 *   <paramComment> is an optional comment
 *
 * For example:
 *
 *   @output MailTo : String
 *   @output JobName : String
 *   @output MailTemplate : String
 *   @output MailFrom : String
 *
 */

var Status = require('dw/system/Status');
var Site = require('dw/system/Site');

function execute(args)
{
    try {
        args.MailTo = Site.getCurrent().getCustomPreferenceValue('PR_Email');
        args.MailFrom = Site.getCurrent().getCustomPreferenceValue('PR_Email_From');
        args.JobName = 'Power Reviews Import Data job';
        args.MailTemplate = 'premail.isml';

    } catch (e) {
        return false;
    }

    return new Status(Status.OK);
}

exports.execute = execute;
