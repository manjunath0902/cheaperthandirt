'use strict';

var Status = require('dw/system/Status');

/**
 * Imports the reviews from the impex location
 * 
 *   @param {Object} args - the arguments to be passed to the import job
 *   @return {Status} Status - the status of the job
 *
 */
function execute(args)
{
    args.Actions = ['importReviews'];
    args.nextMethod = 'xmlFileHasNext';
    args.Module.openXMLFile();
    args.Module.closeProductIterator();

    return new Status(Status.OK);

}

exports.execute = execute;
