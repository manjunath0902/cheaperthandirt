'use strict';

var server = require('server');

var processor = require('~/cartridge/scripts/jobcontrol/processor');

/**
* Imports the data from the FTP location
*/
server.get('ImportData', function (req, res, next) {
	processor.ImportData();
	next();
});

module.exports = server.exports();

