# PowerReviews for MFRA

PowerReview integration functionality (storefront, jobs, etc) for Mobile First Reference Architecture (MFRA).

# Getting Started

1. Clone this repository.
3. Upload the `int_powerreviews_mfra/` folder to the WebDav location for cartridges for your Sandbox through CyberDuck or any other WebDAV client.
4. Add the int_powerreviews_mfra cartridge to your cartridge path.
4. Follow the configuration and custom code sections of the documentation in `documentation/PowerReviews LINK Integration Documentation v18.3.0 MFRA.docx`

# NPM scripts

Use the provided NPM scripts to lint this project.

```
$ npm run lint
```
