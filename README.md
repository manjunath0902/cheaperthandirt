# Cheaper Than Dirt Guns (CTD)

This is a repository for the Cheaper Than Dirt application.


# Repository Structure

* We have different directory each representing a plugin, link or storefront reference architecture
* cheaperthandirt directory is used to extend base cartridge functionality and achieve customizations 
* It is required to install the npm modules at every directory which requires compilation. This is to be run once upon workspace setup


## Important 

Editing the app_storefront_base cartridge or server module voids the guarantee of backward compatibility and hinders feature and fix adoption. Instead, use B2C Commerce script methods to extend base cartridge functionality. The JSON objects created by the server module and the app_storefront_base models retain their structure and don't change properties between point releases. However, Commerce Cloud Engineering reserves the right to change how these objects are created.

# The latest version

The latest version of SFRA is 3.3.0

# Getting Started

1 Clone this repository.

2 Run NodeInstallation.bat for installing the npm at each repository level. This is to be run once upon workspace setup

3 Run compileJS-SCSS.bat that would compile all client-side JS files and SCSS files



##  Best Practices 

* All customizations are to be made on the app_cheaperthandirt_storefront cartridge.
* Don't override pdict variables.
* Don't delete properties off the pdict.
* Don't replace something in pdict with something else with a different signature.
* isscript: Only used to add client-side JavaScript/CSS to the page using the asset module.


# NPM scripts
Use the provided NPM scripts to compile and upload changes to your Sandbox.

## Compiling your application

* Navigate to the particular repository directory and run the below commands to compile the JS and SCSS manually
* `npm run compile:scss` - Compiles all .scss files into CSS.
* `npm run compile:js` - Compiles all .js files and aggregates them.
* `npm run compile:fonts` - Copies all needed font files. Usually, this only has to be run once.

## Linting your code

`npm run lint` - Execute linting for all JavaScript and SCSS files in the project. You should run this command before committing your code.

## Watching for changes and uploading

`npm run watch` - Watches everything and recompiles (if necessary) and uploads to the sandbox. Requires a valid dw.json file at the root that is configured for the sandbox to upload.

## Adding Custom Cartridges

* mkdir mysite
* cd mysite
* npm install sgmf-scripts
* sgmf-scripts --createCartridge app_custom_storefront
* update package.json and webpack.config.js files for dependencies 
* npm install
