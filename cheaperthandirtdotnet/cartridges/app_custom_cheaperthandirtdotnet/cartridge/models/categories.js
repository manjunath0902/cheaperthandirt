'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var URLUtils = require('dw/web/URLUtils');

/**
 * Get category url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
function getCategoryUrl(category) {
    return category.custom && 'alternativeUrl' in category.custom && category.custom.alternativeUrl ?
        category.custom.alternativeUrl :
        URLUtils.abs('Search-Show', 'cgid', category.getID()).toString();
}

/**
 * Identifies the category level
 * @param {dw.catalog.Category} category - A single category
 * @param {number} level - Level of the category at the initial stage
 * @returns {number} level - Level of the category
 */
function getCategoryLevel(category, level) {
    if (!category.root) {
        return getCategoryLevel(category.parent, level + 1);
    }
    return level;
}

/**
 * Converts a given category from dw.catalog.Category to plain object
 * @param {dw.catalog.Category} category - A single category
 * @param {boolean} excludeShowInCheck - Boolean to decide to include the showinmenu check
 * @returns {Object} plain object that represents a category
 */
function categoryToObject(category, excludeShowInCheck) {
    /* eslint-disable */
    if (excludeShowInCheck) {
        if (!category.custom && category.custom.categoryOnNet) {
            return null;
        }
    } else {
        if (!category.custom || (!category.custom.showinMenuOnNet && category.custom.categoryOnNet)) {
            return null;
        }
    }
    /* eslint-enable */
    var result = {
        name: category.getDisplayName(),
        url: getCategoryUrl(category),
        id: category.ID,
        level: getCategoryLevel(category, 0),
        custom: {
            shopbyBrand: 'shopbyBrand' in category.custom && category.custom.shopbyBrand ? category.custom.shopbyBrand : null,
            shopbyAttribute: 'shopbyAttribute' in category.custom && category.custom.shopbyAttribute ? category.custom.shopbyAttribute : null,
            gridImage: 'categoryTileCoverImage' in category.custom && category.custom.categoryTileCoverImage ? category.custom.categoryTileCoverImage : null,
            topCategory: 'topCategory' in category.custom && category.custom.topCategory ? category.custom.topCategory : null,
            builderPageImageList: 'builderPageImageList' in category.custom && category.custom.builderPageImageList ? category.custom.builderPageImageList : null,
            categoryContentID: 'categoryContentID' in category.custom && category.custom.categoryContentID ? category.custom.categoryContentID : null,
            categoryFlyoutContentId: 'categoryFlyoutContentId' in category.custom && category.custom.categoryFlyoutContentId ? category.custom.categoryFlyoutContentId : null,
            removeNavigationLink: 'removeNavigationLink' in category.custom && category.custom.removeNavigationLink ? category.custom.removeNavigationLink : false,
            overrideWithL3Font: 'overrideWithL3Font' in category.custom && category.custom.overrideWithL3Font ? category.custom.overrideWithL3Font : false
        }
    };

    var subCategories = category.hasOnlineSubCategories() ?
        category.getOnlineSubCategories() : null;

    if (subCategories) {
        collections.forEach(subCategories, function (subcategory) {
            var converted = null;
            if (subcategory.hasOnlineProducts() || subcategory.hasOnlineSubCategories()) {
                converted = categoryToObject(subcategory, excludeShowInCheck);
            }
            if (converted) {
                if (!result.subCategories) {
                    result.subCategories = [];
                }
                result.subCategories.push(converted);
            }
            var topCategory = null;
            if (subcategory.hasOnlineProducts() && subcategory.custom.topCategory) {
                topCategory = categoryToObject(subcategory, excludeShowInCheck);
            }
            if (topCategory) {
                if (!result.topCategories) {
                    result.topCategories = [];
                }
                result.topCategories.push(topCategory);
            }
        });
        if (result.subCategories) {
            result.complexSubCategories = result.subCategories.some(function (item) {
                return !!item.subCategories;
            });
        }
    }

    return result;
}


/**
 * Represents a single category with all of it's children
 * @param {dw.util.ArrayList<dw.catalog.Category>} items - Top level categories
 * @param {boolean} excludeShowInCheck - Boolean to decide to include the showinmenu check
 * @constructor
 */
function categories(items, excludeShowInCheck) {
    this.categories = [];
    if (excludeShowInCheck) {
        collections.forEach(items, function (item) {
            if (item.custom && (item.custom.categoryOnNet) &&
                (item.hasOnlineProducts() || item.hasOnlineSubCategories())) {
                this.categories.push(categoryToObject(item, true));
            }
        }, this);
    } else {
        collections.forEach(items, function (item) {
            if (item.custom && (item.custom.showinMenuOnNet && item.custom.categoryOnNet) &&
                (item.hasOnlineProducts() || item.hasOnlineSubCategories())) {
                this.categories.push(categoryToObject(item, false));
            }
        }, this);
    }
}

module.exports = categories;
