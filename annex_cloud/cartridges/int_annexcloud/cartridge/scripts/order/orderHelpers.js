'use strict';

var base = module.superModule;
var URLUtils = require('dw/web/URLUtils');

/**
 * Preparing products JSON object for Annex Pixel
 * @param {Object} Order - Order object
 */
function getOrderProductsInfo(order){
	var PLIs = order.allProductLineItems;
	var productObj = [];
	var productCat;
	for(var i=0;i<PLIs.length;i++){
		var prod = PLIs[i];
		var product = prod.getProduct();
		if(!empty(product)){
			productCat = product.isVariant() ? product.getVariationModel().getMaster().getPrimaryCategory() : product.getPrimaryCategory();
			productObj.push({
				"id" : prod.productID,
				"price": prod.adjustedGrossPrice.value,
				"qty": prod.quantity.value.toFixed(),
				"product_name": prod.productName,
				"product_url": URLUtils.abs('Product-Show', 'pid', prod.productID).toString(),
				"product_image_url":encodeURI(product.getImage('large') !== null && product.getImage('large').getAbsURL()? product.getImage('large').getAbsURL() : ''),
				"category_id":productCat.ID,
				"category_name":productCat.displayName
			});
		}
	}
	return productObj;
}

/**
 * Preparing couponCodes which contains in the order
 * @param {Object} Order - Order object
 */
function getOrderCouponCodes(order){
	var couponCodes = '';
	if(order.couponLineItems.length != 0){
		for(var i=0;i<order.couponLineItems.length;i++){
			if(i<=0){	
				couponCodes = order.couponLineItems[i].couponCode;
			}
			else if(i>=1){
				couponCodes +=','; 
				couponCodes += order.couponLineItems[i].couponCode;
			}	
		}
	}
	return couponCodes;
}

module.exports = {
    getOrders: base.getOrders,
    getOrderProductsInfo: getOrderProductsInfo,
    getOrderCouponCodes: getOrderCouponCodes
};
