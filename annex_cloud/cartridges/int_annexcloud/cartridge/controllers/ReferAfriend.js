var server = require('server');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');

/**
 * Renders the Refer-A-Friend dashboard content
 */
server.get('ShowDashboard', function (req, res, next) {
	try {
		 res.render('referafriend/showRAFdashboard');
   	}  catch (e){
   		var ex = e;
   	 res.render('referafriend/errorRAF');
   	}
   	next();
});

/**
 * Renders AnnexProductDetails links on PDP pages
 * */
server.get('AnnexProductDetails', function (req, res, next) {
	res.render('product/annexproductDetails');
   	next();
});

/**
 * Renders landing page for refer a friend link
 */
server.get('Landing', function(req, res, next) {
	res.render('referafriend/RAFUnlockCoupCode');
	next();
});

module.exports = server.exports();