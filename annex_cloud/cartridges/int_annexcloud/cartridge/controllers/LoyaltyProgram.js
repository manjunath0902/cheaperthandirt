var server = require('server');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var giveUserPointsAPI = require('~/cartridge/scripts/loyaltyprogram/givePointsAPI');

/**
 * Renders the Loyalty Program-Dashboard content
 */
server.get('ShowDashboard', userLoggedIn.validateLoggedIn, function (req, res, next) {
	try {
		var customerProfile = req.currentCustomer.profile;
		 res.render('loyaltyprogram/showDashboard');
   	}  catch (e){
   		var ex = e;
   		res.render('loyaltyprogram/errorLoyalty');
   	}
   	next();
});

/**
 * Assign the points to user against the action id.
 */
server.get('GivePoints', function (req, res, next) {
	try {	
		var emailId = request.httpParameterMap.emailId.stringValue;
		var actionId = request.httpParameterMap.actionId.stringValue;
		var firstName = request.httpParameterMap.firstName.stringValue;
		var lastName = request.httpParameterMap.lastName.stringValue;
		
		var giveUserPointsAPI = require('~/cartridge/scripts/loyaltyprogram/givePointsAPI');
		var opResponse = giveUserPointsAPI.giveUserPoints(emailId, actionId, firstName, lastName);
		
		res.render('powerreviews/writereview', {
			htmlOPresponse: opResponse
		});
	}catch(e) {
		var ex = e;
		res.render('loyaltyprogram/errorLoyalty');
	}
	next();
});

module.exports = server.exports();
