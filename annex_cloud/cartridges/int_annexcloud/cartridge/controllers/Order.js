var server = require('server');
var OrderHelpers = require('*/cartridge/scripts/order/orderHelpers');
var OrderMgr = require('dw/order/OrderMgr');

server.extend(module.superModule);

server.append('Confirm', function (req, res, next) {
    var viewData = res.getViewData();
    var order = OrderMgr.getOrder(req.querystring.ID);
    var ProductInfoJsonArray = OrderHelpers.getOrderProductsInfo(order);
    ProductInfoJsonArray = JSON.stringify(ProductInfoJsonArray);
    var couponCodes = OrderHelpers.getOrderCouponCodes(order);
    res.setViewData({
    	ProductInfoJsonArray: '"'+ProductInfoJsonArray+'"',
    	couponCodes: couponCodes
    });

    next();
});

module.exports = server.exports();
