'use strict';

function windowLoad() {
	var annexSrc = $('.start-sharing').data('src');
	if (annexSrc !== undefined && annexSrc !== null) {
		$(window).on('load', function() {
			$('head').append('<script type="text/javascript" src='+annexSrc+'></script>');
		});
	}
}

function annexInitEvent() {
	windowLoad();
}
function callSaActionScript(user_email,first_name,last_name,actionid,siteid,accesstoken, callback)
{
	 var xmlhttp;
	 if(window.XMLHttpRequest)
	 {
	  	xmlhttp=new XMLHttpRequest();
	 }
	 else
	 {
	 	 xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	 }
	 xmlhttp.onreadystatechange=function()
	 {
		if (callback !== undefined && callback !== null) {
			callback();
		}
	 }
	 xmlhttp.open("POST","//s15.socialannex.com/v2_api/sa_apiv2_action_script.php",true);
	 xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	 xmlhttp.send("user_email="+user_email+"&first_name="+first_name+"&last_name="+last_name+"&actionid="+actionid+"&siteid="+siteid+"&token="+accesstoken);
}
window.writeAReviewSaActionScript = function(user_email,first_name,last_name,actionid,siteid,accesstoken) {
	callSaActionScript(user_email,first_name,last_name,actionid,siteid,accesstoken);
}
module.exports = {
		annexInitEvent: annexInitEvent,
		callSaActionScript: callSaActionScript
};
