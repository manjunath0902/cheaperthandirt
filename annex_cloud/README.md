Annex Cloud Integration
==============

--------------------------

Overview
------------
Salesforce Commerce Cloud and Annex Cloud have formed a partnership to offer Annex Cloud’s Customer Loyalty, Referral Marketing and User Generated Content solutions to integrate with and complement the Commerce Cloud platform. With Annex Cloud’s unified solutions, you will be able to identify your highest-potential customers, nurture their potential, and motivate your loyal customers to promote their positive voice about your company and products.

Annex Cloud’s unified platform offers easy to use, actionable data and pre-built technology integrations to deliver smarter insights and seamless experiences. Our tailored solutions deliver unique reporting features, customized user experiences, tailored designs, and any customization necessary to meet your business needs. You will be guided by our dedicated Customer Success team and Implementation Architect--from strategy through implementation, ongoing maintenance, and optimization.




Documentation
-------------
* Customizable loyalty actions and events
* Flexible rewards and inclusive rewards management
* Receipt Data Aggregator: allows customers to take a picture or upload their physical receipts to earn rewards
* Advanced segmentation and campaign building capability: create intelligent campaigns powered by highly targeted customer segments to accomplish any number of goals such
* Native User Generated Content, Referral Marketing, Social Login integration
* Pre-built ESP and marketing eco-system integrations
* Comprehensive reporting and analytics
* Full mobile support including app integration, and responsive and adaptive design
* Powerful Referral Campaigns and Advanced Segmentation
* Comprehensive reporting and analytics delivered via intuitive dashboards