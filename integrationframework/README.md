Integration Framework
=================

Platform-based Job Framework - Support Discontinued
---------------------------------------------------
At longs last, we are happy to announce that an Integration Framework pendant was integrated into the Commerce Cloud platform. The new tool is called Job Framework and can be found in the Business Manager. It replaces not only the Integration Framework, but also the platform Job Scheduler you may have been using so far. 
Adoption of the new feature is strongly encouraged. To help make the process as smooth as possible, we came up with a way to continue to use your old Workflow Components and created a Business Manager tool called Migration Cockpit that supports a semi-automatic transition to the new tool. Please refer to its documentation, represented by a PDF file in the documentation directory. Read this document and stick closely to its instructions. Deviating from it will be at own risk.
As the Job Framework is meant to fully replace the Integration Framework in the near future, we appreciate your understanding that we will no longer add new features to the Integration Framework as of today. There will be fixes for high severity bugs, but no new releases apart from that.


Introduction
------------
A framework to streamline jobs and batch integrations. It provides a lot of prebuild components or sub jobs if you like, that will make your project life a lot easier. You can for instance configure to download a demandware xml file from an FTP and import it into the system on a daily basis. On top of that you have a nice UI to configure your jobs and to view the history as well as the upcoming schedule.

It is part of the [Community Suite](https://xchange.demandware.com/community/developer/community-suite) set of tools.


Documentation
-------------
Please check the [Wiki](https://bitbucket.org/demandware/integrationframework/wiki) and the [DSDoc](http://demandware.bitbucket.org/jsdoc/integrationframework/)

Release History
---------------
- 2017/01/09 - [1.6.0](https://bitbucket.org/demandware/integrationframework/commits/tag/1.6.0)
	- Added script wrappers allowing for re-use of old Workflow Components in context of the new platform Job Framework
	- Added initial version of Migration Cockpit cartridge
	- Added migration manual
- 2016/08/05 - [1.5.2](https://bitbucket.org/demandware/integrationframework/commits/tag/1.5.2)
    - Support for script only workflow components (With this change it is possible to use script only workflow components. For more info see https://bitbucket.org/demandware/integrationframework/pull-requests/99/support-for-script-only-workflow/diff)
    - Minor logging improvements
- 2016/05/23 - [1.5.1](https://bitbucket.org/demandware/integrationframework/commits/tag/1.5.1)
    - Increase reusability of FinalizeImport: Check whether LogFileName is given in pdict
    - (S)FTP download: Added possibility to configure which status is triggered when no files were found
    - Fixing execution time calculation (In some cases the execution time for schedules was calculated wrongly)
    - Fixed typo PIPELET_ERROR in UpdateWorkflowScheduleDefinition.ds (#123)
- 2016/02/22 - [1.5.0](https://bitbucket.org/demandware/integrationframework/commits/tag/1.5.0)
	- UploadFiles to support SFTP (#110)
	- Fix for workflow detail changes (#114)
	- Reduce memory consumption when converting iterator to list
	- Compatibility fix for bc_library file utils
	- Minor code cleansing
- 2015/10/16 - [1.4.1](https://bitbucket.org/demandware/integrationframework/commits/tag/1.4.1)
    - Checking download file name against regex properly (#109)
    - Added missing meta data (#28)
- 2015/09/23 - [1.4.0](https://bitbucket.org/demandware/integrationframework/commits/tag/1.4.0)
    - Allow site aware upload and download by using the {siteID} placeholder
	- Workflow schedule on Firefox 37.0.1 and later (#97)
	- Fixed a problem in WorkflowComponentDefinition.xml that causes saving of Export-Pricebook component to fail
	- Displayname added to components in workflow schedule and plan (#28)
    - Invalid JSON syntax in StandardComponents-DownloadFiles and StandardComponents-UploadFiles (#105)
	- Added custom decorator instead of reusing BM decorator
	- Remove dependencies to default ExtJS resources provided by Business Manager (#106)
	- Key count for StandardComponents-Import 'keyvaluemapping' (#107)
	- Use a different temporary file for each site to avoid a race condition when importing (#108)
	- Mark older transfer components as deprecated. Use standard components instead.
- 2015/02/03 - [1.3.0](https://bitbucket.org/demandware/integrationframework/commits/tag/1.3.0)
    - Improved error handling for file post processing (#93)
	- Configurable workflow at FTP no files found (#94)
	- Placeholder support for configurable file names (note, dependency to Demandware library 1.5.0, https://bitbucket.org/demandware/demandware-library/commits/tag/1.5.0)
	- Support for standard key/value mappings import
	- Fixed FF UI bug
	- Various other improvements
- 2015/13/01 - [1.2.8](https://bitbucket.org/demandware/integrationframework/commits/tag/1.2.8)
    - Added placeholder support (#71)
	- Changed hardcoded extension checks with regex checks (#81)
- 2014/16/09 - [1.2.7](https://bitbucket.org/demandware/integrationframework/commits/tag/1.2.7)
    - Add support to include log messages in notification emails
	- Add support for downloading literally an arbitrary number of files without running into script timeout problems
	- Fix StringIndexOutOfBoundsException if the user wants to read the job log file via DownloadCustomLogFile
	- Fix a problem with FTPClient vs SFTPClient error handling
- 2014/02/09 - [1.2.6](https://bitbucket.org/demandware/integrationframework/commits/tag/1.2.6)
	- Add support for remote archiving (archive downloaded files on the server)
	- Add support for remote reporting (write download log files, which will be put on the server)
	- Fix issue with not being able to download files from SFTP and WebDAV
- 2014/10/07 - [1.2.5](https://bitbucket.org/demandware/integrationframework/commits/tag/1.2.5)
	- Use new API to get all sites, making the custom global preference siteIDs obsolete (#87)
	- Recursive file download (it is possible to define a target base directory other than IMPEX and tell the script to download files recursively. Files found in sub directories of the source directory will be stored relatively to the target base directory)
	- Bugfix for script expressions in pipelines
- 2014/22/04 - [1.2.4](https://bitbucket.org/demandware/integrationframework/commits/tag/1.2.4)
	- Download Files component compatible with demandware-library [1.4.0](https://bitbucket.org/demandware/demandware-library/commits/tag/1.4.0)
- 2014/22/04 - [1.2.3](https://bitbucket.org/demandware/integrationframework/commits/tag/1.2.3)
	- Download Files component compatible with demandware-library [1.3.2](https://bitbucket.org/demandware/demandware-library/commits/tag/1.3.2)
	- Variuous Bugfixes
- 2013/12/09 - [1.2.2](https://bitbucket.org/demandware/integrationframework/commits/tag/1.2.2)
	- Incorrect file naming in the CleanUpFiles-Component (GeneralIMport-CleanUpFiles) causing files to never be found and removed is fixed.
	- The addition of new generic components (StandardComponents.xml) has rendered various other components obsolete. These were removed from the source code as well as from the site template (Metadata).
	- A Cleanup job that can be run through the normal Demandware BM Job Scheduler to remove custom objects for the Integration-Framework was created. This allows an easy way to help get the Integration-Framework running again when there is an issue with these Custom-Objects and the Framework is no longer able to properly process.
	- FTPDownload and FTPuUpload components now allow upload and download  of files to every folder relative to IMPEX/.
	- A new generic component to upload files (StandardComponents-UploadFiles) was added.
	- Typo in FTPDownload script was removed.
    - Fixed [#43](https://bitbucket.org/demandware/integrationframework/issue/43) - Added control for component log and harmonized logging API (note that metadata needs to be updated for this change)
- 2013/08/30 - [1.2.1](https://bitbucket.org/demandware/integrationframework/commits/tag/1.2.1) 
    - Changed directory structure to allow Build Suite deployments
- 2013/08/28 - [1.2.0](https://bitbucket.org/demandware/integrationframework/commits/tag/1.2.0) 
    - Major Bugfix release which bundles a couple of fixes
- 2013/06/03 - [1.1.0](https://bitbucket.org/demandware/integrationframework/commits/tag/1.1.0) 
    - Major Bugfix release
- 2012/09/04 - [1.0.1](https://bitbucket.org/demandware/integrationframework/commits/tag/1.0.1) 
    - Bugfix release
- 2012/09/04 - [1.0.0](https://bitbucket.org/demandware/integrationframework/commits/tag/1.0.0) 
    - Initial release

Dependency
---------------
https://bitbucket.org/demandware/demandware-library/

Support / Contributing
----------------------
Feel free to create issues and enhancement requests or discuss on the existing ones, this will help us understanding in which area the biggest need is. For discussions please start a topic on the [Community Suite discussion board](https://xchange.demandware.com/community/developer/community-suite/content).

License
-------
Licensed under the current NDA and licensing agreement in place with your organization. (This is explicitly not open source licensing.)