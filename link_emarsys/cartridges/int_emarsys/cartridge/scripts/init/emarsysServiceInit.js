'use strict';

/**
 * Initialize  HTTP service
 */

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var Site = require('dw/system/Site');
var enableLoggers = Site.getCurrent().getCustomPreferenceValue('enableRequestLoggers');
var Logger = require('dw/system/Logger');
var emarsysLogger = Logger.getLogger('Emarsys-Log','Emarsys-Log');
/**
 *
 * google reCAPTCHA http Services
 *
 * @return {HTTPService} the service
 */
function emarsysService(currService) {
  var emarsysServiceObj = LocalServiceRegistry.createService(currService, {
  createRequest: function (svc, params) {
    var reqHeader = params.headerauth;
    var reqbody = params.reqBody;
    
    if (params.requestType && params.requestType === 'PUT') {
    	svc.setRequestMethod('PUT');
    	svc = svc.addParam('create_if_not_exists', 1);
    }else { svc.setRequestMethod('POST'); }
    
    svc = svc.addHeader('X-WSSE',reqHeader);
    svc = svc.addHeader('Content-Type', 'application/json');
    svc = svc.addHeader('Accept', 'application/json');
    if (enableLoggers) {
        emarsysLogger.info('Emarsys Request-' + JSON.stringify(reqbody));
    }
    return JSON.stringify(reqbody);
   },
   parseResponse: function (svc, client) {
       if (enableLoggers) {
            emarsysLogger.info('Emarsys Response - ' + client.text);
       }
     return client.text;
        },
   /* eslint-disable */
   mockCall: function (service, request) {
     return {
          success: true
            };
        },
        /* eslint-enable */
  filterLogMessage: function (msg) {
     return msg;
        }
  });
  return emarsysServiceObj;
}

module.exports = {
  emarsysService: emarsysService
};
