'use strict';
var Logger = require('dw/system/Logger').getLogger('order-ship-confirmation-email');


function triggerShipEmail() {
	var emarsysHelper = require('int_emarsys/cartridge/scripts/helpers/emarsysHelper');
	var ServiceObject = require('int_emarsys/cartridge/scripts/init/emarsysServiceInit.js');
	var accountHepler = require('*/cartridge/scripts/helpers/accountHelpers');
	var Transaction = require('dw/system/Transaction');
	var OrderMgr = require('dw/order/OrderMgr');
	var currService = 'int_emarsys_shipment_confirmation';
	try{
		var querySQL = "(custom.orderConfirmationEmailSent={0} AND custom.newTrackingNumAvailable={1} AND custom.shipConfirmationEmailSent={2} AND custom.legacyOrder={2})";
    	var ordersForExport  = dw.object.SystemObjectMgr.querySystemObjects("Order", querySQL, "orderNo asc", true, true, false, false);
	}catch(e){
		var error = e;
	}
	var orderExportList = ordersForExport.asList(0,50).iterator();
	while(orderExportList.hasNext()){
		var order = orderExportList.next();
		Logger.info('Ship Confirmation : Processing the order...' + order.getOrderNo());
		var emarsysServiceObj = ServiceObject.emarsysService(currService);
		for(var i=0; i< order.shipments.length; i++){
			var params = getParams(order, order.shipments[i], currService);
		    var result = emarsysServiceObj.call(params);
		    Logger.info('Ship Confirmation for Shipment : '+ order.shipments[i].shippingMethod.ID + '.  Response is : '+ result.toString());
		}
		if(result.ok){
			Transaction.wrap(function () {
				order.custom.newTrackingNumAvailable = false;
				Logger.info('Ship Confirmation : Email Sent for :' + order.getOrderNo());
			});
	    }
	}
    
    return;
}

/**
 * To get the params
 * @param {obj} registeredUser - object that contains user's email address and name information.
 * @param {currService} currService - object that contains service ID
 * @returns {string} password reset token string
 */
function getParams(order, shipment, currService) {
	var emarsysHelper = require('int_emarsys/cartridge/scripts/helpers/emarsysHelper');
    var headerauth = emarsysHelper.getHeader();
    var orderData = {
    		order: order,
    		shipment: shipment
    };
    var reqBody = emarsysHelper.getRequestBody(orderData, currService);
    var params = {
        headerauth: headerauth,
        reqBody: reqBody,
        Service_ID: currService
    };
    return params;
}

exports.triggerShipEmail= triggerShipEmail;
