'use strict';
var Logger = require('dw/system/Logger').getLogger('order-confirmation-email');


function triggerOrderEmail() {
	var ServiceObject = require('int_emarsys/cartridge/scripts/init/emarsysServiceInit.js');
	var accountHelper = require('*/cartridge/scripts/helpers/accountHelpers');
	var Transaction = require('dw/system/Transaction');
	var OrderMgr = require('dw/order/OrderMgr');
	var args = arguments[0];
	var maxOrders = args.maxOrders ? args.maxOrders : 100;
	try{
		var querySQL = "(exportStatus={0} OR exportStatus={1}) AND (custom.orderConfirmationEmailSent={2} AND custom.legacyOrder={3})";
    	var ordersForExport  = dw.object.SystemObjectMgr.querySystemObjects("Order", querySQL, "orderNo asc", dw.order.Order.EXPORT_STATUS_READY, dw.order.Order.EXPORT_STATUS_EXPORTED, false, false);
	}catch(e){
		var error = e;
	}
	var orderExportList = ordersForExport.asList(0,maxOrders).iterator();
	while(orderExportList.hasNext()){ 
		var order = orderExportList.next();
		Logger.info('Order Confirmation : Processing the order...' + order.getOrderNo());
		
		var currService = 'int_emarsys_update_contact';
		var emarsysServiceObj = ServiceObject.emarsysService(currService);
	    var orderCustomer = {
	    		firstName: order.billingAddress.firstName,
	    		lastName: order.billingAddress.lastName,
                email: order.customerEmail
            };
	    var params = accountHelper.getParams(orderCustomer, currService);
	    params.requestType = 'PUT';
	    var result = emarsysServiceObj.call(params);
	    
	    if (result.ok) {
			currService = 'int_emarsys_order_confirmation';
			emarsysServiceObj = ServiceObject.emarsysService(currService);
		    params = accountHelper.getParams(order, currService);
		    result = emarsysServiceObj.call(params);
			Logger.info('Order Confirmation : Response : ' + result.toString());
			if(result.ok){
				Transaction.wrap(function () {
					order.custom.orderConfirmationEmailSent = true;
				});
				Logger.info('Order Confirmation : Email Sent for :' + order.getOrderNo());
		    }
	    }
	}
    
    return;
}

exports.triggerOrderEmail= triggerOrderEmail;
