'use strict';

var stub = require('dw/rpc/Stub');
const Crypto = require('dw/crypto');
const Bytes = require('dw/util/Bytes');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
var TotalsModel = require('*/cartridge/models/totals');
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');

function getHeader(){
	var merchantID = dw.system.Site.current.preferences.custom.emarsys_merchantID;
	  secreteKey = dw.system.Site.current.preferences.custom.emarsys_secretekey;
	  timestamp = dw.util.StringUtils.formatCalendar(dw.util.Calendar(), "yyyy-MM-dd'T'HH:mm:ss'Z'");
	
	var Nonce = getNonce(secreteKey);
	var PasswordDigest = getPasswordDigest(Nonce,timestamp,secreteKey);
	var headerAuth = 'UsernameToken Username="'+merchantID+'", PasswordDigest="'+PasswordDigest+'", Nonce="'+Nonce+'", Created="'+timestamp+'"';
	
	return headerAuth;
}
function getRequestBody(params,args){
	var reqBody = {};
	
	switch(args){
	case 'int_emarsys_update_contact':
		reqBody = getContactReqBody(params);
		break;
	case 'int_emarsys_account_creation':
		reqBody = getCreateAccountRequestBody(params);
		break;
	case 'int_emarsys_tell_a_friend':
		reqBody = getTellAFriendRequestBody(params);
		break;
	case 'int_emarsys_passwordReset':
		reqBody = getpasswordResetRequestBody(params);
		break;
	case 'int_emarsys_order_confirmation':
		reqBody = getOrderConfirmationRequestBody(params);
		break;
	case 'int_emarsys_shipment_confirmation':
		reqBody = getShipmentConfirmationRequestBody(params);
		break;
	}	
	return reqBody;
	
}
function getContactReqBody(params){
	var requestBody = {
			"key_id": "3",
			"contacts": [
			    {
			      "1": params.firstName,
			      "2": params.lastName,
			      "3": params.email
			    }
			  ]
		}
		if (params.optIn !== null && params.optIn !== undefined) {
			requestBody.contacts[0]['31'] = '1';
		}
	return requestBody;
}
function getCreateAccountRequestBody(profile){
	var requestBody = {
				"data": {
					"messageHeaders": {
						"messageHeaderMap": {
							"serviceType": "CreateNewAccount",
							"environment": dw.system.Site.current.preferences.custom.emarsys_environment,
							"siteCode": dw.system.Site.current.preferences.custom.siteCode,
							"MESSAGE_TYPE": dw.system.Site.current.preferences.custom.emarsys_messageType,
							"merchant": dw.system.Site.current.preferences.custom.emarsys_merchant,
							"PRIORITY": 6,
							"SITE_CODE": dw.system.Site.current.preferences.custom.siteCode,
							"recipientEmail": profile.email
						}
					},
					"stringProperties": {
						"siteCode": dw.system.Site.current.preferences.custom.siteCode,
						"fromName": dw.system.Site.current.preferences.custom.fromName,
						"recipientEmail": profile.email,
						"fromEmail": dw.system.Site.current.preferences.custom.fromemail
					},
					"mapProperties": {},
					"sessionId": session.getSessionID()
				},
				"key_id": "3",
				"external_id": profile.email
	}
	return requestBody;
}
function getNonce(secreteKey){
	const nonce = new Crypto.SecureRandom().nextInt().toString();
	var dataBytes = new Bytes(nonce, "UTF-8");
	var bytesHex = Crypto.Encoding.toHex(dataBytes );
	var noncefinal = bytesHex.toString().toLowerCase() + secreteKey;
	
	return noncefinal;
}

function getPasswordDigest(nonce, timestamp, secreteKey){
	var nonceFinal = nonce+timestamp+secreteKey; 
	var dataBytes = new Bytes(nonceFinal, "UTF-8"); 
	var messageDigest = new Crypto.MessageDigest(Crypto.MessageDigest.DIGEST_SHA_1);
	var bytes = messageDigest.digestBytes(dataBytes);
	var bytesHex = Crypto.Encoding.toHex(bytes);
	var hmacString = Crypto.Encoding.toBase64(new Bytes(bytesHex));
	var value= hmacString.toString();

	return value;
}

function getTellAFriendRequestBody(profile){
	var requestBody = {
			"data": {
				"messageHeaders": {
					"messageHeaderMap": {
						"serviceType": "TellAFriend",
						"environment": dw.system.Site.current.preferences.custom.emarsys_environment,
						"siteCode": dw.system.Site.current.preferences.custom.siteCode,
						"MESSAGE_TYPE": "TellAFriend",
						"merchant": dw.system.Site.current.preferences.custom.emarsys_merchant,
						"PRIORITY": 6,
						"SITE_CODE": dw.system.Site.current.preferences.custom.siteCode,
						"recipientEmail": profile.recipientEmail
					}
				},
				"stringProperties": {
					"siteCode": dw.system.Site.current.preferences.custom.siteCode,
					"fromName": dw.system.Site.current.preferences.custom.fromName,
					"recipientEmail": profile.recipientEmail,
					"fromEmail": dw.system.Site.current.preferences.custom.fromemail
				},
				"mapProperties": {
					"categorycodes": profile.productID,
					"product": profile.productURL,
					"csemail": dw.system.Site.current.preferences.custom.customerServiceEmail,
					"sender": profile.senderName,
					"recipient": profile.recipientName,
					"productcodes": profile.productID,
					"company": "Cheaper Than Dirt",
					"csphone": dw.system.Site.current.preferences.custom.customerPhoneNumber,
					"timestamp": "1550596922072"
				},
				"sessionId": session.getSessionID()
			},
			"key_id": "3",
			"external_id": profile.recipientEmail
		}
	return requestBody;
	}

function getpasswordResetRequestBody(params){
	var requestBody = {
			"data": {
				"messageHeaders": {
					"messageHeaderMap": {
						"serviceType": "PasswordRecovery",
						"environment": dw.system.Site.current.preferences.custom.emarsys_environment,
						"siteCode": dw.system.Site.current.preferences.custom.siteCode,
						"MESSAGE_TYPE": "PasswordRecovery",
						"merchant": dw.system.Site.current.preferences.custom.emarsys_merchant,
						"PRIORITY": 9,
						"SITE_CODE": dw.system.Site.current.preferences.custom.siteCode,
						"recipientEmail": params.recoveryEmail
					}
				},
				"stringProperties": {
					"siteCode": dw.system.Site.current.preferences.custom.siteCode,
					"fromName": dw.system.Site.current.preferences.custom.fromName,
					"recipientEmail": params.recoveryEmail,
					"fromEmail": dw.system.Site.current.preferences.custom.fromemail
				},
				"mapProperties": {
					"company": "Cheaper Than Dirt",
					"emaillink": params.url,
					"csphone": dw.system.Site.current.preferences.custom.customerPhoneNumber
				},
				"sessionId": session.getSessionID()
			},
			"key_id": "3",
			"external_id": params.recoveryEmail
		}
	return requestBody;
	}
function getOrderConfirmationRequestBody(order){
	var totalsModel = new TotalsModel(order);
	var orderModel ={
			orderNumber: order.orderNo,
			billingAddress: order.billingAddress,
			paymentMethod: order.paymentInstrument,
			paymentTransactions:order.paymentInstruments,
			shippingMethod: order.shipments[0].shippingMethod != null ? order.shipments[0].shippingMethod.displayName : '',
			shippingAddress: order.shipments[0].shippingAddress,
			productlineitems: order.productLineItems,
			email:order.customerEmail,
			orderedDate: order.creationDate,
			orderStatus:order.status.displayValue,
			totalsModel: totalsModel,
			ShipmentTotal: order.adjustedShippingTotalPrice.value,
			shipments: order.shipments
	}
	
	var orderText = renderTemplateHelper.getRenderedHtml(orderModel, 'orderconfirmation/orderConfirmationOrderText');
	var orderConfirmationDetails = renderTemplateHelper.getRenderedHtml(orderModel, 'orderconfirmation/orderConfirmationDetails');
	
	var requestBody = {
			"data": {
				"messageHeaders": {
					"messageHeaderMap": {
						"serviceType": "OrderConfirmation",
						"firstname": order.billingAddress.firstName,
						"siteCode": dw.system.Site.current.preferences.custom.siteCode,
						"city": order.billingAddress.city,
						"MESSAGE_TYPE": "OrderConfirmation",
						"merchant": dw.system.Site.current.preferences.custom.emarsys_merchant,
						"PRIORITY": 9,
						"SITE_CODE": dw.system.Site.current.preferences.custom.siteCode,
						"lastname": order.billingAddress.lastName,
						"zipcode": order.billingAddress.postalCode,
						"environment": dw.system.Site.current.preferences.custom.emarsys_environment,
						"state": order.billingAddress.lastName,
						"recipientEmail": order.customerEmail
					}
				},
				"stringProperties": {
					"zipcode": order.billingAddress.postalCode,
					"firstname": order.billingAddress.firstName,
					"siteCode": dw.system.Site.current.preferences.custom.siteCode,
					"city": order.billingAddress.city,
					"fromName": dw.system.Site.current.preferences.custom.fromName,
					"state": order.billingAddress.stateCode,
					"recipientEmail": order.customerEmail,
					"lastname": order.billingAddress.lastName,
					"fromEmail": dw.system.Site.current.preferences.custom.fromemail
				},
				"mapProperties": {
					"categorycodes": "17696",
					"csemail": dw.system.Site.current.preferences.custom.customerServiceEmail,
					"company": "Cheaper Than Dirt",
					"ordertext": orderText,
					"rtiRecommendations":getRTIReccomendations(),
					"order":orderConfirmationDetails,
					"csphone": "(800)555-5555",
					"timestamp": "1550612640859"
				},
				"sessionId": session.getSessionID()
			},
			"key_id": "3",
			"external_id": order.customerEmail
		}
	return requestBody;
	}

function getShipmentConfirmationRequestBody(orderData){
	try{
		var order = orderData.order;
		var shipment = orderData.shipment;
		var trackingData = order.custom.trackingNumberData;
		var shipmentType = 'shipmentType' in shipment.custom && shipment.custom.shipmentType ? 'instore' : 'shipToHome';
		var getTrackingNumber = JSON.parse(trackingData);
		var shipmentData = [];
		var reloadTrackingData = [];
		var trackLinkService;
		if('trackingNumbersJSON' in dw.system.Site.getCurrent().preferences.getCustom()){
			trackLinkService = JSON.parse(dw.system.Site.current.preferences.custom.trackingNumbersJSON);
		}
		var carrier,trackingURL;
		for(var i=0; i< getTrackingNumber.length; i++){
			var objData = getTrackingNumber[i];
			if(objData.shipemailsent == 'N' && objData.shipmentType.equalsIgnoreCase(shipmentType)){
				carrier = trackLinkService[objData.shipmethod];
				if(!empty(carrier)){
					trackingURL = carrier.trackingUrl+"&tracknum="+objData.trackNo;
				}else{
					trackingURL = trackLinkService['NoTracking'].trackingUrl;
				}
				shipmentData.push({
					"trackNumber" : objData.trackNo,
					"shippingMethod":objData.shipmethod,
					"shipmentType": shipmentType,
					"trackingURL":trackingURL
				});
				objData.shipemailsent = "Y";
				reloadTrackingData.push(objData);
			}else{
				reloadTrackingData.push(objData);
			}
		}
		Transaction.wrap(function () {
			order.custom.trackingNumberData = JSON.stringify(reloadTrackingData);
		});
	}catch(e){
		var error = e;
		var ee = error;
	}

	var sitePrefs = Site.getCurrent().getPreferences();
	var customShipeedStatus = 'orderShippedStatus' in sitePrefs.custom && sitePrefs.custom.orderShippedStatus !== null ? sitePrefs.custom.orderShippedStatus : ''; 
	var shippingModel = {
			shippingAddress: shipment.shippingAddress,
			orderNumber: order.orderNo,
			productlineitems: shipment.productLineItems,
			status: 'customOrderStatus' in order.custom && order.custom.customOrderStatus !== null ? order.custom.customOrderStatus : customShipeedStatus,
			shipmentData: shipmentData
	}
	var shipConfirmationText = renderTemplateHelper.getRenderedHtml(shippingModel, 'shipment_confirmation/shipConfirmationShipment');
	
	var requestBody = {
			"data": {
				"messageHeaders": {
					"messageHeaderMap": {
						"serviceType": "ShipmentShipped",
						"environment": dw.system.Site.current.preferences.custom.emarsys_environment,
						"siteCode": dw.system.Site.current.preferences.custom.siteCode,
						"MESSAGE_TYPE": "ShipmentShipped",
						"merchant":  dw.system.Site.current.preferences.custom.emarsys_merchant,
						"PRIORITY": 5,
						"SITE_CODE": dw.system.Site.current.preferences.custom.siteCode,
						"recipientEmail": order.customerEmail
					}
				},
				"stringProperties": {
					"siteCode": dw.system.Site.current.preferences.custom.siteCode,
					"fromName": dw.system.Site.current.preferences.custom.fromName,
					"recipientEmail": order.customerEmail,
					"fromEmail": dw.system.Site.current.preferences.custom.fromemail
				},
				"mapProperties": {
					"shipment":shipConfirmationText,
					"rtiRecommendations": getRTIReccomendations()
				},
				"sessionId": session.getSessionID()
				},
				"key_id": "3",
				"external_id": order.customerEmail
	}
	return requestBody;
}

function getRTIReccomendations(){
	var productModel = {
			RTI_Products: dw.system.Site.current.preferences.custom.RTIProducts
	}
	var RTIRecommendations = renderTemplateHelper.getRenderedHtml(productModel, 'orderconfirmation/orderConfirmationRecomm');
	
	return RTIRecommendations;
}

exports.getHeader = getHeader;
exports.getRequestBody = getRequestBody;
