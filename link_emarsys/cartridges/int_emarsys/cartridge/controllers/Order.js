var server = require('server');
var OrderMgr = require('dw/order/OrderMgr');
var Transaction = require('dw/system/Transaction');

server.extend(module.superModule);

server.append('Confirm', function (req, res, next) {
    var viewData = res.getViewData();
    var order = OrderMgr.getOrder(req.querystring.ID);
    Transaction.wrap(function(){
       order.custom.orderConfirmationEmailSent = false;
       order.custom.shipConfirmationEmailSent = false;
       order.custom.legacyOrder = false;
    });
    
    next();
});

module.exports = server.exports();
