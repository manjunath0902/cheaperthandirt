<isdecorate template="common/layout/page">
    <isscript>
        var assets = require('*/cartridge/scripts/assets.js');
        assets.addCss('/css/cart.css');
        assets.addJs('/js/cart.js');
        assets.addJs('/js/cartInstorePickup.js');
    </isscript>

	<isinclude template="/product/powerReviewsInclude" />
    <isif condition="${pdict.reportingURLs && pdict.reportingURLs.length}">
        <isinclude template="reporting/reportingUrls" />
    </isif>

    <div class="cart-error-messaging cart-error">
        <isif condition="${pdict.valid.error && pdict.items.length !== 0}">
            <div class="alert alert-danger alert-dismissible valid-cart-error fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                ${pdict.valid.message}
            </div>
        </isif>
    </div>
    <div class="cart-main">
	    <div class="container">
	        <h1 class="page-title h1-black text-uppercase">${Resource.msg('title.cart','cart',null)}</h1>
	        <div class="row cart-header">
	            <div class="col-md-4 order-2 order-md-0">
	                <a class="continue-shopping-link b3-black" href="${URLUtils.url('Home-Show')}" title="${Resource.msg('link.continue.shopping','cart',null)}">
	                    ${Resource.msg('link.continue.shopping','cart',null)}
	                </a>
	            </div>
	            <div class="col-md-3 order-1 order-md-0 text-center">
	                <isif condition="${pdict.numItems != 1}">
	                    <h5 class="number-of-items">${Resource.msgf('label.number.items.in.cart','cart', null, pdict.numItems)}</h5>
	                <iselse>
	                    <h5 class="number-of-items">${Resource.msgf('label.number.items.in.cart.single','cart', null, pdict.numItems)}</h5>
	                </isif>
	            </div>
	            <div class="col-md-5 order-3 text-right hidden-md-down b3-black">
	                <div>
	                    <span>${Resource.msg('info.need.help','cart',null)}</span>
	                    <span><a class="help-phone-number" href="tel:${Resource.msg('info.phone.number','common',null)}">${Resource.msg('info.phone.number','common',null)}</a></span>
	                </div>
	            </div>
	        </div>
	        <hr class="no-margin-top">
	    </div>
	
	    <isif condition="${pdict.items.length === 0}">
	        <div class="container cart-empty">
	            <div class="row">
	                <div class="col-12 text-center">
	                    <h1 class="h1-black">${Resource.msg('info.cart.empty.msg','cart',null)}</h1>
	                </div>
	            </div>
	        </div>
	    <iselse/>
	        <div class="container cart cart-page">
	            <div class="row cart-row">
	                <!---product cards--->
	                <div class="col-md-9 cart-info">
	                    <isloop items="${pdict.items}" var="lineItem">
	                        <isif condition="${lineItem.productType === 'bundle'}">
	                            <isinclude template="cart/productCard/cartBundleCard" />
	                        <iselse/>
	                            <isinclude template="cart/productCard/cartProductCard" />
	                        </isif>
	                    </isloop>
	                    <isinclude template="cart/cartApproachingDiscount" />
	                </div>
	                <!---totals, and checkout actions--->
	                <div class="col-md-3 totals">
	                    <isinclude template="cart/cartPromoCode" />
	                    <div class="coupons-and-promos promo-code-form">
	                        <isinclude template="cart/cartCouponDisplay" />
	                    </div>
	                    <isif condition="${!pdict.onlyStoreShipment}">
		                    <div class="row ship-rate-form">
		                        <isinclude template="cart/cartEstimateShipping" />
		                    </div>
	                    </isif>
	                    <div class="row shipping-selection shipAddress d-none">
	                        <isinclude template="cart/cartShippingMethodSelection" />
	                    </div>
	                    <isinclude template="cart/cartTotals" />
	                    <div class="row">
	                        <div class="col-12 checkout-continue">
	                            <isinclude template="cart/checkoutButtons" />
	                        </div>
	                    </div>
	                    <br/>
	                </div>
	            </div>
	        </div>
	        <isinclude template="cart/cartRemoveProductModal"/>
	        <isinclude template="cart/cartRemoveCouponModal"/>
	    </isif>
	    <div class="container cart-recommendation">
	        <isslot id="cart-recommendations-m" description="Recommended products" context="global" />
	    </div>
    </div>
</isdecorate>
