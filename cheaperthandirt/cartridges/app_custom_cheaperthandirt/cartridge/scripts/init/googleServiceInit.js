'use strict';

/**
 * Initialize  HTTP service
 */

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
/**
 *
 * google reCAPTCHA http Services
 *
 * @return {HTTPService} the service
 */
function googleReCAptchaService() {
    return LocalServiceRegistry.createService('int_googleCaptcha.post', {
        createRequest: function (svc, params) {
            svc.setRequestMethod('POST');
            svc.addParam('secret', params.secret);
            svc.addParam('response', params.response);
            svc.addParam('remoteip', params.remoteip);
        },
        parseResponse: function (svc, client) {
            return client.text;
        },
        /* eslint-disable */
        mockCall: function (service, request) {
            return {
                success: true
            };
        },
        /* eslint-enable */
        filterLogMessage: function (msg) {
            return msg;
        }
    });
}

module.exports = {
    get: googleReCAptchaService
};
