'use strict';

var base = module.superModule;

var ShippingModel = require('*/cartridge/models/shipping');
var collections = require('*/cartridge/scripts/util/collections');

/**
 * Validates the shipping method restrictions and returns the restricted shipmethods
 * @param {dw.util.Collection} shippingMethods - Available shipping methods
 * @returns {dw.util.ArrayList} an array of restricted shipping methods
 */
function getRestrictedShipMethods(shippingMethods) {
    var BasketMgr = require('dw/order/BasketMgr');
    var ArrayList = require('dw/util/ArrayList');
    var restrictedShipMethods = new ArrayList();
    var restrictShipMethodID;
    var currentBasket = BasketMgr.getCurrentBasket();
    if (currentBasket === null) return restrictedShipMethods;
    var restrictHelper = require('*/cartridge/scripts/cart/restrictHelper.js');
    restrictHelper.validateShippingMethodRestrictions(currentBasket);
    if ('restrictShipMethodID' in session.custom && session.custom.restrictShipMethodID !== '') { // eslint-disable-line
        restrictShipMethodID = session.custom.restrictShipMethodID; // eslint-disable-line
    } else {
        return restrictedShipMethods;
    }

    collections.forEach(shippingMethods, function (method) {
        if (restrictShipMethodID !== '' && restrictShipMethodID.indexOf(method.custom.legacyShipID) > -1) {
            restrictedShipMethods.push(method);
        }
    });

    return restrictedShipMethods;
}

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.Shipment} shipment - the target Shipment
 * @param {Object} [address] - optional address object
 * @returns {dw.util.Collection} an array of ShippingModels
 */
function getApplicableShippingMethods(shipment, address) {
    var ShippingMgr = require('dw/order/ShippingMgr');
    var ShippingMethodModel = require('*/cartridge/models/shipping/shippingMethod');
    delete session.custom.restrictShipMethodID; // eslint-disable-line
    if (!shipment) return null;
    var shipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment);

    var shippingMethods;
    if (address) {
        shippingMethods = shipmentShippingModel.getApplicableShippingMethods(address);
    } else {
        shippingMethods = shipmentShippingModel.getApplicableShippingMethods();
    }

    // Move Pickup in store method to the end of the list
    var pickupInstoreMethod = collections.find(shippingMethods, function (method) {
        return method.custom.storePickupEnabled;
    });

    var electronicShippingMethod = collections.find(shippingMethods, function (method) {
        return method.ID.toLowerCase() === 'electronic';
    });

    if (pickupInstoreMethod) {
        shippingMethods.remove(pickupInstoreMethod);
    }

    if (electronicShippingMethod) {
        shippingMethods.remove(electronicShippingMethod);
    }

    // Remove the AIR shipping methods - this is to handle the registered user with default address and the shipping method display in cart page.
    var airShippingMethod = collections.find(shippingMethods, function (method) { // eslint-disable-line
        if ('legacyShipID' in method.custom && method.custom.legacyShipID !== '000') {
            return method.custom.legacyShipID;
        }
    });

    if (airShippingMethod) {
        var restrictedShipMethods = getRestrictedShipMethods(shippingMethods);
        collections.forEach(restrictedShipMethods, function (method) {
            shippingMethods.remove(method);
        });
    }

    return shippingMethods.toArray().map(function (shippingMethod) {
        return new ShippingMethodModel(shippingMethod, shipment);
    });
}

/**
 * Returns the first shipping method (and maybe prevent in store pickup)
 * @param {dw.util.Collection} methods - Applicable methods from ShippingShipmentModel
 * @param {boolean} filterPickupInStore - whether to exclude PUIS method
 * @returns {dw.order.ShippingMethod} - the first shipping method (maybe non-PUIS)
 */
function getFirstApplicableShippingMethod(methods, filterPickupInStore) {
    var method;
    var iterator = methods.iterator();
    while (iterator.hasNext()) {
        method = iterator.next();
        if (method.ID !== 'Electronic') {
            if (!filterPickupInStore || (filterPickupInStore && !method.custom.storePickupEnabled)) {
                break;
            }
        }
    }
    return method;
}

/**
 * Sets the shipping method of the basket's default shipment
 * @param {dw.order.Shipment} shipment - Any shipment for the current basket
 * @param {string} shippingMethodID - The shipping method ID of the desired shipping method
 * @param {dw.util.Collection} shippingMethods - List of applicable shipping methods
 * @param {Object} address - the address
 */
function selectShippingMethod(shipment, shippingMethodID, shippingMethods, address) {
    var ShippingMgr = require('dw/order/ShippingMgr');
    var applicableShippingMethods;
    var defaultShippingMethod = ShippingMgr.getDefaultShippingMethod();
    var shippingAddress;

    if (address && shipment) {
        shippingAddress = shipment.shippingAddress;

        if (shippingAddress) {
            if (address.stateCode && shippingAddress.stateCode !== address.stateCode) {
                shippingAddress.stateCode = address.stateCode;
            }
            if (address.postalCode && shippingAddress.postalCode !== address.postalCode) {
                shippingAddress.postalCode = address.postalCode;
            }
        }
    }

    var isShipmentSet = false;

    if (shippingMethods) {
        applicableShippingMethods = shippingMethods;
    } else {
        var shipmentModel = ShippingMgr.getShipmentShippingModel(shipment);
        applicableShippingMethods = address ? shipmentModel.getApplicableShippingMethods(address) :
            shipmentModel.applicableShippingMethods;
    }
    if (shippingMethodID) {
        // loop through the shipping methods to get shipping method
        var iterator = applicableShippingMethods.iterator();
        while (iterator.hasNext()) {
            var shippingMethod = iterator.next();
            if (shippingMethod.ID === shippingMethodID) {
                shipment.setShippingMethod(shippingMethod);
                isShipmentSet = true;
                break;
            }
        }
    }
    if (!isShipmentSet) {
        if (collections.find(applicableShippingMethods, function (sMethod) {
            return sMethod.ID === defaultShippingMethod.ID;
        })) {
            shipment.setShippingMethod(defaultShippingMethod);
        } else if (applicableShippingMethods.length > 0) {
            var firstMethod = getFirstApplicableShippingMethod(applicableShippingMethods, true);
            shipment.setShippingMethod(firstMethod);
        } else {
            shipment.setShippingMethod(null);
        }
    }
}
/**
 * Retrieve raw address JSON object from request.form
 * @param {Request} req - the DW Request object
 * @returns {Object} - raw JSON representing address form data
 */
function getAddressFromRequest(req) {
    return {
        firstName: req.form.firstName,
        lastName: req.form.lastName,
        address1: req.form.address1,
        address2: req.form.address2,
        companyName: req.form.companyName,
        city: req.form.city,
        stateCode: req.form.stateCode,
        postalCode: req.form.postalCode,
        countryCode: req.form.countryCode,
        phone: req.form.phone
    };
}
/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {Shipment} shipment - the target Shipment object
 * @param {Object} customer - the associated Customer Model object
 * @param {string} containerView - view of the shipping models (order or basket)
 * @returns {dw.util.ArrayList} an array of ShippingModels
 */
function getShippingModel(shipment, customer, containerView) {
    if (!shipment) return [];
    return new ShippingModel(shipment, null, customer, containerView);
}

module.exports = {
    getShippingModel: getShippingModel,
    getShippingModels: base.getShippingModels,
    selectShippingMethod: selectShippingMethod,
    ensureShipmentHasMethod: base.ensureShipmentHasMethod,
    getShipmentByUUID: base.getShipmentByUUID,
    getAddressFromRequest: getAddressFromRequest,
    getApplicableShippingMethods: getApplicableShippingMethods,
    markShipmentForPickup: base.markShipmentForPickup,
    markShipmentForShipping: base.markShipmentForShipping
};
