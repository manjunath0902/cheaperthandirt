'use strict';

var base = module.superModule;

var BasketMgr = require('dw/order/BasketMgr');
var HookMgr = require('dw/system/HookMgr');
var OrderMgr = require('dw/order/OrderMgr');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
/**
 * handles the payment authorization for each payment instrument
 * @param {dw.order.Order} order - the order object
 * @param {string} orderNumber - The order number for the order
 * @param {request} req - The request object
 * @returns {Object} an error object
 */
function handlePayments(order, orderNumber, req) {
    var result = {};
    var authorizationResult;
    if (order.totalNetPrice !== 0.00) {
        var paymentInstruments = order.paymentInstruments;

        if (paymentInstruments.length === 0) {
            Transaction.wrap(function () {
                OrderMgr.failOrder(order);
            });
            result.error = true;
        }
        var paymentInstrument;
        if (!result.error) {
            for (var i = 0; i < paymentInstruments.length; i++) {
                paymentInstrument = paymentInstruments[i];
                if (paymentInstrument.paymentMethod === 'CREDIT_CARD') {
                    var paymentProcessor = PaymentMgr
                        .getPaymentMethod(paymentInstrument.paymentMethod)
                        .paymentProcessor;
                    if (paymentProcessor === null) {
                        Transaction.begin();
                        paymentInstrument.paymentTransaction.setTransactionID(orderNumber);
                        Transaction.commit();
                    } else {
                        if (HookMgr.hasHook('app.payment.processor.' +
                                paymentProcessor.ID.toLowerCase())) {
                            authorizationResult = HookMgr.callHook(
                                'app.payment.processor.' + paymentProcessor.ID.toLowerCase(),
                                'Authorize',
                                order,
                                paymentInstrument,
                                paymentProcessor,
                                req
                            );
                        } else {
                            authorizationResult = HookMgr.callHook(
                                'app.payment.processor.default',
                                'Authorize'
                            );
                        }

                        if (authorizationResult.error) {
                            Transaction.wrap(function () {
                                OrderMgr.failOrder(order);
                            });
                            break;
                        }
                    }
                }
            }
        }
    }

    return authorizationResult;
}

/**
 * Copies a CustomerAddress to a Shipment as its Shipping Address
 * @param {dw.customer.CustomerAddress} address - The customer address
 * @param {dw.order.Shipment} [shipmentOrNull] - The target shipment
 */
function copyCustomerAddressToShipment(address, shipmentOrNull) {
    var currentBasket = BasketMgr.getCurrentBasket();
    var shipment = shipmentOrNull || currentBasket.defaultShipment;
    var shippingAddress = shipment.shippingAddress;

    Transaction.wrap(function () {
        if (shippingAddress === null) {
            shippingAddress = shipment.createShippingAddress();
        }

        shippingAddress.setFirstName(address.firstName);
        shippingAddress.setLastName(address.lastName);
        shippingAddress.setAddress1(address.address1);
        shippingAddress.setAddress2(address.address2);
        shippingAddress.setCompanyName(address.companyName);
        shippingAddress.setCity(address.city);
        shippingAddress.setPostalCode(address.postalCode);
        shippingAddress.setStateCode(address.stateCode);
        var countryCode = address.countryCode;
        shippingAddress.setCountryCode(countryCode.value);
        shippingAddress.setPhone(address.phone);
    });
}

/**
 * Copies a CustomerAddress to a Basket as its Billing Address
 * @param {dw.customer.CustomerAddress} address - The customer address
 */
function copyCustomerAddressToBilling(address) {
    var currentBasket = BasketMgr.getCurrentBasket();
    var billingAddress = currentBasket.billingAddress;

    Transaction.wrap(function () {
        if (!billingAddress) {
            billingAddress = currentBasket.createBillingAddress();
        }

        billingAddress.setFirstName(address.firstName);
        billingAddress.setLastName(address.lastName);
        billingAddress.setAddress1(address.address1);
        billingAddress.setAddress2(address.address2);
        billingAddress.setCompanyName(address.companyName);
        billingAddress.setCity(address.city);
        billingAddress.setPostalCode(address.postalCode);
        billingAddress.setStateCode(address.stateCode);
        var countryCode = address.countryCode;
        billingAddress.setCountryCode(countryCode.value);
        if (!billingAddress.phone) {
            billingAddress.setPhone(address.phone);
        }
    });
}

/**
 * Copies information from the shipping form to the associated shipping address
 * @param {Object} shippingData - the shipping data
 * @param {dw.order.Shipment} [shipmentOrNull] - the target Shipment
 */
function copyShippingAddressToShipment(shippingData, shipmentOrNull) {
    var currentBasket = BasketMgr.getCurrentBasket();
    var shipment = shipmentOrNull || currentBasket.defaultShipment;

    var shippingAddress = shipment.shippingAddress;

    Transaction.wrap(function () {
        if (shippingAddress === null) {
            shippingAddress = shipment.createShippingAddress();
        }

        shippingAddress.setFirstName(shippingData.address.firstName);
        shippingAddress.setLastName(shippingData.address.lastName);
        shippingAddress.setAddress1(shippingData.address.address1);
        shippingAddress.setAddress2(shippingData.address.address2);
        shippingAddress.setCompanyName(shippingData.address.companyName !== undefined ? shippingData.address.companyName : '');
        shippingAddress.setCity(shippingData.address.city);
        shippingAddress.setPostalCode(shippingData.address.postalCode);
        shippingAddress.setStateCode(shippingData.address.stateCode);
        var countryCode = shippingData.address.countryCode.value ? shippingData.address.countryCode.value : shippingData.address.countryCode;
        shippingAddress.setCountryCode(countryCode);
        shippingAddress.setPhone(shippingData.address.phone);

        ShippingHelper.selectShippingMethod(shipment, shippingData.shippingMethod);
    });
}

/**
 * Check whether cart has only GiftCard shipment
 * @param {Collection} productLineItems - Basket Line Items
 * @return {boolean} onlyGC - boolean value
 */
function checkOnlyGiftCardInCart(productLineItems) {
    var onlyGC = true;
    try {
        for (var i = 0; i < productLineItems.length; i++) {
            var productLineItem = productLineItems[i];
            if (!('giftCardRecipientEmail' in productLineItem.custom && productLineItem.custom.giftCardRecipientEmail)) {
                onlyGC = false;
                break;
            }
        }
    } catch (e) {
        onlyGC = false;
    }
    return onlyGC;
}

/**
 * Check whether cart has E-GiftCard shipment
 * @param {Collection} productLineItems - Basket Line Items
 * @return {boolean} hasGC - boolean value
 */
function hasGiftCardInCart(productLineItems) {
    var hasGC = false;
    for (var i = 0; i < productLineItems.length; i++) {
        var productLineItem = productLineItems[i];
        if (productLineItem.product !== null && 'giftCardRecipientEmail' in productLineItem.custom && productLineItem.custom.giftCardRecipientEmail) {
            hasGC = true;
            break;
        }
    }
    return hasGC;
}

/**
 * Check whether cart has any GiftCard shipment
 * @param {Collection} productLineItems - Basket Line Items
 * @return {boolean} onlyGC - boolean value
 */
function hasOnlyGiftCardShipment(productLineItems) {
    var onlyGC = true;
    try {
        for (var i = 0; i < productLineItems.length; i++) {
            var productLineItem = productLineItems[i];
            if (!(productLineItem.product !== null && 'giftcardProduct' in productLineItem.product.custom && productLineItem.product.custom.giftcardProduct)) {
                onlyGC = false;
                break;
            }
        }
    } catch (e) {
        onlyGC = false;
    }
    return onlyGC;
}

/**
 * Copies information from the shipping form to the associated shipping address
 * @param {dw.order.Shipment} [shipmentOrNull] - the target Shipment
 */
function copyGCAddressToShipment(shipmentOrNull) {
    var currentBasket = BasketMgr.getCurrentBasket();
    var shipment = shipmentOrNull || currentBasket.defaultShipment;

    var shippingAddress = shipment.shippingAddress;
    var shippingAddressString = Site.getCurrent().getCustomPreferenceValue('electronicShipmentAddress').replace(/\s/g, '').trim();

    if (shippingAddressString !== null || shippingAddressString !== undefined) {
        var shippingData = JSON.parse(shippingAddressString);
        Transaction.wrap(function () {
            if (shippingAddress === null) {
                shippingAddress = shipment.createShippingAddress();
            }

            shippingAddress.setFirstName(shippingData.address.firstName);
            shippingAddress.setLastName(shippingData.address.lastName);
            shippingAddress.setAddress1(shippingData.address.address1);
            shippingAddress.setAddress2(shippingData.address.address2);
            shippingAddress.setCompanyName(shippingData.address.companyName);
            shippingAddress.setCity(shippingData.address.city);
            shippingAddress.setPostalCode(shippingData.address.postalCode);
            shippingAddress.setStateCode(shippingData.address.stateCode);
            var countryCode = shippingData.address.countryCode.value ? shippingData.address.countryCode.value : shippingData.address.countryCode;
            shippingAddress.setCountryCode(countryCode);
            shippingAddress.setPhone(shippingData.address.phone);
            if (shippingData.address.email != null) {
                currentBasket.setCustomerEmail(shippingData.address.email);
            }

            ShippingHelper.selectShippingMethod(shipment, shippingData.shippingMethod);
        });
    }
}


/**
 * Copies information from the shipping form to the associated shipping address
 * @param {dw.order.Shipment} [shipmentOrNull] - the target Shipment
 */
function clearShippingAddressOnShipment(shipmentOrNull) {
    var currentBasket = BasketMgr.getCurrentBasket();
    var shipment = shipmentOrNull || currentBasket.defaultShipment;
    var ShippingMgr = require('dw/order/ShippingMgr');

    var shippingAddress = shipment.shippingAddress;

    if (shippingAddress !== null || shippingAddress !== undefined) {
        Transaction.wrap(function () {
            shippingAddress.setFirstName('');
            shippingAddress.setLastName('');
            shippingAddress.setAddress1('');
            shippingAddress.setAddress2('');
            shippingAddress.setCompanyName('');
            shippingAddress.setCity('');
            shippingAddress.setPostalCode('');
            shippingAddress.setStateCode('');
            shippingAddress.setCountryCode('');
            shippingAddress.setPhone('');
            if (ShippingMgr.getDefaultShippingMethod().ID !== null) {
                ShippingHelper.selectShippingMethod(shipment, ShippingMgr.getDefaultShippingMethod().ID);
            }
        });
    }
}

/**
 * Copies a raw address object to the baasket billing address
 * @param {Object} address - an address-similar Object (firstName, ...)
 * @param {Object} currentBasket - the current shopping basket
 */
function copyBillingAddressToBasket(address, currentBasket) {
    var billingAddress = currentBasket.billingAddress;

    Transaction.wrap(function () {
        if (!billingAddress) {
            billingAddress = currentBasket.createBillingAddress();
        }

        billingAddress.setFirstName(address.firstName);
        billingAddress.setLastName(address.lastName);
        billingAddress.setAddress1(address.address1);
        billingAddress.setAddress2(address.address2);
        billingAddress.setCompanyName(address.companyName);
        billingAddress.setCity(address.city);
        billingAddress.setPostalCode(address.postalCode);
        billingAddress.setStateCode(address.stateCode);
        billingAddress.setCountryCode(address.countryCode.value);
        if (!billingAddress.phone) {
            billingAddress.setPhone(address.phone);
        }
    });
}

/**
 * Sets the payment transaction amount
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {dw.value.Money} Money open to be charget for CC payment
 */
function getRemainingTotal(currentBasket) {
    var collections = require('*/cartridge/scripts/util/collections');
    var Money = require('dw/value/Money');
    var paymentInstruments = currentBasket !== null ? currentBasket.paymentInstruments : null;
    var amountOpen;
    try {
        // TODO: This function will need to account for gift certificates at a later date
        var giftCardsTotal = new Money(0.0, currentBasket.getCurrencyCode());
        collections.forEach(paymentInstruments, function (paymentInstrument) {
            if (paymentInstrument.getPaymentMethod() === 'GiftCard') {
                giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
            }
        });
        // Gets the order total.
        var orderTotal = currentBasket.totalGrossPrice;

        // Calculates the amount to charge for the payment instrument.
        // This is the remaining open order total that must be paid.
        amountOpen = orderTotal.subtract(giftCardsTotal);
        if (amountOpen.value <= 0.0) {
            amountOpen = new Money(0.0, 'USD');
        }
    } catch (e) {
        //  catch block
    }
    return amountOpen;
}


/**
 * Sets the payment transaction amount
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {Object} an error object
 */
function calculatePaymentTransaction(currentBasket) {
    var collections = require('*/cartridge/scripts/util/collections');
    var arrayHelper = require('*/cartridge/scripts/util/array');
    var Money = require('dw/value/Money');
    var result = { error: false };
    var paymentInstruments = currentBasket.paymentInstruments;
    try {
        Transaction.wrap(function () {
            // TODO: This function will need to account for gift certificates at a later date
            var giftCardsTotal = new Money(0.0, currentBasket.getCurrencyCode());
            collections.forEach(paymentInstruments, function (paymentInstrument) {
                if (paymentInstrument.getPaymentMethod() === 'GiftCard') {
                    giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
                }
            });
            // Gets the order total.
            var orderTotal = currentBasket.totalGrossPrice;

            // Calculates the amount to charge for the payment instrument.
            // This is the remaining open order total that must be paid.
            var amountOpen = orderTotal.subtract(giftCardsTotal);
            var creditCardPaymentInstrument = arrayHelper.find(paymentInstruments, function (paymentInstrument) {
                return paymentInstrument.getPaymentMethod() === 'CREDIT_CARD';
            });
            if (creditCardPaymentInstrument) {
                creditCardPaymentInstrument.getPaymentTransaction().setAmount(amountOpen);
            }
        });
    } catch (e) {
        result.error = true;
    }

    return result;
}

/**
 * Remove gift card paymentInstrument if cart contains gift card
 * @param {dw.order.Basket} currentBasket - The current basket
 */
function removeGiftCardPaymentInstruments(currentBasket) {
    var collections = require('*/cartridge/scripts/util/collections');
    var paymentInstruments = currentBasket.paymentInstruments;
    Transaction.wrap(function () {
        collections.forEach(paymentInstruments, function (paymentInstrument) {
            if (paymentInstrument.getPaymentMethod() === 'GiftCard') {
                currentBasket.removePaymentInstrument(paymentInstrument);
            }
        });
    });
}

/**
 * saves payment instruemnt to customers wallet
 * @param {Object} billingData - billing information entered by the user
 * @param {dw.order.Basket} currentBasket - The current basket
 * @param {dw.customer.Customer} customer - The current customer
 * @returns {dw.customer.CustomerPaymentInstrument} newly stored payment Instrument
 */
function savePaymentInstrumentToWallet(billingData, currentBasket, customer) {
    var wallet = customer.getProfile().getWallet();
    var processor = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD).getPaymentProcessor();
    var formInfo = {};
    formInfo.cardNumber = billingData.paymentInformation.cardNumber.value;
    formInfo.expirationYear = billingData.paymentInformation.expirationYear.value;
    formInfo.expirationMonth = billingData.paymentInformation.expirationMonth.value;
    var tokenResponse = HookMgr.callHook(
        'app.payment.processor.' + processor.ID.toLowerCase(),
        'createToken',
        customer,
        formInfo
    );
    return Transaction.wrap(function () {
        if (!tokenResponse.error) {
            var storedPaymentInstrument = wallet.createPaymentInstrument(PaymentInstrument.METHOD_CREDIT_CARD);

            storedPaymentInstrument.setCreditCardHolder(
                currentBasket.billingAddress.fullName
            );
            storedPaymentInstrument.setCreditCardNumber(
                billingData.paymentInformation.cardNumber.value
            );
            storedPaymentInstrument.setCreditCardType(
                billingData.paymentInformation.cardType.value
            );
            storedPaymentInstrument.setCreditCardExpirationMonth(
                billingData.paymentInformation.expirationMonth.value
            );
            storedPaymentInstrument.setCreditCardExpirationYear(
                billingData.paymentInformation.expirationYear.value
            );

            storedPaymentInstrument.setCreditCardToken(tokenResponse.customerPaymentProfileId);

            return storedPaymentInstrument;
        }
        return null;
    });
}

/**
 * Checks whether the basket requires CC payment
 * @param {dw.order.Basket} basket - the basket object
 * @returns {boolean} requireCC requires a cc payment or not
 */
function validateIfOrderRequirePayment(basket) {
    var requireCC = true;
    var remainingTotal = getRemainingTotal(basket);
    if (basket !== null) {
        if (basket.totalNetPrice === 0.00 || (remainingTotal && remainingTotal.value === 0.00)) {
            requireCC = false;
        }
    }
    return requireCC;
}
/**
 *  Validate if the customer is eligible for postal shipping method.
 *  @param {dw.order.Basket} currentBasket - Current User basket
 *  @return {boolean} eligible - Decides whether customer is eligible for postal shipping method
 */
function checkPostalShippingEligibility(currentBasket) {
    var eligible = false;
    try {
        var maxWeightForPostalShipping = Site.getCurrent().getCustomPreferenceValue('maxWeightForPostalShipping');
        var maxCombinedDollar = Site.getCurrent().getCustomPreferenceValue('maxCombinedDollarForPostalShip');
        var shipToHomeShipment = base.getShipToHomeShipment(currentBasket);
        var shipWeight = 0;
        var totalProductPrice = 0;
        var productLineItem;
        var productLineItems = shipToHomeShipment.getProductLineItems().iterator();
        while (productLineItems.hasNext()) {
            productLineItem = productLineItems.next();
            shipWeight += productLineItem.getProduct().custom.unitWeight * productLineItem.getQuantityValue();
            totalProductPrice += productLineItem.adjustedPrice.value;
        }
        eligible = shipWeight <= maxWeightForPostalShipping && totalProductPrice <= maxCombinedDollar;
        session.custom.postalShipEligible = eligible;  // eslint-disable-line
    } catch (e) {
        // intentionally kept blank
    }
    return eligible;
}

/**
 *  Validate if the customer is eligible for free shipping method.
 *  @param {dw.order.Basket} currentBasket - Current User basket
 *  @return {boolean} eligible - Decides whether customer is eligible for free shipping method
 */
function checkFreeShippingEligibility(currentBasket) {
    var eligible = false;
    try {
        var PromotionMgr = require('dw/campaign/PromotionMgr');
        var collections = require('*/cartridge/scripts/util/collections');
        var promotionID = Site.getCurrent().getCustomPreferenceValue('shippingPromotionID');
        var shippingPromotions = PromotionMgr.activeCustomerPromotions.getShippingPromotions();
        var shipToHomeShipment = base.getShipToHomeShipment(currentBasket);
        collections.forEach(shippingPromotions, function (shippingPromotion) {
            if (shippingPromotion.ID === promotionID) {
                // if a gift card shipment is found, we offer free shipping method. It is up to user to select free shipping over default shipping
                if (shipToHomeShipment !== null && shipToHomeShipment !== undefined) {
                    eligible = hasOnlyGiftCardShipment(shipToHomeShipment.getProductLineItems());
                    session.custom.freeShipEligible = eligible; // eslint-disable-line
                }
                // check the threshold order limit to allow the customer to order with Free Shipping method
                if (!eligible && 'orderTotalThresholdForFreeShipping' in shippingPromotion.custom) {
                    eligible = currentBasket.totalGrossPrice.value > shippingPromotion.custom.orderTotalThresholdForFreeShipping;
                    session.custom.freeShipEligible = eligible; // eslint-disable-line
                }
            }
        });
        // leveraging the call points updated for free shipping to check postalShip Eligibility
        checkPostalShippingEligibility(currentBasket);
    } catch (e) {
        // intentionally kept blank
    }
    return eligible;
}

/**
 * renders the user's stored payment Instruments
 * @param {Object} req - The request object
 * @param {Object} accountModel - The account model for the current customer
 * @param {boolean} requirecc - Updates the view if cc pay is required
 * @returns {string|null} newly stored payment Instrument
 */
function getRenderedPaymentInstruments(req, accountModel, requirecc) {
    var result;

    if (req.currentCustomer.raw.authenticated
        && req.currentCustomer.raw.registered
        && req.currentCustomer.raw.profile.wallet.paymentInstruments.getLength()
    ) {
        var context;
        var template = 'checkout/billing/storedPaymentInstruments';

        context = { customer: accountModel, requireCCPayment: requirecc };
        result = renderTemplateHelper.getRenderedHtml(
            context,
            template
        );
    }

    return result || null;
}

/**
 * Loop through all shipments and make sure all not null
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {boolean} - allValid
 */
function ensureValidShipments(lineItemContainer) {
    var collections = require('*/cartridge/scripts/util/collections');
    var shipments = lineItemContainer.shipments;
    var storeAddress = true;
    var allValid = collections.every(shipments, function (shipment) {
        if (shipment) {
            var hasStoreID = shipment.custom && shipment.custom.fromStoreId;
            if (shipment.shippingMethod !== null && shipment.shippingMethod.custom && shipment.shippingMethod.custom.storePickupEnabled && !hasStoreID) {
                storeAddress = false;
            }
            var address = shipment.shippingAddress;
            return address && address.address1 && storeAddress;
        }
        return false;
    });
    return allValid;
}

/**
 * Check for the legacy order and return the legacy shipping cost
 * @param {dw.order} order - current order
 * @return {Object} legacyOrderCheck - Legacy order flag and shipping cost
 */
function checkforLegacyOrder(order) {
    var collections = require('*/cartridge/scripts/util/collections');
    var formatMoney = require('dw/util/StringUtils').formatMoney;
    var ShippingMgr = require('dw/order/ShippingMgr');
    var Money = require('dw/value/Money');
    var result = { isLegacyOrder: false, legacyShippingTotal: 0.00, containsFFL: false, fflShipCost: 0.00 };
    try {
        if (order && 'legacyOrder' in order.custom && order.custom.legacyOrder) {
            result.isLegacyOrder = true;
            if ('legacyShippingTotal' in order.custom) {
                result.legacyShippingTotal = order.custom.legacyShippingTotal;
            }
            // get the firearm handling fee for legacy FFL shipments
            var shipments = order.shipments;
            collections.forEach(shipments, function (shipment) {
                if (shipment) {
                    if (shipment.shippingMethod !== null && shipment.shippingMethod.ID === 'FFL') {
                        var ShipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment);
                        result.fflShipCost = ShipmentShippingModel.getShippingCost(shipment.getShippingMethod()).amount;
                        result.containsFFL = true;
                    }
                }
            });
        }
        result.legacyShippingTotal = formatMoney(new Money(result.legacyShippingTotal, 'USD'));
        result.fflShipCost = !result.fflShipCost.available ? '-' : formatMoney(result.fflShipCost);
    } catch (e) {
        result.isLegacyOrder = false;
    }
    return result;
}

/**
 * Loop through all shipments and make sure all not null
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 */
function ensureStoreShipmentHasValidAddr(lineItemContainer) {
    var collections = require('*/cartridge/scripts/util/collections');
    var StoreMgr = require('dw/catalog/StoreMgr');
    var shipments = lineItemContainer.shipments;
    var storeShippingMethodID = Site.getCurrent().getCustomPreferenceValue('storeShippingMethodID');
    storeShippingMethodID = storeShippingMethodID ? storeShippingMethodID : 'FFL'; //eslint-disable-line
    collections.every(shipments, function (shipment) {
        if (shipment) {
            if (shipment.custom && 'shipmentType' in shipment.custom && shipment.custom.shipmentType === 'instore') {
                var storeID = shipment.custom.fromStoreId;
                Transaction.wrap(function () {
                    var store = StoreMgr.getStore(storeID);
                    var storeAddress = {
                        address: {
                            firstName: store.name,
                            lastName: '',
                            address1: store.address1,
                            address2: store.address2,
                            city: store.city,
                            stateCode: store.stateCode,
                            postalCode: store.postalCode,
                            countryCode: store.countryCode.value,
                            phone: store.phone,
                            email: store.email
                        },
                        shippingMethod: storeShippingMethodID
                    };
                    copyShippingAddressToShipment(storeAddress, shipment);
                });
            }
        }
    });
}


module.exports = {
    getFirstNonDefaultShipmentWithProductLineItems: base.getFirstNonDefaultShipmentWithProductLineItems,
    ensureNoEmptyShipments: base.ensureNoEmptyShipments,
    getProductLineItem: base.getProductLineItem,
    isShippingAddressInitialized: base.isShippingAddressInitialized,
    prepareShippingForm: base.prepareShippingForm,
    prepareBillingForm: base.prepareBillingForm,
    copyCustomerAddressToShipment: copyCustomerAddressToShipment,
    copyCustomerAddressToBilling: copyCustomerAddressToBilling,
    copyShippingAddressToShipment: copyShippingAddressToShipment,
    copyBillingAddressToBasket: copyBillingAddressToBasket,
    validateFields: base.validateFields,
    validateShippingForm: base.validateShippingForm,
    validateBillingForm: base.validateBillingForm,
    validatePayment: base.validatePayment,
    validateCreditCard: base.validateCreditCard,
    calculatePaymentTransaction: calculatePaymentTransaction,
    getNonGiftCardAmount: base.getNonGiftCardAmount,
    recalculateBasket: base.recalculateBasket,
    handlePayments: handlePayments,
    createOrder: base.createOrder,
    placeOrder: base.placeOrder,
    savePaymentInstrumentToWallet: savePaymentInstrumentToWallet,
    getRenderedPaymentInstruments: getRenderedPaymentInstruments,
    sendConfirmationEmail: base.sendConfirmationEmail,
    ensureValidShipments: ensureValidShipments,
    setGift: base.setGift,
    getFinalOrderTotal: base.getFinalOrderTotal,
    onlyStoreShipment: base.onlyStoreShipment,
    getRemainingTotal: getRemainingTotal,
    getShipToHomeShipment: base.getShipToHomeShipment,
    getShipToStoreShipment: base.getShipToStoreShipment,
    calculateUPSShipRate: base.calculateUPSShipRate,
    copyGCAddressToShipment: copyGCAddressToShipment,
    hasGiftCardInCart: hasGiftCardInCart,
    checkOnlyGiftCardInCart: checkOnlyGiftCardInCart,
    clearShippingAddressOnShipment: clearShippingAddressOnShipment,
    validateIfOrderRequirePayment: validateIfOrderRequirePayment,
    removeGiftCardPaymentInstruments: removeGiftCardPaymentInstruments,
    checkFreeShippingEligibility: checkFreeShippingEligibility,
    checkPostalShippingEligibility: checkPostalShippingEligibility,
    checkforLegacyOrder: checkforLegacyOrder,
    ensureStoreShipmentHasValidAddr: ensureStoreShipmentHasValidAddr
};
