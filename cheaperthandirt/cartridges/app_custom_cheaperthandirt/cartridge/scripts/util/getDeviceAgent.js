'use strict';

/**
* To find out the device type.
* To define input and output parameters, create entries of the form:
*/

/**
 * @constructor
 * @classdesc Returns images for a given product
 * @returns {string} Device type accessed by the user
 */
function getDeviceType() {
    var deviceType = 'desktop';
    var iPhoneDevice = 'iPhone';
    var iPadDevice = 'iPad';
    var androidDevice = 'Android';
    /* eslint-disable */
    var httpUserAgent = request.httpUserAgent;

    if (!httpUserAgent) {
        return deviceType;
    }

    if (httpUserAgent.indexOf(iPhoneDevice) > -1) {
        deviceType = 'mobile';

    } else if (httpUserAgent.indexOf(androidDevice) > -1) {
        if (httpUserAgent.toLowerCase().indexOf('mobile') > -1) {
            deviceType = 'mobile';
        }
    } else if (httpUserAgent.indexOf(iPadDevice) > -1) {
        deviceType = 'tablet';
    }
    /* eslint-enable */
    return deviceType;
}


module.exports = {
    getDeviceType: getDeviceType
};
