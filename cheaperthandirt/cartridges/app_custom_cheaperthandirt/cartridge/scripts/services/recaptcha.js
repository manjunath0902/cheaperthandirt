'use strict';

/**
 *
 * @module services/recaptcha
 */

var Site = require('dw/system/Site');

// service
var service = require('../init/googleServiceInit.js');

/**
 * @return {JSON} service status
 * @param {Object} req - request object
 */
function validateRecaptcha(req) {
    // Initialize local variables
    var secretKey = 'googleReCaptchaSecret' in Site.current.preferences.custom ?
        Site.current.preferences.custom.googleReCaptchaSecret : null;
    var serviceParams = null;
    var result = null;

    // Populate the values required for validation
    if (req != null) {
        serviceParams = {
            secret: secretKey,
            response: req.form.recaptcha_token,
            remoteip: req.remoteAddress
        };
    }

    if (serviceParams.secret != null && serviceParams.response != null) {
        // Capture the Google reCaptcha status
        var googleReCAptchaServiceObj = service.get();
        result = googleReCAptchaServiceObj.call(serviceParams);
        result = JSON.parse(result.object);
    }

    return result;
}

module.exports = {
    validate: validateRecaptcha
};
