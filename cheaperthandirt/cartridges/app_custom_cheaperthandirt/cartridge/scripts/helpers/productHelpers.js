'use strict';
var base = module.superModule;
/**
 * Creates the breadcrumbs object
 * @param {string} cgid - category ID from navigation and search
 * @param {string} pid - product ID
 * @param {Array} breadcrumbs - array of breadcrumbs object
 * @returns {Array} an array of breadcrumb objects
 */
function getAllBreadcrumbs(cgid, pid, breadcrumbs) {
    var Resource = require('dw/web/Resource');
    var URLUtils = require('dw/web/URLUtils');
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var ProductMgr = require('dw/catalog/ProductMgr');

    var category;
    var product;
    if (pid) {
        product = ProductMgr.getProduct(pid);
        category = product.variant ?
            product.masterProduct.primaryCategory :
            product.primaryCategory;
    } else if (cgid) {
        category = CatalogMgr.getCategory(cgid);
    }

    if (product) {
        var suffix = product.name.length > 60 ? '...' : '';
        var productName = product.name.substring(0, 60) + suffix;
        breadcrumbs.push({
            htmlValue: productName
        });
    }

    if (category) {
        breadcrumbs.push({
            htmlValue: category.displayName,
            url: URLUtils.abs('Search-Show', 'cgid', category.ID)
        });

        if (category.parent && category.parent.ID !== 'root') {
            return getAllBreadcrumbs(category.parent.ID, null, breadcrumbs);
        } else if (category.parent.ID === 'root') {
            breadcrumbs.push({
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            });
        }
    }

    return breadcrumbs;
}


/**
 * Renders the Product Details Page
 * @param {Object} querystring - query string parameters
 * @param {Object} reqPageMetaData - request pageMetaData object
 * @returns {Object} contain information needed to render the product page
 */
function showProductPage(querystring, reqPageMetaData) {
    var URLUtils = require('dw/web/URLUtils');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

    var params = querystring;
    var product = ProductFactory.get(params);
    var addToCartUrl = URLUtils.url('Cart-AddProduct');
    var breadcrumbs = getAllBreadcrumbs(null, product.id, []).reverse();
    var template = (product.template) ? product.template : 'product/productDetails';

    if (product.productType === 'bundle') {
        template = 'product/bundleDetails';
    } else if (product.productType === 'set') {
        template = 'product/setDetails';
    }

    pageMetaHelper.setPageMetaData(reqPageMetaData, product);
    pageMetaHelper.setPageMetaTags(reqPageMetaData, product);

    return {
        template: template,
        product: product,
        addToCartUrl: addToCartUrl,
        resources: base.getResources(),
        breadcrumbs: breadcrumbs
    };
}

module.exports = {
    getOptionValues: base.getOptionValues,
    getOptions: base.getOptions,
    getCurrentOptionModel: base.getCurrentOptionModel,
    getSelectedOptionsUrl: base.getSelectedOptionsUrl,
    getProductType: base.getProductType,
    getVariationModel: base.getVariationModel,
    getConfig: base.getConfig,
    getLineItemOptions: base.getLineItemOptions,
    getDefaultOptions: base.getDefaultOptions,
    getLineItemOptionNames: base.getLineItemOptionNames,
    showProductPage: showProductPage,
    getAllBreadcrumbs: getAllBreadcrumbs,
    getResources: base.getResources
};
