'use strict';

var URLUtils = require('dw/web/URLUtils');
var ArrayList = require('dw/util/ArrayList');

/**
 * Set search configuration values
 *
 * @param {dw.catalog.ProductSearchModel} apiProductSearch - API search instance
 * @param {Object} params - Provided HTTP query parameters
 * @return {dw.catalog.ProductSearchModel} - API search instance
 */
function setupSearch(apiProductSearch, params) {
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var search = require('*/cartridge/scripts/search/search');

    var sortingRule = params.srule ? CatalogMgr.getSortingRule(params.srule) : null;
    var selectedCategory = CatalogMgr.getCategory(params.cgid);
    selectedCategory = selectedCategory && selectedCategory.online ? selectedCategory : null;

    search.setProductProperties(apiProductSearch, params, selectedCategory, sortingRule);

    if (params.preferences) {
        search.addRefinementValues(apiProductSearch, params.preferences);
    }

    return apiProductSearch;
}

/**
 * Retrieve a category's template filepath if available
 *
 * @param {dw.catalog.ProductSearchModel} apiProductSearch - API search instance
 * @return {string} - Category's template filepath
 */
function getCategoryTemplate(apiProductSearch) {
    return apiProductSearch.category ? apiProductSearch.category.template : '';
}

/**
 * Set content search configuration values
 *
 * @param {Object} params - Provided HTTP query parameters
 * @return {Object} - content search instance
 */
function setupContentSearch(params) {
    var ContentSearchModel = require('dw/content/ContentSearchModel');
    var ContentSearch = require('*/cartridge/models/search/contentSearch');
    var apiContentSearchModel = new ContentSearchModel();

    apiContentSearchModel.setRecursiveFolderSearch(true);
    apiContentSearchModel.setSearchPhrase(params.q);
    apiContentSearchModel.search();
    var contentSearchResult = apiContentSearchModel.getContent();
    var count = Number(apiContentSearchModel.getCount());
    var contentSearch = new ContentSearch(contentSearchResult, count, params.q, params.startingPage, null);

    return contentSearch;
}
/**
 * Set Folder Search Configuration values
 *  @param {Object} params - Provided HTTP query parameters
 *  @return {Object} - Folder search instance
 */
function setupFolderSearch(params) {
    var ContentSearchModel = require('dw/content/ContentSearchModel');
    var apiContentSearchModel = new ContentSearchModel();

    apiContentSearchModel.setRecursiveFolderSearch(true);
    apiContentSearchModel.setSearchPhrase(params.q);
    apiContentSearchModel.search();
    var FolderSearch = apiContentSearchModel.getDeepestCommonFolder();

    return FolderSearch;
}
/**
 * Get category url
 * @param {dw.catalog.Category} category - Current category
 * @returns {string} - Url of the category
 */
function getCategoryUrl(category) {
    return category.custom && 'alternativeUrl' in category.custom && category.custom.alternativeUrl ?
        category.custom.alternativeUrl :
        URLUtils.abs('Search-Show', 'cgid', category.getID()).toString();
}

/**
 * Creates the breadcrumbs object
 * @param {string} category - category from navigation and search
 * @param {Array} breadcrumbs - array of breadcrumbs object
 * @param {boolean}requireLink - array of breadcrumbs object
 * @returns {Array}breadcrumbs - array of breadcrumbs object
 */
function getCatBreadcrumbs(category, breadcrumbs, requireLink) {
    var Resource = require('dw/web/Resource');
    if (category && category.parent !== null) {
        if (requireLink) {
            breadcrumbs.push({
                htmlValue: category.displayName,
                url: getCategoryUrl(category)
            });
        } else {
            breadcrumbs.push({
                htmlValue: category.displayName
            });
        }
        if (category.parent && category.parent.ID !== 'root') {
            return getCatBreadcrumbs(category.parent, breadcrumbs, true);
        } else if (category.parent !== null && category.parent.ID === 'root') {
            breadcrumbs.push({
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            });
        }
    } else {
        breadcrumbs.push({
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        });
    }

    return breadcrumbs;
}


/**
 * Creates the breadcrumbs object
 * @param {string} category - category from navigation and search
 * @param {Array} breadcrumbs - array of breadcrumbs object
 * @param {boolean}requireLink - array of breadcrumbs object
 * @returns {Array}breadcrumbs - array of breadcrumbs object
 */
function getCatBreadcrumbsExcludeHome(category, breadcrumbs, requireLink) {
    if (category.parent !== null && category.parent.ID === '90000') {
        return getCatBreadcrumbsExcludeHome(category.parent, breadcrumbs, true);
    } else if (category) {
        if (requireLink) {
            breadcrumbs.push({
                htmlValue: category.displayName,
                url: getCategoryUrl(category)
            });
        } else {
            breadcrumbs.push({
                htmlValue: category.displayName
            });
        }
        if (category.parent && category.parent.ID !== 'root') {
            return getCatBreadcrumbsExcludeHome(category.parent, breadcrumbs, true);
        }
    }

    return breadcrumbs;
}

/**
 * Creates the breadcrumbs object
 * @param {dw.catalog.Category} selectedCategory - category from navigation and search
 * @param {Object} params - array of breadcrumbs object
 * @param {Array} breadcrumbs - array of breadcrumbs object
 * @param {boolean}requireLink - requireLink for SEO
 * @returns {Array}breadcrumbs - array of breadcrumbs object
 */
function getBrandBreadcrumbs(selectedCategory, params, breadcrumbs, requireLink) {
    var Resource = require('dw/web/Resource');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var brandBreadCrumbs = [];
    var brandCategoryObj;
    if (params.brand !== undefined || (selectedCategory && selectedCategory.parent !== null && selectedCategory.parent.parent !== null && selectedCategory.parent.parent.ID === '90000')) {
        var query = params.brand !== undefined ? params.brand : selectedCategory.displayName;
        brandCategoryObj = CustomObjectMgr.getCustomObject('BrandCategory', query.replace(/\s/g, ''));
    }
    breadcrumbs.push({
        htmlValue: Resource.msg('global.home', 'common', null),
        url: URLUtils.home().toString()
    });
    if (!(selectedCategory && selectedCategory.parent !== null && selectedCategory.parent.parent !== null && selectedCategory.parent.parent.ID === '90000')) {
        if (brandCategoryObj && brandCategoryObj.custom.brandName && brandCategoryObj.custom.brandCategoryID) {
            breadcrumbs.push({
                htmlValue: brandCategoryObj.custom.brandName,
                url: URLUtils.abs('Search-Show', 'cgid', brandCategoryObj.custom.brandCategoryID).toString()
            });
        } else {
            breadcrumbs.push({
                htmlValue: params.brand !== undefined ? params.brand : selectedCategory.displayName
            });
        }
    }
    if (!(selectedCategory && selectedCategory.parent !== null && selectedCategory.parent.parent !== null && selectedCategory.parent.parent.ID === '90000')) {
        if (params.brand !== undefined) {
            brandBreadCrumbs = getCatBreadcrumbsExcludeHome(selectedCategory, [], requireLink).reverse();
        }
    } else {
        breadcrumbs.push({
            htmlValue: selectedCategory.parent !== null && selectedCategory.parent.parent !== null ? selectedCategory.parent.parent.displayName : '90000',
            url: URLUtils.abs('Search-Custom', 'cgid', 'root', 'searchBy', 'Brand').toString()
        });
        breadcrumbs.push({
            htmlValue: selectedCategory.displayName
        });
    }

    if (brandBreadCrumbs !== undefined && brandBreadCrumbs !== null) {
        for (var i = 0; i < brandBreadCrumbs.length; i++) {
            var brandBC = brandBreadCrumbs[i];
            breadcrumbs.push(brandBC);
        }
    }

    return breadcrumbs;
}

/**
 * Creates the breadcrumbs object
 * @param {Object} search - product search object
 * @param {Object} params - request object
 * @param {boolean} isRootCat - boolean attribute
 * @returns {Array} an array of breadcrumb objects
 */
function setupBrandPage(search, params, isRootCat) {
    var SortedMap = require('dw/util/SortedMap');
    var refinements;
    var refinement;
    var refValues;
    var brandsMappings = new SortedMap();

    refinements = search.refinements;
    for (var i = 0; i < refinements.length; i++) {
        refinement = refinements[i];
        if (refinement.isAttributeRefinement && refinement.displayName.toLowerCase() === params.searchBy.toLowerCase()) {
            refValues = refinement.values;
            break;
        }
    }
    if (refValues) {
        refValues.forEach(function (values) {
            var brands = new ArrayList();
            var CustomObjectMgr = require('dw/object/CustomObjectMgr');
            var brandCategoryObj = CustomObjectMgr.getCustomObject('BrandCategory', values.brand);
            if (brandCategoryObj && brandCategoryObj.custom.brandName && brandCategoryObj.custom.brandCategoryID && isRootCat) {
                values.url = URLUtils.https('Search-Show', 'cgid', brandCategoryObj.custom.brandCategoryID, 'brand', values.brandValue).toString(); // eslint-disable-line
            } else if (values.url.indexOf('?') > -1 && !isRootCat) {
                values.url = values.url + '&brand=' + values.brand; // eslint-disable-line
            } else if (!isRootCat) {
                values.url = values.url + '?brand=' + values.brand; // eslint-disable-line
            }
            if (brandsMappings != null && (brandsMappings.get(values.displayValue.charAt(0).toUpperCase()) != null || brandsMappings.get(values.displayValue.charAt(1).toUpperCase()) != null)) {
                if (values.displayValue.charAt(0) === '.') {
                    brands.addAll(brandsMappings.get(values.displayValue.charAt(1).toUpperCase()));
                } else {
                    brands.addAll(brandsMappings.get(values.displayValue.charAt(0).toUpperCase()));
                }
                brands.add(values);
            } else {
                brands.add(values);
            }
            if (values.displayValue.charAt(0) === '.') {
                brandsMappings.put(values.displayValue.charAt(1).toUpperCase(), brands);
            } else {
                brandsMappings.put(values.displayValue.charAt(0).toUpperCase(), brands);
            }
        });

        return brandsMappings;
    }
    return true;
}


/**
 * Returns the level 1 category always
 * @param {category} category The category object
 * @returns {Object} The level 1 category object of the current category
 */
function getLevelOneCategory(category) {
    var Categories = require('*/cartridge/models/categories');
    if (category.parent !== null && category.parent.root) {
        return new Categories(new ArrayList(category));
    }
    return category.parent !== null ? getLevelOneCategory(category.parent) : new Categories(new ArrayList(category), true);
}

exports.setupSearch = setupSearch;
exports.getCategoryTemplate = getCategoryTemplate;
exports.setupContentSearch = setupContentSearch;
exports.getCatBreadcrumbs = getCatBreadcrumbs;
exports.setupBrandPage = setupBrandPage;
exports.setupFolderSearch = setupFolderSearch;
exports.getCategoryUrl = getCategoryUrl;
exports.getLevelOneCategory = getLevelOneCategory;
exports.getBrandBreadcrumbs = getBrandBreadcrumbs;
