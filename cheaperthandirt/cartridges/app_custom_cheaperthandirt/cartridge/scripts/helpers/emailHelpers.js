'use strict';

var base = module.superModule;

module.exports = {
    send: base.send,
    sendEmail: base.sendEmail,

    emailTypes: {
        registration: 1,
        passwordReset: 2,
        passwordChanged: 3,
        orderConfirmation: 4,
        accountLocked: 5,
        accountEdited: 6,
        contactUs: 7,
        rmaRequest: 8
    }
};
