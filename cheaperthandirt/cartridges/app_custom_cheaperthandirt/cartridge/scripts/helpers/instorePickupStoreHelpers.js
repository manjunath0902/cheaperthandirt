'use strict';

var StoreMgr = require('dw/catalog/StoreMgr');
var Transaction = require('dw/system/Transaction');

/**
 * Sets the store and its inventory list for the given product line item.
 * @param {string} storeId - The store id
 * @param {dw.order.ProductLineItem} productLineItem - The ProductLineItem object
 */
function setStoreInProductLineItem(storeId, productLineItem) {
    Transaction.wrap(function () {
        if (storeId) {
            var store = StoreMgr.getStore(storeId);
            if (store) {
                productLineItem.custom.fromStoreId = store.ID; // eslint-disable-line
            }
        }
    });
}

/**
 * Returns the available to sell value for the product at the specified store.
 * @param {Object} storeId - the store ID to lookup the inventory
 * @param {Object} productId - the product ID to lookup the inventory
 * @returns {number} - the available to sell value
 */
function getStoreInventory(storeId, productId) {// eslint-disable-line
    var availableToSellValue = 0;

    return availableToSellValue;
}

module.exports = {
    setStoreInProductLineItem: setStoreInProductLineItem,
    getStoreInventory: getStoreInventory
};
