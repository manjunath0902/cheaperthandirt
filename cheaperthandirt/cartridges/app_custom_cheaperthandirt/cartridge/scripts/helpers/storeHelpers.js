'use strict';
/**
 * Returns the By Pass store object
 * @returns {dw.catalog.StoreMgr} ByPass Store Object
 */
function getByPassStore() {
    var StoreMgr = require('dw/catalog/StoreMgr');
    var byPass = StoreMgr.getStore('ByPass');
    return byPass;
}


/**
 * Searches for stores and creates a plain object of the stores returned by the search
 * @param {string} radius - selected radius
 * @param {string} postalCode - postal code for search
 * @param {string} lat - latitude for search by latitude
 * @param {string} long - longitude for search by longitude
 * @param {Object} geolocation - geloaction object with latitude and longitude
 * @param {boolean} showMap - boolean to show map
 * @param {dw.web.URL} url - a relative url
 * @returns {Object} a plain object containing the results of the search
 */
function getStores(radius, postalCode, lat, long, geolocation, showMap, url) {
    var StoresModel = require('*/cartridge/models/stores');
    var StoreMgr = require('dw/catalog/StoreMgr');
    var Site = require('dw/system/Site');
    var URLUtils = require('dw/web/URLUtils');

    var countryCode = 'US';
    var distanceUnit = countryCode === 'US' ? 'mi' : 'km';
    var resolvedRadius = radius ? parseInt(radius, 10) : 15;

    var searchKey = {};
    var storeMgrResult = null;
    var location = {};

    if (postalCode && postalCode !== '') {
        // find by postal code
        searchKey = postalCode;
        storeMgrResult = StoreMgr.searchStoresByPostalCode(
            countryCode,
            searchKey,
            distanceUnit,
            resolvedRadius
        );
        searchKey = { postalCode: searchKey };
    } else {
        // find by coordinates (detect location)
        location.lat = lat && long ? parseFloat(lat) : geolocation.latitude;
        location.long = long && lat ? parseFloat(long) : geolocation.longitude;

        storeMgrResult = StoreMgr.searchStoresByCoordinates(location.lat, location.long, distanceUnit, resolvedRadius);
        searchKey = { lat: location.lat, long: location.long };
    }

    var actionUrl = url || URLUtils.url('Stores-FindStores', 'showMap', showMap).toString();
    var apiKey = Site.getCurrent().getCustomPreferenceValue('mapAPI');
    var storeMgrResultKeySet = storeMgrResult.keySet();

    //    Remove the ByPass store from the store results
    var byPass = getByPassStore();
    if (byPass) {
        storeMgrResultKeySet.remove(byPass);
    }

    var stores = new StoresModel(storeMgrResultKeySet, searchKey, resolvedRadius, actionUrl, apiKey);

    return stores;
}

/**
 * create the stores results html
 * @param {Array} storesInfo - an array of objects that contains store information
 * @returns {string} The rendered HTML
 */
function createStoresResultsHtml(storesInfo) {
    var HashMap = require('dw/util/HashMap');
    var Template = require('dw/util/Template');

    var context = new HashMap();
    var object = { stores: { stores: storesInfo } };

    Object.keys(object).forEach(function (key) {
        context.put(key, object[key]);
    });

    var template = new Template('storeLocator/storeLocatorResults');
    return template.render(context).text;
}

/**
 * Get preferred storeID
 * @param {Array} req - Request as parameter.
 * @returns {string} Store ID
 */
function getPreferredStore(req) {
    var preferrredStoreID = null;
    if (req.currentCustomer.raw.authenticated) {
        var CustomerMgr = require('dw/customer/CustomerMgr');
        var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
        preferrredStoreID = customer.profile.custom.preferredStoreID;
    } else {
        preferrredStoreID = req.session.privacyCache.get('SetPreferredDealer');
    }
    return preferrredStoreID;
}

/**
 * Sets preferred storeID
 * @param {Array} req - Request as parameter.
 * @returns {boolean} - Returns True.
 */
function setPreferredStore(req) {
    var Transaction = require('dw/system/Transaction');
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var storeid = req.querystring.storeID;

    if (req.currentCustomer.raw.authenticated) {
        // For Authenticated customer storeID saved at customer profile.
        var customer = CustomerMgr.getCustomerByCustomerNumber(req.currentCustomer.profile.customerNo);
        Transaction.wrap(function () {
            customer.profile.custom.preferredStoreID = storeid;
        });
    } else {
        // For UnAuthenticated Customer storeID saved at session level.
        req.session.privacyCache.set('SetPreferredDealer', storeid);
    }
    return true;
}

/**
 * Get FFL States. Assume US as the country always.
 * @returns {dw.util.SortedSet} - Returns collection of unique states.
 */
function getFFLStates() {
    var SystemObjectMgr = require('dw/object/SystemObjectMgr');
    var SortedSet = require('dw/util/SortedSet');

    var stores = SystemObjectMgr.querySystemObjects('Store', 'countryCode={0}', 'stateCode asc', 'US');
    var FFLStates = new SortedSet();
    var store;
    while (stores.hasNext()) {
        store = stores.next();
        if (!(!store.getStateCode() || FFLStates.contains(store.getStateCode()))) {
            FFLStates.add(store.getStateCode());
        }
    }

    return FFLStates;
}

/**
 * Get FFL Cities. Assume US as the country always.
 * @param {Array} req - Request as parameter.
 * @param {Array} FFLCitiesHeaderNav - Array of alphabets (A-Z).
 * @returns {dw.util.SortedMap} - Key being starting alphabet of city and value being collection of stores starting with that alphabet.
 */
function getFFLCities(req, FFLCitiesHeaderNav) {
    var SystemObjectMgr = require('dw/object/SystemObjectMgr');
    var SortedMap = require('dw/util/SortedMap');
    var SortedSet = require('dw/util/SortedSet');

    var stores = SystemObjectMgr.querySystemObjects('Store', 'countryCode={0} AND stateCode={1}', 'city asc', 'US', req.querystring.stateCode);

    var FFLCities = new SortedMap();
    var store;
    var city;
    var cities;
    var cityStartingAlpa;

    while (stores.hasNext()) {
        store = stores.next();
        city = store.getCity();
        if (city) {
            cityStartingAlpa = city.charAt(0).toUpperCase();
            if (FFLCities.containsKey(cityStartingAlpa)) {
                cities = new SortedSet(FFLCities.get(cityStartingAlpa));
                if (!cities.contains(city)) {
                    cities.add(city);
                    FFLCities.put(cityStartingAlpa, cities);
                }
            } else {
                cities = new SortedSet();
                cities.add(city);
                FFLCities.put(cityStartingAlpa, cities);
            }
            if (FFLCitiesHeaderNav.indexOf(city.charAt(0).toUpperCase()) > -1) {
                FFLCitiesHeaderNav[FFLCitiesHeaderNav.indexOf(city.charAt(0).toUpperCase())] = {// eslint-disable-line no-param-reassign
                    ID: city.charAt(0).toUpperCase()
                };
            }
        }
    }

    return FFLCities;
}

/**
 * Get FFL Stores. Assume US as the country always.
 * @param {Array} req - Request as parameter.
 * @returns {dw.util.SortedSet} - Returns collection of unique states.
 */
function getFFLStores(req) {
    var SystemObjectMgr = require('dw/object/SystemObjectMgr');
    var ArrayList = require('dw/util/ArrayList');

    var stores = SystemObjectMgr.querySystemObjects('Store', 'countryCode={0} AND stateCode={1} AND city={2}', 'city asc', 'US', req.querystring.stateCode, req.querystring.city);
    var FFLStores = new ArrayList();
    var store;
    while (stores.hasNext()) {
        store = stores.next();
        FFLStores.add({
            name: store.getName(),
            address1: store.getAddress1(),
            address2: store.getAddress2(),
            city: store.getCity(),
            stateCode: store.getStateCode(),
            postalCode: store.getPostalCode(),
            phone: store.getPhone(),
            latitude: store.getLatitude(),
            longitude: store.getLongitude()
        });
    }

    return FFLStores;
}

module.exports = exports = {
    createStoresResultsHtml: createStoresResultsHtml,
    getStores: getStores,
    getByPassStore: getByPassStore,
    setPreferredStore: setPreferredStore,
    getPreferredStore: getPreferredStore,
    getFFLStates: getFFLStates,
    getFFLCities: getFFLCities,
    getFFLStores: getFFLStores
};
