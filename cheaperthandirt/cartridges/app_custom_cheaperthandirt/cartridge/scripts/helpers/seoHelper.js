'use strict';

var Logger = require('dw/system/Logger');
var Calendar = require('dw/util/Calendar');
var Site = require('dw/system/Site');
var ProductMgr = require('dw/catalog/ProductMgr');

/**
 * Get the breadcrumb of each url
 * @param {int} position - specifies the position of the breadcrumb
 * @param {string} itemId - specifies the url of the breadcrumb
 * @param {string} itemName - specifies the html name of the breadcrumb
 * @returns {Object} expired - Specifies whether the product is expired or not
 */
function getSeoElement(position, itemId, itemName) {
    var element = {};
    element['@type'] = 'ListItem';
    element.position = position || '';
    var item = {};
    item['@id'] = itemId || '';
    item.name = itemName || '';
    element.item = item;
    return element;
}

/**
 * Check if the product is in new condition or not
 * @param {Object} searchProduct - specifies the product being checked
 * @returns {boolean} element - the JSON object of the breadcrumb
 */
function calculateIfExpired(searchProduct) {
    try {
        var currentCalendar = new Calendar();
        var onlineFrom = null;
        var expired = false;
        if (searchProduct.onlineFromDate != null) onlineFrom = new Calendar(searchProduct.onlineFromDate);
        var configExpForProduct = Site.getCurrent().getCustomPreferenceValue('newConditionFlagDays');
        var futureExpirationDate = null;
        if (configExpForProduct != null && onlineFrom != null) {
            var fDate = new Date(onlineFrom.getTime().setDate(onlineFrom.getTime().getDate() + configExpForProduct));
            futureExpirationDate = new Calendar(fDate);
        }
        if (onlineFrom != null && currentCalendar.compareTo(onlineFrom) > 0 && futureExpirationDate != null && currentCalendar.compareTo(futureExpirationDate) <= 0) {
            expired = false;
        } else if (onlineFrom != null && currentCalendar.compareTo(onlineFrom) > 0 && futureExpirationDate != null && currentCalendar.compareTo(futureExpirationDate) > 0) {
            expired = true;
        }
        return expired;
    } catch (e) {
        return false;
    }
}

module.exports = {

    getBreadcrumbs: function (breadcrumbs) {
        try {
            var jsonContainer = {};
            jsonContainer['@context'] = 'http://schema.org/';
            jsonContainer['@type'] = 'BreadcrumbList';
            var itemListElements = [];
            for (var i = 0; i < breadcrumbs.length; i++) {
                if (breadcrumbs[i] != null) {
                    var element = getSeoElement(i + 1, (breadcrumbs[i].url != null) ? breadcrumbs[i].url : '', breadcrumbs[i].htmlValue);
                    itemListElements.push(element);
                }
            }
            jsonContainer.itemListElement = itemListElements;
            return jsonContainer;
        } catch (e) {
            Logger.error(e.message);
            return {};
        }
    },
    getProductDetails: function (seoProduct, params) {
        try {
            var jsonContainer = {};
            jsonContainer['@context'] = 'http://schema.org/';
            jsonContainer['@type'] = 'Product';
            jsonContainer.name = seoProduct.productName || '';
            if (seoProduct.images.large && seoProduct.images.large.length > 0) {
                jsonContainer.image = (seoProduct.images.large[0].url).toString() || '';
            }
            jsonContainer.description = seoProduct.shortDescription ? seoProduct.shortDescription : seoProduct.productName;
            jsonContainer.offers = {};
            if (seoProduct.price.type === 'range') {
                jsonContainer.offers['@type'] = 'AggregateOffer';
                if (seoProduct.price.min.sales.value != null) {
                    jsonContainer.offers.priceCurrency = seoProduct.price.min.sales.currency || '';
                    jsonContainer.offers.lowPrice = seoProduct.price.min.sales.value || '';
                }
                if (seoProduct.price.max.sales.value != null) {
                    jsonContainer.offers.highPrice = seoProduct.price.max.sales.value || '';
                }
            } else {
                jsonContainer.offers['@type'] = 'Offer';
                jsonContainer.offers.priceCurrency = seoProduct.price.sales.currency || '';
                jsonContainer.offers.price = seoProduct.price.sales.value || '';
            }
            var product = ProductMgr.getProduct(params.pid);
            var availableNumber = product.getAvailabilityModel().getAvailability();
            if (availableNumber > 0) {
                jsonContainer.offers.availability = 'http://schema.org/InStock';
            } else {
                jsonContainer.offers.availability = 'http://schema.org/OutOfStock';
            }
            var isExpired = calculateIfExpired(seoProduct);
            if (!isExpired) {
                jsonContainer.offers.itemCondition = 'http://schema.org/NewCondition';
            }
            return jsonContainer;
        } catch (e) {
            Logger.error(e.message);
            return {};
        }
    },
    getVideoDetails: function () {
        try {
            var assetNames = Site.getCurrent().getCustomPreferenceValue('videoAssets');
            var seoName = Site.getCurrent().getCustomPreferenceValue('videoSeoName');
            var uploadDate = Site.getCurrent().getCustomPreferenceValue('videoUploadDate').toString();
            var ArrayList = require('dw/util/ArrayList');
            var videoSchemaArr = new ArrayList();
            var ContentMgr = require('dw/content/ContentMgr');
            for (var i = 0; i < assetNames.length; i++) {
                var content = ContentMgr.getContent(assetNames[i]);
                if (content.custom.videoContent && content.custom.videoThumbnailImageurl && content.custom.videoDescription) {
                    var assetVideoUrls = content.custom.videoContent;
                    var videoThumbUrl = content.custom.videoThumbnailImageurl;
                    var videoDescriptions = content.custom.videoDescription;
                    for (var j = 0; j < assetVideoUrls.length; j++) {
                        var jsonContainer = {};
                        jsonContainer['@context'] = 'http://schema.org/';
                        jsonContainer['@type'] = 'VideoObject';
                        jsonContainer.name = seoName || '';
                        jsonContainer.contentUrl = assetVideoUrls[j].split('|')[1];
                        jsonContainer.duration = assetVideoUrls[j].split('|')[2];
                        jsonContainer.thumbnailUrl = videoThumbUrl[j];
                        jsonContainer.description = videoDescriptions[j];
                        jsonContainer.uploadDate = uploadDate;
                        videoSchemaArr.add(jsonContainer);
                    }
                }
            }
            return videoSchemaArr;
        } catch (e) {
            Logger.error(e.message);
            return null;
        }
    }
};
