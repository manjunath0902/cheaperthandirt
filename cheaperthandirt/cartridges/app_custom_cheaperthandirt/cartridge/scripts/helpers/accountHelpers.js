'use strict';

var base = module.superModule;

var emarsysHelper = require('int_emarsys/cartridge/scripts/helpers/emarsysHelper');
var ServiceObject = require('int_emarsys/cartridge/scripts/init/emarsysServiceInit.js');
var giveUserPointsAPI = require('int_annexcloud/cartridge/scripts/loyaltyprogram/givePointsAPI');
var URLUtils = require('dw/web/URLUtils');
var Site = require('dw/system/Site');

/**
 * To get the params
 * @param {obj} registeredUser - object that contains user's email address and name information.
 * @param {currService} currService - object that contains service ID
 * @returns {string} password reset token string
 */
function getParams(registeredUser, currService) {
    var headerauth = emarsysHelper.getHeader();
    var reqBody = emarsysHelper.getRequestBody(registeredUser, currService);
    var params = {
        headerauth: headerauth,
        reqBody: reqBody,
        Service_ID: currService
    };
    return params;
}

/**
 * Send an email that would notify the user that account was created using emarsys
 * @param {obj} registeredUser - object that contains user's email address and name information.
 * @returns {string} password reset token string
 */
function sendCreateAccountEmail(registeredUser) {
    var currService = 'int_emarsys_update_contact';
    var emarsysServiceObj = ServiceObject.emarsysService(currService);
    var params = getParams(registeredUser, currService);
    params.requestType = 'PUT';
    var result = emarsysServiceObj.call(params);
    if (result.ok) {
        currService = 'int_emarsys_account_creation';
        params = getParams(registeredUser, currService);
        emarsysServiceObj = ServiceObject.emarsysService(currService);
        result = emarsysServiceObj.call(params);
    }
    giveUserPointsAPI.createUser(registeredUser.email, registeredUser.firstName, registeredUser.lastName, 'POST');
    return result;
}
/**
 * Calculating annex account update expiration
 * @param {string} LastModified - LastModified time.
 * @returns {string} return true/false
 */
function calculateIfExpired(LastModified) {
    var expired = false;
    try {
        if (LastModified) {
            var configExpForProduct = Site.getCurrent().getCustomPreferenceValue('annexProfileExpiration');
            var today = new Date();
            var onlineFrom = new Date(LastModified.getTime() + (configExpForProduct * 60 * 1000));
            if (onlineFrom > today) {
                expired = false;
            } else if (onlineFrom < today) {
                expired = true;
            }
        } else {
            expired = true;
        }
        return expired;
    } catch (e) {
        return true;
    }
}

/* eslint-disable */
/**
 * Send an email that would notify the user that account was edited
 * @param {obj} profile - object that contains user's profile information.
 */
function sendAccountEditedEmail(profile) { // 
    // This is kept intentional empty. To add this feature down the line.
}
/* eslint-enable */

/**
 * Calculating annex last visit expiration
 * @param {string} lastVisit - lastLogin time
 * @returns {string} return true/false
 */
function calculateAnnexLastVisitExpired(lastVisit) {
    try {
        var expired = false;
        var today = new Date();
        if (lastVisit > today) {
            expired = false;
        } else if (lastVisit < today) {
            expired = true;
        }
        return expired;
    } catch (e) {
        return false;
    }
}

/**
 * Gets the password reset token of a customer
 * @param {Object} customer - the customer requesting password reset token
 * @returns {string} password reset token string
 */
function getPasswordResetToken(customer) {
    var Transaction = require('dw/system/Transaction');

    var passwordResetToken;
    Transaction.wrap(function () {
        passwordResetToken = customer.profile.credentials.createResetPasswordToken();
    });
    return passwordResetToken;
}

/**
 * Sends the email with password reset instructions
 * @param {string} email - email for password reset
 * @param {Object} resettingCustomer - the customer requesting password reset
 */
function sendPasswordResetEmail(email, resettingCustomer) {
    var passwordResetToken = getPasswordResetToken(resettingCustomer);
    var url = URLUtils.https('Account-SetNewPassword', 'token', passwordResetToken);
    var resetCustomer = {
        recoveryEmail: email,
        resettingCustomer: resettingCustomer,
        url: url.toString()
    };
    var currService = 'int_emarsys_passwordReset';
    var emarsysServiceObj = ServiceObject.emarsysService(currService);
    var params = getParams(resetCustomer, currService);
    emarsysServiceObj.call(params);
}

module.exports = {
    getLoginRedirectURL: base.getLoginRedirectURL,
    sendCreateAccountEmail: sendCreateAccountEmail,
    sendPasswordResetEmail: sendPasswordResetEmail,
    sendAccountEditedEmail: sendAccountEditedEmail,
    calculateIfExpired: calculateIfExpired,
    calculateAnnexLastVisitExpired: calculateAnnexLastVisitExpired,
    getParams: getParams
};
