'use strict';

var Money = require('dw/value/Money');
var formatMoney = require('dw/util/StringUtils').formatMoney;

/**
 * Process the attribute values for an attribute that has image swatches
 *
 * @param {Object} product - the product JSON object of the set.
 * @return {string} formattedPrice - The formatted price of the full set calculated by adding the sale prices of the individual products in the set.
 */
function calculateSetPrice(product) {
    var totalPrice = null;
    var individualPrice;
    var priceCurrency;
    for (var i = 0; i < product.individualProducts.length; i++) {
        var individualProduct = product.individualProducts[i];
        if (individualProduct.price.sales !== undefined) {
            individualPrice = individualProduct.price.sales.value;
            priceCurrency = (product.price.type && product.price.type === 'range') ? (product.price.min.sales.currency) : (product.price.sales.currency);
        } else if (individualProduct.price.min !== undefined) {
            individualPrice = individualProduct.price.min.sales.value;
            priceCurrency = (product.price.type && product.price.type === 'range') ? (product.price.min.sales.currency) : (product.price.sales.currency);
        }
        totalPrice += individualPrice;
    }
    var totalSetPrice = new Money(totalPrice, priceCurrency);
    var formattedPrice = formatMoney(totalSetPrice);
    return formattedPrice;
}

exports.calculateSetPrice = calculateSetPrice;
