'use strict';


/**
 * Determines whether a product's is restricted to ship to base on the restrict group
 *
 * @param {Object} product - product object
 * @param {string} validationType - Based on the validationType do group vs item validation
 * the previous selected
 */
function validateRestrictedGroupShip(product, validationType) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var restrictRow = validationType === 'GroupValidation' ? product.restrictGroup.prodRestrictedGroup : product.restrictGroup.restrictItemGroup;
    var restrictRowCO = CustomObjectMgr.getCustomObject('RestrictedGroups', restrictRow);
    var excludeRestrictRow;
    var itrRestrictRow;
    if (restrictRowCO === null) {
        excludeRestrictRow = restrictRow !== undefined && restrictRow !== null ? restrictRow.substring(1) : '';
        restrictRowCO = CustomObjectMgr.getCustomObject('RestrictedGroups', excludeRestrictRow);
    }
    if (restrictRowCO != null) {
        if (restrictRowCO.custom.restrictShip !== null) {
        	session.custom.restrictShipMethodID = restrictRowCO.custom.restrictShip; // eslint-disable-line
        }
    }
    if (restrictRowCO === null && restrictRow !== null && restrictRow.indexOf(',') > -1) {
        var arrRestrictRow = restrictRow.split(',');
        for (var i = 0; i < arrRestrictRow.length; i++) {
            itrRestrictRow = arrRestrictRow[i];
            restrictRowCO = CustomObjectMgr.getCustomObject('RestrictedGroups', itrRestrictRow);
            if (restrictRowCO === null) {
                itrRestrictRow = arrRestrictRow[i].substring(1);
                restrictRowCO = CustomObjectMgr.getCustomObject('RestrictedGroups', itrRestrictRow);
            }
            if (restrictRowCO != null) {
                if (restrictRowCO.custom.restrictShip !== null) {
                    session.custom.restrictShipMethodID = restrictRowCO.custom.restrictShip; // eslint-disable-line
                }
            }
        }
    }
}

/**
 * Validate shipping method restrictions for all the products in the basket.
 *
 * @param {dw.order.Basket} currentBasket - current basket in the user's session
 */
function validateShippingMethodRestrictions(currentBasket) {
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var collections = require('*/cartridge/scripts/util/collections');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var shipToHomeShipment = COHelpers.getShipToHomeShipment(currentBasket);
    if (shipToHomeShipment !== null && shipToHomeShipment !== undefined) {
        var productLineItems = shipToHomeShipment.getProductLineItems();

        delete session.custom.restrictShipMethodID; // eslint-disable-line    
        collections.forEach(productLineItems, function (pli) {
            var product = ProductFactory.get({ pid: pli.product.ID });
            var restrictShip = product.restrictGroup.restrictItemShip;
            if (restrictShip !== null && restrictShip !== '') {
                session.custom.restrictShipMethodID = restrictShip; // eslint-disable-line
            }

            // validate FOID State Code
            validateRestrictedGroupShip(product, 'GroupValidation');
            validateRestrictedGroupShip(product, 'ItemValidation');
        });
    }
}

/**
 * Retrieve the state code by using the postal code.
 *
 * @param {int} zipCode - zipCode to retrieve state code
 * @return {string} stateCode - based on zipCode
 * the previous selected
 */
function getStateCode(zipCode) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var stateCode;
    var postalCode = parseInt(zipCode); // eslint-disable-line
    var stateCodeIterator = CustomObjectMgr.getAllCustomObjects('StateLookUpByZipCode');

    while (stateCodeIterator.hasNext()) {
        var stateCO = stateCodeIterator.next();
        for (var i = 0; i < stateCO.custom.zipMin.length; i++) {
            if (postalCode >= stateCO.custom.zipMin[i] && postalCode <= stateCO.custom.zipMax[i]) {
                stateCode = stateCO.custom.stateCode;
            }
        }
    }

    return stateCode;
}

/**
 * Determines whether a product's is restricted to ship to base on the zip code
 *
 * @param {Object} params - include address object
 * @return {boolean} isShippable - Based on isShippable flag
 * the previous selected
 */
function validateZipCode(params) {
    var isShippable = true;
    var product = params.product;
    var restrictZip = product.restrictGroup.restrictItemZip;
    if (restrictZip != null && restrictZip.indexOf(params.postalCode) > -1) {
        isShippable = false;
    }

    return isShippable;
}

/**
 * Determines whether a product's is restricted to ship to base on the state code
 *
 * @param {Object} params - include address object
 * @return {boolean} isShippable - Based on isShippable flag
 * the previous selected
 */
function validateStateCode(params) {
    var isShippable = true;
    var product = params.product;
    var zipState = product.restrictGroup.restrictItemState;
    var excludeFirstCharZipState = zipState !== null ? zipState.substring(1) : '';
    if ((zipState != null && zipState.indexOf(params.stateCode) > -1) || (excludeFirstCharZipState !== '' && excludeFirstCharZipState.indexOf(params.stateCode) > -1)) {
        isShippable = false;
    }
    return isShippable;
}

/**
 * Determines whether a product's is restricted to ship to base on the state code
 *
 * @param {Object} params - include address object
 * @return {boolean} isShippable - Based on isShippable flag
 * the previous selected
 */
function validateFOIDStateCode(params) {
    var isShippable = true;
    var product = params.product;
    var restrictFOID = product.restrictGroup.restrictItemFOID;
    var excludeFirstCharRestrictFOID = restrictFOID !== null ? restrictFOID.substring(1) : '';
    if ((restrictFOID != null && restrictFOID.indexOf(params.stateCode) > -1) || (excludeFirstCharRestrictFOID !== '' && excludeFirstCharRestrictFOID.indexOf(params.stateCode) > -1)) {
        isShippable = false;
    }
    return isShippable;
}


/**
 * Determines whether a product's is restricted to ship to base on the shipping method
 * @param {Object} params - include address object
 * @return {boolean} isShippable - Based on isShippable flag
 * the previous selected
 */
function validateShipping(params) {
    var isShippable = true;
    var product = params.product;
    var restrictShip = product.restrictGroup.restrictItemShip;
    var excludeFirstCharRestrictShip = restrictShip !== null ? restrictShip.substring(1) : '';
    if ((restrictShip !== null && params.legacyShippingMethodID !== '' && restrictShip.indexOf(params.legacyShippingMethodID) > -1) || (excludeFirstCharRestrictShip !== '' && params.legacyShippingMethodID !== '' && excludeFirstCharRestrictShip.indexOf(params.legacyShippingMethodID) > -1)) {
        isShippable = false;
    }
    if (restrictShip !== null && restrictShip !== '') {
        session.custom.restrictShipMethodID = restrictShip; // eslint-disable-line
    }
    return isShippable;
}


/**
 * Determines whether a product's is restricted to ship to base on the restrict group
 *
 * @param {Object} params - include address object
 * @param {string} validationType - Based on the validationType do group vs item validation
 * @return {boolean} isShippable - Based on isShippable flag
 * the previous selected
 */
function validateRestrictedGroup(params, validationType) {
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var product = params.product;
    var HashSet = require('dw/util/HashSet');
    var restrictedProducts = new HashSet();
    var FOIDProducts = new HashSet();
    var restrictRow = validationType === 'GroupValidation' ? product.restrictGroup.prodRestrictedGroup : product.restrictGroup.restrictItemGroup;
    var restrictRowCO = CustomObjectMgr.getCustomObject('RestrictedGroups', restrictRow);
    var excludeRestrictRow;
    var itrRestrictRow;
    if (restrictRowCO === null) {
        excludeRestrictRow = restrictRow !== undefined && restrictRow !== null ? restrictRow.substring(1) : '';
        restrictRowCO = CustomObjectMgr.getCustomObject('RestrictedGroups', excludeRestrictRow);
    }
    if (restrictRowCO != null) {
        // Validate the zip code
        if (restrictRowCO.custom.restrictZip != null && restrictRowCO.custom.restrictZip.indexOf(params.postalCode) > -1) {
            restrictedProducts.add(product);
        }

        // Validate the state code
        if (restrictRowCO.custom.restrictState != null && restrictRowCO.custom.restrictState.indexOf(params.stateCode) > -1) {
            restrictedProducts.add(product);
        }

        // Validate the state code
        if (restrictRowCO.custom.restrictShip != null && restrictRowCO.custom.restrictShip.indexOf(params.legacyShippingMethodID) > -1) {
            restrictedProducts.add(product);
        }
        if (restrictRowCO.custom.restrictShip !== null) {
        	session.custom.restrictShipMethodID = restrictRowCO.custom.restrictShip; // eslint-disable-line
        }
        // validate FOID State Code
        if (restrictRowCO.custom.restrictFOIDState != null && restrictRowCO.custom.restrictFOIDState.indexOf(params.stateCode) > -1) {
            FOIDProducts.add(product);
        }
    }
    if (restrictRowCO === null && restrictRow !== null && restrictRow.indexOf(',') > -1) {
        var arrRestrictRow = restrictRow.split(',');
        for (var i = 0; i < arrRestrictRow.length; i++) {
            itrRestrictRow = arrRestrictRow[i];
            restrictRowCO = CustomObjectMgr.getCustomObject('RestrictedGroups', itrRestrictRow);
            if (restrictRowCO === null) {
                itrRestrictRow = arrRestrictRow[i].substring(1);
                restrictRowCO = CustomObjectMgr.getCustomObject('RestrictedGroups', itrRestrictRow);
            }
            if (restrictRowCO != null) {
                // Validate the zip code
                if (restrictRowCO.custom.restrictZip != null && restrictRowCO.custom.restrictZip.indexOf(params.postalCode) > -1) {
                    restrictedProducts.add(product);
                }

                // Validate the state code
                if (restrictRowCO.custom.restrictState != null && restrictRowCO.custom.restrictState.indexOf(params.stateCode) > -1) {
                    restrictedProducts.add(product);
                }

                // Validate the state code
                if (restrictRowCO.custom.restrictShip != null && restrictRowCO.custom.restrictShip.indexOf(params.legacyShippingMethodID) > -1) {
                    restrictedProducts.add(product);
                }
                if (restrictRowCO.custom.restrictShip !== null) {
                    session.custom.restrictShipMethodID = restrictRowCO.custom.restrictShip; // eslint-disable-line
                }
                // validate FOID State Code
                if (restrictRowCO.custom.restrictFOIDState != null && restrictRowCO.custom.restrictFOIDState.indexOf(params.stateCode) > -1) {
                    FOIDProducts.add(product);
                }
            }
        }
    }
    var obj = {
        restrictedProducts: restrictedProducts,
        FOIDProducts: FOIDProducts
    };
    return obj;
}

/**
 * Determines whether a product is restricted to ship to base on the zipcode
 * @param {Object} params - include address object
 * @return {boolean} isShippable - Based on isShippable flag
 * the previous selected
 */
function validateShipToYou(params) {
    var isShippable = true;
    var shipToYouData = params;
    var stateCode = getStateCode(shipToYouData.postalCode);

    if (stateCode != null) {
        shipToYouData.stateCode = stateCode;
    }

    var groupResponse = validateRestrictedGroup(shipToYouData, 'GroupValidation');
    var itemResponse = validateRestrictedGroup(shipToYouData, 'ItemValidation');

    if (!validateZipCode(shipToYouData) || !validateStateCode(shipToYouData) || !groupResponse.restrictedProducts.empty || !itemResponse.restrictedProducts.empty) {
        isShippable = false;
    }

    return isShippable;
}

/**
 * Determines whether a product's is restricted to ship to base on the restrict group
 *
 * @param {Object} shippingData - include address object
 * @param {dw.order.Basket} currentBasket - Based on the validationType do group vs item validation
 * @return {Object} obj - Object
 * the previous selected
 */
function validateRestrictGroupAndItem(shippingData, currentBasket) {
    var collections = require('*/cartridge/scripts/util/collections');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var HashSet = require('dw/util/HashSet');
    var restrictedProducts = new HashSet();
    var FOIDProducts = new HashSet();
    var ContentMgr = require('dw/content/ContentMgr');
    var ContentModel = require('*/cartridge/models/content');
    var restrictAsset = ContentMgr.getContent('restrict-items-message');
    var restrictAssetHTML = new ContentModel(restrictAsset, 'components/content/contentAssetInc');
    var foidAsset = ContentMgr.getContent('foid-message');
    var foidAssetHTML = new ContentModel(foidAsset, 'components/content/contentAssetInc');
    var shipToHomeShipment = COHelpers.getShipToHomeShipment(currentBasket);
    var productLineItems = shipToHomeShipment.getProductLineItems();
    var shippingMethod = shipToHomeShipment.getShippingMethod();
    var legacyShippingMethodID;

    if (shippingMethod !== null && 'legacyShipID' in shippingMethod.custom) {
        legacyShippingMethodID = shippingMethod.custom.legacyShipID;
    } else {
        legacyShippingMethodID = '';
    }
    delete session.custom.restrictShipMethodID; // eslint-disable-line    
    collections.forEach(productLineItems, function (pli) {
        var product = ProductFactory.get({ pid: pli.product.ID });
        shippingData.product = product;  // eslint-disable-line
        shippingData.shippingMethodID = shipToHomeShipment.getShippingMethodID();  // eslint-disable-line
        shippingData.legacyShippingMethodID = legacyShippingMethodID; // eslint-disable-line
        // Validate the zip code
        if (!validateZipCode(shippingData)) {
            restrictedProducts.add(product);
        }

        // Validate the state code
        if (!validateStateCode(shippingData)) {
            restrictedProducts.add(product);
        }

        // Validate the state code
        if (!validateShipping(shippingData)) {
            restrictedProducts.add(product);
        }
        // validate FOID State Code
        if (!validateFOIDStateCode(shippingData)) {
            FOIDProducts.add(product);
        }

        // validate FOID State Code
        var groupResponse = validateRestrictedGroup(shippingData, 'GroupValidation');
        restrictedProducts.addAll(groupResponse.restrictedProducts);
        FOIDProducts.addAll(groupResponse.FOIDProducts);

        var itemResponse = validateRestrictedGroup(shippingData, 'ItemValidation');
        restrictedProducts.addAll(itemResponse.restrictedProducts);
        FOIDProducts.addAll(itemResponse.FOIDProducts);
    });
    var viewData = {
        restrictedProducts: restrictedProducts,
        FOIDProducts: FOIDProducts,
        restrictAssetHTML: restrictAssetHTML,
        foidAssetHTML: foidAssetHTML
    };
    var restrictItemsHtml = restrictedProducts.length > 0
    ? renderTemplateHelper.getRenderedHtml(viewData, 'product/components/restrictedItems')
    : null;
    var FOIDItemsHtml = FOIDProducts.length > 0
    ? renderTemplateHelper.getRenderedHtml(viewData, 'product/components/foidProductsConsent')
    : null;
    var result = {
        error: (restrictedProducts.length > 0 || FOIDProducts.length > 0),
        restrictItemsHtml: restrictItemsHtml,
        FOIDItemsHtml: FOIDItemsHtml,
        hasRestrictedItems: restrictedProducts.length > 0,
        hasFOIDItems: FOIDProducts.length > 0,
        restrictAssetHTML: restrictAssetHTML.body.markup,
        foidAssetHTML: foidAssetHTML.body.markup,
        restrictedProducts: restrictedProducts
    };
    return result;
}

module.exports = {
    validateShipToYou: validateShipToYou,
    validateRestrictGroupAndItem: validateRestrictGroupAndItem,
    validateShippingMethodRestrictions: validateShippingMethodRestrictions
};
