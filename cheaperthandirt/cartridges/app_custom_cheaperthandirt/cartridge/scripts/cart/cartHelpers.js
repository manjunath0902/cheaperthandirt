'use strict';

var base = module.superModule;

var StoreMgr = require('dw/catalog/StoreMgr');
var ProductMgr = require('dw/catalog/ProductMgr');
var ShippingMgr = require('dw/order/ShippingMgr');
var Resource = require('dw/web/Resource');
var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
var arrayHelper = require('*/cartridge/scripts/util/array');
var collections = require('*/cartridge/scripts/util/collections');
var instorePickupStoreHelper = require('*/cartridge/scripts/helpers/instorePickupStoreHelpers');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

/**
 * Determines whether a product's current instore pickup store setting are
 * the same as the previous selected
 *
 * @param {string} existingStoreId - store id currently associated with this product
 * @param {string} selectedStoreId - store id just selected
 * @return {boolean} - Whether a product's current store setting is the same as
 * the previous selected
 */
function hasSameStore(existingStoreId, selectedStoreId) {
    if (existingStoreId === null || selectedStoreId === null) {
        return false;
    }
    return existingStoreId === selectedStoreId;
}

/**
 * Loops through all Shipments and attempts to select a ShippingMethod, where absent
 * @param {dw.catalog.Product} product - Product object
 * @param {string} productId - Product ID to match
 * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - Collection of the Cart's
 *     product line items
 * @param {string[]} childProducts - the products' sub-products
 * @param {SelectedOption[]} options - product options
 * @param {string} storeId - store id
 * @return {dw.order.ProductLineItem} - Filtered the product line item matching productId
 *  and has the same bundled items or options and the same instore pickup store selection
 */
function getExistingProductLineItemInCartWithTheSameStore(
    product,
    productId,
    productLineItems,
    childProducts,
    options,
    storeId) {
    var existingProductLineItem = null;
    var matchingProducts = base.getExistingProductLineItemsInCart(
        product,
        productId,
        productLineItems,
        childProducts,
        options);
    if (matchingProducts.length) {
        existingProductLineItem = arrayHelper.find(matchingProducts, function (matchingProduct) {
            return hasSameStore(matchingProduct.custom.fromStoreId, storeId);
        });
    }
    return existingProductLineItem;
}

/**
 * Loops through all Shipments and attempts to select a ShippingMethod, where absent
 * @param {dw.catalog.Product} product - Product object
 * @param {string} productId - Product ID to match
 * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - Collection of the Cart's
 *     product line items
 * @param {string[]} childProducts - the products' sub-products
 * @param {SelectedOption[]} options - product options
 * @param {string} giftCardEmail - Recipient Email
 * @return {dw.order.ProductLineItem} - Filtered the product line item matching productId
 *  and has the same bundled items or options and the same instore pickup store selection
 */
function getExistingGiftCardLineItem(
    product,
    productId,
    productLineItems,
    childProducts,
    options,
    giftCardEmail) {
    var existingProductLineItem = null;
    var matchingProducts = base.getExistingProductLineItemsInCart(
        product,
        productId,
        productLineItems,
        childProducts,
        options);
    if (matchingProducts.length) {
        existingProductLineItem = arrayHelper.find(matchingProducts, function (matchingProduct) {
            return matchingProduct.custom.giftCardRecipientEmail === giftCardEmail;
        });
    }
    if (existingProductLineItem === undefined) {
        existingProductLineItem = null;
    }
    return existingProductLineItem;
}

/**
 * Update Store Details On Existing ProductLineItems
 * @param {dw.util.Collection<dw.order.ProductLineItem>} productLineItems - Collection of the Cart's
 *     product line items
 * @param {string} storeId - store id
 *  and has the same bundled items or options and the same instore pickup store selection
 */
function updateStoreDetailsOnExistingProductLineItems(
    productLineItems,
    storeId) {
    if (storeId) {
        collections.forEach(productLineItems, function (lineItem) {
            if (lineItem.custom.fromStoreId) {
                lineItem.custom.fromStoreId = storeId; // eslint-disable-line no-param-reassign
            }
        });
    }
}

/**
 * Get the existing in store pickup shipment in cart by storeId
 * @param {dw.order.Basket} basket - the target Basket object
 * @param {string} storeId - store id
 * @return {dw.order.Shipment} returns Shipment object if the existing shipment has the same storeId
 */
function getInStorePickupShipmentInCartByStoreId(basket, storeId) {
    var existingShipment = null;
    if (basket && storeId) {
        var shipments = basket.getShipments();
        if (shipments.length) {
            existingShipment = arrayHelper.find(shipments, function (shipment) {
                return shipment.custom.shipmentType === 'instore';
            });
        }
    }
    return existingShipment;
}

/**
 * create a new instore pick shipment if the store shipment
 * is not exist in the basket for adding product line item
 * @param {dw.order.Basket} basket - the target Basket object
 * @param {string} storeId - store id
 * @param {Object} req - The local instance of the request object
 * @return {dw.order.Shipment} returns Shipment object
 */
function createInStorePickupShipmentForLineItem(basket, storeId, req) {
    var shipment = null;
    if (basket && storeId) {
        // check if the instore pickup shipment is already exist.
        shipment = getInStorePickupShipmentInCartByStoreId(basket, storeId);
        // create a new shipment to put this product line item in
        if (!shipment) {
            shipment = basket.createShipment('ShipToStore');
        }

        shipment.custom.fromStoreId = storeId;
        shipment.custom.shipmentType = 'instore';
        req.session.privacyCache.set(shipment.UUID, 'valid');

        // Find in-store method in shipping methods.
        var shippingMethods =
            ShippingMgr.getShipmentShippingModel(shipment).getApplicableShippingMethods();
        var shippingMethod = collections.find(shippingMethods, function (method) {
            return method.custom.storePickupEnabled;
        });
        var store = StoreMgr.getStore(storeId);
        var storeAddress = {
            address: {
                firstName: store.name,
                lastName: store.name,
                address1: store.address1,
                address2: store.address2,
                city: store.city,
                stateCode: store.stateCode,
                postalCode: store.postalCode,
                countryCode: store.countryCode.value,
                phone: store.phone,
                email: store.email
            },
            shippingMethod: shippingMethod.ID
        };
        COHelpers.copyShippingAddressToShipment(storeAddress, shipment);
    }
    return shipment;
}


/**
 * create a new instore pick shipment if the store shipment
 * is not exist in the basket for adding product line item
 * @param {dw.order.Basket} basket - the target Basket object
 * @param {string} storeId - store id
 * @param {Object} req - The local instance of the request object
 * @return {dw.order.Shipment} returns Shipment object
 */
function updateStoreDetailsOnShipment(basket, storeId, req) {
    var shipment = null;
    if (basket && storeId) {
        // check if the instore pickup shipment is already exist.
        shipment = getInStorePickupShipmentInCartByStoreId(basket, storeId);
        // create a new shipment to put this product line item in
        if (!shipment) {
            shipment = basket.createShipment('ShipToStore');
        }

        shipment.custom.fromStoreId = storeId;
        shipment.custom.shipmentType = 'instore';
        req.session.privacyCache.set(shipment.UUID, 'valid');

        // Find in-store method in shipping methods.
        var shippingMethods =
            ShippingMgr.getShipmentShippingModel(shipment).getApplicableShippingMethods();
        var shippingMethod = collections.find(shippingMethods, function (method) {
            return method.custom.storePickupEnabled;
        });
        var store = StoreMgr.getStore(storeId);
        var storeAddress = {
            address: {
                firstName: store.name,
                lastName: store.name,
                address1: store.address1,
                address2: store.address2,
                city: store.city,
                stateCode: store.stateCode,
                postalCode: store.postalCode,
                countryCode: store.countryCode.value,
                phone: store.phone,
                email: store.email
            },
            shippingMethod: shippingMethod.ID
        };
        COHelpers.copyShippingAddressToShipment(storeAddress, shipment);
    }
    return shipment;
}

/**
 * Adds a product to the cart. If the product is already in the cart it increases the quantity of
 * that product.
 * @param {dw.order.Basket} currentBasket - Current users's basket
 * @param {string} productId - the productId of the product being added to the cart
 * @param {number} quantity - the number of products to the cart
 * @param {string[]} childProducts - the products' sub-products
 * @param {SelectedOption[]} options - product options
 * @param {string} storeId - store id
 * @param {Object} req - The local instance of the request object
 * @return {Object} returns an error object
 */
function addProductToCart(currentBasket, productId, quantity, childProducts, options, storeId, req) {
    var availableToSell;
    var defaultShipment = currentBasket.defaultShipment;
    var perpetual;
    var product = ProductMgr.getProduct(productId);
    var productInCart;
    var productLineItems = currentBasket.productLineItems;
    var productQuantityInCart;
    var quantityToSet;
    var optionModel = productHelper.getCurrentOptionModel(product.optionModel, options);
    var result = {
        error: false,
        message: Resource.msg('text.alert.addedtobasket', 'product', null)
    };
    var Transaction = require('dw/system/Transaction');

    var totalQtyRequested = 0;
    var canBeAdded = false;

    if (product.bundle) {
        canBeAdded = base.checkBundledProductCanBeAdded(childProducts, productLineItems, quantity);
    } else {
        totalQtyRequested = quantity + base.getQtyAlreadyInCart(productId, productLineItems);
        perpetual = product.availabilityModel.inventoryRecord.perpetual;
        canBeAdded =
            (perpetual ||
                totalQtyRequested <= product.availabilityModel.inventoryRecord.ATS.value);
    }

    if (!canBeAdded) {
        result.error = true;
        result.message = Resource.msg(
            'error.alert.selected.quantity.cannot.be.added.for',
            'product',
            null
        );
        return result;
    }
    // Get the existing product line item from the basket if the new product item
    // has the same bundled items or options and the same instore pickup store selection
    productInCart = getExistingProductLineItemInCartWithTheSameStore(
        product, productId, productLineItems, childProducts, options, storeId);
    if (productInCart) {
        productQuantityInCart = productInCart.quantity.value;
        quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
        availableToSell = productInCart.product.availabilityModel.inventoryRecord.ATS.value;

        if (availableToSell >= quantityToSet || perpetual) {
            productInCart.setQuantityValue(quantityToSet);
            result.uuid = productInCart.UUID;
            updateStoreDetailsOnExistingProductLineItems(
                productLineItems, storeId);
            updateStoreDetailsOnShipment(currentBasket, storeId, req);
        } else {
            result.error = true;
            result.message = availableToSell === productQuantityInCart ?
                Resource.msg('error.alert.max.quantity.in.cart', 'product', null) :
                Resource.msg('error.alert.selected.quantity.cannot.be.added', 'product', null);
        }
    } else {
        var productLineItem;
        // Create a new instore pickup shipment as default shipment for product line item
        // if the shipment if not exist in the basket
        var inStoreShipment = createInStorePickupShipmentForLineItem(currentBasket, storeId, req);
        var shipToHomeShipment = currentBasket.getShipment('ShipToHome');
        var shipment = inStoreShipment || shipToHomeShipment || defaultShipment;

        if (shipment.shippingMethod && shipment.shippingMethod.custom.storePickupEnabled && !storeId) {
            shipment = currentBasket.createShipment('ShipToHome');
        }
        updateStoreDetailsOnExistingProductLineItems(
                productLineItems, storeId);
        productInCart = getExistingProductLineItemInCartWithTheSameStore(
            product, productId, productLineItems, childProducts, options, storeId);
        if (productInCart) {
            productLineItem = productInCart;
            productQuantityInCart = productInCart.quantity.value;
            quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
            availableToSell = productInCart.product.availabilityModel.inventoryRecord.ATS.value;

            if (availableToSell >= quantityToSet || perpetual) {
                productInCart.setQuantityValue(quantityToSet);
                result.uuid = productInCart.UUID;
            } else {
                result.error = true;
                result.message = availableToSell === productQuantityInCart ?
                    Resource.msg('error.alert.max.quantity.in.cart', 'product', null) :
                    Resource.msg('error.alert.selected.quantity.cannot.be.added', 'product', null);
            }
        } else {
            if ('giftcardType' in product.custom && product.custom.giftcardType.toLowerCase() === 'electronic') { // eslint-disable-line
                var form = req.form;
                var giftcardEmail = form.giftcardEmail;
                var recipientName = form.recipientName;
                var senderName = form.senderName;
                var message = form.message;

                // get existing Gift Card LineItem
                var existingGCLineItem = getExistingGiftCardLineItem(
                        product, productId, productLineItems, childProducts, options, giftcardEmail);
                if (existingGCLineItem === null) {
                    var dummyProduct = ProductMgr.getProduct('01234567890');
                    if (dummyProduct && dummyProduct.isOnline() && dummyProduct.isAssignedToSiteCatalog()) {
                        productLineItem = base.addLineItem(
                                currentBasket,
                                dummyProduct,
                                quantity,
                                childProducts,
                                optionModel,
                                shipment
                            );
                        productLineItem.replaceProduct(product);
                    } else {
                        productLineItem = base.addLineItem(
                                currentBasket,
                                product,
                                quantity,
                                childProducts,
                                optionModel,
                                shipment
                            );
                    }
                } else {
                    productLineItem = existingGCLineItem;
                    productQuantityInCart = productLineItem.quantity.value;
                    quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
                    availableToSell = productLineItem.product.availabilityModel.inventoryRecord.ATS.value;

                    if (availableToSell >= quantityToSet || perpetual) {
                        productLineItem.setQuantityValue(quantityToSet);
                        result.uuid = productLineItem.UUID;
                    } else {
                        result.error = true;
                        result.message = availableToSell === productQuantityInCart ?
                            Resource.msg('error.alert.max.quantity.in.cart', 'product', null) :
                            Resource.msg('error.alert.selected.quantity.cannot.be.added', 'product', null);
                    }
                }
                // update the gift card attributes to the productlineitem item
                productLineItem.custom.giftCardRecipientEmail = giftcardEmail;
                productLineItem.custom.giftCardRecipient = recipientName !== null && recipientName !== undefined ? recipientName : '';
                productLineItem.custom.giftCardSender = senderName !== null && senderName !== undefined ? senderName : '';
                productLineItem.custom.giftCardMessage = message;
                COHelpers.copyGCAddressToShipment(shipment);
            } else {
                productInCart = base.getExistingProductLineItemsInCart(product, productId, productLineItems, childProducts, options)[0];
                if (productInCart) {
                    productLineItem = productInCart;
                    productQuantityInCart = productInCart.quantity.value;
                    quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
                    availableToSell = productInCart.product.availabilityModel.inventoryRecord.ATS.value;

                    if (availableToSell >= quantityToSet || perpetual) {
                        productInCart.setQuantityValue(quantityToSet);
                        result.uuid = productInCart.UUID;
                    } else {
                        result.error = true;
                        result.message = availableToSell === productQuantityInCart ?
                            Resource.msg('error.alert.max.quantity.in.cart', 'product', null) :
                            Resource.msg('error.alert.selected.quantity.cannot.be.added', 'product', null);
                    }
                } else {
                    productLineItem = base.addLineItem(
                            currentBasket,
                            product,
                            quantity,
                            childProducts,
                            optionModel,
                            shipment
                        );
                }
            }
        }

        // Once the new product line item is added, set the instore pickup fromStoreId for the item
        if (productLineItem.product.custom.availableForInStorePickup) {
            if (storeId) {
                instorePickupStoreHelper.setStoreInProductLineItem(storeId, productLineItem);
            }
        }
        result.uuid = productLineItem.UUID;
    }

    Transaction.wrap(function () {
        COHelpers.ensureNoEmptyShipments(req);
    });

    return result;
}

/**
 * Adds a product to the cart. If the product is already in the cart it increases the quantity of
 * that product.
 * @param {dw.order.Basket} currentBasket - Current users's basket
 * @param {string} productId - the productId of the product being added to the cart
 * @param {number} quantity - the number of products to the cart
 * @param {string[]} childProducts - the products' sub-products
 * @param {SelectedOption[]} options - product options
 * @param {string} storeId - store id
 * @param {Object} req - The local instance of the request object
 * @return {Object} returns an error object
 */
function addProductToCartOnNet(currentBasket, productId, quantity, childProducts, options, storeId, req) {
    var defaultShipment = currentBasket.defaultShipment;
    var product = ProductMgr.getProduct(productId);
    var productInCart;
    var productLineItems = currentBasket.productLineItems;
    var productQuantityInCart;
    var quantityToSet;
    var optionModel = productHelper.getCurrentOptionModel(product.optionModel, options);
    var result = {
        error: false,
        message: Resource.msg('text.alert.addedtobasket', 'product', null)
    };
    var Transaction = require('dw/system/Transaction');

    // Get the existing product line item from the basket if the new product item
    // has the same bundled items or options and the same instore pickup store selection
    productInCart = getExistingProductLineItemInCartWithTheSameStore(
        product, productId, productLineItems, childProducts, options, storeId);
    if (productInCart) {
        productQuantityInCart = productInCart.quantity.value;
        quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
        productInCart.setQuantityValue(quantityToSet);
        result.uuid = productInCart.UUID;
        updateStoreDetailsOnExistingProductLineItems(
            productLineItems, storeId);
        updateStoreDetailsOnShipment(currentBasket, storeId, req);
    } else {
        var productLineItem;
        // Create a new instore pickup shipment as default shipment for product line item
        // if the shipment if not exist in the basket
        var inStoreShipment = createInStorePickupShipmentForLineItem(currentBasket, storeId, req);
        var shipToHomeShipment = currentBasket.getShipment('ShipToHome');
        var shipment = inStoreShipment || shipToHomeShipment || defaultShipment;

        if (shipment.shippingMethod && shipment.shippingMethod.custom.storePickupEnabled && !storeId) {
            shipment = currentBasket.createShipment('ShipToHome');
        }
        updateStoreDetailsOnExistingProductLineItems(
                productLineItems, storeId);
        productInCart = getExistingProductLineItemInCartWithTheSameStore(
            product, productId, productLineItems, childProducts, options, storeId);
        if (productInCart) {
            productLineItem = productInCart;
            productQuantityInCart = productInCart.quantity.value;
            quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
            productInCart.setQuantityValue(quantityToSet);
            result.uuid = productInCart.UUID;
        } else {
            if ('giftcardType' in product.custom && product.custom.giftcardType.toLowerCase() === 'electronic') { // eslint-disable-line
                var form = req.querystring;
                var giftcardEmail = form.giftcardEmail;
                var recipientName = form.recipientName;
                var senderName = form.senderName;
                var message = form.message;

                // get existing Gift Card LineItem
                var existingGCLineItem = getExistingGiftCardLineItem(
                        product, productId, productLineItems, childProducts, options, giftcardEmail);
                if (existingGCLineItem === null) {
                    var dummyProduct = ProductMgr.getProduct('01234567890');
                    if (dummyProduct && dummyProduct.isOnline() && dummyProduct.isAssignedToSiteCatalog()) {
                        productLineItem = base.addLineItem(
                                currentBasket,
                                dummyProduct,
                                quantity,
                                childProducts,
                                optionModel,
                                shipment
                            );
                        productLineItem.replaceProduct(product);
                    } else {
                        productLineItem = base.addLineItem(
                                currentBasket,
                                product,
                                quantity,
                                childProducts,
                                optionModel,
                                shipment
                            );
                    }
                } else {
                    productLineItem = existingGCLineItem;
                    productQuantityInCart = productLineItem.quantity.value;
                    quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
                    productLineItem.setQuantityValue(quantityToSet);
                    result.uuid = productLineItem.UUID;
                }
                // update the gift card attributes to the productlineitem item
                productLineItem.custom.giftCardRecipientEmail = giftcardEmail;
                productLineItem.custom.giftCardRecipient = recipientName !== null && recipientName !== undefined ? recipientName : '';
                productLineItem.custom.giftCardSender = senderName !== null && senderName !== undefined ? senderName : '';
                productLineItem.custom.giftCardMessage = message;
                COHelpers.copyGCAddressToShipment(shipment);
            } else {
                productInCart = base.getExistingProductLineItemsInCart(product, productId, productLineItems, childProducts, options)[0];
                if (productInCart) {
                    productLineItem = productInCart;
                    productQuantityInCart = productInCart.quantity.value;
                    quantityToSet = quantity ? quantity + productQuantityInCart : productQuantityInCart + 1;
                    productInCart.setQuantityValue(quantityToSet);
                    result.uuid = productInCart.UUID;
                } else {
                    productLineItem = base.addLineItem(
                            currentBasket,
                            product,
                            quantity,
                            childProducts,
                            optionModel,
                            shipment
                        );
                }
            }
        }

        // Once the new product line item is added, set the instore pickup fromStoreId for the item
        if (productLineItem.product.custom.availableForInStorePickup) {
            if (storeId) {
                instorePickupStoreHelper.setStoreInProductLineItem(storeId, productLineItem);
            }
        }
        result.uuid = productLineItem.UUID;
    }

    Transaction.wrap(function () {
        COHelpers.ensureNoEmptyShipments(req);
    });

    return result;
}

module.exports = {
    addLineItem: base.addLineItem,
    addProductToCart: addProductToCart,
    addProductToCartOnNet: addProductToCartOnNet,
    checkBundledProductCanBeAdded: base.checkBundledProductCanBeAdded,
    ensureAllShipmentsHaveMethods: base.ensureAllShipmentsHaveMethods,
    getQtyAlreadyInCart: base.getQtyAlreadyInCart,
    getNewBonusDiscountLineItem: base.getNewBonusDiscountLineItem,
    getExistingProductLineItemInCart: base.getExistingProductLineItemInCart,
    getExistingProductLineItemsInCart: base.getExistingProductLineItemsInCart,
    getMatchingProducts: base.getMatchingProducts,
    allBundleItemsSame: base.allBundleItemsSame,
    hasSameOptions: base.hasSameOptions,
    BONUS_PRODUCTS_PAGE_SIZE: base.BONUS_PRODUCTS_PAGE_SIZE
};
