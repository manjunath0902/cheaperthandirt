/* eslint-disable */
'use strict';

var ShippingMgr = require('dw/order/ShippingMgr');
var Status = require('dw/system/Status');

/**
 * @module calculate.js
 *
 */

exports.calculateShipping = function (basket) {
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var shipToHomeShipment = COHelpers.getShipToHomeShipment(basket);
    var Transaction = require('dw/system/Transaction');
    var shipment;
    var hasUPSRates = false;
    if (shipToHomeShipment) {
        shipment = shipToHomeShipment;
    } else {
        shipment = basket.defaultShipment;
    }
    
    if (shipment) {
        var upsRates = 'calculatedShipRate' in shipment.custom && shipment.custom.calculatedShipRate ? shipment.custom.calculatedShipRate : null;
        var upsCodeForShippingMethod = shipment.shippingMethod && shipment.shippingMethod.custom ? shipment.shippingMethod.custom.service_code_ups : null;
        if (upsRates != null && upsRates !== '' && upsRates !== undefined && upsCodeForShippingMethod !== null) {
            upsRates = JSON.parse(upsRates);
            Transaction.wrap(function () {
                if (upsRates !== null && upsRates[upsCodeForShippingMethod] !== null && upsRates[upsCodeForShippingMethod] !== undefined) {
                    var price = parseFloat(upsRates[upsCodeForShippingMethod].split('|')[1]);
                    if (price !== null) {
                        hasUPSRates = true;
                        shipment.standardShippingLineItem.setPriceValue(price);
                    }
                }
            });
        }
        if (!hasUPSRates) {
            ShippingMgr.applyShippingCost(basket);
        }
    }
    return new Status(Status.OK);
}
