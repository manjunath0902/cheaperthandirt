'use strict';

/**
 * The onRequest hook is called with every top-level request in a site. This happens both for requests to cached and non-cached pages.
 * For performance reasons the hook function should be kept short.
 *
 * @module  request/OnRequest
 */

var URLUtils = require('dw/web/URLUtils');
var Site = require('dw/system/Site');
var Logger = require('dw/system/Logger');

/**
 * This is to find if configured Regex patterns are found in the query string and replacing it.
 * @param {string} queryStr - Current URL Value
 * @param {array} listOfPattern - Regex pattern from sitepreference.
 * @returns {boolean} returns Boolean value if doesn't matches regex pattern.
 */
function checkForXSSPattern(queryStr, listOfPattern) {
    var finalqueryString = '';
    if (queryStr !== undefined && listOfPattern !== undefined) {
        var regexp;
        for (var i = 0; i < listOfPattern.length; i++) {
            regexp = new RegExp(listOfPattern[i]);
            if (queryStr.toLowerCase().search(regexp) !== -1) {
                finalqueryString = queryStr.toLowerCase().replace(regexp, '');
                Logger.debug('\n Query String has XSS Cross site scripting regex: ' + regexp);
                return finalqueryString;
            }
        }
    }
    return finalqueryString;
}

/**
 * The onRequest hook function.
 */
exports.onRequest = function () {
	// logic to handle XSS
    var sitePrefs = Site.getCurrent().getPreferences();
    var enableXSS = 'EnableXSSInputValidation' in sitePrefs.custom ? sitePrefs.custom.EnableXSSInputValidation : false;
    if (enableXSS) {
        var CheckCrossSiteScript = false;
        var currentRequest = request; // eslint-disable-line
        var queryString = currentRequest.httpQueryString;
        var finalqueryString = '';

		// Getting the list of XSS Cross Site Scripting strings from Site Preferences for verification
        var listOfXSSStrings = JSON.parse(Site.current.getCustomPreferenceValue('listOfXSSStrings'));

		// List of XSS Patterns
        var xssListOfPatterns = listOfXSSStrings.XSSListOfPatterns;

        try {
            if (queryString !== undefined) {
                Logger.debug('\n Query String for XSS Cross site scripting check: before decoding ' + queryString);

                queryString = decodeURIComponent(queryString);
                Logger.debug('\n Query String for XSS Cross site scripting check: after decoding ' + queryString);
                var exclusionXSS = [];
                exclusionXSS = sitePrefs.custom.ExclusionXSS;
                for (var i = 0; i < exclusionXSS.length; i++) {
                    var exlPipeline = exclusionXSS[i];
                    if (request.httpURL.toString().indexOf(exlPipeline) > -1) { // eslint-disable-line
                        enableXSS = false;
                        break;
                    }
                }
                // Step 1: Check if Regex patterns are found in the query string and replace the parameters from the query string with empty string.
                if (enableXSS && xssListOfPatterns !== undefined) {
                    finalqueryString = checkForXSSPattern(queryString, xssListOfPatterns);
                }
                // Step 2: Check if query string is empty or not.If the query string redirecting to 404 page.
                if (enableXSS && finalqueryString !== undefined && finalqueryString !== '') {
                    CheckCrossSiteScript = true;
                }
            }
        } catch (e) {
            Logger.error('Error in executing CheckCrossSiteScript.ds ' + e);
            CheckCrossSiteScript = true;
        }

        if (CheckCrossSiteScript) {
            response.redirect(URLUtils.url('Home-ErrorNotFound')); // eslint-disable-line
            return;
        }
    }
    return;
};
