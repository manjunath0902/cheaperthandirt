'use strict';

var server = require('server');
var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var Categories = require('*/cartridge/models/categories');
var ArrayList = require('dw/util/ArrayList');
var ProductSearchModel = require('dw/catalog/ProductSearchModel');
var ProductSearch = require('*/cartridge/models/search/productSearch');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var URLUtils = require('dw/web/URLUtils');
var assets = require('*/cartridge/scripts/assets');
var cache = require('*/cartridge/scripts/middleware/cache');
var Site = require('dw/system/Site');

server.extend(module.superModule);

server.replace('Show', cache.applyShortPromotionSensitiveCache, function (req, res, next) {
    var seo = require('*/cartridge/models/seo');
    var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
    var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
    var selectedCategory = CatalogMgr.getCategory(req.querystring.cgid);
    var categoryTemplate = '';
    var productSearch;
    var isAjax = Object.hasOwnProperty.call(req.httpHeaders, 'x-requested-with') &&
        req.httpHeaders['x-requested-with'] === 'XMLHttpRequest';
    var isBrandCategory = false;
    if (selectedCategory !== null && selectedCategory !== undefined) {
        isBrandCategory = selectedCategory.parent !== null && selectedCategory.parent.parent !== null && selectedCategory.parent.parent.ID === '90000';
    }
    var brandTemplate = isBrandCategory || Object.prototype.hasOwnProperty.call(req.querystring, 'brand') ? 'search/searchResultsBrand' : 'search/searchResults';
    var resultsTemplate = isAjax ? 'search/searchResultsNoDecorator' : brandTemplate;
    var apiProductSearch = new ProductSearchModel();
    var maxSlots = 4;
    var reportingURLs;
    var searchRedirect = req.querystring.q ?
        apiProductSearch.getSearchRedirect(req.querystring.q) :
        null;

    if (searchRedirect) {
        res.redirect(searchRedirect.getLocation());
        return next();
    }

    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
    apiProductSearch.search();

    categoryTemplate = searchHelper.getCategoryTemplate(apiProductSearch);
    productSearch = new ProductSearch(
        apiProductSearch,
        req.querystring,
        req.querystring.srule,
        CatalogMgr.getSortingOptions(),
        CatalogMgr.getSiteCatalog().getRoot()
    );

    pageMetaHelper.setPageMetaTags(req.pageMetaData, productSearch);
    if (isBrandCategory) {
        var brandMetaTag = {};
        brandMetaTag.pageTitle = selectedCategory.pageTitle ? selectedCategory.pageTitle : selectedCategory.displayName;
        brandMetaTag.pageTitle = brandMetaTag.pageTitle + ' | Cheaper Than Dirt'; // eslint-disable-line
        brandMetaTag.pageDescription = selectedCategory.pageDescription ? selectedCategory.pageDescription : selectedCategory.description;
        pageMetaHelper.setPageMetaTags(req.pageMetaData, brandMetaTag);
        pageMetaHelper.setPageMetaData(req.pageMetaData, brandMetaTag);
    }

    var refineurl = URLUtils.url('Search-Refinebar');
    var sortRefineurl = URLUtils.abs('Search-Show');
    var whitelistedParams = ['q', 'cgid', 'pmin', 'pmax', 'srule'];
    var isRefinedSearch = false;
    Object.keys(req.querystring).forEach(function (element) {
        if (isBrandCategory && element === 'cgid') {
            refineurl.append(element, selectedCategory.ID);
            sortRefineurl.append(element, selectedCategory.ID);
        } else if (whitelistedParams.indexOf(element) > -1) {
            refineurl.append(element, req.querystring[element]);
            sortRefineurl.append(element, req.querystring[element]);
        }

        if (['pmin', 'pmax'].indexOf(element) > -1) {
            isRefinedSearch = true;
        }

        if (element === 'preferences') {
            var i = 1;
            isRefinedSearch = true;
            Object.keys(req.querystring[element]).forEach(function (preference) {
                refineurl.append('prefn' + i, preference);
                refineurl.append('prefv' + i, req.querystring[element][preference]);
                sortRefineurl.append('prefn' + i, preference);
                sortRefineurl.append('prefv' + i, req.querystring[element][preference]);
                i++;
            });
        }
    });

    if (productSearch.searchKeywords !== null && !isRefinedSearch) {
        reportingURLs = reportingUrlsHelper.getProductSearchReportingURLs(productSearch);
    }
    // Set isBrandPLP to false for a normal PLP or search page. Set it to true only if it is a brand template
    // append brand param to the refinement url to filter the display of the brands
    if (isBrandCategory || Object.prototype.hasOwnProperty.call(req.querystring, 'brand')) {
        refineurl.append('removebrand', req.querystring.brand);
        refineurl.append('isBrandPLP', isBrandCategory);
        refineurl.append('brand', req.querystring.brand);
    } else {
        refineurl.append('isBrandPLP', false);
    }

    if (selectedCategory) {
        var catBreadcrumbs;
        var seoCatBreadcrumbs;
        if (isBrandCategory || Object.prototype.hasOwnProperty.call(req.querystring, 'brand')) {
            if (!isBrandCategory) {
                refineurl.append('prefn1', 'brand');
                refineurl.append('prefv1', selectedCategory.displayName.replace(/\s/g, ''));
                sortRefineurl.append('prefn1', 'brand');
                sortRefineurl.append('prefv1', selectedCategory.displayName.replace(/\s/g, ''));
            }
            catBreadcrumbs = searchHelper.getBrandBreadcrumbs(selectedCategory, req.querystring, [], false);
            seoCatBreadcrumbs = searchHelper.getBrandBreadcrumbs(selectedCategory, req.querystring, [], true);
        } else {
            catBreadcrumbs = searchHelper.getCatBreadcrumbs(selectedCategory, [], false).reverse();
            seoCatBreadcrumbs = searchHelper.getCatBreadcrumbs(selectedCategory, [], true).reverse();
        }
        var seoBreadCrumbs = seo.getSeoBreadCrumbs(seoCatBreadcrumbs);
        var singleCategoryIterator = new ArrayList(selectedCategory);
        var currentParentCategory = selectedCategory.parent;
        var currentCatObject = new Categories(singleCategoryIterator, true);
        var levelOneCategory = searchHelper.getLevelOneCategory(selectedCategory);

        res.setViewData({
            currentCatObject: currentCatObject,
            levelOneCategory: levelOneCategory,
            currentParentCategory: currentParentCategory,
            breadcrumbs: catBreadcrumbs,
            seoBreadCrumbs: seoBreadCrumbs,
            loadSixthAsset: true
        });
    }
    var PrEnable = Site.getCurrent().getCustomPreferenceValue('PR_Online_Status');
    if (PrEnable !== null && PrEnable) {
        assets.addJs('https://ui.powerreviews.com/stable/4.0/ui.js');
        assets.addJs(URLUtils.https('PowerReviews-Config.js').toString());
        assets.addJs('/js/powerreviews.js');
        assets.addCss('/css/powerreviews.css');
    }

    if (
        productSearch.isCategorySearch &&
        !productSearch.isRefinedCategorySearch &&
        categoryTemplate
    ) {
        pageMetaHelper.setPageMetaData(req.pageMetaData, productSearch.category);

        if (isAjax) {
            res.render(resultsTemplate, {
                productSearch: productSearch,
                maxSlots: maxSlots,
                reportingURLs: reportingURLs,
                refineurl: refineurl,
                sortRefineurl: sortRefineurl
            });
        } else {
            res.render(categoryTemplate, {
                productSearch: productSearch,
                maxSlots: maxSlots,
                category: apiProductSearch.category,
                reportingURLs: reportingURLs,
                refineurl: refineurl,
                sortRefineurl: sortRefineurl
            });
        }
    } else if (
            productSearch.isCategorySearch &&
            !productSearch.isRefinedCategorySearch
    ) {
        res.render(resultsTemplate, {
            productSearch: productSearch,
            maxSlots: maxSlots,
            category: apiProductSearch.category,
            reportingURLs: reportingURLs,
            refineurl: refineurl,
            sortRefineurl: sortRefineurl
        });
    } else {
        res.render(resultsTemplate, {
            productSearch: productSearch,
            brandCategory: selectedCategory,
            maxSlots: maxSlots,
            reportingURLs: reportingURLs,
            refineurl: refineurl,
            sortRefineurl: sortRefineurl
        });
    }

    return next();
}, pageMetaData.computedPageMetaData);

server.append('UpdateGrid', function (req, res, next) {
    res.setViewData({
        loadSixthAsset: false
    });
    next();
});

server.get('Custom', cache.applyShortPromotionSensitiveCache, function (req, res, next) {
    var seo = require('*/cartridge/models/seo');
    var Resource = require('dw/web/Resource');
    var category = CatalogMgr.getCategory(req.querystring.cgid);
    var apiProductSearch = new ProductSearchModel();
    var brandPagePref;
    var productSearch;
    var catBreadcrumbs = searchHelper.getCatBreadcrumbs(category, [], true).reverse();
    var seoCatBreadcrumbs = searchHelper.getCatBreadcrumbs(category, [], true).reverse();
    var alphaNums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    if (category.custom.shopbyBrand != null) {
        catBreadcrumbs.push({
            htmlValue: Resource.msgf('heading.shop.brand', 'search', null, req.querystring.searchBy, category.displayName),
            url: URLUtils.url('Search-Custom', 'cgid', category.ID, 'searchBy', 'Brand').toString()
        });
        seoCatBreadcrumbs.push({
            htmlValue: Resource.msgf('heading.shop.brand', 'search', null, req.querystring.searchBy),
            url: URLUtils.url('Search-Custom', 'cgid', category.ID, 'searchBy', 'Brand').toString()
        });
    } else if (category.custom.shopbyAttribute != null) {
        catBreadcrumbs.push({
            htmlValue: Resource.msgf('heading.shop.brand', 'search', null, req.querystring.searchBy),
            url: URLUtils.url('Search-Custom', 'cgid', category.ID, 'searchBy', category.custom.shopbyAttribute).toString()
        });
        seoCatBreadcrumbs.push({
            htmlValue: Resource.msgf('heading.shop.brand', 'search', null, req.querystring.searchBy),
            url: URLUtils.url('Search-Custom', 'cgid', category.ID, 'searchBy', category.custom.shopbyAttribute).toString()
        });
    }
    var seoBreadCrumbs = seo.getSeoBreadCrumbs(seoCatBreadcrumbs);

    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
    apiProductSearch.search();
    productSearch = new ProductSearch(
        apiProductSearch,
        req.querystring,
        req.querystring.srule,
        CatalogMgr.getSortingOptions(),
        CatalogMgr.getSiteCatalog().getRoot()
    );

    brandPagePref = searchHelper.setupBrandPage(productSearch, req.querystring, apiProductSearch.category.ID.toLowerCase() === 'root');

    res.render('rendering/category/shopBy', {
        category: apiProductSearch.category,
        alphaNums: alphaNums,
        brandPagePref: brandPagePref,
        queryString: req.querystring,
        breadcrumbs: catBreadcrumbs,
        seoBreadCrumbs: seoBreadCrumbs
    });

    next();
});

server.get('LegacyRedirect', function (req, res, next) {
    var redirectID = '';

    var redirectURL = '';
    var MapMgr = require('dw/util/MappingMgr');
    var MapKey = require('dw/util/MappingKey');
    try {
        if (req.querystring.url !== null) {
            var url = req.querystring.url.toString();
            var urlKey = new MapKey('/' + url + '.do');
            redirectID = MapMgr.get('CTD-Product-Mapping', urlKey);

            if (redirectID !== null && redirectID !== '') {
                redirectURL = URLUtils.https('Product-Show', 'pid', redirectID.get('SFCC_Master_Product_ID')).toString();
            } else {
                redirectURL = URLUtils.https('Home-Show').toString();
            }
        }

        if (redirectURL !== '') {
            res.setRedirectStatus(301);
            res.redirect(redirectURL);
        } else {
            res.setStatusCode(410);
            res.render('error/notFound');
        }
    } catch (ex) {
        redirectURL = URLUtils.https('Home-Show').toString();
        res.setRedirectStatus(301);
        res.redirect(redirectURL);
    }

    next();
});

server.get('BlogRedirect', function (req, res, next) {
    var redirectURL = '';
    try {
        if (req.querystring.url !== null) {
            var url = req.querystring.url.toString();
            var blogUrl = Site.getCurrent().getCustomPreferenceValue('blogUrl');
            redirectURL = blogUrl + '/' + url;
        }

        if (redirectURL !== '') {
            res.setRedirectStatus(301);
            res.redirect(redirectURL);
        } else {
            res.setStatusCode(410);
            res.render('error/notFound');
        }
    } catch (ex) {
        redirectURL = URLUtils.https('Home-Show').toString();
        res.setRedirectStatus(301);
        res.redirect(redirectURL);
    }

    next();
});

server.get('Folder', cache.applyShortPromotionSensitiveCache, function (req, res, next) {
    var seo = require('*/cartridge/models/seo');
    var Resource = require('dw/web/Resource');
    var ContentMgr = require('dw/content/ContentMgr');
    var apiContent = ContentMgr.getContent('seo-search-folder');
    if (apiContent) {
        var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
        pageMetaHelper.setPageMetaData(req.pageMetaData, apiContent);
        pageMetaHelper.setPageMetaTags(req.pageMetaData, apiContent);
    }
    var folderSearch = searchHelper.setupFolderSearch(req.querystring);
    var breadcrumbs = [{
        htmlValue: Resource.msg('global.home', 'common', null),
        url: URLUtils.home().toString()
    },
    {
        htmlValue: folderSearch.displayName,
        url: URLUtils.url('Search-Folder', 'q', req.querystring.q).toString()
    }];
    var seoBreadCrumbs = seo.getSeoBreadCrumbs(breadcrumbs);
    res.render('rendering/folder/contentGrid', {
        folderSearch: folderSearch,
        breadcrumbs: [{
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: folderSearch.displayName
        }],
        seoBreadCrumbs: seoBreadCrumbs
    });
    next();
});

server.get('Include', cache.applyShortPromotionSensitiveCache, function (req, res, next) {
    var apiProductSearch = new ProductSearchModel();
    var productSearch;
    var params = req.querystring;
    params.sz = 3;
    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
    apiProductSearch.search();
    productSearch = new ProductSearch(
        apiProductSearch,
        params,
        null, // not required, it will fetch default sort rule in ProductOptions.js
        CatalogMgr.getSortingOptions(),
        CatalogMgr.getSiteCatalog().getRoot()
    );
    res.render('search/ar15productGrid', {
        productSearch: productSearch
    });
    next();
});

server.replace('Refinebar', cache.applyDefaultCache, function (req, res, next) {
    var collections = require('*/cartridge/scripts/util/collections');
    var apiProductSearch = new ProductSearchModel();
    apiProductSearch = searchHelper.setupSearch(apiProductSearch, req.querystring);
    apiProductSearch.search();
    var refinedCategories;
    var refinements;
    var refineCatIDs = new ArrayList();
    if (apiProductSearch.refinements !== null) {
        refinements = apiProductSearch.refinements;
        refinedCategories = refinements.getRefinementValues(refinements.getCategoryRefinementDefinition());
        collections.forEach(refinedCategories, function (refineCatg) {
            refineCatIDs.push(refineCatg.value);
        });
    } // end search refinement category filter


    var productSearch = new ProductSearch(
        apiProductSearch,
        req.querystring,
        req.querystring.srule,
        CatalogMgr.getSortingOptions(),
        CatalogMgr.getSiteCatalog().getRoot()
    );

    res.render('/search/searchRefineBar', {
        productSearch: productSearch,
        querystring: req.querystring,
        refinedCategories: refineCatIDs
    });

    next();
});

module.exports = server.exports();
