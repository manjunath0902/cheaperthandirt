'use strict';

var server = require('server');
var ServiceObject = require('int_emarsys/cartridge/scripts/init/emarsysServiceInit.js');
var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');

server.use('SubscribeRequest', function (req, res, next) {
    var emailId = req.form.hpEmailSignUp;
    var signUpForm = server.forms.getForm('signUp');

    signUpForm.email.value = emailId || '';
    res.render('emailsignup/emailSignUpForm', {
        signUpForm: signUpForm
    });
    next();
});

server.post('SubmitRequest', function (req, res, next) {
    var submittedSignUpForm = server.forms.getForm('signUp');
    var currService = 'int_emarsys_update_contact';
    var emarsysServiceObj = ServiceObject.emarsysService(currService);
    var registeredUser = {
        firstName: submittedSignUpForm.firstName.value,
        lastName: submittedSignUpForm.lastName.value,
        email: submittedSignUpForm.email.value,
        optIn: true
    };
    var params = accountHelpers.getParams(registeredUser, currService);
    params.requestType = 'PUT';
    emarsysServiceObj.call(params);
    res.render('emailsignup/signUpAcknowledge');
    next();
});

module.exports = server.exports();
