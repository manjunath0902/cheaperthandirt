/**
 * Description of the Controller and the logic it provides
 *
 * @module  controllers/BISN
 */
'use strict';

var server = require('server');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Resource = require('dw/web/Resource');
var ProductMgr = require('dw/catalog/ProductMgr');
var Transaction = require('dw/system/Transaction');

server.post('Create', function (req, res, next) {
    var result = {
        success: false,
        msg: Resource.msg('notify.me.failure', 'product', null)
    };
    try {
        var email = req.form.email;
        var productID = req.form.productID;
        var product = ProductMgr.getProduct(productID);
        var masterProductID = product.variant ? product.masterProduct.ID : productID;
        var existingObject = CustomObjectMgr.getCustomObject('Back-In-Stock-Notifications', email + '|' + productID);
        if (existingObject == null && productID != null && email != null) {
            Transaction.wrap(function () {
                var bisnObj = CustomObjectMgr.createCustomObject('Back-In-Stock-Notifications', email + '|' + productID);
                bisnObj.custom.partNumber = productID;
                bisnObj.custom.masterProductID = masterProductID;
                bisnObj.custom.emailID = email;
            });
            result.success = true;
            result.msg = Resource.msg('notify.me.success', 'product', null);
            res.json(result);
        } else if (existingObject != null) {
            result.success = true;
            result.msg = Resource.msg('notify.me.success', 'product', null);
            res.json(result);
        }
    } catch (e) {
        res.json(result);
    }
    next();
});

module.exports = server.exports();
