'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');


server.replace('SelectShippingMethod', server.middleware.https, function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Resource = require('dw/web/Resource');
    var Transaction = require('dw/system/Transaction');
    var AccountModel = require('*/cartridge/models/account');
    var OrderModel = require('*/cartridge/models/order');
    var URLUtils = require('dw/web/URLUtils');
    var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
    var Locale = require('dw/util/Locale');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var CustomerModel = require('*/cartridge/models/customer.js');
    var customerObj = new CustomerModel(req.currentCustomer.raw);


    var currentBasket = BasketMgr.getCurrentBasket();

    // Ensure we dont have any empty shipments
    Transaction.wrap(function () {
        COHelpers.ensureNoEmptyShipments(req);
    });

    if (!currentBasket) {
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        return next();
    }

    var shipToHomeShipment = COHelpers.getShipToHomeShipment(currentBasket);
    var shippingMethodID = req.querystring.methodID || req.form.methodID;
    var shipment;
    if (shipToHomeShipment) {
        shipment = shipToHomeShipment;
    } else {
        shipment = currentBasket.defaultShipment;
    }

    var viewData = res.getViewData();
    viewData.address = ShippingHelper.getAddressFromRequest(req);
    viewData.isGift = req.form.isGift === 'true';
    viewData.giftMessage = req.form.isGift ? req.form.giftMessage : null;
    res.setViewData(viewData);

    this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
        var shippingData = res.getViewData();
        var address = shippingData.address;

        try {
            Transaction.wrap(function () {
                var shippingAddress = shipment.shippingAddress;

                if (!shippingAddress) {
                    shippingAddress = shipment.createShippingAddress();
                }

                shippingAddress.setFirstName(address.firstName || '');
                shippingAddress.setLastName(address.lastName || '');
                shippingAddress.setAddress1(address.address1 || '');
                shippingAddress.setAddress2(address.address2 || '');
                shippingAddress.setCity(address.city || '');
                shippingAddress.setPostalCode(address.postalCode || '');
                shippingAddress.setStateCode(address.stateCode || '');
                shippingAddress.setCountryCode(address.countryCode || '');
                shippingAddress.setPhone(address.phone || '');

                ShippingHelper.selectShippingMethod(shipment, shippingMethodID);

                basketCalculationHelpers.calculateTotals(currentBasket);
            });

            if (shipment.shippingMethod.custom.storePickupEnabled) {
                ShippingHelper.markShipmentForShipping(shipment);

                if (customerObj.addressBook && customerObj.addressBook.preferredAddress) {
                    var preferredAddress = customerObj.addressBook.preferredAddress;
                    var countryCode = preferredAddress.countryCode.value;

                    Object.keys(viewData.address).forEach(function (key) {
                        var value = preferredAddress[key];
                        if (value) {
                            if (key === 'countryCode') {
                                viewData.address[key] = countryCode;
                            } else {
                                viewData.address[key] = value;
                            }
                        } else {
                            viewData.address[key] = null;
                        }
                    });
                } else {
                    Object.keys(viewData.address).forEach(function (key) {
                        viewData.address[key] = null;
                    });
                }
                res.setViewData(viewData);
            }
        } catch (e) {
            res.setStatusCode(500);
            res.json({
                error: true,
                errorMessage: Resource.msg('error.cannot.select.shipping.method', 'cart', null)
            });
            return;
        }

        COHelpers.setGift(shipment, shippingData.isGift, shippingData.giftMessage);

        var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
        var currentLocale = Locale.getLocale(req.locale.id);

        var basketModel = new OrderModel(
            currentBasket,
            { usingMultiShipping: usingMultiShipping, countryCode: currentLocale.country, containerView: 'basket' }
        );

        res.json({
            customer: new AccountModel(req.currentCustomer),
            order: basketModel
        });
    });

    return next();
});


/**
 * Handle Ajax shipping form submit
 */
server.replace(
    'SubmitShipping',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var BasketMgr = require('dw/order/BasketMgr');
        var URLUtils = require('dw/web/URLUtils');
        var Resource = require('dw/web/Resource');
        var Transaction = require('dw/system/Transaction');
        var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
        var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
        var CustomerModel = require('*/cartridge/models/customer.js');
        var customerObj = new CustomerModel(req.currentCustomer.raw);
        var currentBasket = BasketMgr.getCurrentBasket();

        var shipToStoreShipment = COHelpers.getShipToStoreShipment(currentBasket);

        if (!currentBasket) {
            res.json({
                error: true,
                cartError: true,
                fieldErrors: [],
                serverErrors: [],
                upsAddressReturned: false,
                redirectUrl: URLUtils.url('Cart-Show').toString()
            });
            return next();
        }

        var form = server.forms.getForm('shipping');
        var result = {};

        req.session.privacyCache.set(shipToStoreShipment.UUID, 'valid');

        result.address = {
            firstName: form.shippingAddress.addressFields.firstName.value,
            lastName: form.shippingAddress.addressFields.lastName.value,
            address1: form.shippingAddress.addressFields.address1.value,
            address2: form.shippingAddress.addressFields.address2.value,
            city: form.shippingAddress.addressFields.city.value,
            postalCode: form.shippingAddress.addressFields.postalCode.value,
            countryCode: form.shippingAddress.addressFields.country.value,
            phone: form.shippingAddress.addressFields.phone.value
        };
        if (Object.prototype.hasOwnProperty
            .call(form.shippingAddress.addressFields, 'states')) {
            result.address.stateCode =
                form.shippingAddress.addressFields.states.stateCode.value;
        }

        result.shippingMethod = form.shippingAddress.shippingMethodID.value ?
            form.shippingAddress.shippingMethodID.value.toString() :
            null;

        result.isGift = form.shippingAddress.isGift.checked;

        result.giftMessage = result.isGift ? form.shippingAddress.giftMessage.value : null;

        res.setViewData(result);
        /* eslint-disable no-shadow */
        this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
            var AccountModel = require('*/cartridge/models/account');
            var OrderModel = require('*/cartridge/models/order');
            var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
            var Locale = require('dw/util/Locale');

            if (!currentBasket.billingAddress) {
                if (customerObj.addressBook &&
                    customerObj.addressBook.preferredAddress) {
                    // Copy over preferredAddress (use addressUUID for matching)
                    COHelpers.copyBillingAddressToBasket(
                        customerObj.addressBook.preferredAddress, currentBasket);
                }
            }
            var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
            if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
                req.session.privacyCache.set('usingMultiShipping', false);
                usingMultiShipping = false;
            }
            if (shipToStoreShipment != null && shipToStoreShipment.shippingMethod.custom.storePickupEnabled) {
                if (!req.form.storeId) {
                    res.setStatusCode(500);
                    res.json({
                        error: true,
                        errorMessage: Resource.msg('error.no.store.selected', 'storeLocator', null),
                        upsAddressReturned: false
                    });
                } else {
                    var viewData = res.getViewData();
                    delete viewData.fieldErrors;
                    viewData.error = false;
                    viewData.shipmentUUID = req.form.shipmentUUID;
                    viewData.storeId = req.form.storeId;
                    viewData.shippingMethod = shipToStoreShipment.shippingMethodID;

                    res.setViewData(viewData);
                    var StoreMgr = require('dw/catalog/StoreMgr');

                    var storeId = viewData.storeId;
                    var store = StoreMgr.getStore(storeId);

                    if (storeId) {
                        ShippingHelper.markShipmentForPickup(shipToStoreShipment, storeId);

                        Transaction.wrap(function () {
                            var storeAddress = {
                                address: {
                                    firstName: store.name,
                                    lastName: '',
                                    address1: store.address1,
                                    address2: store.address2,
                                    city: store.city,
                                    stateCode: store.stateCode,
                                    postalCode: store.postalCode,
                                    countryCode: store.countryCode.value,
                                    phone: store.phone
                                },
                                shippingMethod: viewData.shippingMethod
                            };
                            COHelpers.copyShippingAddressToShipment(storeAddress, shipToStoreShipment);
                        });
                    }

                    COHelpers.recalculateBasket(currentBasket);


                    var currentLocale = Locale.getLocale(req.locale.id);
                    var basketModel = new OrderModel(
                        currentBasket, {
                            usingMultiShipping: usingMultiShipping,
                            shippable: true,
                            countryCode: currentLocale.country,
                            containerView: 'basket'
                        }
                    );

                    res.json({
                        customer: new AccountModel(req.currentCustomer),
                        order: basketModel,
                        form: server.forms.getForm('shipping'),
                        upsAddressReturned: false
                    });
                }
            }
        });
        /* eslint-enable no-shadow */
        return next();
    });

module.exports = server.exports();
