'use strict';

var server = require('server');
server.extend(module.superModule);
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var HookMgr = require('dw/system/HookMgr');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

server.replace('PlaceOrder', server.middleware.https, function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var OrderMgr = require('dw/order/OrderMgr');
    var Resource = require('dw/web/Resource');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var arrayHelper = require('*/cartridge/scripts/util/array');
    var Site = require('dw/system/Site');
    var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
    var ShippingMgr = require('dw/order/ShippingMgr');

    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.json({
            error: true,
            cartError: true,
            fieldErrors: [],
            serverErrors: [],
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        return next();
    }
    var ccPaymentInst;
    if (currentBasket) {
        var paymentInstruments = currentBasket.getPaymentInstruments(
                PaymentInstrument.METHOD_CREDIT_CARD
            );
        ccPaymentInst = arrayHelper.find(paymentInstruments, function (paymentInstrument) {
            return paymentInstrument.paymentMethod === 'CREDIT_CARD';
        });
    }

    // this method is called to ensure the shipping method and shipping address on store-shipment is correctly retained
    COHelpers.ensureStoreShipmentHasValidAddr(currentBasket);

    var hasGCInCart = COHelpers.hasGiftCardInCart(currentBasket.getProductLineItems());
    var onlyGCInCart = COHelpers.checkOnlyGiftCardInCart(currentBasket.getProductLineItems());
    var shipToHomeShipment = COHelpers.getShipToHomeShipment(currentBasket);
    // Do not allow the customer to make gift card purchase using gift card.
    if (hasGCInCart) {
        COHelpers.removeGiftCardPaymentInstruments(currentBasket);
    }
    if (!onlyGCInCart && shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingAddress !== null && shipToHomeShipment.shippingAddress !== undefined && shipToHomeShipment.shippingAddress.address2 !== null && shipToHomeShipment.shippingAddress.address2 !== undefined && shipToHomeShipment.shippingAddress.address2.indexOf('CTD') > -1) {
        if (hasGCInCart && currentBasket.getShipments().length === 1) {
            res.json({
                error: true,
                cartError: true,
                redirectUrl: URLUtils.url('Checkout-Begin', 'stage', 'shipping').toString(),
                errorMessage: Resource.msg('error.no.shipping.address', 'checkout', null)
            });
            return next();
        }
    }
    // Check if the shipment has free shipping method but the customer is not eligible for free shipping
    var freeShippingMethodID = Site.getCurrent().getCustomPreferenceValue('freeShippingMethodID');
    if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod !== undefined && shipToHomeShipment.shippingMethod.ID === freeShippingMethodID) {
        var eligibleForFreeShipping = COHelpers.checkFreeShippingEligibility(currentBasket);
        if (!eligibleForFreeShipping) {
            Transaction.wrap(function () {
                ShippingHelper.selectShippingMethod(shipToHomeShipment, ShippingMgr.getDefaultShippingMethod().ID);
            });
        }
    }

    // Check if the shipment has postal shipping method but the customer is not eligible for postal shipping
    var postalShippingMethodID = Site.getCurrent().getCustomPreferenceValue('postalShippingMethodID');
    if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod !== undefined && shipToHomeShipment.shippingMethod.ID === postalShippingMethodID) {
        var eligibleForPostalShipping = COHelpers.checkPostalShippingEligibility(currentBasket);
        if (!eligibleForPostalShipping) {
            Transaction.wrap(function () {
                ShippingHelper.selectShippingMethod(shipToHomeShipment, ShippingMgr.getDefaultShippingMethod().ID);
            });
        }
    }
    // Default the shipment shippingmethod to default if the state code is not AK or HI but the ship method was of AK or HI
    var shipMethodsForAKHI = Site.getCurrent().getCustomPreferenceValue('shipMethodsForAKHI');
    var shipArr = shipMethodsForAKHI.split('|');
    var isARHIMethod = false;
    if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod !== undefined) {
        for (var i = 0; i < shipArr.length; i++) {
            if (shipArr[i] === shipToHomeShipment.shippingMethod.ID) {
                isARHIMethod = true;
                break;
            }
        }
    }
    var allowedStateCode;
    // Default the shipment shippingmethod to default if the state code is not AK or HI but the ship method was of AK or HI
    if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod !== undefined && isARHIMethod) {
        allowedStateCode = session.custom.shippingStateCode !== null && session.custom.shippingStateCode !== undefined ? session.custom.shippingStateCode : ''; // eslint-disable-line
        if (!(allowedStateCode.indexOf('AK') > -1 || allowedStateCode.indexOf('HI') > -1)) {
            Transaction.wrap(function () {
                ShippingHelper.selectShippingMethod(shipToHomeShipment, ShippingMgr.getDefaultShippingMethod().ID);
            });
        }
    }
    var standardMethodForAKHI = Site.getCurrent().getCustomPreferenceValue('standardMethodForAKHI');
    // Default the shipment shippingmethod to default if the state code is AK or HI but the ship method was of AK or HI
    if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod !== undefined && !isARHIMethod) {
        allowedStateCode = session.custom.shippingStateCode !== null && session.custom.shippingStateCode !== undefined ? session.custom.shippingStateCode : ''; // eslint-disable-line
        var allowedOtherMethods = Site.getCurrent().getCustomPreferenceValue('allowedOtherMethodsForAKHI');
        if ((allowedStateCode.indexOf('AK') > -1 || allowedStateCode.indexOf('HI') > -1) && allowedOtherMethods.indexOf(shipToHomeShipment.shippingMethod.ID) === -1) {
            Transaction.wrap(function () {
                ShippingHelper.selectShippingMethod(shipToHomeShipment, standardMethodForAKHI);
            });
        }
    }
    if ((shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingAddress === null) || (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingAddress !== null && shipToHomeShipment.shippingAddress.address1 === null)) {
        res.json({
            error: true,
            cartError: true,
            redirectUrl: URLUtils.url('Checkout-Begin', 'stage', 'shipping').toString(),
            errorMessage: Resource.msg('error.no.shipping.address', 'checkout', null)
        });
        return next();
    }
    if (req.session.privacyCache.get('fraudDetectionStatus')) {
        res.json({
            error: true,
            cartError: true,
            redirectUrl: URLUtils.url('Error-ErrorCode', 'err', '01').toString(),
            errorMessage: Resource.msg('error.technical', 'checkout', null)
        });

        return next();
    }

    COHelpers.calculateUPSShipRate(currentBasket);

    var restrictHelper = require('*/cartridge/scripts/cart/restrictHelper.js');
    if (shipToHomeShipment !== null && shipToHomeShipment !== undefined) {
        var shippingAddress = shipToHomeShipment.getShippingAddress();
        if (shippingAddress !== null) {
            var address = {};
            address.postalCode = shippingAddress.postalCode;
            address.shippingMethodID = shipToHomeShipment.getShippingMethodID();
            address.stateCode = shippingAddress.stateCode;
            var restrictResponse = restrictHelper.validateRestrictGroupAndItem(address, currentBasket);
            if (restrictResponse.error && restrictResponse.hasRestrictedItems) {
                session.custom.restrictedShipMethods = true; // eslint-disable-line
                res.json({
                    error: true,
                    errorClass: 'shipping-restricted',
                    errorStage: {
                        stage: 'shipping',
                        step: 'address'
                    },
                    errorMessage: Resource.msg('error.restrict.serviceerror', 'checkout', null)
                });
                return next();
            }
        }
    }

    var validationOrderStatus = hooksHelper('app.validate.order', 'validateOrder', currentBasket, require('*/cartridge/scripts/hooks/validateOrder').validateOrder);
    if (validationOrderStatus.error) {
        res.json({
            error: true,
            errorMessage: validationOrderStatus.message
        });
        return next();
    }

    // Check to make sure there is a shipping address
    if (currentBasket.defaultShipment.shippingAddress === null) {
        res.json({
            error: true,
            errorStage: {
                stage: 'shipping',
                step: 'address'
            },
            errorMessage: Resource.msg('error.no.shipping.address', 'checkout', null)
        });
        return next();
    }

    // Check to make sure billing address exists
    if (!currentBasket.billingAddress) {
        res.json({
            error: true,
            errorStage: {
                stage: 'payment',
                step: 'billingAddress'
            },
            errorMessage: Resource.msg('error.no.billing.address', 'checkout', null)
        });
        return next();
    }

    // Calculate the basket
    Transaction.wrap(function () {
        basketCalculationHelpers.calculateTotals(currentBasket);
    });

    // Re-validates existing payment instruments
    var validPayment = COHelpers.validatePayment(req, currentBasket);
    if (validPayment.error) {
        res.json({
            error: true,
            errorStage: {
                stage: 'payment',
                step: 'paymentInstrument'
            },
            errorMessage: Resource.msg('error.payment.not.valid', 'checkout', null)
        });
        return next();
    }
    var requireCCPayment = COHelpers.validateIfOrderRequirePayment(currentBasket);
    if (requireCCPayment && !ccPaymentInst) {
        res.json({
            error: true,
            errorStage: {
                stage: 'payment',
                step: 'paymentInstrument'
            },
            requirecc: true,
            errorMessage: Resource.msg('error.payment.requirecc', 'checkout', null)
        });
        return next();
    }


    // Re-calculate the payments.
    var calculatedPaymentTransactionTotal = COHelpers.calculatePaymentTransaction(currentBasket);
    if (calculatedPaymentTransactionTotal.error) {
        res.json({
            error: true,
            errorMessage: Resource.msg('error.technical', 'checkout', null)
        });
        return next();
    }

    // Creates a new order.
    var order = COHelpers.createOrder(currentBasket);
    if (!order) {
        res.json({
            error: true,
            errorMessage: Resource.msg('error.technical', 'checkout', null)
        });
        return next();
    }

    // Handles payment authorization
    if (requireCCPayment && ccPaymentInst) {
        var handlePaymentResult = COHelpers.handlePayments(order, order.orderNo, req);
        if (handlePaymentResult.error) {
            res.json({
                error: true,
                errorMessage: handlePaymentResult.errorMessage
            });
            return next();
        }
    }

    var fraudDetectionStatus = hooksHelper('app.fraud.detection', 'fraudDetection', currentBasket, require('*/cartridge/scripts/hooks/fraudDetection').fraudDetection);
    if (fraudDetectionStatus.status === 'fail') {
        Transaction.wrap(function () { OrderMgr.failOrder(order); });

        // fraud detection failed
        req.session.privacyCache.set('fraudDetectionStatus', true);

        res.json({
            error: true,
            cartError: true,
            redirectUrl: URLUtils.url('Error-ErrorCode', 'err', fraudDetectionStatus.errorCode).toString(),
            errorMessage: Resource.msg('error.technical', 'checkout', null)
        });

        return next();
    }

    // Places the order
    var placeOrderResult = COHelpers.placeOrder(order, fraudDetectionStatus);
    if (placeOrderResult.error) {
        res.json({
            error: true,
            errorMessage: Resource.msg('error.technical', 'checkout', null)
        });
        return next();
    }

    // Reset usingMultiShip after successful Order placement
    req.session.privacyCache.set('usingMultiShipping', false);

    var signupForEmail = req.session.privacyCache.get('signupForEmail');
    if (signupForEmail !== undefined && signupForEmail) {
        try {
            var ServiceObject = require('int_emarsys/cartridge/scripts/init/emarsysServiceInit.js');
            var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
            var currService = 'int_emarsys_update_contact';
            var emarsysServiceObj = ServiceObject.emarsysService(currService);
            var registeredUser = {
                firstName: order.billingAddress.firstName,
                lastName: order.billingAddress.lastName,
                email: order.getCustomerEmail(),
                optIn: true
            };
            var params = accountHelpers.getParams(registeredUser, currService);
            params.requestType = 'PUT';
            emarsysServiceObj.call(params);
        } catch (e) {
            // As a fail safe, if there is any technical error on the signup call. The page should not navigate to error page
        }
    }
    var customer = req.currentCustomer.raw;
    if (!customer.authenticated) {
        delete session.custom.shippingStateCode; // eslint-disable-line
    }
    // TODO: Exposing a direct route to an Order, without at least encoding the orderID
    //  is a serious PII violation.  It enables looking up every customers orders, one at a
    //  time.
    res.json({
        error: false,
        orderID: order.orderNo,
        orderToken: order.orderToken,
        continueUrl: URLUtils.url('Order-Confirm').toString()
    });

    return next();
});


/**
 *  Handle Ajax payment (and billing) form submit
 */
server.replace(
    'SubmitPayment',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var paymentForm = server.forms.getForm('billing');
        var billingFormErrors = {};
        var creditCardErrors = {};
        var viewData = {};
        var BasketMgr = require('dw/order/BasketMgr');
        var Transaction = require('dw/system/Transaction');
        var PaymentInstrument = require('dw/order/PaymentInstrument');
        var collections = require('*/cartridge/scripts/util/collections');

        // verify billing form data
        billingFormErrors = COHelpers.validateBillingForm(paymentForm.addressFields);

        var currentBasket = BasketMgr.getCurrentBasket();

        // to add a check to validate the payment is required for cc or not.
        var requireCCPayment = COHelpers.validateIfOrderRequirePayment(currentBasket);

        if (!req.form.storedPaymentUUID && requireCCPayment) {
            // verify credit card form data
            creditCardErrors = COHelpers.validateCreditCard(paymentForm);
        }

        if (!requireCCPayment) {
            Transaction.wrap(function () {
                var paymentInstruments = currentBasket.getPaymentInstruments(
                    PaymentInstrument.METHOD_CREDIT_CARD
                );

                collections.forEach(paymentInstruments, function (item) {
                    currentBasket.removePaymentInstrument(item);
                });
            });
        }

        if (Object.keys(creditCardErrors).length || Object.keys(billingFormErrors).length) {
            // respond with form data and errors
            res.json({
                form: paymentForm,
                fieldErrors: [billingFormErrors, creditCardErrors],
                serverErrors: [],
                error: true
            });
        } else {
            viewData.address = {
                firstName: { value: paymentForm.addressFields.firstName.value },
                lastName: { value: paymentForm.addressFields.lastName.value },
                address1: { value: paymentForm.addressFields.address1.value },
                address2: { value: paymentForm.addressFields.address2.value },
                city: { value: paymentForm.addressFields.city.value },
                postalCode: { value: paymentForm.addressFields.postalCode.value },
                countryCode: { value: paymentForm.addressFields.country.value }
            };

            if (Object.prototype.hasOwnProperty
                .call(paymentForm.addressFields, 'states')) {
                viewData.address.stateCode =
                    { value: paymentForm.addressFields.states.stateCode.value };
            }

            viewData.paymentMethod = {
                value: paymentForm.paymentMethod.value,
                htmlName: paymentForm.paymentMethod.value
            };

            viewData.paymentInformation = {
                cardType: {
                    value: paymentForm.creditCardFields.cardType.value,
                    htmlName: paymentForm.creditCardFields.cardType.htmlName
                },
                cardNumber: {
                    value: paymentForm.creditCardFields.cardNumber.value,
                    htmlName: paymentForm.creditCardFields.cardNumber.htmlName
                },
                securityCode: {
                    value: paymentForm.creditCardFields.securityCode.value,
                    htmlName: paymentForm.creditCardFields.securityCode.htmlName
                },
                expirationMonth: {
                    value: parseInt(
                        paymentForm.creditCardFields.expirationMonth.selectedOption,
                        10
                    ),
                    htmlName: paymentForm.creditCardFields.expirationMonth.htmlName
                },
                expirationYear: {
                    value: parseInt(paymentForm.creditCardFields.expirationYear.value, 10),
                    htmlName: paymentForm.creditCardFields.expirationYear.htmlName
                }
            };

            if (req.form.storedPaymentUUID) {
                viewData.storedPaymentUUID = req.form.storedPaymentUUID;
            }

            viewData.email = {
                value: paymentForm.creditCardFields.email.value
            };

            viewData.phone = { value: paymentForm.creditCardFields.phone.value };

            viewData.saveCard = paymentForm.creditCardFields.saveCard.checked;

            res.setViewData(viewData);

            this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
                var CustomerMgr = require('dw/customer/CustomerMgr');
                var Resource = require('dw/web/Resource');
                var PaymentMgr = require('dw/order/PaymentMgr');
                var AccountModel = require('*/cartridge/models/account');
                var OrderModel = require('*/cartridge/models/order');
                var URLUtils = require('dw/web/URLUtils');
                var array = require('*/cartridge/scripts/util/array');
                var Locale = require('dw/util/Locale');
                var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
                var hooksHelper = require('*/cartridge/scripts/helpers/hooks');
                var onlyGCInCart = COHelpers.checkOnlyGiftCardInCart(currentBasket.getProductLineItems());
                var hasGCInCart = COHelpers.hasGiftCardInCart(currentBasket.getProductLineItems());
                var storeShipment = COHelpers.onlyStoreShipment(currentBasket.getShipments());
                var billingData = res.getViewData();

                if (!currentBasket) {
                    delete billingData.paymentInformation;

                    res.json({
                        error: true,
                        cartError: true,
                        fieldErrors: [],
                        serverErrors: [],
                        redirectUrl: URLUtils.url('Cart-Show').toString()
                    });
                    return;
                }

                var billingAddress = currentBasket.billingAddress;
                var billingForm = server.forms.getForm('billing');
                var paymentMethodID = billingData.paymentMethod.value;
                var result;

                billingForm.creditCardFields.cardNumber.htmlValue = '';
                billingForm.creditCardFields.securityCode.htmlValue = '';

                Transaction.wrap(function () {
                    if (!billingAddress) {
                        billingAddress = currentBasket.createBillingAddress();
                    }

                    billingAddress.setFirstName(billingData.address.firstName.value);
                    billingAddress.setLastName(billingData.address.lastName.value);
                    billingAddress.setAddress1(billingData.address.address1.value);
                    billingAddress.setAddress2(billingData.address.address2.value);
                    billingAddress.setCity(billingData.address.city.value);
                    billingAddress.setPostalCode(billingData.address.postalCode.value);
                    if (Object.prototype.hasOwnProperty.call(billingData.address, 'stateCode')) {
                        billingAddress.setStateCode(billingData.address.stateCode.value);
                    }
                    billingAddress.setCountryCode(billingData.address.countryCode.value);
                    if (onlyGCInCart || storeShipment || (hasGCInCart && currentBasket.getShipments().length > 1)) {
                        if (req.form.storedPaymentUUID) {
                            billingAddress.setPhone(req.currentCustomer.profile.phone);
                            currentBasket.setCustomerEmail(req.currentCustomer.profile.email);
                        }
                        if (billingData.phone.value !== null && billingData.phone.value !== undefined) {
                            billingAddress.setPhone(billingData.phone.value);
                        }
                        if (billingData.email.value !== null && billingData.email.value !== undefined) {
                            currentBasket.setCustomerEmail(billingData.email.value);
                        }
                    }
                });

                // if there is no selected payment option and balance is greater than zero
                if (!paymentMethodID && currentBasket.totalGrossPrice.value > 0) {
                    var noPaymentMethod = {};

                    noPaymentMethod[billingData.paymentMethod.htmlName] =
                        Resource.msg('error.no.selected.payment.method', 'creditCard', null);

                    delete billingData.paymentInformation;

                    res.json({
                        form: billingForm,
                        fieldErrors: [noPaymentMethod],
                        serverErrors: [],
                        error: true
                    });
                    return;
                }

                // check to make sure there is a payment processor
                if (!PaymentMgr.getPaymentMethod(paymentMethodID).paymentProcessor) {
                    throw new Error(Resource.msg(
                        'error.payment.processor.missing',
                        'checkout',
                        null
                    ));
                }

                var processor = PaymentMgr.getPaymentMethod(paymentMethodID).getPaymentProcessor();

                if (billingData.storedPaymentUUID
                    && req.currentCustomer.raw.authenticated
                    && req.currentCustomer.raw.registered
                    && requireCCPayment
                ) {
                    var paymentInstruments = req.currentCustomer.wallet.paymentInstruments;
                    var paymentInstrument = array.find(paymentInstruments, function (item) {
                        return billingData.storedPaymentUUID === item.UUID;
                    });

                    billingData.paymentInformation.cardNumber.value = paymentInstrument
                        .creditCardNumber;
                    billingData.paymentInformation.cardType.value = paymentInstrument
                        .creditCardType;
                    billingData.paymentInformation.securityCode.value = req.form.securityCode;
                    billingForm.creditCardFields.securityCode.value = req.form.securityCode;
                    billingData.paymentInformation.expirationMonth.value = paymentInstrument
                        .creditCardExpirationMonth;
                    billingData.paymentInformation.expirationYear.value = paymentInstrument
                        .creditCardExpirationYear;
                    billingData.paymentInformation.creditCardToken = paymentInstrument
                        .raw.creditCardToken;
                }

                if (HookMgr.hasHook('app.payment.processor.' + processor.ID.toLowerCase()) && requireCCPayment) {
                    result = HookMgr.callHook('app.payment.processor.' + processor.ID.toLowerCase(),
                        'Handle',
                        currentBasket,
                        billingData.paymentInformation
                    );
                } else if (requireCCPayment) {
                    result = HookMgr.callHook('app.payment.processor.default', 'Handle');
                }

                // need to invalidate credit card fields
                if (requireCCPayment) {
                    if (result.error) {
                        delete billingData.paymentInformation;

                        res.json({
                            form: billingForm,
                            fieldErrors: result.fieldErrors,
                            serverErrors: result.serverErrors,
                            error: true
                        });
                        return;
                    }
                }


                if (!billingData.storedPaymentUUID
                    && req.currentCustomer.raw.authenticated
                    && req.currentCustomer.raw.registered
                    && billingData.saveCard
                    && requireCCPayment
                    && (paymentMethodID === 'CREDIT_CARD')
                ) {
                    var customer = CustomerMgr.getCustomerByCustomerNumber(
                        req.currentCustomer.profile.customerNo
                    );

                    var saveCardResult = COHelpers.savePaymentInstrumentToWallet(
                        billingData,
                        currentBasket,
                        customer
                    );
                    if (saveCardResult) {
                        req.currentCustomer.wallet.paymentInstruments.push({
                            creditCardHolder: saveCardResult.creditCardHolder,
                            maskedCreditCardNumber: saveCardResult.maskedCreditCardNumber,
                            creditCardType: saveCardResult.creditCardType,
                            creditCardExpirationMonth: saveCardResult.creditCardExpirationMonth,
                            creditCardExpirationYear: saveCardResult.creditCardExpirationYear,
                            UUID: saveCardResult.UUID,
                            creditCardNumber: Object.hasOwnProperty.call(
                                saveCardResult,
                                'creditCardNumber'
                            )
                                ? saveCardResult.creditCardNumber
                                : null,
                            raw: saveCardResult
                        });
                    }
                }

                // Calculate the basket
                Transaction.wrap(function () {
                    basketCalculationHelpers.calculateTotals(currentBasket);
                });

                // Re-calculate the payments.
                var calculatedPaymentTransaction = COHelpers.calculatePaymentTransaction(
                    currentBasket
                );

                if (calculatedPaymentTransaction.error) {
                    res.json({
                        form: paymentForm,
                        fieldErrors: [],
                        serverErrors: [Resource.msg('error.technical', 'checkout', null)],
                        error: true
                    });
                    return;
                }

                var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
                if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
                    req.session.privacyCache.set('usingMultiShipping', false);
                    usingMultiShipping = false;
                }

                hooksHelper('app.customer.subscription', 'subscribeTo', [paymentForm.subscribe.checked, paymentForm.creditCardFields.email.htmlValue], function () {});

                var currentLocale = Locale.getLocale(req.locale.id);

                var basketModel = new OrderModel(
                    currentBasket,
                    { usingMultiShipping: usingMultiShipping, countryCode: currentLocale.country, containerView: 'basket' }
                );

                var accountModel = new AccountModel(req.currentCustomer);
                var renderedStoredPaymentInstrument = COHelpers.getRenderedPaymentInstruments(
                    req,
                    accountModel,
                    requireCCPayment
                );

                delete billingData.paymentInformation;

                res.json({
                    renderedPaymentInstruments: renderedStoredPaymentInstrument,
                    customer: accountModel,
                    order: basketModel,
                    form: billingForm,
                    error: false
                });
            });
        }
        return next();
    }
);

module.exports = server.exports();

