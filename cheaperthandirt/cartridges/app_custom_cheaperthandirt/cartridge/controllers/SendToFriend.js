'use strict';

var server = require('server');
var URLUtils = require('dw/web/URLUtils');
var productHelpers = require('*/cartridge/scripts/helpers/productHelpers');
server.get('Start', function (req, res, next) {
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');
    var collections = require('*/cartridge/scripts/util/collections');
    var params = req.querystring;
    var product = ProductFactory.get(params);

    var customObjects = CustomObjectMgr.getAllCustomObjects('TellaFriendMessages');
    var messages = [];
    collections.forEach(customObjects.asList(), function (CO) {
        messages.push(CO.custom.TellAFriendMessage);
    });
    var sendtofriendForm = server.forms.getForm('sendtofriend');
    sendtofriendForm.clear();
    res.render('/social/sendToFriend', {
        product: product,
        SendToFriendForm: sendtofriendForm,
        breadcrumbs: productHelpers.getAllBreadcrumbs(null, params.pid, []).reverse(),
        messages: messages
    });
    next();
});

server.post('Initiate', function (req, res, next) {
    var accountHepler = require('*/cartridge/scripts/helpers/accountHelpers');
    var ServiceObject = require('int_emarsys/cartridge/scripts/init/emarsysServiceInit.js');
    var form = server.forms.getForm('sendtofriend');
    var productURL = URLUtils.abs('Product-Show', 'pid', req.querystring.pid).toString(); // eslint-disable-line
    var currService = 'int_emarsys_tell_a_friend';
    var registeredUser = {
        recipientName: form.friendName.htmlValue,
        recipientEmail: form.friendEmail.htmlValue,
        senderName: form.yourName.htmlValue,
        senderEmail: form.yourEmail.htmlValue,
        productID: req.querystring.pid,
        productURL: productURL
    };
    var emarsysServiceObj = ServiceObject.emarsysService(currService);
    var reqParams = accountHepler.getParams(registeredUser, currService);
    emarsysServiceObj.call(reqParams);
    var allBreadcrumbs = productHelpers.getAllBreadcrumbs(null, req.querystring.pid, []).reverse();
    res.render('/social/sendToFriendSuccess', {
        breadcrumbs: allBreadcrumbs
    });
    next();
});
module.exports = server.exports();
