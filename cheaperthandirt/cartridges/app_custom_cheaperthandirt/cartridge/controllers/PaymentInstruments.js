'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

server.append('List', function (req, res, next) {
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var customer = CustomerMgr.getCustomerByCustomerNumber(
            req.currentCustomer.profile.customerNo
        );
    var wallet = customer.getProfile().getWallet();
    var piLength = wallet.getPaymentInstruments().getLength();
    var breadcrumb =
        [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('page.title.myaccount', 'account', null),
                url: URLUtils.abs('Account-Show').toString()
            },
            {
                htmlValue: Resource.msg('link.header.PaymentMethods', 'account', null)
            }
        ];
    res.setViewData({
        breadcrumbs: breadcrumb,
        piLength: piLength
    });
    next();
});

/**
 * Checks if a credit card is valid or not
 * @param {Object} card - plain object with card details
 * @param {Object} form - form object
 * @returns {boolean} a boolean representing card validation
 */
function verifyCard(card, form) {
    var collections = require('*/cartridge/scripts/util/collections');
    var PaymentMgr = require('dw/order/PaymentMgr');
    var PaymentStatusCodes = require('dw/order/PaymentStatusCodes');

    var paymentCard = PaymentMgr.getPaymentCard(card.cardType);
    var error = false;
    var cardNumber = card.cardNumber;
    var creditCardStatus;
    var formCardNumber = form.cardNumber;

    if (paymentCard) {
        creditCardStatus = paymentCard.verify(
            card.expirationMonth,
            card.expirationYear,
            cardNumber
        );
    } else {
        formCardNumber.valid = false;
        formCardNumber.error =
            Resource.msg('error.message.creditnumber.invalid', 'forms', null);
        error = true;
    }

    if (creditCardStatus && creditCardStatus.error) {
        collections.forEach(creditCardStatus.items, function (item) {
            switch (item.code) {
                case PaymentStatusCodes.CREDITCARD_INVALID_CARD_NUMBER:
                    formCardNumber.valid = false;
                    formCardNumber.error =
                        Resource.msg('error.message.creditnumber.invalid', 'forms', null);
                    error = true;
                    break;

                case PaymentStatusCodes.CREDITCARD_INVALID_EXPIRATION_DATE:
                    var expirationMonth = form.expirationMonth;
                    var expirationYear = form.expirationYear;
                    expirationMonth.valid = false;
                    expirationMonth.error =
                        Resource.msg('error.message.creditexpiration.expired', 'forms', null);
                    expirationYear.valid = false;
                    error = true;
                    break;
                default:
                    error = true;
            }
        });
    }
    return error;
}
/**
 * Creates an object from form values
 * @param {Object} paymentForm - form object
 * @returns {Object} a plain object of payment instrument
 */
function getDetailsObject(paymentForm) {
    return {
        name: paymentForm.cardOwner.value,
        cardNumber: paymentForm.cardNumber.value,
        cardType: paymentForm.cardType.value,
        expirationMonth: paymentForm.expirationMonth.value,
        expirationYear: paymentForm.expirationYear.value,
        paymentForm: paymentForm
    };
}

server.replace('SavePayment', csrfProtection.validateAjaxRequest, function (req, res, next) {
    var formErrors = require('*/cartridge/scripts/formErrors');
    var dwOrderPaymentInstrument = require('dw/order/PaymentInstrument');
    var HookMgr = require('dw/system/HookMgr');

    var CustomerMgr = require('dw/customer/CustomerMgr');
    var paymentForm = server.forms.getForm('creditCard');
    var PaymentMgr = require('dw/order/PaymentMgr');
    var PaymentInstrument = require('dw/order/PaymentInstrument');
    var result = getDetailsObject(paymentForm);
    var customer = CustomerMgr.getCustomerByCustomerNumber(
            req.currentCustomer.profile.customerNo
        );
    if (paymentForm.valid && !verifyCard(result, paymentForm)) {
        res.setViewData(result);
        this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
            var Transaction = require('dw/system/Transaction');

            var formInfo = res.getViewData();

            var paymentObj = {};
            paymentObj.cardNumber = paymentForm.cardNumber.htmlValue.replace(/[^0-9 ]/g, '');
            paymentObj.expirationMonth = paymentForm.expirationMonth.htmlValue.replace(/[^0-9 ]/g, '');
            paymentObj.expirationYear = paymentForm.expirationYear.htmlValue.replace(/[^0-9 ]/g, '');

            var wallet = customer.getProfile().getWallet();
            var processor = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD).getPaymentProcessor();
            var tokenResponse = HookMgr.callHook(
                    'app.payment.processor.' + processor.ID.toLowerCase(),
                    'createToken',
                    customer,
                    paymentObj
                );

            if (!tokenResponse.error) {
                var paymentInstrument;
                Transaction.wrap(function () {
                    paymentInstrument = wallet.createPaymentInstrument(dwOrderPaymentInstrument.METHOD_CREDIT_CARD);
                    paymentInstrument.setCreditCardHolder(formInfo.name);
                    paymentInstrument.setCreditCardNumber(formInfo.cardNumber);
                    paymentInstrument.setCreditCardType(formInfo.cardType);
                    paymentInstrument.setCreditCardExpirationMonth(formInfo.expirationMonth);
                    paymentInstrument.setCreditCardExpirationYear(formInfo.expirationYear);
                    paymentInstrument.setCreditCardToken(tokenResponse.customerPaymentProfileId);
                });

                res.json({
                    success: true,
                    redirectUrl: URLUtils.url('PaymentInstruments-List').toString()
                });
            } else {
                res.json({
                    success: false,
                    errorMessage: tokenResponse.responseMsg
                });
            }
        });
    } else {
        res.json({
            success: false,
            fields: formErrors.getFormErrors(paymentForm)
        });
    }
    return next();
});

server.replace('DeletePayment', userLoggedIn.validateLoggedInAjax, function (req, res, next) {
    var array = require('*/cartridge/scripts/util/array');
    var DeletePaymentProfile = require('*/cartridge/models/deletePaymentProfile.js');

    var data = res.getViewData();
    if (data && !data.loggedin) {
        res.json();
        return next();
    }

    var UUID = req.querystring.UUID;
    var paymentInstruments = req.currentCustomer.wallet.paymentInstruments;
    var paymentToDelete = array.find(paymentInstruments, function (item) {
        return UUID === item.UUID;
    });
    res.setViewData(paymentToDelete);
    this.on('route:BeforeComplete', function () { // eslint-disable-line no-shadow
        var CustomerMgr = require('dw/customer/CustomerMgr');
        var Transaction = require('dw/system/Transaction');


        var payment = res.getViewData();
        var customer = CustomerMgr.getCustomerByCustomerNumber(
            req.currentCustomer.profile.customerNo
        );
        var params = {};
        params.authorizeProfileID = customer.getProfile().custom.authorizeProfileID;
        params.paymentID = payment.raw.getCreditCardToken();
        var deletePaymentTokenResponse = new DeletePaymentProfile(params);
        if (!deletePaymentTokenResponse.error) {
            var wallet = customer.getProfile().getWallet();
            Transaction.wrap(function () {
                wallet.removePaymentInstrument(payment.raw);
            }); // Send account edited email

            if (wallet.getPaymentInstruments().length === 0) {
                res.json({
                    success: true,
                    UUID: UUID,
                    message: Resource.msg('msg.no.saved.payments', 'payment', null)
                });
            } else {
                res.json({ UUID: UUID, success: true });
            }
        } else {
            res.json({
                success: false,
                message: deletePaymentTokenResponse.responseMsg
            });
        }
    });

    return next();
});

server.append('AddPayment', function (req, res, next) {
    res.setViewData({
        breadcrumbs: [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('page.title.myaccount', 'account', null),
                url: URLUtils.abs('Account-Show').toString()
            },
            {
                htmlValue: Resource.msg('page.heading.payments', 'payment', null)
            }
        ]
    });
    next();
});


module.exports = server.exports();
