'use strict';

var server = require('server');

var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Site = require('dw/system/Site');
var HashSet = require('dw/util/HashSet');

var cache = require('*/cartridge/scripts/middleware/cache');
var recaptcha = require('*/cartridge/scripts/services/recaptcha');
var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
var formErrors = require('*/cartridge/scripts/formErrors');

server.get('RMARequest', function (req, res, next) {
    var rmaForm = server.forms.getForm('rma');
    var breadcrumbs = [
        {
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('breadcrumb.text.customerservice', 'customerService', null),
            url: URLUtils.url('CustomerService-Show').toString()
        },
        {
            htmlValue: Resource.msg('breadcrumb.text.rmarequest', 'customerService', null)
        }
    ];
    res.render('customerservice/rmaRequest', {
        rmaForm: rmaForm,
        breadcrumbs: breadcrumbs
    });
    next();
});

server.post('ReturnItemRequest', function (req, res, next) {
    var rmaForm = server.forms.getForm('rma');

    var result = recaptcha.validate(req);
    if (result !== null && result.success) {
        var OrderMgr = require('dw/order/OrderMgr');
        var collections = require('*/cartridge/scripts/util/collections');
        var order = OrderMgr.getOrder(rmaForm.orderNumber.value);

        if (order === null) {
            rmaForm.orderNumber.valid = false;
            rmaForm.orderNumber.error = Resource.msgf('error.message.rma.oredernotfound', 'customerService', null, rmaForm.orderNumber.value);
            rmaForm.valid = false;
            res.json({
                success: false,
                message: Resource.msg('error.message.rma.fielderror', 'customerService', null),
                fields: formErrors.getFormErrors(rmaForm)
            });
            return next();
        }

        if (order !== null && order.customerEmail !== rmaForm.email.value) {
            rmaForm.email.valid = false;
            rmaForm.email.error = Resource.msg('error.message.rma.email.mismatch', 'customerService', null);
            rmaForm.valid = false;
            res.json({
                success: false,
                message: Resource.msg('error.message.rma.fielderror', 'customerService', null),
                fields: formErrors.getFormErrors(rmaForm)
            });
            return next();
        }

        var productLineItems = order.productLineItems;
        var ItemIDSet = new HashSet();
        if (order !== null && productLineItems.length > 0) {
            for (var i = 0; i < productLineItems.length; i++) {
                ItemIDSet.add(productLineItems[i].productID);
            }
        }
        var itemNumberOne = rmaForm.itemNumberOne.value;
        if (!ItemIDSet.isEmpty() && itemNumberOne && !ItemIDSet.contains(itemNumberOne)) {
            rmaForm.itemNumberOne.valid = false;
            rmaForm.itemNumberOne.error = Resource.msg('error.message.rma.itemNumber.mismatch', 'customerService', null);
            rmaForm.valid = false;
        }
        var itemNumberTwo = rmaForm.itemNumberTwo.value;
        if (!ItemIDSet.isEmpty() && itemNumberTwo && !ItemIDSet.contains(itemNumberTwo)) {
            rmaForm.itemNumberTwo.valid = false;
            rmaForm.itemNumberTwo.error = Resource.msg('error.message.rma.itemNumber.mismatch', 'customerService', null);
            rmaForm.valid = false;
        }
        var itemNumberThree = rmaForm.itemNumberThree.value;
        if (!ItemIDSet.isEmpty() && itemNumberThree && !ItemIDSet.contains(itemNumberThree)) {
            rmaForm.itemNumberThree.valid = false;
            rmaForm.itemNumberThree.error = Resource.msg('error.message.rma.itemNumber.mismatch', 'customerService', null);
            rmaForm.valid = false;
        }
        var itemNumberFour = rmaForm.itemNumberFour.value;
        if (!ItemIDSet.isEmpty() && itemNumberFour && !ItemIDSet.contains(itemNumberFour)) {
            rmaForm.itemNumberFour.valid = false;
            rmaForm.itemNumberFour.error = Resource.msg('error.message.rma.itemNumber.mismatch', 'customerService', null);
            rmaForm.valid = false;
        }
        var itemNumberFive = rmaForm.itemNumberFive.value;
        if (!ItemIDSet.isEmpty() && itemNumberFive && !ItemIDSet.contains(itemNumberFive)) {
            rmaForm.itemNumberFive.valid = false;
            rmaForm.itemNumberFive.error = Resource.msg('error.message.rma.itemNumber.mismatch', 'customerService', null);
            rmaForm.valid = false;
        }

        if (rmaForm.valid) {
            var itemNumbers = '';
            var setSize = ItemIDSet.size() - 1;
            collections.forEach(ItemIDSet, function (itemID, index) {
                if (index === setSize) {
                    itemNumbers += itemID;
                } else {
                    itemNumbers += itemID + ', ';
                }
            });

            var objectForEmail = {
                email: rmaForm.email.value,
                phone: rmaForm.phone.value,
                orderNumber: rmaForm.orderNumber.value,
                reasonForReturn: rmaForm.reasonForReturn.value,
                itemNumbers: itemNumbers
            };

            var emailObj = {
                to: Site.current.getCustomPreferenceValue('rmaCustomerServiceEmail') || '',
                subject: Resource.msg('subject.rma.return.request', 'customerService', null),
                from: rmaForm.email.value,
                type: emailHelpers.emailTypes.rmaRequest
            };

            emailHelpers.sendEmail(emailObj, 'customerservice/rmaRequestEmail', objectForEmail);

            res.json({
                success: true,
                redirectUrl: URLUtils.url('CustomerService-RMASuccess').toString()
            });
        } else {
            res.json({
                success: false,
                message: Resource.msg('error.message.rma.fielderror', 'customerService', null),
                fields: formErrors.getFormErrors(rmaForm)
            });
        }
    } else {
        res.json({
            success: false,
            message: Resource.msg('error.message.reccaptcha.failed', 'customerService', null)
        });
    }
    return next();
});

server.get('Contact', function (req, res, next) {
    var contactUsForm = server.forms.getForm('contactUs');
    var csConcernsJSON = {};
    var cscIDArray = [];

    var cscCustomObjectList = CustomObjectMgr.getAllCustomObjects('CustomerServiceConcerns').asList();

    cscIDArray[0] = Resource.msg('select.text.selecttopic', 'customerService', null);
    for (var i = 0; i < cscCustomObjectList.length; i++) {
        csConcernsJSON[cscCustomObjectList[i].custom.ID] = cscCustomObjectList[i].custom.displayName;
        cscIDArray[i + 1] = cscCustomObjectList[i].custom.ID;
    }

    var breadcrumbs = [
        {
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('breadcrumb.text.customerservice', 'customerService', null),
            url: URLUtils.url('CustomerService-Show').toString()
        },
        {
            htmlValue: Resource.msg('breadcrumb.text.contactus', 'customerService', null)
        }
    ];
    res.render('customerservice/contactUsForm', {
        contactUsForm: contactUsForm,
        csConcernsJSON: csConcernsJSON,
        cscIDArray: cscIDArray,
        breadcrumbs: breadcrumbs
    });
    next();
});

server.post('SubmitRequest', function (req, res, next) {
    var contactUsForm = server.forms.getForm('contactUs');

    if (contactUsForm.valid) {
        var result = recaptcha.validate(req);
        if (result !== null && result.success) {
            var topicID = contactUsForm.concerns.value;
            var cscCustomDefinition = CustomObjectMgr.getCustomObject('CustomerServiceConcerns', topicID);

            var objectForEmail = {
                topic: cscCustomDefinition.custom.displayName,
                message: contactUsForm.message.value
            };

            var emailObj = {
                to: cscCustomDefinition.custom.email,
                subject: cscCustomDefinition.custom.displayName,
                from: contactUsForm.email.value,
                type: emailHelpers.emailTypes.contactUs
            };

            emailHelpers.sendEmail(emailObj, 'customerservice/contactUsEmail', objectForEmail);

            res.json({
                success: true,
                message: Resource.msg('success.message.contactus.submitted', 'customerService', null),
                redirectUrl: URLUtils.url('CustomerService-Submit').toString()
            });
        } else {
            res.json({
                success: false,
                message: Resource.msg('error.message.reccaptcha.failed', 'customerService', null)
            });
        }
    } else {
        res.json({
            success: false,
            fields: formErrors.getFormErrors(contactUsForm)
        });
    }
    next();
});

server.get('Show', cache.applyDefaultCache, function (req, res, next) {
    var breadcrumbs = [
        {
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('breadcrumb.text.customerservice', 'customerService', null)
        }
    ];
    res.render('customerservice/customerService', {
        breadcrumbs: breadcrumbs
    });
    next();
});

server.get('RMASuccess', function (req, res, next) {
    res.render('customerservice/rmaSuccess');
    next();
});

server.get('Submit', function (req, res, next) {
    res.render('customerservice/contactusSuccess');
    next();
});
module.exports = server.exports();
