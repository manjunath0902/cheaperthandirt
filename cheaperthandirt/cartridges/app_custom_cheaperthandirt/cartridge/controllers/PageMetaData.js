/**
 * Controller to generate page meta tags.
 *
 * @module  controllers/PageMetaData
 */
'use strict';

var server = require('server');

var ContentMgr = require('dw/content/ContentMgr');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var ProductMgr = require('dw/catalog/ProductMgr');
var Resource = require('dw/web/Resource');
var Site = require('dw/system/Site');

var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var cache = require('*/cartridge/scripts/middleware/cache');

server.get('IncludeMetaTags', cache.applyDefaultCache, function (req, res, next) {
    var action = req.querystring.action;
    var apiContent = {};
    switch (action) {
        case 'Home-Show':
            apiContent = Site.getCurrent();
            break;

        case 'Search-Show':
            if (req.querystring.cgid) {
                apiContent = CatalogMgr.getCategory(req.querystring.cgid);
            }
            break;

        case 'Product-Show':
            if (req.querystring.pid) {
                apiContent = ProductMgr.getProduct(req.querystring.pid);
            }
            break;

        case 'Page-Show':
            if (req.querystring.cid) {
                apiContent = ContentMgr.getContent(req.querystring.cid);
            }
            break;
        case 'Stores-FFLByCity':
            apiContent.pageTitle = Resource.msgf('ffl.dealers.in.capital', 'storeLocator', null, (req.querystring.city + ', ' + req.querystring.stateCode)) + ' | Cheaper Than Dirt';
            apiContent.pageDescription = Resource.msgf('ffl.dealers.in.capital', 'storeLocator', null, (req.querystring.city + ', ' + req.querystring.stateCode)) + ' | Cheaper Than Dirt';
            break;
        case 'Stores-FFLByState':
            apiContent.pageTitle = Resource.msgf('ffl.dealers.in.capital', 'storeLocator', null, Resource.msg('state.us.' + req.querystring.stateCode, 'common', null)) + ' | Cheaper Than Dirt';
            apiContent.pageDescription = Resource.msgf('ffl.dealers.in.capital', 'storeLocator', null, Resource.msg('state.us.' + req.querystring.stateCode, 'common', null)) + ' | Cheaper Than Dirt';
            break;
        default:
            apiContent = ContentMgr.getContent('seo-' + action.toLowerCase());
    }

    if (apiContent) {
        var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
        pageMetaHelper.setPageMetaData(req.pageMetaData, apiContent);
        pageMetaHelper.setPageMetaTags(req.pageMetaData, apiContent);
    }
    res.render('seo/pageMetaData', {
        metaRemoteInclude: req.querystring.metaRemoteInclude
    });
    next();
}, pageMetaData.computedPageMetaData);

module.exports = server.exports();
