'use strict';

var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var URLUtils = require('dw/web/URLUtils');

server.extend(module.superModule);

/**
 * Gets the Ship to Home Shipment
 * @param {req} req - The request obj
 * @param {res} res - The response obj
 * @param {next} next - The next obj
 */
function calculateShipRateOnLoad(req, res, next) {
    var customer = req.currentCustomer.raw;
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var BasketMgr = require('dw/order/BasketMgr');
    var HookMgr = require('dw/system/HookMgr');
    var currentBasket = BasketMgr.getCurrentBasket();
    var shipToHomeShipment = COHelpers.getShipToHomeShipment(currentBasket);
    try {
        // checking shipping address so that this call is made only once on page load and on the first load
        // subsequent calls to get udpated shiprate is called during update of state code
        if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingAddress === null) {
            if (customer.authenticated && customer.addressBook.preferredAddress) {
                COHelpers.calculateUPSShipRate(currentBasket, customer.addressBook.preferredAddress);
                HookMgr.callHook('dw.order.calculate', 'calculate', currentBasket);
            }
        }
    } catch (e) {
        // empty block
    }
    next();
}


// Main entry point for Checkout
server.replace(
    'Begin',
    server.middleware.https,
    csrfProtection.generateToken,
    calculateShipRateOnLoad,
    function (req, res, next) {
        try {
            var BasketMgr = require('dw/order/BasketMgr');
            var TotalsModel = require('*/cartridge/models/totals');
            var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
            var GiftCardsModel = require('*/cartridge/models/giftcards');
            var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
            var ShippingMgr = require('dw/order/ShippingMgr');
            var Transaction = require('dw/system/Transaction');
            var AccountModel = require('*/cartridge/models/account');
            var OrderModel = require('*/cartridge/models/order');
            var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
            var Locale = require('dw/util/Locale');
            var collections = require('*/cartridge/scripts/util/collections');
            var currentBasket = BasketMgr.getCurrentBasket();
            var totalsModel = new TotalsModel(currentBasket);
            var giftCards = new GiftCardsModel(currentBasket, totalsModel);
            var CustomerModel = require('*/cartridge/models/customer.js');
            var customerObj = new CustomerModel(req.currentCustomer.raw);
            var Site = require('dw/system/Site');
            if (!currentBasket) {
                res.redirect(URLUtils.url('Cart-Show'));
                return next();
            }

            var actionUrls = {
                submitCouponCodeUrl: URLUtils.url('Cart-AddCoupon', 'checkout', true).toString(),
                removeCouponLineItem: URLUtils.url('Cart-RemoveCouponLineItem', 'checkout', true).toString(),
                submitGiftCardUrl: URLUtils.url('GiftCard-AddGiftCard').toString(),
                removeGiftCardUrl: URLUtils.url('GiftCard-RemoveGiftCard').toString()
            };
            var storeShipment = null;
            storeShipment = COHelpers.onlyStoreShipment(currentBasket.getShipments());
            var shipToHomeShipment = COHelpers.getShipToHomeShipment(currentBasket);

            var customer = req.currentCustomer.raw;

            var customerEmail = '';
            if (currentBasket.getCustomerEmail()) {
                customerEmail = currentBasket.getCustomerEmail();
            } else if (customer.authenticated) {
                customerEmail = customer.profile.email;
            }
            var hasGCInCart = false;
            var onlyGCInCart = false;
            var piLength = 0;

            // perform a check to see if free shipping method is eligible
            var eligibleForFreeShipping = COHelpers.checkFreeShippingEligibility(currentBasket);

            // this method is called to ensure the shipping method and shipping address on store-shipment is correctly retained
            COHelpers.ensureStoreShipmentHasValidAddr(currentBasket);

            // get the length of the payment instruemnts of customer.
            if (customer && customer.authenticated) {
                var wallet = customer.getProfile().getWallet();
                if (wallet) {
                    piLength = wallet.getPaymentInstruments().getLength();
                }
            }

            var requireCCPayment = COHelpers.validateIfOrderRequirePayment(currentBasket);
            hasGCInCart = COHelpers.hasGiftCardInCart(currentBasket.getProductLineItems());

            onlyGCInCart = COHelpers.checkOnlyGiftCardInCart(currentBasket.getProductLineItems());

            var currentStage = req.querystring.stage ? req.querystring.stage : 'shipping';

            var onlyGCShippingMessage = Site.getCurrent().getCustomPreferenceValue('onlyElectronicShipMsg');

            // If there cart is only gift card purchase, then set the step to payment as shipping would be default address added.
            if (onlyGCInCart) {
                currentStage = 'payment';
            }
            if ((shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingAddress === null) || (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingAddress !== null && shipToHomeShipment.shippingAddress.address1 === null)) {
                currentStage = 'shipping';
            }
            if (!onlyGCInCart && shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingAddress !== null && shipToHomeShipment.shippingAddress !== undefined && shipToHomeShipment.shippingAddress.address2 !== null && shipToHomeShipment.shippingAddress.address2 !== undefined && shipToHomeShipment.shippingAddress.address2.indexOf('CTD') > -1) {
                if (hasGCInCart && currentBasket.getShipments().length === 1) {
                    currentStage = 'shipping';
                }
            }
            if (shipToHomeShipment !== null && shipToHomeShipment !== undefined) {
                var onlyGCInShipToHomeShipment = COHelpers.checkOnlyGiftCardInCart(shipToHomeShipment.getProductLineItems());
                if (onlyGCInShipToHomeShipment) {
                    Transaction.wrap(function () {
                        ShippingHelper.selectShippingMethod(shipToHomeShipment, 'Electronic');
                    });
                }
            }
            // Check if the shipment has mixed products and a shipping method other than electronic
            if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod !== undefined && shipToHomeShipment.shippingMethod.ID === 'Electronic' && !onlyGCInCart) {
                var onlyGCInShipToHome = COHelpers.checkOnlyGiftCardInCart(shipToHomeShipment.getProductLineItems());
                if (!onlyGCInShipToHome) {
                    Transaction.wrap(function () {
                        ShippingHelper.selectShippingMethod(shipToHomeShipment, ShippingMgr.getDefaultShippingMethod().ID);
                    });
                }
            }
            // Check if the shipment has free shipping method but the customer is not eligible for free shipping
            var freeShippingMethodID = Site.getCurrent().getCustomPreferenceValue('freeShippingMethodID');
            if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod !== undefined && shipToHomeShipment.shippingMethod.ID === freeShippingMethodID) {
                if (!eligibleForFreeShipping) {
                    Transaction.wrap(function () {
                        ShippingHelper.selectShippingMethod(shipToHomeShipment, ShippingMgr.getDefaultShippingMethod().ID);
                    });
                }
            }
            // Default the shipment shippingmethod to default if the state code is not AK or HI but the ship method was of AK or HI
            var shipMethodsForAKHI = Site.getCurrent().getCustomPreferenceValue('shipMethodsForAKHI');
            var standardMethodForAKHI = Site.getCurrent().getCustomPreferenceValue('standardMethodForAKHI');
            var shipArr = shipMethodsForAKHI.split('|');
            var isARHIMethod = false;
            if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod !== undefined) {
                for (var i = 0; i < shipArr.length; i++) {
                    if (shipArr[i] === shipToHomeShipment.shippingMethod.ID) {
                        isARHIMethod = true;
                        break;
                    }
                }
            }
            if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingAddress === null) {
                if (customer.authenticated) {
                    if (customerObj.addressBook && customerObj.addressBook.preferredAddress) {
                        var preferredAdd = customerObj.addressBook.preferredAddress;
                        if (preferredAdd && preferredAdd.stateCode !== undefined && preferredAdd.stateCode !== null) {
                            session.custom.shippingStateCode = preferredAdd.stateCode; // eslint-disable-line
                        }
                    }
                }
            }
            var allowedStateCode;
            if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod !== undefined && isARHIMethod) {
                allowedStateCode = session.custom.shippingStateCode !== null && session.custom.shippingStateCode !== undefined ? session.custom.shippingStateCode : ''; // eslint-disable-line
                if (!(allowedStateCode.indexOf('AK') > -1 || allowedStateCode.indexOf('HI') > -1)) {
                    Transaction.wrap(function () {
                        ShippingHelper.selectShippingMethod(shipToHomeShipment, ShippingMgr.getDefaultShippingMethod().ID);
                    });
                }
            }
            // Default the shipment shippingmethod to default if the state code is not AK or HI but the ship method was of AK or HI
            if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod !== undefined && !isARHIMethod) {
                allowedStateCode = session.custom.shippingStateCode !== null && session.custom.shippingStateCode !== undefined ? session.custom.shippingStateCode : ''; // eslint-disable-line
                if (allowedStateCode.indexOf('AK') > -1 || allowedStateCode.indexOf('HI') > -1) {
                    Transaction.wrap(function () {
                        ShippingHelper.selectShippingMethod(shipToHomeShipment, standardMethodForAKHI);
                    });
                }
            }
            // Check if the shipment has postal shipping method but the customer is not eligible for postal shipping
            var postalShippingMethodID = Site.getCurrent().getCustomPreferenceValue('postalShippingMethodID');
            if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod !== undefined && shipToHomeShipment.shippingMethod.ID === postalShippingMethodID) {
                var eligibleForPostalShipping = COHelpers.checkPostalShippingEligibility(currentBasket);
                if (!eligibleForPostalShipping) {
                    Transaction.wrap(function () {
                        ShippingHelper.selectShippingMethod(shipToHomeShipment, ShippingMgr.getDefaultShippingMethod().ID);
                    });
                }
            }
            if (shipToHomeShipment !== null && shipToHomeShipment !== undefined && shipToHomeShipment.shippingAddress !== null) {
                var address = {};
                address.stateCode = shipToHomeShipment.shippingAddress.stateCode;
                address.postalCode = shipToHomeShipment.shippingAddress.postalCode;
                var restrictHelper = require('*/cartridge/scripts/cart/restrictHelper.js');
                var restrictResponse = restrictHelper.validateRestrictGroupAndItem(address, currentBasket);
                if (restrictResponse.hasRestrictedItems) {
                    Transaction.wrap(function () {
                        ShippingHelper.selectShippingMethod(shipToHomeShipment, ShippingMgr.getDefaultShippingMethod().ID);
                    });
                }
            }

            // Do not allow the customer to make gift card purchase using gift card.
            if (hasGCInCart) {
                COHelpers.removeGiftCardPaymentInstruments(currentBasket);
            }

            res.setViewData({
                piLength: piLength,
                actionUrls: actionUrls,
                totals: totalsModel,
                giftCards: giftCards,
                onlyStoreShipment: storeShipment,
                customerEmail: customerEmail,
                currentStage: currentStage,
                hasGCInCart: hasGCInCart,
                onlyGCInCart: onlyGCInCart,
                onlyGCShippingMessage: onlyGCShippingMessage,
                requireCCPayment: requireCCPayment
            });

            var billingAddress = currentBasket.billingAddress;

            var currentCustomer = req.currentCustomer.raw;
            var currentLocale = Locale.getLocale(req.locale.id);
            var preferredAddress;

            // only true if customer is registered
            if (customerObj.addressBook && customerObj.addressBook.preferredAddress) {
                var shipments = currentBasket.shipments;
                preferredAddress = customerObj.addressBook.preferredAddress;

                collections.forEach(shipments, function (shipment) {
                    if (!shipment.shippingAddress) {
                        COHelpers.copyCustomerAddressToShipment(preferredAddress, shipment);
                        session.custom.shippingStateCode = preferredAddress.stateCode; // eslint-disable-line
                    }
                });

                if (!billingAddress) {
                    COHelpers.copyCustomerAddressToBilling(preferredAddress);
                }
            }

            // Calculate the basket
            Transaction.wrap(function () {
                COHelpers.ensureNoEmptyShipments(req);
            });

            if (currentBasket.shipments.length <= 1) {
                req.session.privacyCache.set('usingMultiShipping', false);
            }

            if (currentBasket.currencyCode !== req.session.currency.currencyCode) {
                Transaction.wrap(function () {
                    currentBasket.updateCurrency();
                });
            }

            COHelpers.recalculateBasket(currentBasket);

            var shippingForm = COHelpers.prepareShippingForm(currentBasket);
            var billingForm = COHelpers.prepareBillingForm(currentBasket);
            var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');

            if (preferredAddress) {
                shippingForm.copyFrom(preferredAddress);
                billingForm.copyFrom(preferredAddress);
            }

            // Loop through all shipments and make sure all are valid
            var allValid = COHelpers.ensureValidShipments(currentBasket);

            var orderModel = new OrderModel(
                currentBasket,
                {
                    customer: currentCustomer,
                    usingMultiShipping: usingMultiShipping,
                    shippable: allValid,
                    countryCode: currentLocale.country,
                    containerView: 'basket'
                }
            );

            // Get rid of this from top-level ... should be part of OrderModel???
            var currentYear = new Date().getFullYear();
            var creditCardExpirationYears = [];

            for (var j = 0; j < 10; j++) {
                creditCardExpirationYears.push(currentYear + j);
            }

            var accountModel = new AccountModel(req.currentCustomer);

            var reportingURLs;
            reportingURLs = reportingUrlsHelper.getCheckoutReportingURLs(
                currentBasket.UUID,
                2,
                'Shipping'
            );

            res.render('checkout/checkout', {
                order: orderModel,
                customer: accountModel,
                customerObj: customerObj,
                forms: {
                    shippingForm: shippingForm,
                    billingForm: billingForm
                },
                expirationYears: creditCardExpirationYears,
                reportingURLs: reportingURLs
            });
            return next();
        } catch (e) {
            response.redirect(URLUtils.url('Cart-Show')); // eslint-disable-line
            return; // eslint-disable-line
        }
    }
);


module.exports = server.exports();
