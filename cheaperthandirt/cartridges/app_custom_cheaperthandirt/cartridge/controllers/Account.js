'use strict';

var server = require('server');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var giveUserPointsAPI = require('int_annexcloud/cartridge/scripts/loyaltyprogram/givePointsAPI');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');

server.extend(module.superModule);

/**
 * Creates an account model for the current customer
 * @param {Object} req - local instance of request object
 * @returns {Object} a plain object of the current customer's account
 */
function getModel(req) {
    var OrderMgr = require('dw/order/OrderMgr');
    var Order = require('dw/order/Order');
    var AccountModel = require('*/cartridge/models/account');
    var AddressModel = require('*/cartridge/models/address');
    var OrderModel = require('*/cartridge/models/order');
    var Locale = require('dw/util/Locale');

    var orderModel;
    var preferredAddressModel;

    if (!req.currentCustomer.profile) {
        return null;
    }

    var customerNo = req.currentCustomer.profile.customerNo;
    var Site = require('dw/system/Site');
    var showOnlySFCCOrders = Site.current.getCustomPreferenceValue('showOnlySFCCOrders');
    var customerOrders;
    if (showOnlySFCCOrders) {
        customerOrders = OrderMgr.searchOrders(
                'customerNo={0} AND status!={1} AND status!={2} AND (custom.legacyOrder= false)',
                'creationDate desc',
                customerNo,
                Order.ORDER_STATUS_REPLACED,
                Order.ORDER_STATUS_FAILED
            );
    } else {
        customerOrders = OrderMgr.searchOrders(
                'customerNo={0} AND status!={1} AND status!={2}',
                'creationDate desc',
                customerNo,
                Order.ORDER_STATUS_REPLACED,
                Order.ORDER_STATUS_FAILED
           );
    }
    var order = customerOrders.first();

    if (order) {
        var currentLocale = Locale.getLocale(req.locale.id);

        var config = {
            numberOfLineItems: 'single'
        };

        try {
            orderModel = new OrderModel(order, {
                config: config,
                countryCode: currentLocale.country
            });
        } catch (e) {
            orderModel = null;
        }
    } else {
        orderModel = null;
    }

    if (req.currentCustomer.addressBook.preferredAddress) {
        preferredAddressModel = new AddressModel(req.currentCustomer.addressBook.preferredAddress);
    } else {
        preferredAddressModel = null;
    }

    return new AccountModel(req.currentCustomer, preferredAddressModel, orderModel);
}

server.replace('Show', server.middleware.https, userLoggedIn.validateLoggedIn, consentTracking.consent, function (req, res, next) {
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var Transaction = require('dw/system/Transaction');
    var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
    var wishListAccount = require('*/cartridge/models/account/wishListAccount');
    var productListMgr = require('dw/customer/ProductListMgr');
    var accountHepler = require('*/cartridge/scripts/helpers/accountHelpers');
    var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
    var reportingURLs;
    // Get reporting event Account Open url
    if (req.querystring.registration && req.querystring.registration === 'submitted') {
        reportingURLs = reportingUrlsHelper.getAccountOpenReportingURLs(
            CustomerMgr.registeredCustomerCount
        );
    }
    var accountModel = getModel(req);
    var wishlist;
    var apiWishList;
    if (!productListMgr.getProductLists(req.currentCustomer.raw, '10').empty) {
        apiWishList = productListMgr.getProductLists(req.currentCustomer.raw, '10')[0];
        wishListAccount(accountModel, apiWishList);
        wishlist = {
            UUID: apiWishList.ID
        };
    }
    var customer = req.currentCustomer.raw;
    var lastModified = customer.profile.custom.loyalityLastUpdatedTime;
    var expr = accountHepler.calculateIfExpired(lastModified);
    var today = new Date();
    if (expr) {
        var result = giveUserPointsAPI.createUser(accountModel.profile.email, '', '', 'GET');
        if (result.ok) {
            var resultobj = JSON.parse(result.object);
            Transaction.wrap(function () {
                customer.profile.custom.used_points = resultobj.data.available_points;
                customer.profile.custom.lifetime_points = resultobj.data.lifetime_points;
                customer.profile.custom.loyalty_id = resultobj.data.loyalty_id;
                customer.profile.custom.loyality_active = resultobj.data.active;
                customer.profile.custom.loyalityLastUpdatedTime = today;
            });
        }
    }
    var wallet = customer.getProfile().getWallet();
    var piLength;
    if (wallet) {
        piLength = wallet.getPaymentInstruments().getLength();
    }

    var preferredStoreID = storeHelpers.getPreferredStore(req);
    var preferredStore = null;
    if (preferredStoreID !== null) {
        var StoreMgr = require('dw/catalog/StoreMgr');
        preferredStore = StoreMgr.getStore(preferredStoreID);
    }
    res.render('account/accountDashboard', {
        account: accountModel,
        piLength: piLength,
        wishlistItems: (typeof apiWishList !== 'undefined' && !apiWishList.items.empty) ? apiWishList.items.length : '',
        accountlanding: true,
        breadcrumbs: [{
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('page.title.myaccount', 'account', null)
        }
        ],
        reportingURLs: reportingURLs,
        socialLinks: true,
        wishlist: wishlist,
        usedPoints: customer.profile.custom.used_points ? customer.profile.custom.used_points : '',
        lifetimePoints: customer.profile.custom.lifetime_points ? customer.profile.custom.lifetime_points : '',
        preferredStore: preferredStore
    });
    next();
});

server.append('EditProfile', function (req, res, next) {
    res.setViewData({
        breadcrumbs: [{
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('page.title.myaccount', 'account', null),
            url: URLUtils.abs('Account-Show').toString()
        },
        {
            htmlValue: Resource.msg('label.profile.edit', 'account', null)
        }
        ]
    });
    next();
});

server.append('EditPassword', function (req, res, next) {
    res.setViewData({
        breadcrumbs: [{
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('page.title.myaccount', 'account', null),
            url: URLUtils.abs('Account-Show').toString()
        },
        {
            htmlValue: Resource.msg('label.profile.editpassword', 'account', null)
        }
        ]
    });
    next();
});

server.replace(
    'Login',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var CustomerMgr = require('dw/customer/CustomerMgr');
        var Site = require('dw/system/Site');
        var Transaction = require('dw/system/Transaction');

        var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
        var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');
        var hooksHelper = require('*/cartridge/scripts/helpers/hooks');

        var email = req.form.loginEmail;
        var password = req.form.loginPassword;
        var rememberMe = req.form.loginRememberMe ?
            (!!req.form.loginRememberMe) :
            false;

        var customerLoginResult = Transaction.wrap(function () {
            var authenticateCustomerResult = CustomerMgr.authenticateCustomer(email, password);

            if (authenticateCustomerResult.status !== 'AUTH_OK') {
                var errorCodes = {
                    ERROR_CUSTOMER_DISABLED: 'error.message.account.disabled',
                    ERROR_CUSTOMER_LOCKED: 'error.message.account.locked',
                    ERROR_CUSTOMER_NOT_FOUND: 'error.message.login.form',
                    ERROR_PASSWORD_EXPIRED: 'error.message.password.expired',
                    ERROR_PASSWORD_MISMATCH: 'error.message.password.mismatch',
                    ERROR_UNKNOWN: 'error.message.error.unknown',
                    default: 'error.message.login.form'
                };

                var errorMessageKey = errorCodes[authenticateCustomerResult.status] || errorCodes.default;
                var errorMessage = Resource.msg(errorMessageKey, 'login', null);

                return {
                    error: true,
                    errorMessage: errorMessage,
                    status: authenticateCustomerResult.status,
                    authenticatedCustomer: null
                };
            }

            return {
                error: false,
                errorMessage: null,
                status: authenticateCustomerResult.status,
                authenticatedCustomer: CustomerMgr.loginCustomer(authenticateCustomerResult, rememberMe)
            };
        });

        if (customerLoginResult.error) {
            if (customerLoginResult.status === 'ERROR_CUSTOMER_LOCKED') {
                var context = {
                    customer: CustomerMgr.getCustomerByLogin(email) || null
                };

                var emailObj = {
                    to: email,
                    subject: Resource.msg('subject.account.locked.email', 'login', null),
                    from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
                    type: emailHelpers.emailTypes.accountLocked
                };

                hooksHelper('app.customer.email', 'sendEmail', [emailObj, 'account/accountLockedEmail', context], function () {});
            }

            res.json({
                error: [customerLoginResult.errorMessage || Resource.msg('error.message.login.form', 'login', null)]
            });

            return next();
        }
        var customerLastVisit = customerLoginResult.authenticatedCustomer.profile.custom.loyalityLastUpdatedTime;
        var annexLastVisitExpired = accountHelpers.calculateAnnexLastVisitExpired(customerLastVisit);
        if (customerLoginResult.authenticatedCustomer) {
            var listGuest = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
            var listLoggedIn = productListHelper.getList(customerLoginResult.authenticatedCustomer, { type: 10 });
            productListHelper.mergelists(listLoggedIn, listGuest, req, { type: 10 });
            productListHelper.updateWishlistPrivacyCache(req.currentCustomer.raw, req, { type: 10 });
            res.setViewData({
                authenticatedCustomer: customerLoginResult.authenticatedCustomer
            });
            res.json({
                success: true,
                redirectUrl: accountHelpers.getLoginRedirectURL(req.querystring.rurl, req.session.privacyCache, false),
                lastVisitExpires: annexLastVisitExpired,
                customerEmail: customerLoginResult.authenticatedCustomer.profile.email,
                customerFirstName: customerLoginResult.authenticatedCustomer.profile.firstName,
                customerLastName: customerLoginResult.authenticatedCustomer.profile.lastName,
                actionID: Site.current.getCustomPreferenceValue('visitSiteActionID')
            });
            req.session.privacyCache.set('rememberMe', rememberMe);
            req.session.privacyCache.set('args', null);
        } else {
            res.json({
                error: [Resource.msg('error.message.login.form', 'login', null)]
            });
        }

        return next();
    }
);

server.replace(
    'SubmitRegistration',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var CustomerMgr = require('dw/customer/CustomerMgr');

        var formErrors = require('*/cartridge/scripts/formErrors');

        var registrationForm = server.forms.getForm('profile');

        var Site = require('dw/system/Site');
        // form validation
        if (registrationForm.customer.email.value.toLowerCase() !==
            registrationForm.customer.emailconfirm.value.toLowerCase()
        ) {
            registrationForm.customer.email.valid = false;
            registrationForm.customer.emailconfirm.valid = false;
            registrationForm.customer.emailconfirm.error =
                Resource.msg('error.message.mismatch.email', 'forms', null);
            registrationForm.valid = false;
        }

        if (registrationForm.login.password.value !==
            registrationForm.login.passwordconfirm.value
        ) {
            registrationForm.login.password.valid = false;
            registrationForm.login.passwordconfirm.valid = false;
            registrationForm.login.passwordconfirm.error =
                Resource.msg('error.message.mismatch.password', 'forms', null);
            registrationForm.valid = false;
        }

        if (!CustomerMgr.isAcceptablePassword(registrationForm.login.password.value)) {
            registrationForm.login.password.valid = false;
            registrationForm.login.passwordconfirm.valid = false;
            registrationForm.login.passwordconfirm.error =
                Resource.msg('error.message.password.constraints.not.matched', 'forms', null);
            registrationForm.valid = false;
        }

        // setting variables for the BeforeComplete function
        var registrationFormObj = {
            firstName: registrationForm.customer.firstname.value,
            lastName: registrationForm.customer.lastname.value,
            phone: registrationForm.customer.phone.value,
            email: registrationForm.customer.email.value,
            emailConfirm: registrationForm.customer.emailconfirm.value,
            password: registrationForm.login.password.value,
            passwordConfirm: registrationForm.login.passwordconfirm.value,
            validForm: registrationForm.valid,
            form: registrationForm
        };

        if (registrationForm.valid) {
            res.setViewData(registrationFormObj);

            this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
                var Transaction = require('dw/system/Transaction');
                var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');
                var authenticatedCustomer;
                var serverError;

                // getting variables for the BeforeComplete function
                var registrationForm = res.getViewData(); // eslint-disable-line

                if (registrationForm.validForm) {
                    var login = registrationForm.email;
                    var password = registrationForm.password;

                    // attempt to create a new user and log that user in.
                    try {
                        Transaction.wrap(function () {
                            var error = {};
                            var newCustomer = CustomerMgr.createCustomer(login, password);

                            var authenticateCustomerResult = CustomerMgr.authenticateCustomer(login, password);
                            if (authenticateCustomerResult.status !== 'AUTH_OK') {
                                error = {
                                    authError: true,
                                    status: authenticateCustomerResult.status
                                };
                                throw error;
                            }

                            authenticatedCustomer = CustomerMgr.loginCustomer(authenticateCustomerResult, false);

                            if (!authenticatedCustomer) {
                                error = {
                                    authError: true,
                                    status: authenticateCustomerResult.status
                                };
                                throw error;
                            } else {
                                // assign values to the profile
                                var newCustomerProfile = newCustomer.getProfile();

                                newCustomerProfile.firstName = registrationForm.firstName;
                                newCustomerProfile.lastName = registrationForm.lastName;
                                newCustomerProfile.phoneHome = registrationForm.phone;
                                newCustomerProfile.email = registrationForm.email;
                                var listGuest = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
                                var listLoggedIn = productListHelper.getList(authenticatedCustomer, { type: 10 });
                                productListHelper.mergelists(listLoggedIn, listGuest, req, { type: 10 });
                            }
                        });
                    } catch (e) {
                        if (e.authError) {
                            serverError = true;
                        } else {
                            registrationForm.validForm = false;
                            registrationForm.form.customer.email.valid = false;
                            registrationForm.form.customer.emailconfirm.valid = false;
                            registrationForm.form.customer.email.error =
                                Resource.msg('error.message.username.invalid', 'forms', null);
                        }
                    }
                }

                delete registrationForm.password;
                delete registrationForm.passwordConfirm;
                formErrors.removeFormValues(registrationForm.form);

                if (serverError) {
                    res.setStatusCode(500);
                    res.json({
                        success: false,
                        errorMessage: Resource.msg('error.message.unable.to.create.account', 'login', null)
                    });

                    return;
                }
                if (registrationForm.validForm) {
                    var customerLastVisit = authenticatedCustomer.profile.custom.loyalityLastUpdatedTime;
                    // send a registration email
                    var registeredUser = {
                        firstName: authenticatedCustomer.profile.firstName,
                        email: authenticatedCustomer.profile.email,
                        lastName: authenticatedCustomer.profile.lastName
                    };
                    if (registrationForm.form.customer.addtoemaillist.checked) {
                        registeredUser.optIn = true;
                    }
                    accountHelpers.sendCreateAccountEmail(registeredUser);

                    res.setViewData({
                        authenticatedCustomer: authenticatedCustomer
                    });
                    res.json({
                        success: true,
                        redirectUrl: accountHelpers.getLoginRedirectURL(req.querystring.rurl, req.session.privacyCache, true),
                        calculateAnnexLastVisitExpired: accountHelpers.calculateAnnexLastVisitExpired(customerLastVisit),
                        customerEmail: authenticatedCustomer.profile.email,
                        customerFirstName: authenticatedCustomer.profile.firstName,
                        customerLastName: authenticatedCustomer.profile.lastName,
                        actionID: Site.current.getCustomPreferenceValue('visitAccountActionID')

                    });
                    req.session.privacyCache.set('args', null);
                } else {
                    res.json({
                        fields: formErrors.getFormErrors(registrationForm)
                    });
                }
            });
        } else {
            res.json({
                fields: formErrors.getFormErrors(registrationForm)
            });
        }

        return next();
    }
);

server.replace('SaveNewPassword', server.middleware.https, function (req, res, next) {
    var CustomerMgr = require('dw/customer/CustomerMgr');
    var Transaction = require('dw/system/Transaction');

    var passwordForm = server.forms.getForm('newPasswords');
    var token = req.querystring.token;

    if (passwordForm.newpassword.value !== passwordForm.newpasswordconfirm.value) {
        passwordForm.valid = false;
        passwordForm.newpassword.valid = false;
        passwordForm.newpasswordconfirm.valid = false;
        passwordForm.newpasswordconfirm.error =
            Resource.msg('error.message.mismatch.newpassword', 'forms', null);
    }

    if (!CustomerMgr.isAcceptablePassword(passwordForm.newpassword.value)) {
        passwordForm.newpassword.valid = false;
        passwordForm.newpasswordconfirm.valid = false;
        passwordForm.newpasswordconfirm.error =
            Resource.msg('error.message.password.constraints.not.matched', 'forms', null);
        passwordForm.valid = false;
    }

    if (passwordForm.valid) {
        var result = {
            newPassword: passwordForm.newpassword.value,
            newPasswordConfirm: passwordForm.newpasswordconfirm.value,
            token: token,
            passwordForm: passwordForm
        };
        res.setViewData(result);
        this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
            var Site = require('dw/system/Site');
            var emailHelpers = require('*/cartridge/scripts/helpers/emailHelpers');

            var formInfo = res.getViewData();
            var status;
            var resettingCustomer;
            Transaction.wrap(function () {
                resettingCustomer = CustomerMgr.getCustomerByToken(formInfo.token);
                status = resettingCustomer.profile.credentials.setPasswordWithToken(
                    formInfo.token,
                    formInfo.newPassword
                );
            });
            if (status.error) {
                passwordForm.newpassword.valid = false;
                passwordForm.newpasswordconfirm.valid = false;
                passwordForm.newpasswordconfirm.error =
                    Resource.msg('error.message.resetpassword.invalidformentry', 'forms', null);
                res.render('account/password/newPassword', {
                    passwordForm: passwordForm,
                    token: token
                });
            } else {
                var email = resettingCustomer.profile.email;
                var url = URLUtils.https('Login-Show');
                var objectForEmail = {
                    firstName: resettingCustomer.profile.firstName,
                    lastName: resettingCustomer.profile.lastName,
                    url: url
                };

                var emailObj = {
                    to: email,
                    subject: Resource.msg('subject.profile.resetpassword.email', 'login', null),
                    from: Site.current.getCustomPreferenceValue('customerServiceEmail') || 'no-reply@salesforce.com',
                    type: emailHelpers.emailTypes.passwordReset
                };

                emailHelpers.sendEmail(emailObj, 'account/password/passwordChangedEmail', objectForEmail);
                res.redirect(URLUtils.url('Login-Show'));
            }
        });
    } else {
        res.render('account/password/newPassword', { passwordForm: passwordForm, token: token });
    }
    next();
});

server.append('Header', function (req, res, next) {
    res.setViewData({
        customerEmail: req.currentCustomer.profile ? req.currentCustomer.profile.email : ''
    });
    next();
});

module.exports = server.exports();
