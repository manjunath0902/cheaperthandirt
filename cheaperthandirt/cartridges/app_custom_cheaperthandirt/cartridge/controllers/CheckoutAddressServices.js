'use strict';

var server = require('server');
server.extend(module.superModule);

var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var Site = require('dw/system/Site');

server.replace('CreateNewAddress', server.middleware.https, function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var AccountModel = require('*/cartridge/models/account');
    var OrderModel = require('*/cartridge/models/order');
    var URLUtils = require('dw/web/URLUtils');
    var UUIDUtils = require('dw/util/UUIDUtils');
    var Locale = require('dw/util/Locale');

    var basket = BasketMgr.getCurrentBasket();
    if (!basket) {
        res.json({
            redirectUrl: URLUtils.url('Cart-Show').toString(),
            error: true
        });
        return next();
    }

    var uuid = UUIDUtils.createUUID();
    try {
        Transaction.wrap(function () {
            COHelpers.ensureNoEmptyShipments(req);
            COHelpers.recalculateBasket(basket);
        });
    } catch (err) {
        res.json({
            redirectUrl: URLUtils.url('Checkout-Begin').toString(),
            error: true
        });
        return next();
    }

    var currentLocale = Locale.getLocale(req.locale.id);

    res.json({
        uuid: uuid,
        customer: new AccountModel(req.currentCustomer),
        order: new OrderModel(basket, { countryCode: currentLocale.country, containerView: 'basket' })
    });
    return next();
});

server.replace(
    'AddNewAddress',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var BasketMgr = require('dw/order/BasketMgr');
        var Transaction = require('dw/system/Transaction');
        var AccountModel = require('*/cartridge/models/account');
        var OrderModel = require('*/cartridge/models/order');
        var UPSAddress = require('*/cartridge/models/upsAddress');
        var Resource = require('dw/web/Resource');
        var CustomerModel = require('*/cartridge/models/customer.js');
        var customerObj = new CustomerModel(req.currentCustomer.raw);


        var Locale = require('dw/util/Locale');
        var currentBasket = BasketMgr.getCurrentBasket();
        var shipToHomeShipment = COHelpers.getShipToHomeShipment(currentBasket);

        var shipmentUUID = req.form.shipmentSelector || req.form.shipmentUUID;
        var origUUID = req.form.originalShipmentUUID;

        var form = server.forms.getForm('shipping');
        var shippingFormErrors = COHelpers.validateShippingForm(form.shippingAddress.addressFields);
        var basket = BasketMgr.getCurrentBasket();
        var result = {};
        var userAddress = [];
        var message = null;
        var byPassAddressValidation = req.form.byPassAddressValidation;
        var byPassFOIDCheck = req.form.byPassFOIDCheck;


        var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');

        // this method is called to ensure the shipping method and shipping address on store-shipment is correctly retained
        COHelpers.ensureStoreShipmentHasValidAddr(currentBasket);

        if (Object.keys(shippingFormErrors).length > 0) {
            if (shipmentUUID === 'new') {
                req.session.privacyCache.set(origUUID, 'invalid');
            } else {
                req.session.privacyCache.set(shipmentUUID, 'invalid');
            }
            res.json({
                form: form,
                fieldErrors: [shippingFormErrors],
                serverErrors: [],
                error: true,
                upsAddressReturned: false
            });
        } else {
            result.address = {
                firstName: form.shippingAddress.addressFields.firstName.value,
                lastName: form.shippingAddress.addressFields.lastName.value,
                address1: form.shippingAddress.addressFields.address1.value,
                address2: form.shippingAddress.addressFields.address2.value,
                city: form.shippingAddress.addressFields.city.value,
                postalCode: form.shippingAddress.addressFields.postalCode.value,
                countryCode: form.shippingAddress.addressFields.country.value,
                phone: form.shippingAddress.addressFields.phone.value,
                companyName: form.shippingAddress.addressFields.companyName.value
            };

            if (Object.prototype.hasOwnProperty
                    .call(form.shippingAddress.addressFields, 'states')) {
                result.address.stateCode =
                    form.shippingAddress.addressFields.states.stateCode.value;
            }

            result.shippingBillingSame =
                form.shippingAddress.shippingAddressUseAsBillingAddress.value;

            result.shippingMethod =
                form.shippingAddress.shippingMethodID.value ?
                '' + form.shippingAddress.shippingMethodID.value : null;
            result.form = form;

            result.isGift = form.shippingAddress.isGift.checked;
            result.address.countryName = Resource.msg(result.address.countryCode.toLowerCase(), 'checkout', null);

            result.giftMessage = result.isGift ? form.shippingAddress.giftMessage.value : null;
            userAddress[0] = result.address;
            var addressList = {};
            var suggestedAddresses = [];
            // updating the address to basket so that it does not break on the update view data
            if (!COHelpers.isShippingAddressInitialized(shipToHomeShipment)) {
                // First use always applies to defaultShipment
                COHelpers.copyShippingAddressToShipment(result, shipToHomeShipment);
            } else {
                try {
                    Transaction.wrap(function () {
                        // An edit to the address or shipping method
                        COHelpers.copyShippingAddressToShipment(result, shipToHomeShipment);
                        COHelpers.ensureNoEmptyShipments(req);
                    });
                } catch (e) {
                    // no action
                }
            }
            if (byPassAddressValidation === 'false') {
                var upsValidatedAddress = new UPSAddress(result.address);
                if (upsValidatedAddress === null) {
                    res.json({
                        error: true,
                        upsAddressReturned: true,
                        IsExactAddress: true,
                        RequestAddress: userAddress
                    });
                    return next();
                } else {  // eslint-disable-line
                    if (!upsValidatedAddress.error) {
                        for (var i = 0; i < upsValidatedAddress.AddressList.length; i++) {
                            addressList = {};
                            addressList.address1 = upsValidatedAddress.AddressList[i].address1 ? upsValidatedAddress.AddressList[i].address1 : null;
                            addressList.address2 = upsValidatedAddress.AddressList[i].address2 ? upsValidatedAddress.AddressList[i].address2 : null;
                            addressList.city = upsValidatedAddress.AddressList[i].city;
                            addressList.countryCode = upsValidatedAddress.AddressList[i].country;
                            addressList.postalCode = upsValidatedAddress.AddressList[i].postalCode;
                            addressList.stateCode = upsValidatedAddress.AddressList[i].state;
                            addressList.countryName = Resource.msg(addressList.countryCode.toLowerCase(), 'checkout', null);
                            suggestedAddresses[i] = addressList;
                        }
                    } else {
                        if (upsValidatedAddress.errorType !== null && upsValidatedAddress.errorType === 'service unavailable') {
                            message = 'upsAddressServiceUnavailableMsg' in Site.current.preferences.custom &&
                                Site.getCurrent().getCustomPreferenceValue('upsAddressServiceUnavailableMsg') ?
                                Site.getCurrent().getCustomPreferenceValue('upsAddressServiceUnavailableMsg') :
                                'Address validation service is unavaialble';
                        } else {
                            message = 'upsAddressServiceErrorMsg' in Site.current.preferences.custom &&
                                Site.getCurrent().getCustomPreferenceValue('upsAddressServiceErrorMsg') ?
                                Site.getCurrent().getCustomPreferenceValue('upsAddressServiceErrorMsg') :
                                'Address validation service Error';
                        }
                        res.json({
                            upsAddressReturned: true,
                            error: true,
                            order: null,
                            IsExactAddress: upsValidatedAddress.IsExactAddress,
                            RequestAddress: userAddress,
                            upsServiceMessage: message
                        });
                        return next();
                    }
                    result.upsAddressReturned = true;
                    result.error = true;
                    result.upsSuggestedAddress = {
                        Addresses: suggestedAddresses,
                        IsMultiAddress: upsValidatedAddress.IsMultiAddress,
                        IsExactAddress: upsValidatedAddress.IsExactAddress,
                        userEnteredAddress: userAddress
                    };

                    res.json(result);
                    return next();
                }
            }
            var restrictHelper = require('*/cartridge/scripts/cart/restrictHelper.js');
            var restrictResponse = restrictHelper.validateRestrictGroupAndItem(result.address, basket);
            if (restrictResponse.hasRestrictedItems || byPassFOIDCheck === 'false') {
                if (restrictResponse.error) {
                    restrictResponse.upsAddressReturned = false;
                    res.json(restrictResponse);
                    return next();
                }
            }
            COHelpers.calculateUPSShipRate(basket);
            // Email sign up upon placing order.
            var signupForEmail = req.form.signupForNewsLetter;
            if (signupForEmail !== undefined && signupForEmail) {
                req.session.privacyCache.set('signupForEmail', true);
            }
            res.setViewData(result);

            this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
                var viewData = res.getViewData();

                if (viewData.error) {
                    res.json(viewData);
                    return;
                }

                var shipment;

                if (!COHelpers.isShippingAddressInitialized(shipToHomeShipment)) {
                    // First use always applies to defaultShipment
                    COHelpers.copyShippingAddressToShipment(viewData, shipToHomeShipment);
                    shipment = shipToHomeShipment;
                } else {
                    try {
                        Transaction.wrap(function () {
                            // An edit to the address or shipping method
                            shipment = shipToHomeShipment;
                            COHelpers.copyShippingAddressToShipment(viewData, shipment);
                            COHelpers.ensureNoEmptyShipments(req);
                        });
                    } catch (e) {
                        viewData.error = e;
                    }
                }

                if (shipment && shipment.UUID) {
                    req.session.privacyCache.set(shipToHomeShipment.UUID, 'valid');
                    viewData.shipmentUUID = shipToHomeShipment.UUID;
                }

                // Loop through all shipments and make sure all are valid
                var isValid;
                var allValid = true;
                for (var j = 0, jj = basket.shipments.length; j < jj; j++) {
                    isValid = req.session.privacyCache.get(basket.shipments[j].UUID);
                    if (isValid !== 'valid') {
                        allValid = false;
                        break;
                    }
                }
                if (shipToHomeShipment.shippingAddress && shipToHomeShipment.shippingAddress.stateCode) {
                    session.custom.shippingStateCode = shipToHomeShipment.shippingAddress.stateCode; // eslint-disable-line
                }
                if (shipment && viewData && !!viewData.isGift) {
                    var giftResult = COHelpers.setGift(shipToHomeShipment, viewData.isGift, viewData.giftMessage);

                    if (giftResult.error) {
                        res.json({
                            error: giftResult.error,
                            fieldErrors: [],
                            serverErrors: [giftResult.errorMessage]
                        });
                        return;
                    }
                }
                // update the email on the order
                Transaction.wrap(function () {
                    currentBasket.setCustomerEmail(form.shippingAddress.addressFields.email.value);
                });

                if (!basket.billingAddress) {
                    if (customerObj.addressBook &&
                            customerObj.addressBook.preferredAddress) {
                        // Copy over preferredAddress (use addressUUID for matching)
                        COHelpers.copyBillingAddressToBasket(
                                customerObj.addressBook.preferredAddress, basket);
                    } else {
                        // Copy over first shipping address (use shipmentUUID for matching)
                        COHelpers.copyBillingAddressToBasket(shipToHomeShipment.shippingAddress, basket);
                    }
                }
                COHelpers.copyBillingAddressToBasket(shipToHomeShipment.shippingAddress, basket);
                COHelpers.recalculateBasket(basket);

                var currentLocale = Locale.getLocale(req.locale.id);
                var basketModel = new OrderModel(
                    basket, {
                        usingMultiShipping: usingMultiShipping,
                        shippable: allValid,
                        countryCode: currentLocale.country,
                        containerView: 'basket'
                    }
                );

                var accountModel = new AccountModel(req.currentCustomer);

                res.json({
                    form: form,
                    signupForEmail: signupForEmail,
                    data: viewData,
                    order: basketModel,
                    customer: accountModel,
                    fieldErrors: [],
                    serverErrors: [],
                    error: false
                });
            });
        }
        return next();
    });

module.exports = server.exports();
