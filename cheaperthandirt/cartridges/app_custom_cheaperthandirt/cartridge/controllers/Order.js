'use strict';

var server = require('server');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

server.extend(module.superModule);

server.append('History', function (req, res, next) {
    res.setViewData({
        breadcrumbs: [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('page.title.myaccount', 'account', null),
                url: URLUtils.url('Account-Show').toString()
            },
            {
                htmlValue: Resource.msg('label.orderhistory', 'account', null)
            }
        ]
    });
    next();
});

server.append('Details', function (req, res, next) {
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var OrderMgr = require('dw/order/OrderMgr');
    var order = res.getViewData().order;
    var dwOrder;
    if (order) {
        if (req.querystring.orderID !== null) {
            dwOrder = OrderMgr.getOrder(req.querystring.orderID);
        }
        var requireCCPayment = COHelpers.validateIfOrderRequirePayment(dwOrder);
        var legacyOrder = COHelpers.checkforLegacyOrder(dwOrder);
        res.setViewData({
            onlyGCInCart: order.onlyGCInCart,
            requireCCPayment: requireCCPayment,
            isConfirmation: true,
            legacyOrder: legacyOrder
        });
    }
    res.setViewData({
        breadcrumbs: [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('page.title.myaccount', 'account', null),
                url: URLUtils.url('Account-Show').toString()
            },
            {
                htmlValue: Resource.msg('label.orderhistory', 'account', null),
                url: URLUtils.url('Order-History').toString()
            },
            {
                htmlValue: Resource.msg('link.orderdetails.details', 'account', null)
            }
        ]
    });
    next();
});

server.append('Confirm', function (req, res, next) {
    var OrderMgr = require('dw/order/OrderMgr');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var order;
    var onlyGCInCart = false;
    if (req.querystring.ID !== null) {
        order = OrderMgr.getOrder(req.querystring.ID);
    }
    // Check if the payment has CC in it
    var requireCCPayment = COHelpers.validateIfOrderRequirePayment(order);
    var legacyOrder;
    if (order) {
        onlyGCInCart = COHelpers.checkOnlyGiftCardInCart(order.getProductLineItems());
        legacyOrder = COHelpers.checkforLegacyOrder(order);
    }
    res.setViewData({
        onlyGCInCart: onlyGCInCart,
        requireCCPayment: requireCCPayment,
        isConfirmation: true,
        legacyOrder: legacyOrder
    });
    next();
});

server.append('Track', function (req, res, next) {
    var order = res.getViewData().order;
    var OrderMgr = require('dw/order/OrderMgr');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    if (order) {
        var requireCCPayment = true;
        var dwOrder = OrderMgr.getOrder(order.orderNumber);
        var legacyOrder;
        if (dwOrder) {
            requireCCPayment = COHelpers.validateIfOrderRequirePayment(dwOrder);
            legacyOrder = COHelpers.checkforLegacyOrder(dwOrder);
        }
        res.setViewData({
            onlyGCInCart: order.onlyGCInCart,
            requireCCPayment: requireCCPayment,
            isConfirmation: true,
            legacyOrder: legacyOrder
        });
    }
    next();
});

server.replace(
    'CreateAccount',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var CustomerMgr = require('dw/customer/CustomerMgr');
        var OrderMgr = require('dw/order/OrderMgr');

        var formErrors = require('*/cartridge/scripts/formErrors');

        var passwordForm = server.forms.getForm('newPasswords');
        var newPassword = passwordForm.newpassword.htmlValue;
        var confirmPassword = passwordForm.newpasswordconfirm.htmlValue;
        if (newPassword !== confirmPassword) {
            passwordForm.valid = false;
            passwordForm.newpasswordconfirm.valid = false;
            passwordForm.newpasswordconfirm.error =
                Resource.msg('error.message.mismatch.newpassword', 'forms', null);
        }

        if (!CustomerMgr.isAcceptablePassword(passwordForm.newpassword.value)) {
            passwordForm.newpassword.valid = false;
            passwordForm.newpasswordconfirm.valid = false;
            passwordForm.newpasswordconfirm.error =
                Resource.msg('error.message.password.constraints.not.matched', 'forms', null);
            passwordForm.valid = false;
        }

        var order = OrderMgr.getOrder(req.querystring.ID);
        res.setViewData({ orderID: req.querystring.ID });
        var registrationObj = {
            firstName: order.billingAddress.firstName,
            lastName: order.billingAddress.lastName,
            phone: order.billingAddress.phone,
            email: order.customerEmail,
            password: newPassword
        };

        if (passwordForm.valid) {
            res.setViewData(registrationObj);

            this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
                var Transaction = require('dw/system/Transaction');
                var accountHelpers = require('*/cartridge/scripts/helpers/accountHelpers');

                var registrationData = res.getViewData();

                var login = registrationData.email;
                var password = registrationData.password;
                var newCustomer;
                var authenticatedCustomer;
                var newCustomerProfile;
                var errorObj = {};

                delete registrationData.email;
                delete registrationData.password;

                // attempt to create a new user and log that user in.
                try {
                    Transaction.wrap(function () {
                        var error = {};
                        newCustomer = CustomerMgr.createCustomer(login, password);

                        var authenticateCustomerResult = CustomerMgr.authenticateCustomer(login, password);
                        if (authenticateCustomerResult.status !== 'AUTH_OK') {
                            error = { authError: true, status: authenticateCustomerResult.status };
                            throw error;
                        }

                        authenticatedCustomer = CustomerMgr.loginCustomer(authenticateCustomerResult, false);

                        if (!authenticatedCustomer) {
                            error = { authError: true, status: authenticateCustomerResult.status };
                            throw error;
                        } else {
                            // assign values to the profile
                            newCustomerProfile = newCustomer.getProfile();

                            newCustomerProfile.firstName = registrationData.firstName;
                            newCustomerProfile.lastName = registrationData.lastName;
                            newCustomerProfile.phoneHome = registrationData.phone;
                            newCustomerProfile.email = login;

                            order.setCustomer(newCustomer);
                        }
                    });
                } catch (e) {
                    errorObj.error = true;
                    errorObj.errorMessage = e.authError
                        ? Resource.msg('error.message.unable.to.create.account', 'login', null)
                        : Resource.msg('error.message.username.invalid', 'forms', null);
                }

                if (errorObj.error) {
                    res.json({ error: [errorObj.errorMessage] });

                    return;
                }

                accountHelpers.sendCreateAccountEmail(authenticatedCustomer.profile);

                res.json({
                    success: true,
                    redirectUrl: URLUtils.url('Account-Show', 'registration', 'submitted').toString()
                });
            });
        } else {
            res.json({
                fields: formErrors.getFormErrors(passwordForm)
            });
        }

        return next();
    }
);

module.exports = server.exports();
