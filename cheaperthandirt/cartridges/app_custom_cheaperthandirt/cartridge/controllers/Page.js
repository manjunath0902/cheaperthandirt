'use strict';

var server = require('server');

var cache = require('*/cartridge/scripts/middleware/cache');

server.extend(module.superModule);

server.append('IncludeHeaderMenu', server.middleware.include, cache.applyDefaultCache, function (req, res, next) {
    var Site = require('dw/system/Site');
    var currentSite = Site.getCurrent();
    var siteId = currentSite.getID();
    res.setViewData({
        siteID: siteId
    });
    next();
});

module.exports = server.exports();
