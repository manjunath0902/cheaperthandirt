'use strict';

var server = require('server');
server.extend(module.superModule);

server.append('Show', function (req, res, next) {
    var qtyForm = server.forms.getForm('productQty');
    var notifyForm = server.forms.getForm('NotifyMe');
    var postalForm = server.forms.getForm('addressCheck');
    var ContentMgr = require('dw/content/ContentMgr');
    var URLUtils = require('dw/web/URLUtils');
    var productListMgr = require('dw/customer/ProductListMgr');
    var ContentModel = require('*/cartridge/models/content');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var ProductListModel = require('*/cartridge/models/productList');
    var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');
    var seo = require('*/cartridge/models/seo');
    var setHelper = require('*/cartridge/scripts/helpers/productSetHelpers');
    var ProductMgr = require('dw/catalog/ProductMgr');
    var searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');
    var authStatus = req.currentCustomer.raw.authenticated;
    var custEmail = '';
    if (authStatus) {
        custEmail = req.currentCustomer.profile.email;
    }

    var params = req.querystring;
    var product = ProductFactory.get(params);
    var productSeoDetails = seo.getProductSeoDetails(product, params);
    var content;
    var apiContent;
    var isMaster = product.isMaster;
    var setIsFirearm = false;
    if (product.productType === 'set') {
        for (var i = 0; i < product.individualProducts.length; i++) {
            if (product.individualProducts[i].isFirearm) {
                setIsFirearm = true;
            }
        }
    }
    if (product.isFirearm || setIsFirearm) {
        apiContent = ContentMgr.getContent('firearms-purchase-details');
    }

    if (apiContent) {
        content = new ContentModel(apiContent, 'components/content/contentAssetInc');
    }
    var productSetPrice = null;
    if (product.productType === 'set') {
        productSetPrice = setHelper.calculateSetPrice(product);
    }
    var publicView = (req.querystring.publicView === 'true') || false;
    var list;
    if (publicView && req.querystring.id) {
        list = productListMgr.getProductList(req.querystring.id);
    } else {
        list = productListHelper.getList(req.currentCustomer.raw, {
            type: 10
        });
    }
    //	Get category SEO breadcrumbs
    var dwProduct = ProductMgr.getProduct(params.pid);
    var category = dwProduct.variant ? dwProduct.masterProduct.primaryCategory : dwProduct.primaryCategory;
    var seoCatBreadcrumbs = searchHelper.getCatBreadcrumbs(category, [], true).reverse();
    seoCatBreadcrumbs.push({
        htmlValue: product.productName,
        url: URLUtils.abs('Product-Show', 'pid', product.id).toString()
    });
    var seoBreadCrumbs = seo.getSeoBreadCrumbs(seoCatBreadcrumbs);
    var restrictURL = URLUtils.url('Product-RestrictValidation').toString();
    var caProvince = request.geolocation ? request.geolocation.regionCode : null; // eslint-disable-line
    if (caProvince) {
        caProvince = caProvince === 'CA';
    }

    var productListModel = new ProductListModel(
        list, {
            publicView: true,
            pageNumber: 1,
            socialLinks: true
        }
    ).productList;

    res.setViewData({
        qtyForm: qtyForm,
        notifyForm: notifyForm,
        postalForm: postalForm,
        productSetPrice: productSetPrice,
        content: content,
        wishlist: productListModel,
        socialLinks: true,
        custEmail: custEmail,
        productSeoDetails: productSeoDetails,
        seoProduct: product,
        seoBreadCrumbs: seoBreadCrumbs,
        restrictURL: restrictURL,
        caProvince: caProvince,
        isQuickView: false,
        setIsFirearm: setIsFirearm,
        isMaster: isMaster,
        pType: product.productType,
        dwProduct: dwProduct
    });
    next();
});

server.replace('ShowQuickView', function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    var productHelper = require('*/cartridge/scripts/helpers/productHelpers');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

    var params = req.querystring;
    var product = ProductFactory.get(params);
    var addToCartUrl = URLUtils.url('Cart-AddProduct');
    var template = product.productType === 'set' ?
        'product/setQuickView.isml' :
        'product/quickView.isml';
    var isMaster = product.isMaster;
    var setIsFirearm = false;
    if (product.productType === 'set') {
        for (var i = 0; i < product.individualProducts.length; i++) {
            if (product.individualProducts[i].isFirearm) {
                setIsFirearm = true;
            }
        }
    }
    var context = {
        product: product,
        addToCartUrl: addToCartUrl,
        resources: productHelper.getResources(),
        isQuickView: true,
        setIsFirearm: setIsFirearm,
        isMaster: isMaster,
        productType: product.productType,
        action: 'Product-ShowQuickView'
    };

    var renderedTemplate = renderTemplateHelper.getRenderedHtml(context, template);
    var Resource = require('dw/web/Resource');
    var setHelper = require('*/cartridge/scripts/helpers/productSetHelpers');
    var ContentMgr = require('dw/content/ContentMgr');
    var ContentModel = require('*/cartridge/models/content');
    var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
    var StoreMgr = require('dw/catalog/StoreMgr');

    var productSetPrice = null;
    if (product.productType === 'set') {
        productSetPrice = setHelper.calculateSetPrice(product);
    }

    var content;
    var apiContent;
    if (product.isFirearm || setIsFirearm) {
        apiContent = ContentMgr.getContent('firearms-purchase-details');
    }
    if (apiContent) {
        content = new ContentModel(apiContent, 'components/content/contentAssetInc');
    }
    var firearmContent;
    var assetContent;
    if (product.isFirearm || setIsFirearm) {
        assetContent = ContentMgr.getContent('firarm-shipping-handling-message');
    }
    if (apiContent) {
        firearmContent = new ContentModel(assetContent, 'components/content/contentAssetInc');
    }
    var authStatus = req.currentCustomer.raw.authenticated;
    var custEmail = '';
    if (authStatus) {
        custEmail = req.currentCustomer.profile.email;
    }
    var preferredStoreID = storeHelpers.getPreferredStore(req);
    var preferredStoreDetails = null;
    if (preferredStoreID != null && product.isFirearm) {
        var store = StoreMgr.getStore(preferredStoreID);
        var StoreModel = require('*/cartridge/models/store');
        preferredStoreDetails = new StoreModel(store);
    }
    res.setViewData({
        productSetPrice: productSetPrice,
        content: content,
        firearmContent: firearmContent ? firearmContent.body.markup : '',
        custEmail: custEmail,
        purchaseMessage: Resource.msg('label.purchase.detail', 'product', null),
        purchaseBody: content ? content.body.markup : '',
        preferredStoreDetails: preferredStoreDetails
    });
    res.json({
        renderedTemplate: renderedTemplate,
        product: product,
        productUrl: URLUtils.url('Product-Show', 'pid', product.id).relative().toString(),
        masterProductUrl: URLUtils.url('Product-Show', 'pid', product.masterProductID).relative().toString()
    });
    next();
});


server.post('RestrictValidation', function (req, res, next) {
    var postalCode = req.form.postalCode;
    var restrictHelper = require('*/cartridge/scripts/cart/restrictHelper.js');
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var product = ProductFactory.get(req.form);
    var isShippable = false;
    var params = {
        postalCode: postalCode,
        product: product
    };
    isShippable = restrictHelper.validateShipToYou(params);
    res.json({
        success: isShippable,
        responseMsg: isShippable ? 'YES' : 'NO'
    });
    next();
});


/**
 * Renders the last visited products based on the session information (product/lastvisited template).
 */
server.get('LastVisited', function (req, res, next) {
    var RecentlyViewedModel = require('*/cartridge/models/product/recentlyViewedItemsModel');
    var LastVisitedProducts = RecentlyViewedModel.getRecentlyViewedProducts(req, 8);
    res.render('/product/components/lastvisited.isml', {
        LastVisitedProducts: LastVisitedProducts
    });
    next();
});

server.append('Variation', function (req, res, next) {
    var ProductFactory = require('*/cartridge/scripts/factories/product');
    var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
    var params = req.querystring;
    var product = ProductFactory.get(params);
    var preferredStoreID = storeHelpers.getPreferredStore(req);
    var preferredStoreDetails = null;
    if (preferredStoreID != null && product.isFirearm) {
        var StoreMgr = require('dw/catalog/StoreMgr');
        var StoreModel = require('*/cartridge/models/store');
        var store = StoreMgr.getStore(preferredStoreID);
        preferredStoreDetails = new StoreModel(store);
    }
    res.setViewData({
        preferredStoreDetails: preferredStoreDetails
    });
    next();
});

/**
 * Renders the Preferred FFL section on PDP and QuickView.
 */
server.get('GetPreferredFFLSection', function (req, res, next) {
    var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
    var URLUtils = require('dw/web/URLUtils');
    var Site = require('dw/system/Site');
    var params = req.querystring;
    var preferredStoreID = storeHelpers.getPreferredStore(req);
    var preferredStore = null;
    if (preferredStoreID !== null) {
        var StoreMgr = require('dw/catalog/StoreMgr');
        preferredStore = StoreMgr.getStore(preferredStoreID);
    }
    var pickUpInStore = {
        actionUrl: URLUtils.url('Stores-InventorySearch', 'showMap', false, 'horizontalView', true, 'isForm', true).toString(),
        atsActionUrl: URLUtils.url('Stores-getAtsValue').toString(),
        enabled: Site.getCurrent().getCustomPreferenceValue('enableStorePickUp')
    };
    res.render('product/components/pdpInstoreInventory', {
        preferredStore: preferredStore,
        pickUpInStore: pickUpInStore,
        readyToOrder: params.readyToOrder,
        available: params.prodAvailable,
        availableForInStorePickup: params.availableForInStorePickup,
        masterProd: params.masterProd

    });
    next();
});

/**
 * Renders Add to Cart Button on PDP.
 */
server.get('AddToCartProduct', function (req, res, next) {
    var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');
    var params = req.querystring;
    var preferredStoreID = storeHelpers.getPreferredStore(req);
    var preferredStore = false;
    if (preferredStoreID !== null) {
        preferredStore = true;
    }
    res.render('product/components/addToCartProduct', {
        preferredStore: preferredStore,
        addToCartUrl: params.addToCartUrl,
        readyToOrder: params.readyToOrder,
        available: params.prodAvailable,
        isFirearm: params.isFirearm,
        id: params.prodID

    });
    next();
});

module.exports = server.exports();
