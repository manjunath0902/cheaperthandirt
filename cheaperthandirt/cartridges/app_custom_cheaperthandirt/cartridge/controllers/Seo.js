'use strict';

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');

server.get('IncludeVideoSchema', cache.applyShortPromotionSensitiveCache, function (req, res, next) {
    var seos = require('*/cartridge/models/seo');
    var videoSeoObject = seos.getBuildVideoSeoDetails();
    res.render('seo/youtubeSeo', {
        videoSeoObject: videoSeoObject
    });
    next();
});

module.exports = server.exports();
