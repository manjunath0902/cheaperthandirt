'use strict';

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');

server.extend(module.superModule);

// Replacing Tile-Show to ensure that the product URLs in the recommended and last viewed sections on the homepage are absolute URLs in order to obey SEO rules for homepage
server.replace('Show', cache.applyPromotionSensitiveCache, function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    var ProductFactory = require('*/cartridge/scripts/factories/product');

 // The req parameter has a property called querystring. In this use case the querystring could
    // have the following:
    // pid - the Product ID
    // ratings - boolean to determine if the reviews should be shown in the tile.
    // swatches - boolean to determine if the swatches should be shown in the tile.
    //
    // pview - string to determine if the product factory returns a model for
    //         a tile or a pdp/quickview display
    var productTileParams = { pview: 'tile' };
    Object.keys(req.querystring).forEach(function (key) {
        productTileParams[key] = req.querystring[key];
    });

    var product;
    var productUrl;
    var quickViewUrl;

    // TODO: remove this logic once the Product factory is
    // able to handle the different product types
    try {
        product = ProductFactory.get(productTileParams);
        productUrl = URLUtils.abs('Product-Show', 'pid', product.id).toString();
        quickViewUrl = URLUtils.abs('Product-ShowQuickView', 'pid', product.id).toString();
    } catch (e) {
        product = false;
        productUrl = URLUtils.abs('Home-Show');// TODO: change to coming soon page
        quickViewUrl = URLUtils.abs('Home-Show');
    }

    var context = {
        product: product,
        urls: {
            product: productUrl,
            quickView: quickViewUrl
        },
        display: {}
    };

    Object.keys(req.querystring).forEach(function (key) {
        if (req.querystring[key] === 'true') {
            context.display[key] = true;
        } else if (req.querystring[key] === 'false') {
            context.display[key] = false;
        }
    });

    res.render('product/gridTile.isml', context);

    next();
});

module.exports = server.exports();
