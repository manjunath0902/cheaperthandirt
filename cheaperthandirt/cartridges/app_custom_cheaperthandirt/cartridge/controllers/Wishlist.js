'use strict';

var server = require('server');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var productListHelper = require('*/cartridge/scripts/productList/productListHelpers');

server.extend(module.superModule);

server.append('Show', function (req, res, next) {
    res.setViewData({
        breadcrumbs: [
            {
                htmlValue: Resource.msg('global.home', 'common', null),
                url: URLUtils.home().toString()
            },
            {
                htmlValue: Resource.msg('page.title.myaccount', 'account', null),
                url: URLUtils.abs('Account-Show').toString()
            },
            {
                htmlValue: Resource.msg('link.wishlist.myaccount', 'account', null)
            }
        ]
    });
    next();
});

server.get('AddProductOnNet', function (req, res, next) {
    var list = productListHelper.getList(req.currentCustomer.raw, { type: 10 });
    var pid = req.querystring.pid;

    var config = {
        qty: 1,
        optionId: null,
        optionValue: null,
        req: req,
        type: 10
    };
    productListHelper.addItem(list, pid, config);
    res.redirect(URLUtils.https('Wishlist-Show'));
    next();
});


server.replace('RemoveProductAccount', function (req, res, next) {
    productListHelper.removeItem(req.currentCustomer.raw, req.querystring.pid, { req: req, type: 10 });
    var wishListAccount = require('*/cartridge/models/account/wishListAccount');
    var productListMgr = require('dw/customer/ProductListMgr');
    var apiWishList = productListMgr.getProductLists(req.currentCustomer.raw, '10')[0];
    var wishlistAccountModel = {};
    wishListAccount(wishlistAccountModel, apiWishList);
    res.render('account/wishlist/wishlistGridItems', {
        account: {
            wishlist: wishlistAccountModel.wishlist
        }
    });
    next();
});

module.exports = server.exports();
