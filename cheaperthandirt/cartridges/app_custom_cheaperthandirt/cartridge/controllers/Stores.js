'use strict';

var server = require('server');

server.extend(module.superModule);

var cache = require('*/cartridge/scripts/middleware/cache');
var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');

/**
*
* @param {string} preferredStoreID - Preferred Store as input parameter.
* @param {Object} storesModel - stores list as object based on input parameters.
* @returns {Object} storesModel - stores list with preferred store at first.
*/
function getPreferredStoreModel(preferredStoreID, storesModel) {
    for (var i = 0; i < storesModel.stores.length; i++) {
        if (storesModel.stores[i].ID === preferredStoreID) {
            var tempVar = storesModel.stores[i];
            storesModel.stores.splice(i, 1);
            storesModel.stores.unshift(tempVar);
            break;
        }
    }
    return storesModel;
}

/**
*
* @param {string} preferredStoreID - Preferred Store as input parameter.
* @param {string} storeLocations - store locations list as object based on input parameters.
* @returns {string} - store locations list with preferred store at first.
*/
function getPreferredGeoLocations(preferredStoreID, storeLocations) {
    var storeLocation = JSON.parse(storeLocations);
    for (var i = 0; i < storeLocation.length; i++) {
        if (storeLocation[i].infoWindowHtml.toString().indexOf(preferredStoreID) > -1) {
            var tempVar = storeLocation[i];
            storeLocation.splice(i, 1);
            storeLocation.unshift(tempVar);
            break;
        }
    }
    return JSON.stringify(storeLocation);
}

/**
*
* @param {string} products - list of product details info in the form of "productId:quantity,productId:quantity,... "
* @returns {Object} a object containing product ID and quantity
*/
function buildProductListAsJson(products) {
    if (!products) {
        return null;
    }

    return products.split(',').map(function (item) {
        var properties = item.split(':');
        return { id: properties[0], quantity: properties[1] };
    });
}

server.replace('InventorySearch', cache.applyDefaultCache, function (req, res, next) {
    var ContentMgr = require('dw/content/ContentMgr');
    var ContentModel = require('*/cartridge/models/content');
    var StoreModel = require('*/cartridge/models/store');
    var fflAsset = ContentMgr.getContent('firearms-ffl-dealer-text');
    var ByPassPDF = ContentMgr.getContent('firearms-salesform-pdf');
    var fflAssetHTML = '';
    var ByPassPdfHTML = '';
    if (fflAsset) {
        fflAssetHTML = new ContentModel(fflAsset, 'components/content/contentAssetInc');
    }
    if (ByPassPDF) {
        ByPassPdfHTML = new ContentModel(ByPassPDF, 'components/content/contentAssetInc');
    }
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var URLUtils = require('dw/web/URLUtils');

    var radius = req.querystring.radius;
    var postalCode = req.querystring.postalCode;
    var lat = req.querystring.lat;
    var long = req.querystring.long;
    var showMap = req.querystring.showMap || false;
    var horizontalView = req.querystring.horizontalView || false;
    var isForm = req.querystring.isForm || false;

    var products = buildProductListAsJson(req.querystring.products);

    var url = URLUtils.url('Stores-FindStores', 'showMap', showMap, 'products', req.querystring.products).toString();
    var storesModel = storeHelpers.getStores(radius, postalCode, lat, long, req.geolocation, showMap, url, products);
    var store = storeHelpers.getByPassStore();
    var ByPassStore = new StoreModel(store);
    var ByPassStoreStr = JSON.stringify(ByPassStore);

    var viewData = {
        stores: storesModel,
        horizontalView: horizontalView,
        isForm: isForm,
        showMap: showMap,
        fflAssetHTML: fflAssetHTML,
        ByPassStore: ByPassStore,
        ByPassPdfHTML: ByPassPdfHTML,
        ByPassStoreStr: ByPassStoreStr
    };

    var preferredStoreID = storeHelpers.getPreferredStore(req);
    // Code to show selected FFL dealer and map at first.
    if (preferredStoreID !== null && storesModel.stores.length > 0) {
        storesModel = getPreferredStoreModel(preferredStoreID, storesModel);
        storesModel.locations = getPreferredGeoLocations(preferredStoreID, storesModel.locations);
    }

    var storesResultsHtml = storesModel.stores
        ? renderTemplateHelper.getRenderedHtml(viewData, 'storeLocator/storeLocatorNoDecorator')
        : null;

    storesModel.storesResultsHtml = storesResultsHtml;
    res.json(storesModel);
    next();
});

server.replace('getAtsValue', function (req, res, next) {
    var Resource = require('dw/web/Resource');

    var instoreInventory = true;

    var productAtsValue = {
        atsValue: instoreInventory,
        product: {
            available: !!instoreInventory,
            readyToOrder: !!instoreInventory,
            messages: [
                Resource.msg('label.instock', 'common', null)
            ]
        },
        resources: {
            info_selectforstock: Resource.msg('label.ats.notavailable', 'instorePickup', null)
        }
    };

    res.json(productAtsValue);
    next();
});

server.replace('FindStores', function (req, res, next) {
    var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');
    var radius = req.querystring.radius;
    var postalCode = req.querystring.postalCode;
    var lat = req.querystring.lat;
    var long = req.querystring.long;
    var showMap = req.querystring.showMap || false;
    var horizontalView = req.querystring.horizontalView || false;
    var isForm = req.querystring.isForm || false;

    var url = null;
    var products = buildProductListAsJson(req.querystring.products);

    var storesModel = storeHelpers.getStores(radius, postalCode, lat, long, req.geolocation, showMap, url, products);

    var preferredStoreID = storeHelpers.getPreferredStore(req);
    // Code to show selected FFL dealer and map at first.
    if (preferredStoreID !== null && storesModel.stores.length > 0) {
        storesModel = getPreferredStoreModel(preferredStoreID, storesModel);
        storesModel.locations = getPreferredGeoLocations(preferredStoreID, storesModel.locations);
    }

    var context = {
        stores: storesModel,
        horizontalView: horizontalView,
        isForm: isForm,
        showMap: showMap
    };

    var storesResultsHtml = storesModel.stores
        ? renderTemplateHelper.getRenderedHtml(context, 'storeLocator/storeLocatorResults')
        : null;

    storesModel.storesResultsHtml = storesResultsHtml;

    res.json(storesModel);
    next();
});

server.get('SetPreferredDealer', function (req, res, next) {
    storeHelpers.setPreferredStore(req);
    res.json({
        success: true
    });
    next();
});

server.replace('GetStoreById', server.middleware.include, cache.applyDefaultCache, function (req, res, next) {
    var StoreMgr = require('dw/catalog/StoreMgr');
    var StoreModel = require('*/cartridge/models/store');
    var ContentMgr = require('dw/content/ContentMgr');
    var ContentModel = require('*/cartridge/models/content');
    var ByPassPDF = ContentMgr.getContent('firearms-salesform-pdf');
    var ByPassPdfHTML = '';
    if (ByPassPDF) {
        ByPassPdfHTML = new ContentModel(ByPassPDF, 'components/content/contentAssetInc');
    }
    var storeId = req.querystring.storeId ? req.querystring.storeId : '';
    var storeObject = StoreMgr.getStore(storeId);
    var store = new StoreModel(storeObject);
    store.displaySummary = false;
    store.ByPassPdfHTML = ByPassPdfHTML;
    res.render('store/storeDetails', store);
    next();
});

server.get('StoreSummary', function (req, res, next) {
    var StoreMgr = require('dw/catalog/StoreMgr');
    var StoreModel = require('*/cartridge/models/store');
    var storeId = req.querystring.storeId ? req.querystring.storeId : '';
    var storeObject = StoreMgr.getStore(storeId);
    var store = new StoreModel(storeObject);
    var ContentMgr = require('dw/content/ContentMgr');
    var ContentModel = require('*/cartridge/models/content');
    var ByPassPDF = ContentMgr.getContent('firearms-salesform-pdf');
    var ByPassPdfHTML = '';
    if (ByPassPDF) {
        ByPassPdfHTML = new ContentModel(ByPassPDF, 'components/content/contentAssetInc');
    }
    store.displaySummary = true;
    store.ByPassPdfHTML = ByPassPdfHTML;
    res.render('store/storeDetails', store);
    next();
});

server.get('FFLByCountry', cache.applyDefaultCache, function (req, res, next) {
    var FFLStates = storeHelpers.getFFLStates();
    res.render('storeLocator/FFLByCountry', {
        FFLStates: FFLStates
    });
    next();
});

server.get('FFLByState', cache.applyDefaultCache, function (req, res, next) {
    var Resource = require('dw/web/Resource');
    var URLUtils = require('dw/web/URLUtils');

    var FFLCitiesHeaderNav = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    var FFLCities = storeHelpers.getFFLCities(req, FFLCitiesHeaderNav);

    res.render('storeLocator/FFLByState', {
        FFLCities: FFLCities,
        FFLCitiesHeaderNav: FFLCitiesHeaderNav,
        stateCode: req.querystring.stateCode,
        breadcrumbs: [{
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('ffl.dealer.locator', 'storeLocator', null),
            url: URLUtils.url('Stores-Find').toString()
        }, {
            htmlValue: Resource.msg('state.us.' + req.querystring.stateCode, 'common', null),
            url: URLUtils.url('Stores-FFLByState', 'stateCode', req.querystring.stateCode).toString()
        }, {
            htmlValue: Resource.msgf('ffl.dealers.in', 'storeLocator', null, Resource.msg('state.us.' + req.querystring.stateCode, 'common', null))
        }
        ]
    });
    next();
});

server.get('FFLByCity', cache.applyDefaultCache, function (req, res, next) {
    var Resource = require('dw/web/Resource');
    var URLUtils = require('dw/web/URLUtils');
    var FFLStores = storeHelpers.getFFLStores(req);
    res.render('storeLocator/FFLByCity', {
        FFLStores: FFLStores,
        city: req.querystring.city,
        stateCode: req.querystring.stateCode,
        breadcrumbs: [{
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('ffl.dealer.locator', 'storeLocator', null),
            url: URLUtils.url('Stores-Find').toString()
        }, {
            htmlValue: Resource.msg('state.us.' + req.querystring.stateCode, 'common', null),
            url: URLUtils.url('Stores-FFLByState', 'stateCode', req.querystring.stateCode).toString()
        }, {
            htmlValue: req.querystring.city
        }
        ]
    });
    next();
});

server.append('Find', function (req, res, next) {
    var Resource = require('dw/web/Resource');
    var URLUtils = require('dw/web/URLUtils');
    res.setViewData({
        breadcrumbs: [{
            htmlValue: Resource.msg('global.home', 'common', null),
            url: URLUtils.home().toString()
        },
        {
            htmlValue: Resource.msg('page.title.myaccount', 'account', null)
        }]
    });
    next();
});

module.exports = server.exports();
