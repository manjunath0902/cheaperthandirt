'use strict';

var server = require('server');

server.extend(module.superModule);

server.append('Show', function (req, res, next) {
    var Site = require('dw/system/Site');
    var currentSite = Site.getCurrent();
    var siteId = currentSite.getID().toLowerCase();
    res.setViewData({
        siteID: siteId
    });
    next();
});

server.replace('ErrorNotFound', function (req, res, next) {
    res.setStatusCode(410);
    res.render('error/notFound');
    next();
});

module.exports = server.exports();
