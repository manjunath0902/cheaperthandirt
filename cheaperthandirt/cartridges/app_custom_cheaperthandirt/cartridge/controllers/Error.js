'use strict';

var server = require('server');
var system = require('dw/system/System');
var Resource = require('dw/web/Resource');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');
var MessageDigest = require('dw/crypto/MessageDigest');
var Encoding = require('dw/crypto/Encoding');

/* eslint-disable */
var updateCustomerAndGeoData = function (obj) {
    // Customer information
    /* eslint-disable no-param-reassign */
    if (customer.isAuthenticated()) {
        var messageDigest = new MessageDigest(MessageDigest.DIGEST_MD5);
        obj.userEmail = customer.getProfile().getEmail();
        obj.userHashedEmail = Encoding.toBase64(messageDigest.digestBytes(Encoding.fromBase64(obj.userEmail)));
        obj.userID = customer.getProfile().getCustomerNo();
        obj.userLoginState = 'registered';
    } else {
        obj.userEmail = '';
        obj.userHashedEmail = '';
        obj.userID = '';
        obj.userLoginState = 'guest';
    }

    // Customer geo location info
    var geoLocation = request.getGeolocation();
    obj.userCountry = geoLocation.getCountryCode();
    obj.userState = geoLocation.getRegionCode();
    /* eslint-enable no-param-reassign */
};
/* eslint-enable */
server.use('Start', consentTracking.consent, function (req, res, next) {
    res.setStatusCode(500);
    var showError = system.getInstanceType() !== system.PRODUCTION_SYSTEM
        && system.getInstanceType !== system.STAGING_SYSTEM;
    var obj = {};
    obj.pageType = 'error';
    obj.pageName = obj.pageType + ': 404';
    updateCustomerAndGeoData(obj);
    if (req.httpHeaders.get('x-requested-with') === 'XMLHttpRequest') {
        res.json({
            GlobalData: obj,
            error: req.error || {},
            message: Resource.msg('subheading.error.general', 'error', null)
        });
    } else {
        res.render('error', {
            GlobalData: obj,
            error: req.error || {},
            showError: showError,
            message: Resource.msg('subheading.error.general', 'error', null)
        });
    }
    next();
});

server.use('ErrorCode', consentTracking.consent, function (req, res, next) {
    res.setStatusCode(500);
    var errorMessage = 'message.error.' + req.querystring.err;
    var obj = {};
    obj.pageType = 'error';
    obj.pageName = obj.pageType + ': 404';
    updateCustomerAndGeoData(obj);
    res.render('error', {
        GlobalData: obj,
        error: req.error || {},
        message: Resource.msg(errorMessage, 'error', null)
    });
    next();
});

module.exports = server.exports();
