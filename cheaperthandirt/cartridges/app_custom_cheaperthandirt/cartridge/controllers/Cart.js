'use strict';

var server = require('server');

server.extend(module.superModule);

server.append('RemoveCouponLineItem', function (req, res, next) {
    var isCheckout = req.querystring.checkout != null ? req.querystring.checkout : false;
    res.setViewData({ checkoutPage: isCheckout });
    next();
});

server.append('AddCoupon', function (req, res, next) {
    var isCheckout = req.querystring.checkout != null ? req.querystring.checkout : false;
    res.setViewData({
        checkoutPage: isCheckout,
        couponCode: req.querystring.couponCode
    });
    next();
});

server.get('AddProductOnNet', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var HookMgr = require('dw/system/HookMgr');
    var URLUtils = require('dw/web/URLUtils');
    var Transaction = require('dw/system/Transaction');
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');

    var currentBasket = BasketMgr.getCurrentOrNewBasket();
    var productId = req.querystring.pid;
    var storeId = req.querystring.storeId !== undefined && req.querystring.storeId !== 'undefined' ? req.querystring.storeId : null;
    var options = [];
    var quantity;

    if (currentBasket) {
        Transaction.wrap(function () {
            quantity = parseInt(req.querystring.quantity, 10);
            cartHelper.addProductToCartOnNet(
                currentBasket,
                productId,
                quantity,
                [],
                options,
                storeId,
                req
            );
            cartHelper.ensureAllShipmentsHaveMethods(currentBasket);
            HookMgr.callHook('dw.order.calculate', 'calculate', currentBasket);
        });
    }
    res.redirect(URLUtils.https('Cart-Show'));
    next();
});

server.replace('RemoveProductLineItem', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Resource = require('dw/web/Resource');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var CartModel = require('*/cartridge/models/cart');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });

        return next();
    }

    var isProductLineItemFound = false;
    var bonusProductsUUIDs = [];

    Transaction.wrap(function () {
        if (req.querystring.pid && req.querystring.uuid) {
            var productLineItems = currentBasket.getAllProductLineItems(req.querystring.pid);
            var bonusProductLineItems = currentBasket.bonusLineItems;
            var mainProdItem;
            for (var i = 0; i < productLineItems.length; i++) {
                var item = productLineItems[i];
                if ((item.UUID === req.querystring.uuid)) {
                    if (bonusProductLineItems && bonusProductLineItems.length > 0) {
                        for (var j = 0; j < bonusProductLineItems.length; j++) {
                            var bonusItem = bonusProductLineItems[j];
                            mainProdItem = bonusItem.getQualifyingProductLineItemForBonusProduct();
                            if (mainProdItem !== null
                                && (mainProdItem.productID === item.productID)) {
                                bonusProductsUUIDs.push(bonusItem.UUID);
                            }
                        }
                    }

                    var shipmentToRemove = item.shipment;
                    currentBasket.removeProductLineItem(item);
                    if (shipmentToRemove.productLineItems.empty && !shipmentToRemove.default) {
                        currentBasket.removeShipment(shipmentToRemove);
                    }
                    if (shipmentToRemove.productLineItems.empty) {
                        if ('fromStoreId' in shipmentToRemove.custom && shipmentToRemove.custom.fromStoreId) {
                            delete shipmentToRemove.custom.fromStoreId;
                        }
                        if ('shipmentType' in shipmentToRemove.custom && shipmentToRemove.custom.shipmentType) {
                            delete shipmentToRemove.custom.shipmentType;
                        }
                    } else if (shipmentToRemove !== null && shipmentToRemove !== undefined) {
                        var onlyGCInShipToHomeShipment = COHelpers.checkOnlyGiftCardInCart(shipmentToRemove.getProductLineItems());
                        /* eslint-disable */
                        if (onlyGCInShipToHomeShipment) {
                            var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
                            Transaction.wrap(function () {
                                ShippingHelper.selectShippingMethod(shipmentToRemove, 'Electronic');
                            });
                        }
                        /* eslint-enable */
                    }
                    isProductLineItemFound = true;
                    break;
                }
            }
        }
        // Calculate the basket
        Transaction.wrap(function () {
            COHelpers.ensureNoEmptyShipments(req);
        });
        basketCalculationHelpers.calculateTotals(currentBasket);
        COHelpers.recalculateBasket(currentBasket);
        COHelpers.ensureValidShipments(currentBasket);
        COHelpers.checkFreeShippingEligibility(currentBasket);
    });

    if (isProductLineItemFound) {
        var basketModel = new CartModel(currentBasket);
        var basketModelPlus = {
            basket: basketModel,
            toBeDeletedUUIDs: bonusProductsUUIDs
        };
        res.json(basketModelPlus);
    } else {
        res.setStatusCode(500);
        res.json({ errorMessage: Resource.msg('error.cannot.remove.product', 'cart', null) });
    }

    return next();
});


server.use('RemoveRestricted', function (req, res, next) {
    var collections = require('*/cartridge/scripts/util/collections');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var restrictHelper = require('*/cartridge/scripts/cart/restrictHelper.js');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var BasketMgr = require('dw/order/BasketMgr');
    var currentBasket = BasketMgr.getCurrentBasket();
    var shipToHomeShipment = COHelpers.getShipToHomeShipment(currentBasket);
    var address = {};
    var postalCode = req.form.postalCode;
    var stateCode = req.form.stateCode;
    address.postalCode = postalCode !== null && postalCode !== undefined ? postalCode : '';
    address.shippingMethodID = shipToHomeShipment.getShippingMethodID();
    address.stateCode = stateCode !== null && stateCode !== undefined ? stateCode : '';
    var restrictResponse = restrictHelper.validateRestrictGroupAndItem(address, currentBasket);
    collections.forEach(restrictResponse.restrictedProducts, function (product) {
        var pli = currentBasket.getProductLineItems(product.id);
        collections.forEach(pli, function (li) {
            Transaction.wrap(function () {
                currentBasket.removeProductLineItem(li);
                if (shipToHomeShipment.productLineItems.empty && !shipToHomeShipment.default) {
                    currentBasket.removeShipment(shipToHomeShipment);
                }
            });
        });
    });
    // Calculate the basket
    Transaction.wrap(function () {
        COHelpers.ensureNoEmptyShipments(req);
    });
    COHelpers.recalculateBasket(currentBasket);
    COHelpers.ensureValidShipments(currentBasket);
    COHelpers.checkFreeShippingEligibility(currentBasket);
    var hasProductsInCart = currentBasket.getProductLineItems().length > 0;
    var responseURL = '';
    if (hasProductsInCart) {
        responseURL = URLUtils.https('Checkout-Begin').toString();
    } else {
        responseURL = URLUtils.https('Cart-Show').toString();
    }
    res.json({
        responseURL: responseURL
    });
    next();
});

module.exports = server.exports();
