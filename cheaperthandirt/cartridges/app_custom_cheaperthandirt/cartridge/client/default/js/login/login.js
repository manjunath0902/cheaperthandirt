'use strict';
var baseLogin = require('base/login/login');
var formValidation = require('base/components/formValidation');
var createErrorNotification = require('base/components/errorNotification');
var annex = require('annexCloud/annex/annex');

baseLogin.clearResetForm = function () {
    $('#login .modal').on('hidden.bs.modal', function () {
        $('#reset-password-email').val('');
        $('#reset-password-email').closest('.form-group').find('label').removeClass('input-focus');
        $('.modal-dialog .form-control.is-invalid').removeClass('is-invalid');
    });
};

baseLogin.register = function () {
    $('form.registration').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        var url = form.attr('action');
        var annexToken = $('#annexToken').val();
        var siteID = $('#annexSiteID').val();
        var actionID = $('#annexCreateAccountActionID').val();
        form.spinner().start();
        $('form.registration').trigger('login:register', e);
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: form.serialize(),
            success: function (data) {
                form.spinner().stop();
                if (!data.success) {
                    formValidation(form, data);
                } else {
                    /* eslint-disable */
                    function callback() {
                        location.href = data.redirectUrl;
                    };
                    if (actionID !== null) {
                        annex.callSaActionScript(data.email, data.firstName, data.lastName, actionID, siteID, annexToken, callback);
                    } else {
                        location.href = data.redirectUrl;
                    }
                    /* eslint-enable */
                }
            },
            error: function (err) {
                if (err.responseJSON.redirectUrl) {
                    window.location.href = err.responseJSON.redirectUrl;
                } else {
                    createErrorNotification($('.error-messaging'), err.responseJSON.errorMessage);
                }

                form.spinner().stop();
            }
        });
        return false;
    });
};

baseLogin.login = function () {
    $('form.login').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        var url = form.attr('action');
        var annexToken = $('#annexToken').val();
        var siteID = $('#annexSiteID').val();
        form.spinner().start();
        $('form.login').trigger('login:submit', e);
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: form.serialize(),
            success: function (data) {
                form.spinner().stop();
                if (!data.success) {
                    formValidation(form, data);
                    $('form.login').trigger('login:error', data);
                } else {
                    if (data.lastVisitExpires && data.actionID != null) {
                        annex.callSaActionScript(data.customerEmail, data.customerFirstName, data.customerLastName, data.actionID, siteID, annexToken);
                    }
                    $('form.login').trigger('login:success', data);
                    location.href = data.redirectUrl;
                }
            },
            error: function (data) {
                if (data.responseJSON.redirectUrl) {
                    window.location.href = data.responseJSON.redirectUrl;
                } else {
                    $('form.login').trigger('login:error', data);
                    form.spinner().stop();
                }
            }
        });
        return false;
    });
};

module.exports = baseLogin;
