'use strict';
var upsCart = require('ups/cart/cart');

module.exports = {
    estimateShipRate: upsCart.estimateShipRate,
    optionalShipping: function () {
        $('.optional-shipping').click(function () {
            $('.shipping-head').toggle();
            $('.shipping-selection').toggleClass('shipAddress');
        });
    }
};
