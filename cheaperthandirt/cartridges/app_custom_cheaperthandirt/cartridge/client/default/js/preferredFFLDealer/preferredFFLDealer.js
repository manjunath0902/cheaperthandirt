'use strict';

var storeLocator = require('../storeLocator/storeLocator');
var formFields = require('../formFields/formFields');
var PerfectScrollbar = require('perfect-scrollbar/dist/perfect-scrollbar');

/**
 * Generates the modal window on the first call.
 *
 */
function getModalHtmlElement() {
    if ($('#inStoreInventoryModal').length !== 0) {
        $('#inStoreInventoryModal').remove();
    }
    var htmlString = '<!-- Modal -->'
        + '<div class="modal " id="inStoreInventoryModal" role="dialog">'
        + '<div class="modal-dialog in-store-inventory-dialog">'
        + '<!-- Modal content-->'
        + '<div class="modal-content">'
        + '<div class="modal-header">'
        + '    <span class="modal-title text-uppercase">Select Your FFL Dealer</span>'
        + '    <button type="button" class="close pull-right svg-25-Close_Button_Black svg-25-Close_Button_Black-dims" data-dismiss="modal" title="'
        +          $('.btn-get-in-store-inventory').data('modal-close-text') + '">'    // eslint-disable-line
        + '    </button>'
        + '</div>'
        + '<div class="modal-body"></div>'
        + '</div>'
        + '</div>'
        + '</div>';
    $('body').append(htmlString);
    $('#inStoreInventoryModal').modal('show');
}


/**
 * Replaces the content in the modal window with find stores components and
 * the result store list.
 * @param {number} selectedPostalCode - The postal code to search for inventory
 * @param {number} selectedRadius - The radius to search for inventory
 * @param {number} selectedStoreId - The Store ID to search for
 */
function fillModalElement(selectedPostalCode, selectedRadius, selectedStoreId) {
    $('#inStoreInventoryModal').spinner().start();
    var requestData = {};

    if (selectedRadius) {
        requestData.radius = selectedRadius;
    }

    if (selectedPostalCode) {
        requestData.postalCode = selectedPostalCode;
    }

    if (selectedStoreId) {
        requestData.storeId = selectedStoreId;
    }

    $.ajax({
        url: $('.btn-get-in-store-inventory').data('action-url'),
        data: requestData,
        method: 'GET',
        success: function (response) {
            $('.modal-body').empty();
            $('.modal-body').html(response.storesResultsHtml);
            storeLocator.search();
            storeLocator.changeRadius();
            storeLocator.selectStore();
            storeLocator.updateSelectStoreButton();

            if (selectedRadius) {
                $('#radius').val(selectedRadius);
            }

            if (selectedPostalCode) {
                $('#store-postal-code').val(selectedPostalCode);
            }

            if (!$('.results').data('has-results')) {
                $('.store-locator-no-results').show();
            }

            $('#inStoreInventoryModal').modal('show');
            formFields.updateSelect();
            formFields.adjustForAutofill();
            $('#inStoreInventoryModal').spinner().stop();
        },
        error: function () {
            $('#inStoreInventoryModal').spinner().stop();
        }
    });
}

module.exports = {
    showInStoreInventory: function () {
        $('.btn-get-in-store-inventory').on('click', function (e) {
            getModalHtmlElement();
            fillModalElement();
            e.stopPropagation();
        });
    },

    selectStore: function () {
        $('.store-locator-container').on('click', '.select-store', (function (e) {
            e.preventDefault();
            var selectedStore = $(':checked', '.results-card .results');
            var data = {
                storeID: selectedStore.val(),
                searchRadius: $('#radius').val(),
                searchPostalCode: $('.results').data('search-key').postalCode,
                storeDetailsHtml: selectedStore.siblings('label').find('.store-details').html(),
                event: e
            };

            $('body').trigger('store:selected', data);
        }));
    },

    selectStoreWithInventory: function () {
        $('body').on('store:selected', function (e, data) {
            var url = $('.storeID').data('action-url');
            var requestData = {};
            requestData.storeID = data.storeID;

            $.ajax({
                url: url,
                data: requestData,
                method: 'GET',
                success: function () {
                    var storeElement = $('.fflDealer');
                    var $changeStoreButton = $(storeElement).find('.change-store');
                    $($changeStoreButton).data('postal', data.searchPostalCode);
                    $($changeStoreButton).data('radius', data.searchRadius);
                    $(storeElement).find('.selected-store-with-inventory .card-body').empty();
                    $(storeElement).find('.selected-store-with-inventory .card-body').append(data.storeDetailsHtml);
                    $(storeElement).find('.store-name').attr('data-store-id', data.storeID);
                    $(storeElement).find('.selected-store-with-inventory').removeClass('d-none');
                    $(storeElement).find('.card-body .btn-get-in-store-inventory').hide();
                    $('#inStoreInventoryModal').modal('hide');
                    $('#inStoreInventoryModal').remove();
                }
            });
        });
    },

    changeStore: function () {
        $('body').on('click', '.change-store', function () {
            getModalHtmlElement();
            fillModalElement($(this).data('postal'), $(this).data('radius'), $('.store-name').data('store-id'));
        });
    },

    showMoreStores: function () {
        $('body').on('click', '.show-more-stores', function () {
            $(this).closest('.results.striped').find('.card-body').removeClass('show-more');
            $(this).addClass('d-none');
        });
    },

    ajaxCompleteFunction: function () {
        $(document).ajaxComplete(function () {
            if ($('input.select-store-input').length > 0) {
                $('input.select-store-input:first').trigger('click');
            }
            if (window.matchMedia('(min-width: 1024px)').matches && $('.results.striped').length > 0) {
                var resultsDiv = '.perfect-scrollbar';
                var perfectScrollbar = new PerfectScrollbar(resultsDiv);
                perfectScrollbar.update();
            }
        });
    }
};
