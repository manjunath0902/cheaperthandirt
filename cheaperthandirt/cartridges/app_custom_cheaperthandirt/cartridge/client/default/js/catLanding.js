'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./catLanding/catLanding'));
    processInclude(require('./recommendation'));
});
