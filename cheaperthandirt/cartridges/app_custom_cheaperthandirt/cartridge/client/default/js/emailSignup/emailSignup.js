'use strict';

var annex = require('annexCloud/annex/annex');

module.exports = {
    annexProduct: function () {
        $('form.signup-form').submit(function () {
            var annexToken = $('#annexToken').val();
            var siteID = $('#annexSiteID').val();
            var actionID = $('#emailSignupActionID').val();
            var lastName = $('#lastName').val();
            var firstName = $('#firstName').val();
            var email = $('#EmailAddress').val();
            if (actionID != null) {
                annex.callSaActionScript(email, firstName, lastName, actionID, siteID, annexToken);
            }
        });
    }
};
