'use strict';

var base = require('base/components/menu');

/**
 * User authentication menu dropdown
 */
function menu() {
    $('.navbar-header .user-authenticated .my-account .account-hover').on('click focusin', function (e) {
        if (window.matchMedia('(min-width: 1024px)').matches) {
            e.preventDefault();
            if ($('.navbar-header .user-authenticated .popover').length > 0) {
                $('.navbar-header .user-authenticated .popover').addClass('show');
            }
        }
    });

    $('.navbar-header .user-authenticated .my-account').on('mouseleave', function () {
        $('.navbar-header .user-authenticated .popover').removeClass('show');
    });

    $('.navbar-toggler').click(function (e) {
        e.preventDefault();
        $(this).closest('.page').addClass('menu-toggle');
    });

    $('.navbar>.close-menu>.close-button').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.page').removeClass('menu-toggle');
    });

    // GTM - events
    require('gtm/gtm/gtm').navigationClick();
    require('gtm/gtm/gtm').headerClicks();
    require('gtm/gtm/gtm').recommendations();
    require('gtm/gtm/gtm').productClick();
}

// To add an identifier based on no. of assets present on each category.
$('.navbar-nav > .nav-item').each(function () {
    var numassets = $(this).find('.Meg-nav-asset-holder li.dropdown').length;
    $(this).find('> .dropdown-menu').addClass('asset-' + numassets);
});


module.exports = {
    base: base,
    menu: menu
};
