'use strict';
var cart = require('../cart/cart');

var PerfectScrollbar = require('perfect-scrollbar/dist/perfect-scrollbar');

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 */
function displayMessageAndRemoveFromCart(data) {
    $.spinner().stop();
    var status = data.success ? 'alert-success' : 'alert-danger';

    if ($('.add-to-wishlist-messages').length === 0) {
        $('body').append('<div class="add-to-wishlist-messages "></div>');
    }
    $('.add-to-wishlist-messages')
        .append('<div class="add-to-wishlist-alert text-center ' + status + '">' + data.msg + '</div>');

    setTimeout(function () {
        $('.add-to-wishlist-messages').remove();
    }, 1500);
    var $targetElement = $('a[data-pid="' + data.pid + '"]').closest('.product-info').find('.remove-product');
    var itemToMove = {
        actionUrl: $targetElement.data('action'),
        productID: $targetElement.data('pid'),
        productName: $targetElement.data('name'),
        uuid: $targetElement.data('uuid')
    };
    $('body').trigger('afterRemoveFromCart', itemToMove);
    setTimeout(function () {
        $('.cart.cart-page #removeProductModal').modal();
    }, 2000);
}

module.exports = {
    cart: cart,
    initialize: function () {
        $('.minicart').on('count:update', function (event, count) {
            if (count && $.isNumeric(count.quantityTotal)) {
                $('.minicart .minicart-quantity').text(count.quantityTotal);
            }
        });

        $('.minicart').on('mouseenter focusin touchstart', function () {
            if ($('.search:visible').length === 0) {
                return;
            }
            var url = $('.minicart').data('action-url');
            var count = parseInt($('.minicart .minicart-quantity').text(), 10);

            if (count !== 0 && $('.minicart .popover.show').length === 0) {
                $('.minicart .popover').addClass('show');
                $('.minicart .popover').spinner().start();
                $.get(url, function (data) {
                    $('.minicart .popover').empty();
                    $('.minicart .popover').append(data);
                    var resultsDiv = '.perfect-scrollbar';
                    var perfectScrollbar = new PerfectScrollbar(resultsDiv);
                    perfectScrollbar.update();
                    $.spinner().stop();
                });
            }
        });
        $('body').on('touchstart click', function (e) {
            if ($('.minicart').has(e.target).length <= 0) {
                $('.minicart .popover').empty();
                $('.minicart .popover').removeClass('show');
            }
        });
        $('.minicart').on('mouseleave focusout', function (event) {
            if ((event.type === 'focusout' && $('.minicart').has(event.target).length > 0) ||
                (event.type === 'mouseleave' && $(event.target).is('.minicart .quantity')) ||
                $('body').hasClass('modal-open')) {
                event.stopPropagation();
                return;
            }
            $('.minicart .popover').empty();
            $('.minicart .popover').removeClass('show');
        });
        $('body').on('change', '.minicart .quantity', function () {
            if ($(this).parents('.bonus-product-line-item').length && $('.cart-page').length) {
                location.reload();
            }
        });
        $('.minicart').on('blur', '.quantity-form .quantity', function () {
            var $qty = $(this);
            if ($qty.val().length === 0 || parseInt($qty.val(), 10) === 0) {
                $qty.val($(this).data('pre-select-qty'));
            }
        });
    },
    moveToWishlist: function () {
        $('body').on('click', '.product-move .move', function (e) {
            e.preventDefault();
            var $this = $(this);
            var url = $(this).attr('href');
            var pid = $(this).data('pid');
            var optionId = $(this).closest('.product-info').find('.lineItem-options-values').data('option-id');
            var optionVal = $(this).closest('.product-info').find('.lineItem-options-values').data('value-id');
            optionId = optionId || null;
            optionVal = optionVal || null;
            if (!url || !pid) {
                return;
            }

            $.spinner().start();
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: {
                    pid: pid,
                    optionId: optionId,
                    optionVal: optionVal
                },
                success: function (data) {
                    // GTM - event
                    require('gtm/gtm/gtm').cartAddToWishlist($this);

                    displayMessageAndRemoveFromCart(data);
                },
                error: function (err) {
                    displayMessageAndRemoveFromCart(err);
                }
            });
        });
    }

};
