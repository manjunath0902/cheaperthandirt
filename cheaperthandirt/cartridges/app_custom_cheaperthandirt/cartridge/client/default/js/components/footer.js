'use strict';

/**
 * validate the email value wrt
 * @param {string} email - email id
 * @returns {boolean} - return test value
 */
function validateEmail(email) {
    var regex = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
    return regex.test(email);
}

module.exports = function () {
    $('.back-to-top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 500);
    });

    $('body').on('blur', '.signUp-form input[name="hpEmailSignUp"]', function () {
        var $emailField = $(this);

        var isValid = validateEmail($emailField.val());

        if (!isValid) {
            $emailField.addClass('is-invalid');
            $emailField.closest('div').find('.invalid-feedback').text($emailField.data('parse-error'));
            $emailField.closest('div').find('button').attr('disabled', true);
        } else if ($emailField.hasClass('is-invalid')) {
            $emailField.removeClass('is-invalid');
            $emailField.closest('div').find('.invalid-feedback').text('');
            $emailField.closest('div').find('button').removeAttr('disabled');
        }
    });
};
