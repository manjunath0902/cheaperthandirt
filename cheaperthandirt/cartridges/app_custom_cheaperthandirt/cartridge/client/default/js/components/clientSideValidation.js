'use strict';

var base = require('base/components/clientSideValidation');
var productDetail = require('../product/detail');

/**
 * Validate whole form. Requires `this` to be set to form object
 * @param {jQuery.event} event - Event to be canceled if form is invalid.
 * @returns {boolean} - Flag to indicate if form is valid
 */
function validateForm(event) {
    var valid = true;
    if (this.checkValidity && !this.checkValidity()) {
        // safari
        valid = false;
        if (event) {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
        }
        $(this).find('input, select, textarea').each(function () {
            if (!this.validity.valid) {
                $(this).trigger('invalid', this.validity);
            }
        });
    }
    return valid;
}

/**
 * Validate whole form. Requires `this` to be set to form object
 * @param {jQuery.event} event - Event to be canceled if form is invalid on blur.
 * @returns {boolean} - Flag to indicate if form is valid on blur
 */
function validateFormBlur(event) {
    var valid = true;
    if (this.checkValidity && !this.checkValidity()) {
        // safari
        valid = false;
        if (event) {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
        }
        if (!this.validity.valid) {
            this.setCustomValidity('');
            if (!this.validity.valid) {
                var validationMessage = this.validationMessage;
                $(this).addClass('is-invalid');
                if (this.validity.patternMismatch && $(this).data('pattern-mismatch')) {
                    validationMessage = $(this).data('pattern-mismatch');
                }
                if ((this.validity.rangeOverflow || this.validity.rangeUnderflow)
                    && $(this).data('range-error')) {
                    validationMessage = $(this).data('range-error');
                }
                if ((this.validity.tooLong || this.validity.tooShort)
                    && $(this).data('range-error')) {
                    validationMessage = $(this).data('range-error');
                }
                if (this.validity.valueMissing && $(this).data('missing-error')) {
                    validationMessage = $(this).data('missing-error');
                }
                $(this).parents('.form-group').find('.invalid-feedback')
                    .text(validationMessage);
            }
        }
    } else {
        $(this).removeClass('is-invalid');
        $(this).closest('div').find('.invalid-feedback').text('');
    }
    return valid;
}

/**
 * Validate the notify me email field on load
 */
function validateEmailOnLoad() {
    if ($('input#notify').length > 0 && $('input#notify').val().length > 0) {
        $('input#notify').trigger('blur');
    }
    if ($('input.gift-fields').length > 0) {
        $('input.gift-fields').trigger('keyup');
    }
}

base.invalid = function () {
    $('form input, form select, form textarea').on('invalid', function (e) {
        e.preventDefault();
        this.setCustomValidity('');
        if (!this.validity.valid) {
            var validationMessage = this.validationMessage;
            $(this).addClass('is-invalid');
            if (this.validity.patternMismatch && $(this).data('pattern-mismatch')) {
                validationMessage = $(this).data('pattern-mismatch');
            }
            if ((this.validity.rangeOverflow || this.validity.rangeUnderflow)
                && $(this).data('range-error')) {
                validationMessage = $(this).data('range-error');
            }
            if ((this.validity.tooLong || this.validity.tooShort)
                && $(this).data('range-error')) {
                validationMessage = $(this).data('range-error');
            }
            if (this.validity.valueMissing && $(this).data('missing-error')) {
                validationMessage = $(this).data('missing-error');
            }
            $(this).parents('.form-group').find('.invalid-feedback')
                .text(validationMessage);
        }
    });
};

module.exports = {
    invalid: base.invalid,

    submit: base.submit,

    buttonClick: base.buttonClick,

    functions: base.functions,

    focusoutValidation: function () {
        $('body').on('blur', 'input.focusout-validate', function (e) {
            if (!$(this).hasClass('equalto')) {
                validateFormBlur.call(this, e);
                if ($(this).hasClass('notify-me') || $(this).hasClass('zip-code') || $(this).hasClass('zip-code-field')) {
                    var validity = validateForm.call(this, e);
                    $(this).closest('form').find('button.button-submit-enabled').attr('disabled', !validity);
                }
            }
            var $label = $(this).closest('.form-group').find('label');
            if (!$(this).val().length && $label.hasClass('input-focus')) {
                $label.removeClass('input-focus');
            } else {
                $label.addClass('input-focus');
            }
            $(this).trigger('keyup');
        });
    },
    equalToValidate: function () {
        $('body').on('blur', 'input.equalto', function (e) {
            var firstElement = $(this).data('equalto');
            if (firstElement !== undefined) {
                if ($(this).val() !== $(firstElement).val()) {
                    $(this).addClass('is-invalid');
                    $(this).closest('div').find('.invalid-feedback').text($(this).data('mismatch-text'));
                } else {
                    $(this).removeClass('is-invalid');
                    $(this).closest('div').find('.invalid-feedback').text('');
                    return validateFormBlur.call(this, e);
                }
            }
            return true;
        });
    },
    enableAddToCartGC: function () {
        $('body').on('keyup blur', 'input.gift-fields', function () {
            var hasValues = false;
            var validatePass = false;
            var email = $('#giftcard-email').val();
            var confirmEmail = $('#giftcard-confirmEmail').val();
            if (email !== undefined && confirmEmail !== undefined && email !== '' && confirmEmail !== '' && email === confirmEmail) {
                hasValues = true;
            }
            if (($(this).closest('.giftcard-form-fields').find('input.is-invalid').length === 0) && hasValues === true) {
                validatePass = true;
            }
            var readyToOrder = $('.product-availability').data('readyToOrder') === undefined || $('button.add-to-cart').data('ready-to-order') === '' ? false : $('.product-availability').data('ready-to-order');
            var available = $('.product-availability').data('available') === undefined || $('button.add-to-cart').data('available') === '' ? false : $('.product-availability').data('available');
            if ($('button.add-to-cart').length > 0) {
                $('button.add-to-cart').attr('disabled', (!readyToOrder || !available || !hasValues) || !validatePass);
            } else {
                $('button.add-to-cart-global').attr('disabled', (!readyToOrder || !available || !hasValues) || !validatePass);
            }
        });
    },
    validateEmailOnLoad: validateEmailOnLoad,
    validateQuantity: productDetail.validateQuantity,
    validateNumberFields: productDetail.validateNumberFields
};
