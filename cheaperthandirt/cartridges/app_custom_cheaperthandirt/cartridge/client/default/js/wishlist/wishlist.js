'use strict';

var base = require('../product/base');
var wishquickView = require('wishlist/wishlist/wishlist');
var formFields = require('../formFields/formFields');
var PerfectScrollbar = require('perfect-scrollbar/dist/perfect-scrollbar');
/**
 * Generates the modal window on the first call.
 *
 */
function getModalHtmlElement() {
    if ($('#editProductModal').length !== 0) {
        $('#editProductModal').remove();
    }
    var htmlString = '<!-- Modal -->'
        + '<div class="modal fade" id="editWishlistProductModal" role="dialog">'
        + '<div class="modal-dialog quick-view-dialog">'
        + '<!-- Modal content-->'
        + '<div class="modal-content">'
        + '<div class="modal-header">'
        + '    <button type="button" class="close pull-right" data-dismiss="modal">'
        + '        <span class="svg-25-Close_Button_Black svg-25-Close_Button_Black-dims d-inline-block"></span>'
        + '    </button>'
        + '</div>'
        + '<div class="modal-body"></div>'
        + '<div class="modal-footer"></div>'
        + '</div>'
        + '</div>'
        + '</div>';
    $('body').append(htmlString);
}

/**
 * Updates the Mini-Cart quantity value after the customer has pressed the "Add to Cart" button
 * @param {string} response - ajax response from clicking the add to cart button
 */
function handlePostCartAdd(response) {
    $('.minicart').trigger('count:update', response);
    var messageType = response.error ? 'alert-danger' : 'alert-success';

    if ($('.add-to-cart-messages').length === 0) {
        $('body').append(
            '<div class="add-to-cart-messages"></div>'
        );
    }

    $('.add-to-cart-messages').append(
        '<div class="alert ' + messageType + ' add-to-basket-alert text-center" role="alert">'
        + response.message
        + '</div>'
    );

    setTimeout(function () {
        $('.add-to-basket-alert').remove();
    }, 5000);
}

/**
 * Updates the Mini-Cart quantity value after the customer has pressed the "Add to Cart" button
 */
function addToCartFromWishlist() {
    $('body').on('click', '.add-to-cart', function () {
        var pid;
        var addToCartUrl;
        var pidsQty;

        $('body').trigger('product:beforeAddToCart', this);

        pid = $(this).data('pid');
        addToCartUrl = $(this).data('url');
        pidsQty = parseInt($(this).closest('.product-info').find('.quantity-select').val(), 10);

        var form = {
            pid: pid,
            quantity: pidsQty
        };

        if ($(this).data('option')) {
            form.options = JSON.stringify($(this).data('option'));
        }

        $(this).trigger('updateAddToCartFormData', form);
        var firearmProd = $(this).data('firearm-product');
        if (addToCartUrl && !firearmProd) {
            $.ajax({
                url: addToCartUrl,
                method: 'POST',
                data: form,
                success: function (data) {
                    handlePostCartAdd(data);
                    $('body').trigger('product:afterAddToCart', data);
                    $.spinner().stop();
                },
                error: function () {
                    $.spinner().stop();
                }
            });
        } else if (firearmProd) {
            window.location.href = addToCartUrl;
            return;
        }
    });
}

/**
 * Parses the html for a modal window
 * @param {string} html - representing the body and footer of the modal window
 *
 * @return {Object} - Object with properties body and footer.
 */
function parseHtml(html) {
    var $html = $('<div>').append($.parseHTML(html));

    var body = $html.find('.product-quickview');
    var footer = $html.find('.modal-footer').children();

    return { body: body, footer: footer };
}

/**
 * replaces the content in the modal window for product variation to be edited.
 * @param {string} editProductUrl - url to be used to retrieve a new product model
 */
function fillModalElement(editProductUrl) {
    $('#editWishlistProductModal').spinner().start();

    $.ajax({
        url: editProductUrl,
        method: 'GET',
        dataType: 'html',
        success: function (html) {
            var parsedHtml = parseHtml(html);

            $('#editWishlistProductModal .modal-body').empty();
            $('#editWishlistProductModal .modal-body').html(parsedHtml.body);
            $('#editWishlistProductModal .modal-footer').html(parsedHtml.footer);
            $('#editWishlistProductModal').modal('show');
            formFields.updateSelect();
            setTimeout(function () {
                base.loadZoom();
                base.pdpImageSlick();
                var resultsDiv = '.perfect-scrollbar';
                var perfectScrollbar = new PerfectScrollbar(resultsDiv);
                perfectScrollbar.update();
            }, 500);
            $.spinner().stop();
        },
        error: function () {
            $('#editWishlistProductModal').spinner().stop();
        }
    });
}

/**
 * @param {Object} $elementAppendTo - The element to append error html to
 * @param {string} msg - The error message
 * display error message if remove item from wishlist failed
 */
function displayErrorMessage($elementAppendTo, msg) {
    if ($('.remove-from-wishlist-messages').length === 0) {
        $elementAppendTo.append(
            '<div class="remove-from-wishlist-messages "></div>'
        );
    }
    $('.remove-from-wishlist-messages')
        .append('<div class="remove-from-wishlist-alert text-center alert-danger">' + msg + '</div>');

    setTimeout(function () {
        $('.remove-from-wishlist-messages').remove();
    }, 3000);
}

/**
 * renders the list up to a given page number
 * @param {number} pageNumber - current page number
 * @param {boolean} spinner - if the spinner has already started
 */
function renderNewPageOfItems(pageNumber, spinner) {
    var publicView = $('.wishlistItemCardsData').data('public-view');
    var listUUID = $('.wishlistItemCardsData').data('uuid');
    var url = $('.wishlistItemCardsData').data('href');
    if (spinner) {
        $.spinner().start();
    }
    var scrollPosition = document.documentElement.scrollTop;
    var newPageNumber = pageNumber;
    $.ajax({
        url: url,
        method: 'get',
        data: {
            pageNumber: ++newPageNumber,
            publicView: publicView,
            id: listUUID
        }
    }).done(function (data) {
        $('.wishlistItemCards').empty();
        $('body .wishlistItemCards').append(data);
        document.documentElement.scrollTop = scrollPosition;
    }).fail(function () {
        $('.more-wl-items').remove();
    });
    $.spinner().stop();
}
var wishlist = {
    removeFromWishlist: function () {
        $('body').on('click', '.remove-from-wishlist', function (e) {
            e.preventDefault();
            var url = $(this).data('url');
            var elMyAccount = $('.account-wishlist-item').length;
            var count = $('.wishlist-item-count').attr('data-count') - 1;

            // If user is in my account page, call removeWishlistAccount() end point, re-render wishlist cards
            if (elMyAccount > 0) {
                $('.wishlist-account-card').spinner().start();
                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'html',
                    data: {},
                    success: function (html) {
                        $('.wishlist-account-card .card-body-list .card-body').remove();
                        $('.wishlist-account-card .card-body-list').append(html);
                        $('.wishlist-item-count').attr('data-count', count);
                        $('.wishlist-item-count').text(count);
                        $('.wishlist-account-card').spinner().stop();
                        if (count === 0) {
                            $('.card.wishlist-card').find('.card-footer').addClass('d-none');
                        } else {
                            $('.card.wishlist-card').find('.card-footer').removeClass('d-none');
                        }
                    },
                    error: function () {
                        var $elToAppend = $('.wishlist-account-card');
                        $elToAppend.spinner().stop();
                        var msg = $elToAppend.data('error-msg');
                        displayErrorMessage($elToAppend, msg);
                    }
                });
            // else user is in wishlist landing page, call removeProduct() end point, then remove this card
            } else {
                $.spinner().start();
                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    data: {},
                    success: function () {
                        var pageNumber = $('.wishlistItemCardsData').data('page-number') - 1;
                        renderNewPageOfItems(pageNumber, false);
                    },
                    error: function () {
                        $.spinner().stop();
                        var $elToAppendWL = $('.wishlistItemCards');
                        var msg = $elToAppendWL.data('error-msg');
                        displayErrorMessage($elToAppendWL, msg);
                    }
                });
            }
        });
    },

    viewProductViaEdit: function () {
        $('body').on('click', '.edit-add-to-wishlist .edit', function (e) {
            e.preventDefault();

            var editProductUrl = $(this).attr('href');
            getModalHtmlElement();
            fillModalElement(editProductUrl);
            formFields.updateSelect();
            formFields.adjustForAutofill();
        });
    },

    viewProductViaSelectAttribute: function () {
        $('body').on('click', '.select-attributes-btn', function (e) {
            e.preventDefault();

            var editProductUrl = $(this).data('get-product-url');
            getModalHtmlElement();
            fillModalElement(editProductUrl);
        });
    },

    updateWishlistUpdateButton: wishquickView.updateWishlistUpdateButton,

    updateWishListItem: wishquickView.updateWishListItem,

    toggleWishlistStatus: wishquickView.toggleWishlistStatus,

    toggleWishlistItemStatus: wishquickView.toggleWishlistItemStatus,

    addToCartFromWishlist: addToCartFromWishlist,

    moreWLItems: wishquickView.moreWLItems,

    copyWishlistLink: wishquickView.copyWishlistLink,

    submitWishlistSearch: wishquickView.submitWishlistSearch,

    moreWLSearchResults: wishquickView.moreWLSearchResults
};

var exportDetails = $.extend({}, wishlist);
module.exports = exportDetails;
