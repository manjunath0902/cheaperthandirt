'use strict';

module.exports = {
    printInvoice: function () {
        $('body').on('click', '.page .print-receipt', function () {
            window.print();
        });
    },

    windowLoad: function () {
        if ((navigator.userAgent.indexOf('Firefox') > -1)) {
            $('.page').addClass('resolve-print-page');
        } else {
            $('.page').removeClass('resolve-print-page');
        }
    }
};
