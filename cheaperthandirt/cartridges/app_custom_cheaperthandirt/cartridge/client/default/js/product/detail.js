'use strict';

var base = require('./base');
var baseDetail = require('base/product/detail');
var instoreD = require('instorepickup/product/details');

/**
 * Passing padding bottom to footer when AddToCart Sticky Element having fixed position in Touch Devices.
 */
function addToCartStickyElement() {
    if (($(window).width() <= 1023) && ($('.container').hasClass('product-detail'))) {
        var $price = $('.container.product-detail').find('.prices-height');
        var $Cart = $('.container.product-detail').find('.cart-and-ipay');
        var heightPrice = 0;
        var heightCart = 0;
        if ($price.length > 0 && $Cart.length > 0) {
            heightPrice = $price.outerHeight();
            heightCart = $Cart.outerHeight();
        }
        var totalHeight = parseInt(heightPrice, 10) + parseInt(heightCart, 10);
        $('.container.product-detail').siblings('footer').css('padding-bottom', totalHeight);
        if ($('.notify-me-form #notify-me:visible').length > 0) {
            $('.container.product-detail').find('.pdp-right').addClass('out-of-stock-div');
        }
    }
}

/**
 * Generates the modal window on the first call.
 *
 */
function videoImageHtmlElement() {
    if ($('#youtubeVideoModal').length !== 0) {
        $('#youtubeVideoModal').remove();
    }
    var htmlString = '<!-- Modal -->'
        + '<div class="modal fade" id="youtubeVideoModal" role="dialog">'
        + '<div class="modal-dialog youtubeVideoModal">'
        + '<!-- Modal content-->'
        + '<div class="modal-content">'
        + '<div class="modal-header">'
        + '    <button type="button" class="close pull-right" data-dismiss="modal">'
        + '        <span class="svg-25-Close_Button_Black svg-25-Close_Button_Black-dims d-inline-block"></span>'
        + '    </button>'
        + '</div>'
        + '<div class="modal-body"></div>'
        + '</div>'
        + '</div>'
        + '</div>';
    $('body').append(htmlString);
}

/**
 * Appends the modal body on click on video image button and source url on click of close button
 *
 */
function videoImageOnClick() {
    $('body').on('click', '.video-image', function () {
        videoImageHtmlElement();
        $('#youtubeVideoModal .modal-body').empty();
        $('#youtubeVideoModal .modal-body').html($('.video-modal-body').html());
    });

    $('body').on('click', '#youtubeVideoModal .close, .modal-backdrop, #youtubeVideoModal.modal', function () {
        $('#youtubeVideoModal iframe').attr('src', $('#youtubeVideoModal iframe').attr('src'));
    });
}

/**
 * Resize of Touch device
 */
function resizeAddToCartSticky() {
    $(window).resize(function () {
        addToCartStickyElement();
    });
}

/**
 * Character Count for the Physical and Electronic Gift Card
 */
function textarearCharCount() {
    var textMaxium = 255;
    $('body').find('#count_message').html('0/' + textMaxium + ' Characters');
    $('body').keyup('#addMessage', function () {
        var $textLength = $('#addMessage');
        if ($textLength.length > 0) {
            var textLength = $('#addMessage').val().length;
            $('#count_message').html(textLength + '/' + textMaxium + ' Characters');
        }
    });
}

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 */
function displayMessageAndChangeIcon(data) {
    $.spinner().stop();
    var status;
    if (data.success) {
        status = 'alert-success';
    } else {
        status = 'alert-danger';
    }
    if ($('.notifyme-alert').length === 0) {
        $('#notify-me').prepend(
            '<div class="notifyme-alert "></div>'
        );
    }
    $('.notifyme-alert')
        .append('<div class="notifyme-alert-message text-center ' + status + '">' + data.msg + '</div>');

    setTimeout(function () {
        $('.notifyme-alert').remove();
    }, 5000);
}

/**
 * Notify Me events
 */
function notifyMe() {
    $('.notify-form .notify-me').val('');
    if ($('.user-authenticated').length > 0) {
        $('.notify-form .notify-me').val($('.user-authenticated').data('customer-email'));
    }
    $('body').on('click', '.notify-button', function (e) {
        e.preventDefault();
        $.spinner().start();
        var productID = $('.product-id').length > 0 ? $('.product-id').text() : $('#notify').data('productid');
        var requestData = {
            productID: productID,
            email: $('#notify').val()
        };
        $.ajax({
            url: $('.notify-button').data('action-url'),
            data: requestData,
            method: 'POST',
            success: function (response) {
                displayMessageAndChangeIcon(response);
            },
            error: function () {
                displayMessageAndChangeIcon({ success: false, msg: 'Unexpected Error! Please try again after sometime' });
            }
        });
    });
}

/**
 * Making sure the quantity field is replaced with 1 in case it is empty or 0
 */
function validateQuantity() {
    $('body').on('blur', '#quantity', function () {
        var $qty = $(this);
        if ($qty.val().length === 0 || parseInt($qty.val(), 10) === 0) {
            $qty.val('1');
        }
    });
}

/**
 * Making sure the quantity field accepts only numeric values
 */
function validateNumberFields() {
    /* change  Event to handle only number in text fields */
    $('body').on('change input', '#quantity', function () {
        var $this = $(this);
        var maxChar = parseInt($this.attr('maxCharacter'), 10);
        $this.val(this.value.replace(/[^\d.]/g, ''));
        $this.val($this.val().slice(0, maxChar));
    });
}

/**
 * clicking on reviews count should activate the reviews tab
 */
function reviewsCount() {
    $('body').on('click', '.pr-snippet-review-count', function (e) {
        var scrollPosition;
        if (!$('.description-and-detail').find('.tab-pane.reviews').hasClass('active') && window.matchMedia('(min-width: 1024px)').matches) {
            e.preventDefault();
            scrollPosition = $('.description-and-detail').find('#reviews-tab-section').offset().top + 50;
            $('.description-and-detail').find('#reviews-tab-section').trigger('click');
            $('html, body').animate({
                /* eslint-disable */
                'scrollTop': scrollPosition
                /* eslint-enable */
            }, 1000);
        } else if (window.matchMedia('(max-width: 1023px)').matches) {
            if ($('.description-and-detail').find('.reviews a#reviews-tab-section-device').hasClass('collapsed')) {
                e.preventDefault();
                scrollPosition = $('.description-and-detail').find('a#reviews-tab-section-device').offset().top + 50;
                $('.description-and-detail').find('.reviews a#reviews-tab-section-device').trigger('click');
                $('html, body').animate({
                    /* eslint-disable */
                    'scrollTop': scrollPosition
                    /* eslint-enable */
                }, 1000);
            }
        }
    });
}

instoreD.availability = function () {
    $(document).on('change', '.quantity-select', function (e) {
        e.preventDefault();

        var searchPID = $(this).closest('.product-detail').attr('data-pid');
        var selectorPrefix = '.product-detail[data-pid="' + searchPID + '"]';

        if ($(selectorPrefix + ' .selected-store-with-inventory').is(':visible')) {
            return;
        }

        var $productContainer = $(this).closest('.product-detail');
        if (!$productContainer.length) {
            $productContainer = $(this).closest('.modal-content').find('.product-quickview');
        }

        if ($('.bundle-items', $productContainer).length === 0) {
            base.attributeSelect($(e.currentTarget).find('option:selected').data('url'),
                $productContainer, e);
        }
    });
};


var detail = {
    availability: base.availability,

    addToCart: base.addToCart,

    updateAttributesAndDetails: function () {
        $('body').on('product:statusUpdate', function (e, data) {
            var $productContainer = $('.product-detail[data-pid="' + data.id + '"]');
            if ($productContainer.length === 0) {
                $productContainer = $('.product-detail');
            }
            $productContainer.find('.description-and-detail .product-attributes')
                .empty()
                .html(data.attributesHtml);
            if (data.productName) {
                $productContainer.find('.product-name').text(data.productName);
            }
            if (data.longDescription) {
                $productContainer.find('.description-and-detail')
                    .removeClass('hidden-xl-down');
                $productContainer.find('.description-and-detail .content')
                    .empty()
                    .html(data.longDescription);
            }
        });
    },

    showSpinner: baseDetail.showSpinner,

    updateAttribute: baseDetail.updateAttribute,

    updateAddToCart: function () {
        $('body').on('product:updateAddToCart', function (e, response) {
            // update local add to cart (for sets)
            var hasValues = true;
            if ($('.container.product-detail').hasClass('electronic')) {
                hasValues = false;
                var email = $('#giftcard-email').val();
                var confirmEmail = $('#giftcard-confirmEmail').val();
                if (email !== undefined && confirmEmail !== undefined && email !== '' && confirmEmail !== '' && email === confirmEmail) {
                    hasValues = true;
                }
            }
            $('button.add-to-cart', response.$productContainer).attr('disabled',
                (!response.product.readyToOrder || !response.product.available || !hasValues));
            $('button.add-to-cart').data('readyToOrder', response.product.readyToOrder);
            $('button.add-to-cart').data('available', response.product.available);

            var enable = $('.product-availability').toArray().every(function (item) {
                return $(item).data('available') && $(item).data('ready-to-order');
            });
            $('button.add-to-cart-global').attr('disabled', !enable);
        });
    },
    updateAvailability: baseDetail.updateAvailability,
    sizeChart: baseDetail.sizeChart,
    socialSharing: function () {
        window.socialSharedialog = function (url, w, h) {
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            return window.open(url, '_blank', 'width = ' + w + ', height = ' + h + ', top = ' + top + ', left = ' + left);
        };
    },
    validateQuantity: validateQuantity,
    validateNumberFields: validateNumberFields,
    restrictValidation: function () {
        $('body').on('click', '.postal-code-submit', function (e) {
            e.preventDefault();
            var productID = $('.product-id').length > 0 ? $('.product-id').text() : $('#notify').data('productid');
            var $form = $(this).closest('form');
            $form.find('.pid').val(productID);
            $form.spinner().start();
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                dataType: 'json',
                data: $form.serialize(),
                success: function (res) {
                    $form.spinner().stop();
                    if (!res.success) {
                        $form.find('.response-place-holder').removeClass('d-none').removeClass('error').find('span')
                        .text(res.responseMsg);
                    } else {
                        $form.find('.response-place-holder').removeClass('d-none').addClass('error').find('span')
                        .text(res.responseMsg);
                    }
                },
                error: function () {
                    $form.spinner().stop();
                }
            });
        });
    },

    // GTM - Event
    gtmPDPEvents: require('gtm/gtm/gtm').gtmPDPEvents
};

var exportDetails = $.extend({}, base.pdpImageSlick, base.loadZoom, detail, instoreD.availability, { addToCartStickyElement: addToCartStickyElement, resizeAddToCartSticky: resizeAddToCartSticky, textarearCharCount: textarearCharCount, notifyMe: notifyMe, reviewsCount: reviewsCount, videoImageOnClick: videoImageOnClick });
module.exports = exportDetails;
