'use strict';

var base = require('./base');
var quickView = require('base/product/quickView');
var formFields = require('../formFields/formFields');
var PerfectScrollbar = require('perfect-scrollbar/dist/perfect-scrollbar');
var clientSideValidation = require('../components/clientSideValidation');
var productDetail = require('./detail');

/**
 * Generates the modal window on the first call.
 *
 */
function getModalHtmlElement() {
    if ($('#quickViewModal').length !== 0) {
        $('#quickViewModal').remove();
    }
    var htmlString = '<!-- Modal -->'
        + '<div class="modal fade" id="quickViewModal" role="dialog">'
        + '<div class="modal-dialog quick-view-dialog">'
        + '<!-- Modal content-->'
        + '<div class="modal-content">'
        + '<div class="modal-header">'
        + '    <a class="full-pdp-link" href="">View Full Details</a>'
        + '    <button type="button" class="close pull-right" data-dismiss="modal">'
        + '        <span class="svg-25-Close_Button_Black svg-25-Close_Button_Black-dims d-inline-block"></span>'
        + '    </button>'
        + '</div>'
        + '<div class="modal-body"></div>'
        + '<div class="modal-footer"></div>'
        + '</div>'
        + '</div>'
        + '</div>';
    $('body').append(htmlString);
}

/**
 * Generates HTML for preferred storeDetails.
 * @param {json} storeInfo - Renders JSON Object
 * @return {storeDetailsHtml} - storeDetails content
 *
 */
function getStoreInfoElement(storeInfo) {
    var storeDetailsHtml = '<div class="store-details" data-store-id="' + storeInfo.ID + '">'
    + '<input type="hidden" name="storeId" value="' + storeInfo.ID + '" />'
    + '<div class="store-name">' + storeInfo.name + '</div><address>'
    + '<a class="store-map" target="_blank" href="https://maps.google.com/?daddr=' + storeInfo.latitude + ',' + storeInfo.longitude + '">' + storeInfo.address1;
    if (storeInfo.address2) {
        storeDetailsHtml += ' ' + storeInfo.address2;
    }
    if (storeInfo.city) {
        storeDetailsHtml += ' ' + storeInfo.city;
    }
    if (storeInfo.stateCode) {
        storeDetailsHtml += ' ' + storeInfo.stateCode;
    }
    storeDetailsHtml += ' ' + storeInfo.postalCode + '</a>';
    if (storeInfo.phone) {
        storeDetailsHtml += '<p><span><a class="storelocator-phone" href="tel:' + storeInfo.phone + '"><u>' + storeInfo.phone + '</u></a></span></p>';
    }
    storeDetailsHtml += '<div class="store-hours">';
    if (storeInfo.storeHours) {
        storeDetailsHtml += storeInfo.storeHours;
    }
    storeDetailsHtml += '</div></address></div>';
    return storeDetailsHtml;
}

/**
 * @typedef {Object} QuickViewHtml
 * @property {string} body - Main Quick View body
 * @property {string} footer - Quick View footer content
 */

/**
 * Parse HTML code in Ajax response
 *
 * @param {string} html - Rendered HTML from quickview template
 * @return {QuickViewHtml} - QuickView content components
 */
function parseHtml(html) {
    var $html = $('<div>').append($.parseHTML(html));

    var body = $html.find('.product-quickview');
    var footer = $html.find('.modal-footer').children();

    return { body: body, footer: footer };
}

/**
 * replaces the content in the modal window on for the selected product variation.
 * @param {string} selectedValueUrl - url to be used to retrieve a new product model
 */
function fillModalElement(selectedValueUrl) {
    $('.modal-body').spinner().start();
    $.ajax({
        url: selectedValueUrl,
        method: 'GET',
        dataType: 'json',
        success: function (data) {
            var parsedHtml = parseHtml(data.renderedTemplate);

            $('.modal-body').empty();
            $('.modal-body').html(parsedHtml.body);
            $('.modal-footer').html(parsedHtml.footer);
            $('#quickViewModal .full-pdp-link').attr('href', data.masterProductUrl);
            $('#quickViewModal .size-chart').attr('href', data.masterProductUrl);
            $('#quickViewModal .setPrice').html(data.productSetPrice);
            $('#quickViewModal #notify').val(data.custEmail);
            $('#quickViewModal .purchase-message').html(data.purchaseMessage);
            $('#quickViewModal .purchase-body').html(data.purchaseBody);
            $('#quickViewModal .fee-firearm-message').html(data.firearmContent);
            if (data.preferredStoreDetails !== null) {
                $('#quickViewModal .fire-bell-message').addClass('d-none');
                $('#quickViewModal .selected-store-with-inventory').removeClass('d-none');
                $('#quickViewModal .card-body').html(getStoreInfoElement(data.preferredStoreDetails));
                var changeDealer = data.productUrl + '#ffldealer';
                $('#quickViewModal .button-change-dealer').attr('href', changeDealer);
                $('#quickViewModal').find('.store-name').attr('data-store-id', data.preferredStoreDetails.ID);
                $('#quickViewModal .shop-this-item').addClass('d-none');
                $('#quickViewModal .cart-and-ipay').removeClass('d-none');
            }
            $('#quickViewModal').modal('show');
            formFields.updateSelect();
            formFields.adjustForAutofill();
            setTimeout(function () {
                base.loadZoom();
                base.pdpImageSlick();
                var $this;
                if ($('.product-quickview').hasClass('product-set')) {
                    $this = $('.set-item');
                } else if ($('.product-quickview').hasClass('product-bundle')) {
                    $this = $('.bundle-item');
                }

                if ($('.product-quickview').hasClass('product-set') || $('.product-quickview').hasClass('product-bundle')) {
                    var count = 0;
                    $this.each(function () {
                        var resultsDiv = '.perfect-scrollbar' + count;
                        var perfectScrollbar = new PerfectScrollbar(resultsDiv);
                        perfectScrollbar.update();
                        count += 1;
                    });
                } else {
                    var resultsDiv = '.perfect-scrollbar';
                    var perfectScrollbar = new PerfectScrollbar(resultsDiv);
                    perfectScrollbar.update();
                }
                clientSideValidation.validateEmailOnLoad();
                clientSideValidation.enableAddToCartGC();
                clientSideValidation.equalToValidate();
                clientSideValidation.focusoutValidation();
            }, 500);
            if ($('.notify-me-form').hasClass('d-none')) {
                if ($('.btn-shop-item').length > 0 && $('.btn-shop-item').hasClass('d-none')) {
                    $('.btn-shop-item').removeClass('d-none');
                }
                if ($('button.add-to-cart').length > 0 && $('button.add-to-cart').hasClass('d-none')) {
                    $('button.add-to-cart').removeClass('d-none');
                }
                if ($('button.add-to-cart-global').length > 0 && $('button.add-to-cart').hasClass('d-none')) {
                    $('button.add-to-cart-global').removeClass('d-none');
                }
            } else {
                if ($('.btn-shop-item').length > 0) {
                    $('.btn-shop-item').addClass('d-none');
                }
                if ($('button.add-to-cart').length > 0) {
                    $('button.add-to-cart').addClass('d-none');
                }
                if ($('button.add-to-cart-global').length > 0) {
                    $('button.add-to-cart-global').addClass('d-none');
                }
            }
            // GTM - event
            require('gtm/gtm/gtm').quickView(data);
            $('body').trigger('select:noVariation');
            $.spinner().stop();
        },
        error: function () {
            $.spinner().stop();
        }
    });
}

/**
 * Quickview hover JS
 */
var quickViewEvents = function () {
    $('body').on('mouseenter focusin', '.product-tile .product-image', function () {
        if (window.matchMedia('(min-width: 1024px)').matches) {
            $('.product-tile').removeClass('quickview-show');
            $(this).closest('.product-tile').addClass('quickview-show');
        }
    });
    $('body').on('mouseleave', '.product-tile .modal-background-tile', function () {
        $(this).closest('.product-tile').removeClass('quickview-show');
    });

    $('body').on('mouseenter', '.quickview', function () {
        $(this).closest('.product-tile').find('.product-image').trigger('mouseenter');
    });

    $('body').on('click', function (e) {
        var $trigger = $('.product-tile .product-image');
        if ($trigger !== e.target && !$trigger.has(e.target).length) {
            $('.product-tile').removeClass('quickview-show');
        }
    });
    productDetail.notifyMe();
    productDetail.validateQuantity();
    productDetail.validateNumberFields();
    productDetail.textarearCharCount();
    productDetail.updateAddToCart();
    productDetail.videoImageOnClick();
};

quickView.quickViewEvents = quickViewEvents;
quickView.colorAttribute = base.colorAttribute;
quickView.amountAttribute = base.amountAttribute;
quickView.selectAttribute = base.selectAttribute;
quickView.removeBonusProduct = base.removeBonusProduct;
quickView.selectBonusProduct = base.selectBonusProduct;
quickView.enableBonusProductSelection = base.enableBonusProductSelection;
quickView.showMoreBonusProducts = base.showMoreBonusProducts;
quickView.addBonusProductsToCart = base.addBonusProductsToCart;
quickView.availability = base.availability;
quickView.addToCart = base.addToCart;

quickView.showQuickview = function () {
    $('body').on('click', '.quickview', function (e) {
        e.preventDefault();
        var selectedValueUrl = $(this).closest('a.quickview').attr('href');
        $(e.target).trigger('quickview:show');
        getModalHtmlElement();
        fillModalElement(selectedValueUrl);
    });
};

quickView.updateAvailability = function () {
    $('body').on('product:updateAvailability', function (e, response) {
        // bundle individual products
        $('.product-availability', response.$productContainer)
            .data('ready-to-order', response.product.readyToOrder)
            .data('available', response.product.available)
            .find('.availability-msg')
            .empty()
            .html(response.message);

        if (response.message === '<div class="availability-status-msg">Out of Stock</div>') {
            if (response.$productContainer.hasClass('set-item')) {
                var $this = response.$productContainer;
                $this.find('.notify-me-form').removeClass('d-none');
            } else {
                $('.notify-me-form').removeClass('d-none');
            }
            if ($('.btn-shop-item').length > 0) {
                $('.btn-shop-item').addClass('d-none');
            } else if ($('.add-to-cart-global').length > 0) {
                $('.add-to-cart-global').addClass('d-none');
            }
            $('.notify-quickview-message').removeClass('d-none');
        } else if (response.message === '<div class="availability-status-msg">In Stock</div>') {
            $('.notify-me-form').addClass('d-none');
            if ($('.btn-shop-item').length > 0) {
                $('.btn-shop-item').removeClass('d-none');
            } else if ($('.add-to-cart-global').length > 0) {
                $('.add-to-cart-global').removeClass('d-none');
            }
            $('.notify-quickview-message').addClass('d-none');
        }

        var dialog = $(response.$productContainer)
            .closest('.quick-view-dialog');

        if ($('.product-availability', dialog).length) {
            // bundle all products
            var allAvailable = $('.product-availability', dialog).toArray()
                .every(function (item) { return $(item).data('available'); });

            var allReady = $('.product-availability', dialog).toArray()
                .every(function (item) { return $(item).data('ready-to-order'); });

            $('.global-availability', dialog)
                .data('ready-to-order', allReady)
                .data('available', allAvailable);

            $('.global-availability .availability-msg', dialog).empty()
                .html(allReady ? response.message : response.resources.info_selectforstock);
        } else {
            // single product
            $('.global-availability', dialog)
                .data('ready-to-order', response.product.readyToOrder)
                .data('available', response.product.available)
                .find('.availability-msg')
                .empty()
                .html(response.message);
        }
    });
};

quickView.updateAttribute = function () {
    $('body').on('product:afterAttributeSelect', function (e, response) {
        if ($('.modal.show .product-quickview>.bundle-items').length) {
            $('.modal.show').find(response.container).data('pid', response.data.product.id);
            $('.modal.show').find(response.container)
                .find('.product-id').text(response.data.product.id);
        } else if ($('.set-items').length) {
            response.container.find('.product-id').text(response.data.product.id);
        } else {
            $('.modal.show .product-quickview').data('pid', response.data.product.id);
        }
    });
};

module.exports = quickView;
