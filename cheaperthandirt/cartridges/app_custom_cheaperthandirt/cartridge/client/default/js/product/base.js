'use strict';

var base = require('base/product/base');

window.jqueryzoom = window.jqueryZoom = require('jquery-zoom/jquery.zoom.min');
var pinchZoom = require('pinch-zoom-js/src/pinch-zoom');
var imagesLoaded = require('imagesloaded');
var formFields = require('../formFields/formFields');

/**
 * appends params to a url
 * @param {string} url - Original url
 * @param {Object} params - Parameters to append
 * @returns {string} result url with appended parameters
 */
function appendToUrl(url, params) {
    var newUrl = url;
    newUrl += (newUrl.indexOf('?') !== -1 ? '&' : '?') + Object.keys(params).map(function (key) {
        return key + '=' + encodeURIComponent(params[key]);
    }).join('&');

    return newUrl;
}

/**
 * Retrieves the relevant pid value
 * @param {jquery} $el - DOM container for a given add to cart button
 * @return {string} - value to be used when adding product to cart
 */
function getPidValue($el) {
    var pid;

    if ($('#quickViewModal').hasClass('show') && !$('.product-set').length) {
        pid = $($el).closest('.modal-content').find('.product-quickview').data('pid');
    } else if ($('.product-set-detail').length || $('.product-set').length) {
        pid = $($el).closest('.product-detail').find('.product-id').text();
    } else {
        pid = $('.product-detail:not(".bundle-item")').data('pid');
    }

    return pid;
}

/**
 * Retrieve contextual quantity selector
 * @param {jquery} $el - DOM container for the relevant quantity
 * @return {jquery} - quantity selector DOM container
 */
function getQuantitySelector($el) {
    return $el && $('.set-items').length
        ? $($el).closest('.product-detail').find('.quantity-select')
        : $('.quantity-select');
}

/**
 * Retrieves the value associated with the Quantity pull-down menu
 * @param {jquery} $el - DOM container for the relevant quantity
 * @return {string} - value found in the quantity input
 */
function getQuantitySelected($el) {
    return getQuantitySelector($el).val();
}


/**
 * PDP image Slick
 */
function pdpImageSlick() {
    if ($('.product-detail .primaryimage-slick').hasClass('slick-initialized')) {
        $('.product-detail .primaryimage-slick').slick('unslick');
    }
    $('.product-detail .primaryimage-slick').slick({
        dots: true,
        infinite: false,
        arrows: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: 'ease-in-out',
        prevArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-prev"></span>',
        nextArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-next"></span>'
    });
}

/**
 * PDP Zoom initialization Event
 * @param {container} container the element
 */
function initializeZoom(container) {
    imagesLoaded(container).on('always', function () {
        var $container = $(container);
        $(container).trigger('zoom.destroy');
        $(container).find('.zoomImg').remove();
        $container.zoom();
    });
}

/**
 * PDP Load Zoom functionality
 */
function loadZoom() {
    var imgZoom = '.primary-images .desktop-zoom';

    if (window.matchMedia('(max-width: 1024px)').matches) {
        // remove zoom
        $(imgZoom).trigger('zoom.destroy');
        var imageContainer = imgZoom;
        var $imageContainer = $(imageContainer);
        imagesLoaded(imageContainer).on('always', function () {
            $imageContainer.each(function () {
                /* eslint-disable */
                new pinchZoom.default(this, {});
                /* eslint-enable */
            });
        });
    } else {
        var container = imgZoom;
        initializeZoom(container);
    }
}

/**
 * Process the attribute values for an attribute that has image swatches
 *
 * @param {Object} attr - Attribute
 * @param {string} attr.id - Attribute ID
 * @param {Object[]} attr.values - Array of attribute value objects
 * @param {string} attr.values.value - Attribute coded value
 * @param {string} attr.values.url - URL to de/select an attribute value of the product
 * @param {boolean} attr.values.isSelectable - Flag as to whether an attribute value can be
 *     selected.  If there is no variant that corresponds to a specific combination of attribute
 *     values, an attribute may be disabled in the Product Detail Page
 * @param {jQuery} $productContainer - DOM container for a given product
 */
function processSwatchValues(attr, $productContainer) {
    var displayValue = '';
    attr.values.forEach(function (attrValue) {
        var $attrValue = $productContainer.find('[data-attr="' + attr.id + '"] [data-attr-value="' +
            attrValue.value + '"]');
        var $swatchAnchor = $attrValue.parent();
        if (attrValue.selected) {
            displayValue = attrValue.displayValue;
            $attrValue.addClass('selected');
        } else {
            $attrValue.removeClass('selected');
        }

        if (attrValue.url) {
            $swatchAnchor.attr('href', attrValue.url);
        } else {
            $swatchAnchor.removeAttr('href');
        }

        // Disable if not selectable
        $attrValue.removeClass('selectable unselectable');

        $attrValue.addClass('selectable');
    });
    $('.attribute-selected-value').text(displayValue.length > 0 ? ': ' + displayValue : '');
}


/**
 * Generates html for promotions section
 *
 * @param {array} promotions - list of promotions
 * @return {string} - Compiled HTML
 */
function getPromotionsHtml(promotions) {
    if (!promotions) {
        return '';
    }

    var html = '';

    promotions.forEach(function (promotion) {
        html += '<div class="callout" title="' + promotion.details + '">' + promotion.calloutMsg +
            '</div>';
    });

    return html;
}
/**
 * Process attribute values associated with an attribute that does not have image swatches
 *
 * @param {Object} attr - Attribute
 * @param {string} attr.id - Attribute ID
 * @param {Object[]} attr.values - Array of attribute value objects
 * @param {string} attr.values.value - Attribute coded value
 * @param {string} attr.values.url - URL to de/select an attribute value of the product
 * @param {boolean} attr.values.isSelectable - Flag as to whether an attribute value can be
 *     selected.  If there is no variant that corresponds to a specific combination of attribute
 *     values, an attribute may be disabled in the Product Detail Page
 * @param {jQuery} $productContainer - DOM container for a given product
 */
function processNonSwatchValues(attr, $productContainer) {
    var $attr = '[data-attr="' + attr.id + '"]';
    var $defaultOption = $productContainer.find($attr + ' .select-' + attr.id + ' option:first');
    $defaultOption.attr('value', attr.resetUrl);

    attr.values.forEach(function (attrValue) {
        var $attrValue = $productContainer
            .find($attr + ' [data-attr-value="' + attrValue.value + '"]');
        $attrValue.attr('value', attrValue.url)
            .removeAttr('disabled');
        $attrValue.attr('value', attrValue.url)
        .removeAttr('disableSelection');

        // Adding disable only on desktop for product having more than two color values and size options
        if ((!attrValue.selectable) && window.matchMedia('(min-width: 1025px)').matches) {
            $attrValue.attr('disabled', true);
        }

        if (!attrValue.selectable) {
            $attrValue.attr('disableSelection', 'disableSelection');
        }
    });
    if ($('.disable-size-option-if-oos .custom-select').length !== 0) {
        $('.disable-size-option-if-oos .custom-select').find('.selected-option').remove();
        $('.disable-size-option-if-oos .custom-select').find('.selection-list').remove();
        formFields.updateSelect();
    }
}


/**
 * Updates the availability status in the Product Detail Page
 *
 * @param {Object} response - Ajax response object after an
 *                            attribute value has been [de]selected
 * @param {jQuery} $productContainer - DOM element for a given product
 */
function updateAvailability(response, $productContainer) {
    var availabilityValue = '';
    var availabilityMessages = response.product.availability.messages;
    if (!response.product.readyToOrder) {
        availabilityValue = '<div class="availability-status-msg">' + response.resources.info_selectforstock + '</div>';
    } else {
        availabilityMessages.forEach(function (message) {
            availabilityValue += '<div class="availability-status-msg">' + message + '</div>';
            if (message === 'Out of Stock') {
                $('#notify-me').removeClass('d-none');
                if ($('.btn-get-in-store-inventory.button-find-dealer').length > 0) {
                    $('.btn-get-in-store-inventory.button-find-dealer').addClass('d-none');
                }
                if ($('button.add-to-cart').length > 0) {
                    $('button.add-to-cart').addClass('d-none');
                }
                if ($('button.add-to-cart-global').length > 0) {
                    $('button.add-to-cart-global').addClass('d-none');
                }
                if ($('.notify-me-form #notify-me:visible').length > 0) {
                    $('.container.product-detail').find('.pdp-right').addClass('out-of-stock-div');
                }
            } else if (message === 'In Stock') {
                if (!($('#notify-me').hasClass('d-none'))) {
                    $('#notify-me').addClass('d-none');
                }
                if ($('.btn-get-in-store-inventory.button-find-dealer').length > 0) {
                    $('.btn-get-in-store-inventory.button-find-dealer').removeClass('d-none');
                }
                if ($('button.add-to-cart').length > 0) {
                    $('button.add-to-cart').removeClass('d-none');
                }
                if ($('button.add-to-cart-global').length > 0) {
                    $('button.add-to-cart-global').removeClass('d-none');
                }
            }
        });
    }

    $($productContainer).trigger('product:updateAvailability', {
        product: response.product,
        $productContainer: $productContainer,
        message: availabilityValue,
        resources: response.resources
    });
}


/**
 * Routes the handling of attribute processing depending on whether the attribute has image
 *     swatches or not
 *
 * @param {Object} attrs - Attribute
 * @param {string} attr.id - Attribute ID
 * @param {jQuery} $productContainer - DOM element for a given product
 */
function updateAttrs(attrs, $productContainer) {
    // Currently, the only attribute type that has image swatches is Color.
    var attrsWithSwatches = ['color', 'amount'];

    attrs.forEach(function (attr) {
        if (attrsWithSwatches.indexOf(attr.id) > -1) {
            processSwatchValues(attr, $productContainer);
        } else {
            processNonSwatchValues(attr, $productContainer);
        }
    });
}

/**
 * Generates html for product attributes section
 *
 * @param {array} attributes - list of attributes
 * @return {string} - Compiled HTML
 */
function getAttributesHtml(attributes) {
    if (!attributes) {
        return '';
    }

    var html = '';

    attributes.forEach(function (attributeGroup) {
        if (attributeGroup.ID === 'mainAttributes') {
            attributeGroup.attributes.forEach(function (attribute) {
                html += '<div class="attribute-values">' + attribute.label + ': '
                    + attribute.value + '</div>';
            });
        }
    });

    return html;
}

/**
 * Updates DOM using post-option selection Ajax response
 *
 * @param {OptionSelectionResponse} options - Ajax response options from selecting a product option
 * @param {jQuery} $productContainer - DOM element for current product
 */
function updateOptions(options, $productContainer) {
    options.forEach(function (option) {
        var $optionEl = $productContainer.find('.product-option[data-option-id*="' + option.id
            + '"]');
        option.values.forEach(function (value) {
            var valueEl = $optionEl.find('option[data-value-id*="' + value.id + '"]');
            valueEl.val(value.url);
        });
    });
}

/**
 * Updates the quantity DOM elements post Ajax call
 * @param {UpdatedQuantity[]} quantities -
 * @param {jQuery} $productContainer - DOM container for a given product
 */
function updateQuantities(quantities, $productContainer) {
    if (!($productContainer.parent('.bonus-product-item').length > 0)) {
        var optionsHtml = quantities.map(function (quantity) {
            var selected = quantity.selected ? ' selected ' : '';
            return '<option value="' + quantity.value + '"  data-url="' + quantity.url + '"' +
                selected + '>' + quantity.value + '</option>';
        }).join('');
        getQuantitySelector($productContainer).empty().html(optionsHtml);
    }
}

/**
 * Generates HTML for preferred storeDetails.
 * @param {json} storeInfo - Renders JSON Object
 * @return {storeDetailsHtml} - storeDetails content
 *
 */
function getStoreInfoElement(storeInfo) {
    var storeDetailsHtml = '<div class="store-details" data-store-id="' + storeInfo.ID + '">'
    + '<input type="hidden" name="storeId" value="' + storeInfo.ID + '" />'
    + '<div class="store-name">' + storeInfo.name + '</div><address>'
    + '<a class="store-map" target="_blank" href="https://maps.google.com/?daddr=' + storeInfo.latitude + ',' + storeInfo.longitude + '">' + storeInfo.address1;
    if (storeInfo.address2) {
        storeDetailsHtml += ' ' + storeInfo.address2;
    }
    if (storeInfo.city) {
        storeDetailsHtml += ' ' + storeInfo.city;
    }
    if (storeInfo.stateCode) {
        storeDetailsHtml += ' ' + storeInfo.stateCode;
    }
    storeDetailsHtml += ' ' + storeInfo.postalCode + '</a>';
    if (storeInfo.phone) {
        storeDetailsHtml += '<p><span><a class="storelocator-phone" href="tel:' + storeInfo.phone + '"><u>' + storeInfo.phone + '</u></a></span></p>';
    }
    storeDetailsHtml += '<div class="store-hours">';
    if (storeInfo.storeHours) {
        storeDetailsHtml += storeInfo.storeHours;
    }
    storeDetailsHtml += '</div></address></div>';
    return storeDetailsHtml;
}

/**
 * Parses JSON from Ajax call made whenever an attribute value is [de]selected
 * @param {Object} response - response from Ajax call
 * @param {Object} response.product - Product object
 * @param {string} response.product.id - Product ID
 * @param {Object[]} response.product.variationAttributes - Product attributes
 * @param {Object[]} response.product.images - Product images
 * @param {boolean} response.product.hasRequiredAttrsSelected - Flag as to whether all required
 *     attributes have been selected.  Used partially to
 *     determine whether the Add to Cart button can be enabled
 * @param {jQuery} $productContainer - DOM element for a given product.
 */
function handleVariantResponse(response, $productContainer) {
    var isChoiceOfBonusProducts =
        $productContainer.parents('.choose-bonus-product-dialog').length > 0;
    var isVaraint;
    if (response.product.variationAttributes) {
        updateAttrs(response.product.variationAttributes, $productContainer);
        isVaraint = response.product.productType === 'variant';
        if (isChoiceOfBonusProducts && isVaraint) {
            $productContainer.parent('.bonus-product-item')
                .data('pid', response.product.id);

            $productContainer.parent('.bonus-product-item')
                .data('ready-to-order', response.product.readyToOrder);
        }
    }

    // update the PID on the page
    if (response.product.id !== undefined && response.product.id !== null) {
        $('.productid').text(response.product.id);
    }

    // Update primary images
    var primaryImageUrls = response.product.images;
    primaryImageUrls.large.forEach(function (imageUrl, idx) {
        $productContainer.find('.primary-images').find('img').eq(idx)
            .attr('src', imageUrl.url);
    });

    // Update pricing
    if (!isChoiceOfBonusProducts) {
        var $priceSelector = $('.prices .price', $productContainer).length
            ? $('.prices .price', $productContainer)
            : $('.prices .price');
        $priceSelector.replaceWith(response.product.price.html);
    }

    // Update promotions
    $('.promotions').empty().html(getPromotionsHtml(response.product.promotions));
    var updateATC = false;
    if (!$('.noVariation option').length > 0 || response.queryString.indexOf('noVariation') > -1) {
        updateATC = true;
    }
    if (updateATC) {
        updateAvailability(response, $productContainer);
    }

    if (isChoiceOfBonusProducts) {
        var $selectButton = $productContainer.find('.select-bonus-product');
        $selectButton.trigger('bonusproduct:updateSelectButton', {
            product: response.product, $productContainer: $productContainer
        });
    } else {
        if (updateATC) { // eslint-disable-line
            // Enable "Add to Cart" button if all required attributes have been selected
            $('button.add-to-cart, button.add-to-cart-global, button.update-cart-product-global').trigger('product:updateAddToCart', {
                product: response.product, $productContainer: $productContainer
            }).trigger('product:statusUpdate', response.product);
        }
    }

    // Update attributes
    $productContainer.find('.main-attributes').empty()
        .html(getAttributesHtml(response.product.attributes));
}

/**
 * Retrieves url to use when adding a product to the cart
 *
 * @return {string} - The provided URL to use when adding a product to the cart
 */
function getAddToCartUrl() {
    return $('.add-to-cart-url').val();
}

/**
 * updates the product view when a product attribute is selected or deselected or when
 *         changing quantity
 * @param {string} selectedValueUrl - the Url for the selected variation value
 * @param {jQuery} $productContainer - DOM element for current product
 * @param {event} e - event from the source
 */
function attributeSelect(selectedValueUrl, $productContainer, e) {
    if (selectedValueUrl) {
        $('body').trigger('product:beforeAttributeSelect',
            { url: selectedValueUrl, container: $productContainer });

        $.ajax({
            url: selectedValueUrl,
            method: 'GET',
            success: function (data) {
                handleVariantResponse(data, $productContainer);
                updateOptions(data.product.options, $productContainer);
                updateQuantities(data.product.quantities, $productContainer);
                $('body').trigger('product:afterAttributeSelect',
                    { data: data, container: $productContainer });
                loadZoom();
                pdpImageSlick();
                $.spinner().stop();
                if (e.currentTarget.className.indexOf('select-noVariation') === -1) {
                    $('body').find('.select-noVariation').removeClass('no-noVariation-ajax');
                    $('body').trigger('select:noVariation');
                }

                // Update preferred Store Details
                if (data.preferredStoreDetails !== null) {
                    $('.selected-store-with-inventory').removeClass('display-none');
                    $('.selected-store-with-inventory').find('.card-body').html(getStoreInfoElement(data.preferredStoreDetails));
                    $('.card-body').find('.store-name').attr('data-store-id', data.preferredStoreDetails.ID);
                    $('.btn-get-in-store-inventory.button-find-dealer').attr('style', 'display: none');
                    $('body').find('button.add-to-cart').removeClass('doesNotHaveFirearm');
                }
                if (!$('.selected-store-with-inventory').hasClass('display-none')) {
                    if (data.product.isMaster) {
                        $('.button-change-dealer').attr('disabled', true);
                        $('.change-close').addClass('removebuttondisabled');
                    } else {
                        $('.button-change-dealer').attr('disabled', false);
                        $('.change-close').removeClass('removebuttondisabled');
                    }
                    if ($('.button-change-dealer').closest('.product-quickview').length > 0) {
                        var changeDealer = data.product.selectedProductUrl + '#ffldealer';
                        $('.button-change-dealer').attr('href', changeDealer);
                        if (data.product.isMaster) {
                            $('.button-change-dealer').addClass('disabled');
                        } else {
                            $('.button-change-dealer').removeClass('disabled');
                        }
                    }
                }
                if (data.preferredStoreDetails == null) {
                    if ($('.selected-store-with-inventory').hasClass('display-none')) {
                        $('body').find('button.add-to-cart').addClass('doesNotHaveFirearm');
                    } else if ($('.bypass-ffldealer-selection').hasClass('display-none')) {
                        $('body').find('button.add-to-cart').addClass('doesNotHaveFirearm');
                    }
                }
            },
            error: function () {
                $.spinner().stop();
            }
        });
    }
}

/**
 * Retrieves the bundle product item ID's for the Controller to replace bundle master product
 * items with their selected variants
 *
 * @return {string[]} - List of selected bundle product item ID's
 */
function getChildProducts() {
    var childProducts = [];
    $('.bundle-item').each(function () {
        childProducts.push({
            pid: $(this).find('.product-id').text(),
            quantity: parseInt($(this).find('label.quantity').data('quantity'), 10)
        });
    });

    return childProducts.length ? JSON.stringify(childProducts) : [];
}

/**
 * Retrieve product options
 *
 * @param {jQuery} $productContainer - DOM element for current product
 * @return {string} - Product options and their selected values
 */
function getOptions($productContainer) {
    var options = $productContainer
        .find('.product-option')
        .map(function () {
            var $elOption = $(this).find('.options-select');
            var urlValue = $elOption.val();
            var selectedValueId = $elOption.find('option[value="' + urlValue + '"]')
                .data('value-id');
            return {
                optionId: $(this).data('option-id'),
                selectedValueId: selectedValueId
            };
        }).toArray();

    return JSON.stringify(options);
}

/**
 * Parses the html for a modal window
 * @param {string} html - representing the body and footer of the modal window
 *
 * @return {Object} - Object with properties body and footer.
 */
function parseHtml(html) {
    var $html = $('<div>').append($.parseHTML(html));

    var body = $html.find('.choice-of-bonus-product');
    var footer = $html.find('.modal-footer').children();

    return { body: body, footer: footer };
}


/**
 * Retrieves url to use when adding a product to the cart
 *
 * @param {Object} data - data object used to fill in dynamic portions of the html
 */
function chooseBonusProducts(data) {
    $('.modal-body').spinner().start();

    if ($('#chooseBonusProductModal').length !== 0) {
        $('#chooseBonusProductModal').remove();
    }
    var bonusUrl;
    if (data.bonusChoiceRuleBased) {
        bonusUrl = data.showProductsUrlRuleBased;
    } else {
        bonusUrl = data.showProductsUrlListBased;
    }

    var htmlString = '<!-- Modal -->'
        + '<div class="modal fade" id="chooseBonusProductModal" role="dialog">'
        + '<div class="modal-dialog choose-bonus-product-dialog" '
        + 'data-total-qty="' + data.maxBonusItems + '"'
        + 'data-UUID="' + data.uuid + '"'
        + 'data-pliUUID="' + data.pliUUID + '"'
        + 'data-addToCartUrl="' + data.addToCartUrl + '"'
        + 'data-pageStart="0"'
        + 'data-pageSize="' + data.pageSize + '"'
        + 'data-moreURL="' + data.showProductsUrlRuleBased + '"'
        + 'data-bonusChoiceRuleBased="' + data.bonusChoiceRuleBased + '">'
        + '<!-- Modal content-->'
        + '<div class="modal-content">'
        + '<div class="modal-header">'
        + '    <span class="">' + data.labels.selectprods + '</span>'
        + '    <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>'
        + '</div>'
        + '<div class="modal-body"></div>'
        + '<div class="modal-footer"></div>'
        + '</div>'
        + '</div>'
        + '</div>';
    $('body').append(htmlString);
    $('.modal-body').spinner().start();

    $.ajax({
        url: bonusUrl,
        method: 'GET',
        dataType: 'html',
        success: function (html) {
            var parsedHtml = parseHtml(html);
            $('#chooseBonusProductModal .modal-body').empty();
            $('#chooseBonusProductModal .modal-body').html(parsedHtml.body);
            $('#chooseBonusProductModal .modal-footer').html(parsedHtml.footer);
            $('#chooseBonusProductModal').modal('show');
            $.spinner().stop();
        },
        error: function () {
            $.spinner().stop();
        }
    });
}


/**
 * Updates the Mini-Cart quantity value after the customer has pressed the "Add to Cart" button
 * @param {string} response - ajax response from clicking the add to cart button
 */
function handlePostCartAdd(response) {
    $('.minicart').trigger('count:update', response);
    var messageType = response.error ? 'alert-danger' : 'alert-success';
    // show add to cart toast
    if (response.newBonusDiscountLineItem
        && Object.keys(response.newBonusDiscountLineItem).length !== 0) {
        chooseBonusProducts(response.newBonusDiscountLineItem);
    } else {
        if ($('.add-to-cart-messages').length === 0) {
            $('body').append(
            '<div class="add-to-cart-messages"></div>'
            );
        }

        $('.add-to-cart-messages').append(
            '<div class="alert ' + messageType + ' add-to-basket-alert text-center" role="alert">'
            + response.message
            + '</div>'
        );

        setTimeout(function () {
            $('.add-to-basket-alert').remove();
        }, 5000);
    }
}

module.exports = {
    attributeSelect: attributeSelect,
    methods: base.methods,
    amountAttribute: function () {
        $('body').on('click', '[data-attr="amount"] a', function (e) {
            e.preventDefault();

            if ($(this).attr('disabled')) {
                return;
            }
            var $productContainer = $(this).closest('.set-item');
            if (!$productContainer.length) {
                $productContainer = $(this).closest('.product-detail');
            }

            attributeSelect(e.currentTarget.href, $productContainer, e);
        });
    },
    colorAttribute: function () {
        $('body').on('click', '[data-attr="color"] a', function (e) {
            e.preventDefault();

            if ($(this).attr('disabled')) {
                return;
            }
            var $productContainer = $(this).closest('.set-item');
            if (!$productContainer.length) {
                $productContainer = $(this).closest('.product-detail');
            }

            attributeSelect(e.currentTarget.href, $productContainer, e);
        });
    },

    selectAttribute: function () {
        $(document).on('change', 'select[class*="select-"], .options-select', function (e) {
            e.preventDefault();

            var $productContainer = $(this).closest('.set-item');
            if (!$productContainer.length) {
                $productContainer = $(this).closest('.product-detail');
            }
            attributeSelect(e.currentTarget.value, $productContainer, e);
        });
    },

    initializeNoVariation: function () {
        $('body').on('select:noVariation', function () {
            var allOOS = true;
            var isNotVariant = $('body').find('.select-noVariation').hasClass('no-noVariation-ajax');
            var previousSelectedValue = $('body').find('.select-noVariation option:selected').data('attr-value');
            if (!isNotVariant) {
                $('body').find('.select-noVariation option').each(function () { // eslint-disable-line
                    var selectableNoVariant = $(this);
                    if (selectableNoVariant.data('selectable') && selectableNoVariant.attr('disableSelection') !== 'disableSelection') {
                        if (previousSelectedValue !== selectableNoVariant.data('attr-value')) {
                            selectableNoVariant.prop('selected', true);
                            selectableNoVariant.trigger('change');
                        }
                        allOOS = false;
                        return false;
                    }
                });
                if (allOOS) {
                    $('body').find('.select-noVariation:not(".no-noVariation-ajax") option').each(function (loopindex) { // eslint-disable-line
                        var selectableNoVariant = $(this);
                        // since there is no way to tell, what no-Variaiton attribute value has to be selected for the particular combination. We try to fetch the appropriate index of another variation
                        // This still doesn't resolve all use cases but is helpful if the variation values are setup with one-one relation.
                        var index = 1;
                        if ($('body').find('.hasOtherVariations select').length > 0) {
                            index = $('body').find('.hasOtherVariations select option:selected').index();
                        }
                        if (loopindex === index) {
                            if (previousSelectedValue !== selectableNoVariant.data('attr-value')) {
                                selectableNoVariant.prop('selected', true);
                                selectableNoVariant.trigger('change');
                            }
                            return false;
                        }
                    });
                }
            }
        });
        var hasOtherVariations = $('body').find('.hasOtherVariations').length > 0;
        if (!hasOtherVariations) {
            $('body').trigger('select:noVariation');
        }
    },

    availability: function () {
        $(document).on('change', '.quantity-select', function (e) {
            e.preventDefault();

            var $productContainer = $(this).closest('.product-detail');
            if (!$productContainer.length) {
                $productContainer = $(this).closest('.modal-content').find('.product-quickview');
            }

            if ($('.bundle-items', $productContainer).length === 0) {
                attributeSelect($(e.currentTarget).find('option:selected').data('url'),
                    $productContainer);
            }
        });
    },

    addToCart: function () {
        $(document).on('click', 'button.add-to-cart, button.add-to-cart-global', function () {
            var addToCartUrl;
            var pid;
            var pidsObj;
            var setPids;

            if ($(this).hasClass('doesNotHaveFirearm')) {
                $('.btn-get-in-store-inventory').trigger('click');
            } else {
                $('body').trigger('product:beforeAddToCart', this);

                if ($('.set-items').length && $(this).hasClass('add-to-cart-global')) {
                    setPids = [];

                    $('.product-detail').each(function () {
                        if (!$(this).hasClass('product-set-detail')) {
                            setPids.push({
                                pid: $(this).find('.product-id').text(),
                                qty: $(this).find('.quantity-select').val(),
                                options: getOptions($(this))
                            });
                        }
                    });
                    pidsObj = JSON.stringify(setPids);
                }

                pid = getPidValue($(this));

                var $productContainer = $(this).closest('.product-detail');
                if (!$productContainer.length) {
                    $productContainer = $(this).closest('.quick-view-dialog').find('.product-detail');
                }

                addToCartUrl = getAddToCartUrl();

                var form = {
                    pid: pid,
                    pidsObj: pidsObj,
                    childProducts: getChildProducts(),
                    quantity: getQuantitySelected($(this))
                };

                if ($('.giftcard-form').length > 0) {
                    form.giftcardEmail = $('#giftcard-email').val();
                    form.recipientName = $('#recName').val();
                    form.senderName = $('#senderName').val();
                    form.message = $('#addMessage').val();
                }
                var siteID = $('#siteID').data('site-id');
                if (siteID !== 'CTDDOTNET') {
                    if (!$('.bundle-item').length) {
                        form.options = getOptions($productContainer);
                    }
                }

                $(this).trigger('updateAddToCartFormData', form);
                if (siteID === 'CTDDOTNET') {
                    $.spinner().stop();
                    var addProductOnNetURL = $('.product-detail').data('dotnet-atc-url');
                    addProductOnNetURL = appendToUrl(addProductOnNetURL, form);
                    if (addProductOnNetURL) {
                        window.location.href = addProductOnNetURL;
                    }
                } else if (addToCartUrl) {
                    $.ajax({
                        url: addToCartUrl,
                        method: 'POST',
                        data: form,
                        success: function (data) {
                            handlePostCartAdd(data);
                            $('body').trigger('product:afterAddToCart', data);
                            $.spinner().stop();

                            // GTM - event
                            require('gtm/gtm/gtm').addToCart(data, form.quantity);
                        },
                        error: function () {
                            $.spinner().stop();
                        }
                    });
                }
            }
        });
    },
    checkStock: function () {
        if ($('.availability-status-msg:contains(Out of Stock)').length > 0) {
            $('#notify-me').removeClass('d-none');
            if ($('.btn-get-in-store-inventory.button-find-dealer').length > 0) {
                $('.btn-get-in-store-inventory.button-find-dealer').addClass('d-none');
            }
            if ($('button.add-to-cart').length > 0) {
                $('button.add-to-cart').addClass('d-none');
            }
        } else if ($('.availability-status-msg:contains(In Stock)').length > 0) {
            $('#notify-me').addClass('d-none');
            if ($('.btn-get-in-store-inventory.button-find-dealer').length > 0) {
                $('.btn-get-in-store-inventory.button-find-dealer').removeClass('d-none');
            }
            if ($('button.add-to-cart').length > 0) {
                $('button.add-to-cart').removeClass('d-none');
            }
        }
    },
    selectBonusProduct: base.selectBonusProduct,
    removeBonusProduct: base.removeBonusProduct,
    enableBonusProductSelection: base.enableBonusProductSelection,
    showMoreBonusProducts: base.showMoreBonusProducts,
    addBonusProductsToCart: base.addBonusProductsToCart,

    getPidValue: getPidValue,
    getQuantitySelected: getQuantitySelected,
    loadZoom: loadZoom,
    pdpImageSlick: pdpImageSlick
};
