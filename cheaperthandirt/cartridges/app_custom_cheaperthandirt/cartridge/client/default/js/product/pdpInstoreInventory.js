'use strict';

var storeLocator = require('../storeLocator/storeLocator');
var formFields = require('../formFields/formFields');
var PerfectScrollbar = require('perfect-scrollbar/dist/perfect-scrollbar');

/**
 * Generates the modal window on the first call.
 */
function getModalHtmlElement() {
    if ($('#inStoreInventoryModal').length !== 0) {
        $('#inStoreInventoryModal').remove();
        $('.modal-backdrop').remove();
    }
    var htmlString = '<!-- Modal -->'
        + '<div class="modal " id="inStoreInventoryModal" role="dialog">'
        + '<div class="modal-dialog in-store-inventory-dialog">'
        + '<!-- Modal content-->'
        + '<div class="modal-content">'
        + '<div class="modal-header">'
        + '    <span class="modal-title text-uppercase">Select Your FFL Dealer</span>'
        + '    <button type="button" class="close pull-right svg-25-Close_Button_Black svg-25-Close_Button_Black-dims" data-dismiss="modal" title="'
        +          $('.btn-get-in-store-inventory').data('modal-close-text') + '">'    // eslint-disable-line
        + '    </button>'
        + '</div>'
        + '<div class="modal-body"></div>'
        + '</div>'
        + '</div>'
        + '</div>';
    $('body').append(htmlString);
    $('#inStoreInventoryModal').modal('show');
}

/**
 * Replaces the content in the modal window with find stores components and
 * the result store list.
 * @param {string} pid - The product ID to search for
 * @param {number} quantity - Number of products to search inventory for
 * @param {number} selectedPostalCode - The postal code to search for inventory
 * @param {number} selectedRadius - The radius to search for inventory
 * @param {string} selectedStoreId - The ID of the selected store
 */
function fillModalElement(pid, quantity, selectedPostalCode, selectedRadius, selectedStoreId) {
    var requestData = {
        products: pid + ':' + quantity
    };

    if (selectedRadius) {
        requestData.radius = selectedRadius;
    }

    if (selectedPostalCode) {
        requestData.postalCode = selectedPostalCode;
    }

    if (selectedStoreId) {
        requestData.storeId = selectedStoreId;
    }

    $('#inStoreInventoryModal').spinner().start();
    $.ajax({
        url: $('.btn-get-in-store-inventory').data('action-url'),
        data: requestData,
        method: 'GET',
        success: function (response) {
            $('#inStoreInventoryModal').find('.modal-body').empty();
            $('#inStoreInventoryModal').find('.modal-body').html(response.storesResultsHtml);
            storeLocator.search();
            storeLocator.changeRadius();
            storeLocator.selectStore();
            storeLocator.updateSelectStoreButton();

            $('.btn-storelocator-search').attr('data-search-pid', pid);

            if (selectedRadius) {
                $('#radius').val(selectedRadius);
            }

            if (selectedPostalCode) {
                $('#store-postal-code').val(selectedPostalCode);
            }

            if (!$('.results').data('has-results')) {
                $('.store-locator-no-results').show();
            }

            $('#inStoreInventoryModal').modal('show');
            formFields.updateSelect();
            formFields.adjustForAutofill();
            $('#inStoreInventoryModal').spinner().stop();
        },
        error: function () {
            $('#inStoreInventoryModal').spinner().stop();
        }
    });
}

/**
 * Remove the selected store.
 * @param {HTMLElement} $container - the target html element
 */
function deselectStore($container) {
    var storeElement = $($container).find('.selected-store-with-inventory');
    $(storeElement).find('.card-body').empty();
    $(storeElement).addClass('display-none');
    $($container).find('.btn-get-in-store-inventory').show();
}

/**
 * Restore all quantity select options to visible.
 * @param {string} searchPID - The product ID to search for
 */
function restoreQuantitySelection(searchPID) {
    var quantityOptionSelector = '.product-detail[data-pid="' + searchPID + '"] .quantity-select option.d-none';
    $(quantityOptionSelector).removeClass('d-none');
}

/**
 * Update quantity options. Only display quantity options that are available for the store.
 * @param {sring} searchPID - The product ID of the selected product.
 * @param {number} storeId - The store ID selected for in store pickup.
 */
function updateQuantityOptions(searchPID, storeId) {
    var selectorPrefix = '.product-detail[data-pid="' + searchPID + '"]';
    var productIdSelector = selectorPrefix + ' .product-id';
    var quantitySelector = selectorPrefix + ' .quantity-select';
    var quantityOptionSelector = quantitySelector + ' option';

    var requestData = {
        pid: $(productIdSelector).text(),
        quantitySelected: $(quantitySelector).val(),
        storeId: storeId
    };

    $.ajax({
        url: $('.btn-get-in-store-inventory').data('ats-action-url'),
        data: requestData,
        method: 'GET',
        success: function (response) {
            // Hide from dropdown quantity greater than inventory
            var productAtsValue = response.atsValue;
            var availabilityValue = '';

            var $productContainer = $('.product-detail[data-pid="' + searchPID + '"]');

            if (!response.product.readyToOrder) {
                availabilityValue = '<div>' + response.resources.info_selectforstock + '</div>';
            } else {
                response.product.messages.forEach(function (message) {
                    availabilityValue += '<div>' + message + '</div>';
                });
            }

            $($productContainer).trigger('product:updateAvailability', {
                product: response.product,
                $productContainer: $productContainer,
                message: availabilityValue,
                resources: response.resources
            });

            $('button.add-to-cart, button.add-to-cart-global, button.update-cart-product-global').trigger('product:updateAddToCart', {
                product: response.product, $productContainer: $productContainer
            });

            var quantityDropdownLength = $(quantityOptionSelector).length;

            restoreQuantitySelection(searchPID);
            for (var i = quantityDropdownLength - 1; i >= productAtsValue; i--) {
                $(quantityOptionSelector).eq(i).addClass('d-none');
            }
        }
    });
}

module.exports = {
    updateSelectStore: function () {
        $('body').on('product:updateAddToCart', function (e, response) {
            $('.btn-get-in-store-inventory', response.$productContainer).attr('disabled',
                (!response.product.readyToOrder || !response.product.available));
        });
    },
    removeSelectedStoreOnAttributeChange: function () {
        $('body').on('product:afterAttributeSelect', function (e, response) {
            response.container.attr('data-pid', response.data.product.id);
            deselectStore(response.container);
            $('.bypass-ffldealer-selection').addClass('display-none');
        });
    },
    updateAddToCartFormData: function () {
        $('body').on('updateAddToCartFormData', function (e, form) {
            if (form.pidsObj) {
                var pidsObj = JSON.parse(form.pidsObj);
                pidsObj.forEach(function (product) {
                    var storeElement = $('.product-detail[data-pid="' +
                        product.pid
                        + '"]').find('.store-name');
                    product.storeId = $(storeElement).length// eslint-disable-line no-param-reassign
                        ? $(storeElement).attr('data-store-id')
                        : null;
                });

                form.pidsObj = JSON.stringify(pidsObj);// eslint-disable-line no-param-reassign
            }

            var storeElement = $('.product-detail[data-pid="'
                + form.pid
                + '"]');

            if ($(storeElement).length) {
                form.storeId = $(storeElement).find('.store-name') // eslint-disable-line
                    .attr('data-store-id');
            }
        });
    },

    showInStoreInventory: function () {
        $('.btn-get-in-store-inventory').on('click', function (e) {
            var pid = $(this).closest('.product-detail').attr('data-pid');
            var quantity = $(this).closest('.product-detail').find('.quantity-select').val();
            getModalHtmlElement();
            fillModalElement(pid, quantity);
            e.stopPropagation();
        });
    },
    removeStoreSelection: function () {
        $('body').on('click', 'div .change-dealer-close .change-close', (function () {
            deselectStore($(this).closest('.product-detail'));
            restoreQuantitySelection($(this).closest('.product-detail').attr('data-pid'));
            $('body').find('button.add-to-cart').addClass('doesNotHaveFirearm');
        }));
    },
    selectStoreWithInventory: function () {
        $('body').on('store:selected', function (e, data) {
            var searchPID = $('.btn-storelocator-search').attr('data-search-pid');
            var storeElement = $('.product-detail[data-pid="' + searchPID + '"]');
            $(storeElement).find('.selected-store-with-inventory .card-body').empty();
            $(storeElement).find('.selected-store-with-inventory .card-body').append(data.storeDetailsHtml);
            $(storeElement).find('.store-name').attr('data-store-id', data.storeID);
            $(storeElement).find('.selected-store-with-inventory').removeClass('display-none');
            $('.bypass-ffldealer-selection').addClass('display-none');

            var $changeStoreButton = $(storeElement).find('.change-store');
            $($changeStoreButton).data('postal', data.searchPostalCode);
            $($changeStoreButton).data('radius', data.searchRadius);

            $('body').find('button.add-to-cart').removeClass('doesNotHaveFirearm');
            $('.button-change-dealer').attr('disabled', false);

            $(storeElement).find('.btn-get-in-store-inventory').hide();

            updateQuantityOptions(searchPID, data.storeID);

            $('#inStoreInventoryModal').modal('hide');
            $('#inStoreInventoryModal').remove();
            if (!$('.selected-store-with-inventory').hasClass('display-none') && $('.change-close').hasClass('removebuttondisabled')) {
                $('.change-close').removeClass('removebuttondisabled');
            }
        });
    },
    changeStore: function () {
        $('body').on('click', '.change-store', (function () {
            var pid = $(this).closest('.product-detail').attr('data-pid');
            var quantity = $(this).closest('.product-detail').find('.quantity-select').val();
            getModalHtmlElement();
            fillModalElement(pid, quantity, $(this).data('postal'), $(this).data('radius'), $(this).closest('.selected-store-with-inventory').find('.store-name').data('store-id'));
        }));
    },
    byPass: function () {
        $('body').on('click', '.cant-find-link', (function (e) {
            e.preventDefault();
            var storeID = $(this).data('storeid');
            var searchPID = $('.btn-storelocator-search').attr('data-search-pid');
            var storeElement = $('.product-detail[data-pid="' + searchPID + '"]');
            $(storeElement).append('<span class="store-name d-none"></span>');
            $(storeElement).find('.store-name').attr('data-store-id', storeID);

            $('#inStoreInventoryModal').modal('hide');
            $('#inStoreInventoryModal').remove();

            $('body').find('button.add-to-cart').removeClass('doesNotHaveFirearm');

            $('.bypass-ffldealer-selection').removeClass('display-none');
            $('button.btn-get-in-store-inventory').addClass('display-none');
            $('button.btn-get-in-store-inventory').attr('style', 'display: none');
            $('.selected-store-with-inventory').addClass('display-none');
            $('.bypass-message').find('.select-dealer.btn-get-in-store-inventory').attr('style', '');
            if ($('.bypass-ffldealer-selection').hasClass('display-none')) {
                $('.bypass-ffldealer-selection').removeClass('display-none');
            }
            updateQuantityOptions(searchPID, storeID);

            // GTM - event
            require('gtm/gtm/gtm').bypassFFL();
            if (window.location.hash === '#ffldealer') {
                window.location.href = document.location.href.replace(location.hash, '');
            }
        }));
    },
    showMoreStores: function () {
        $('body').on('click', '.show-more-stores', function () {
            $(this).closest('.results.striped').find('.card-body').removeClass('show-more');
            $(this).addClass('d-none');
        });
    },
    ajaxCompleteFunction: function () {
        $(document).ajaxComplete(function () {
            if ($('input.select-store-input').length > 0) {
                $('input.select-store-input:first').trigger('click');
            }
            if (window.matchMedia('(min-width: 1024px)').matches && $('.results.striped').length > 0) {
                var resultsDiv = '.perfect-scrollbar';
                var perfectScrollbar = new PerfectScrollbar(resultsDiv);
                perfectScrollbar.update();
            }
        });
    },
    FFLDealerpopUP: function () {
        if (window.location.hash === '#ffldealer') {
            $('.btn-get-in-store-inventory').trigger('click');
            var pid = $('.change-store').closest('.product-detail').attr('data-pid');
            var quantity = $(this).closest('.product-detail').find('.quantity-select').val();
            getModalHtmlElement();
            fillModalElement(pid, quantity, $(this).data('postal'), $(this).data('radius'), $(this).closest('.selected-store-with-inventory').find('.store-name').data('store-id'));
            window.location.hash = '';
        }
    },
    FFLDealerPopupClose: function () {
        $('body').on('click', '.in-store-inventory-dialog .close', function () {
            if (window.location.hash === '#ffldealer') {
                window.location.hash = '';
            }
        });
    }
};
