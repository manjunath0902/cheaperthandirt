'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./buildPage/buildPage'));
    processInclude(require('./recommendation'));
    processInclude(require('base/product/quickView'));
    processInclude(require('./product/quickView'));
    processInclude(require('./product/wishlistHeart'));
});
