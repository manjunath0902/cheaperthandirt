'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./product/detail'));
    processInclude(require('./product/wishlist'));
    processInclude(require('./product/pdpInstoreInventory'));
    processInclude(require('./recommendation'));
    processInclude(require('./product/wishlistHeart'));
    processInclude(require('annexCloud/annex/annex'));
});
