'use strict';

var addressHelpers = require('./address');
var formFields = require('../formFields/formFields');
var giftCard = require('giftcard/checkout/payment');
var shippingHelpers = require('./shipping');
var billingHelpers = require('./billing');
var summaryHelpers = require('./summary');
var formHelpers = require('base/checkout/formErrors');
var gtm = require('gtm/gtm/gtm');


/**
 * Create the jQuery Checkout Plugin.
 *
 * This jQuery plugin will be registered on the dom element in checkout.isml with the
 * id of "checkout-main".
 *
 * The checkout plugin will handle the different state the user interface is in as the user
 * progresses through the varying forms such as shipping and payment.
 *
 * Billing info and payment info are used a bit synonymously in this code.
 *
 */
(function ($) {
    $.fn.checkout = function () { // eslint-disable-line
        var plugin = this;

        //
        // Collect form data from user input
        //
        var formData = {
            // Shipping Address
            shipping: {},

            // Billing Address
            billing: {},

            // Payment
            payment: {},

            // Gift Codes
            giftCode: {}
        };

        //
        // The different states/stages of checkout
        //
        var checkoutStages = [
            'shipping',
            'payment',
            'placeOrder',
            'submitted'
        ];

        /**
         * Updates the URL to determine stage
         * @param {number} currentStage - The current stage the user is currently on in the checkout
         */
        function updateUrl(currentStage) {
            // Update GTM stage.
            gtm.loginEvent();

            history.pushState(
                checkoutStages[currentStage],
                document.title,
                location.pathname
                + '?stage='
                + checkoutStages[currentStage]
                + '#'
                + checkoutStages[currentStage]
            );
            var title = '';
            if (checkoutStages[currentStage] && checkoutStages[currentStage] === 'shipping') {
                title = 'Checkout - Shipping | Cheaper Than Dirt';
            } else if (checkoutStages[currentStage] && checkoutStages[currentStage] === 'payment') {
                title = 'Checkout - Billing | Cheaper Than Dirt';
            } else if (checkoutStages[currentStage] && checkoutStages[currentStage] === 'placeOrder') {
                title = 'Checkout - Order Review | Cheaper Than Dirt';
            }
            if (title !== '') {
                $('title').text(title);
            }
            // Update GTM stage.
            gtm.updateStage(currentStage);
        }

        //
        // Local member methods of the Checkout plugin
        //
        var members = {

            // initialize the currentStage variable for the first time
            currentStage: 0,
            submittedCurrentStage: true,

            /**
             * Set or update the checkout stage (AKA the shipping, billing, payment, etc... steps)
             * @returns {Object} a promise
             */
            updateStage: function () {
                var stage = checkoutStages[members.currentStage];
                var defer = $.Deferred(); // eslint-disable-line

                if (stage === 'shipping') {
                    //
                    // Clear Previous Errors
                    //
                    members.submittedCurrentStage = false;
                    formHelpers.clearPreviousErrors('.shipping-form');

                    //
                    // Submit the Shipping Address Form
                    //
                    var isMultiShip = $('#checkout-main').hasClass('multi-ship');
                    var formSelector = isMultiShip ?
                            '.multi-shipping .active form' : '.single-shipping .shipping-form';
                    var form = $(formSelector);

                    if (isMultiShip && form.length === 0) {
                        // in case the multi ship form is already submitted
                        var url = $('#checkout-main').attr('data-checkout-get-url');
                        $.ajax({
                            url: url,
                            method: 'GET',
                            success: function (data) {
                                if (!data.error) {
                                    // GTM - event
                                    gtm.checkoutEmailSignup(data);

                                    $('body').trigger('checkout:updateCheckoutView',
                                        { order: data.order, customer: data.customer });
                                    defer.resolve();
                                } else if ($('.shipping-error .alert-danger').length < 1) {
                                    var errorMsg = data.message;
                                    var errorHtml = '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
                                        'fade show" role="alert">' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                                        '<span aria-hidden="true">&times;</span>' +
                                        '</button>' + errorMsg + '</div>';
                                    $('.shipping-error').append(errorHtml);
                                    defer.reject();
                                }
                            },
                            error: function () {
                                // Server error submitting form
                                defer.reject();
                            }
                        });
                    } else {
                        var shippingFormData = form.serialize();

                        $('body').trigger('checkout:serializeShipping', {
                            form: form,
                            data: shippingFormData,
                            callback: function (data) {
                                shippingFormData = data;
                            }
                        });

                        $.ajax({
                            url: form.attr('action'),
                            type: 'post',
                            data: shippingFormData,
                            success: function (data) {
                                // GTM - event
                                gtm.checkoutEmailSignup(data);
                                shippingHelpers.methods.shippingFormResponse(defer, data);
                            },
                            error: function (err) {
                                if (err.responseJSON.redirectUrl) {
                                    window.location.href = err.responseJSON.redirectUrl;
                                }
                                // Server error submitting form
                                defer.reject(err.responseJSON);
                            }
                        });
                    }
                    return defer;
                } else if (stage === 'payment') {
                    //
                    // Submit the Billing Address Form
                    //

                    members.submittedCurrentStage = false;

                    formHelpers.clearPreviousErrors('.payment-form');

                    var paymentForm = $('#dwfrm_billing').serialize();

                    $('body').trigger('checkout:serializeBilling', {
                        form: $('#dwfrm_billing'),
                        data: paymentForm,
                        callback: function (data) { paymentForm = data; }
                    });

                    if ($('.data-checkout-stage').data('customer-type') === 'registered') {
                        // if payment method is credit card
                        if ($('.payment-information').data('payment-method-id') === 'CREDIT_CARD') {
                            if (!($('.payment-information').data('is-new-payment'))) {
                                var cvvCode = $('.saved-payment-instrument.' +
                                    'selected-payment .saved-payment-security-code').val();
                                var requirecc = $('#checkout-main').attr('data-requirecc');
                                if (cvvCode === '' && requirecc === 'true') {
                                    $('.saved-payment-instrument.' +
                                        'selected-payment ' +
                                        '.form-control').addClass('is-invalid');
                                    defer.reject();
                                    return defer;
                                }

                                var $savedPaymentInstrument = $('.saved-payment-instrument' +
                                    '.selected-payment'
                                );

                                paymentForm += '&storedPaymentUUID=' +
                                    $savedPaymentInstrument.data('uuid');

                                paymentForm += '&securityCode=' + cvvCode;
                            }
                        }
                    }

                    $.ajax({
                        url: $('#dwfrm_billing').attr('action'),
                        method: 'POST',
                        data: paymentForm,
                        success: function (data) {
                            // look for field validation errors
                            if (data.error) {
                                if (data.fieldErrors.length) {
                                    data.fieldErrors.forEach(function (error) {
                                        if (Object.keys(error).length) {
                                            formHelpers.loadFormErrors('.payment-form', error);
                                        }
                                    });
                                }

                                if (data.serverErrors.length) {
                                    data.serverErrors.forEach(function (error) {
                                        $('.error-message').show();
                                        $('.error-message-text').text(error);
                                    });
                                }

                                if (data.cartError) {
                                    window.location.href = data.redirectUrl;
                                }

                                defer.reject();
                            } else {
                                //
                                // Populate the Address Summary
                                //
                                $('body').trigger('checkout:updateCheckoutView',
                                    { order: data.order, customer: data.customer });

                                if (data.renderedPaymentInstruments) {
                                    $('.stored-payments').empty().html(
                                        data.renderedPaymentInstruments
                                    );
                                }

                                if (data.customer.registeredUser
                                    && data.customer.customerPaymentInstruments.length
                                ) {
                                    $('.cancel-new-payment').removeClass('checkout-hidden');
                                    $('.user-payment-instruments').removeClass('checkout-hidden');
                                }

                                defer.resolve(data);
                            }
                        },
                        error: function (err) {
                            if (err.responseJSON.redirectUrl) {
                                window.location.href = err.responseJSON.redirectUrl;
                            }
                        }
                    });

                    return defer;
                } else if (stage === 'placeOrder') {
                    members.submittedCurrentStage = false;
                    $.ajax({
                        url: $('.place-order').data('action'),
                        method: 'POST',
                        success: function (data) {
                            if (data.error) {
                                if (data.cartError) {
                                    setTimeout(function () {
                                        window.location.href = data.redirectUrl;
                                    }, 1000);
                                    defer.reject(data);
                                } else {
                                    // go to appropriate stage and display error message
                                    defer.reject(data);
                                }
                            } else {
                                var continueUrl = data.continueUrl;
                                var urlParams = {
                                    ID: data.orderID,
                                    token: data.orderToken
                                };

                                continueUrl += (continueUrl.indexOf('?') !== -1 ? '&' : '?') +
                                    Object.keys(urlParams).map(function (key) {
                                        return key + '=' + encodeURIComponent(urlParams[key]);
                                    }).join('&');

                                window.location.href = continueUrl;
                                defer.resolve(data);
                                members.submittedCurrentStage = true;
                            }
                        },
                        error: function () {
                        }
                    });

                    return defer;
                }
                var p = $('<div>').promise(); // eslint-disable-line
                setTimeout(function () {
                    p.done(); // eslint-disable-line
                }, 500);
                return p; // eslint-disable-line
            },

            /**
             * Initialize the checkout stage.
             *
             * TODO: update this to allow stage to be set from server?
             */
            initialize: function () {
                // set the initial state of checkout
                members.currentStage = checkoutStages
                    .indexOf($('.data-checkout-stage').data('checkout-stage'));
                $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);

                //
                // Handle Payment option selection
                //
                $('input[name$="paymentMethod"]', plugin).on('change', function () {
                    $('.credit-card-form').toggle($(this).val() === 'CREDIT_CARD');
                });

                //
                // Handle Next State button click
                //
                $(plugin).on('click', '.next-step-button button', function () {
                    if ($(this).hasClass('submit-shipping') && ($('.modal#adddress-suggestion:visible').length > 0 || $('.modal#foid-product-overlay:visible').length > 0)) {
                        members.submittedCurrentStage = true;
                    }

                    if (members.submittedCurrentStage && members.submittedCurrentStage === true) {
                        members.nextStage();
                    }
                    $('html, body').animate({
                        scrollTop: 0
                    }, 500);
                });
                // Handler for modal close. Handles the multi-click issue,when address validation modal closed by clicking outside it.
                $(document).on('hidden.bs.modal', '#adddress-suggestion', function () {
                    members.submittedCurrentStage = true;
                });

                $('body').on('ups:exactAddress', function () {
                    members.submittedCurrentStage = true;
                });

                // Handles the multi-click issue,when foid modal closed by clicking outside it.
                $(document).on('hidden.bs.modal', '#foid-product-overlay', function () {
                    members.submittedCurrentStage = true;
                });

                // Handles the multi-click issue,when restricted product modal closed by clicking outside it.
                $(document).on('hidden.bs.modal', '#restricted-product-overlay', function () {
                    members.submittedCurrentStage = true;
                });

                //
                // Handle Edit buttons on shipping and payment summary cards
                //
                $('.shipping-summary .edit-button', plugin).on('click', function () {
                    if (!$('#checkout-main').hasClass('multi-ship')) {
                        $('body').trigger('shipping:selectSingleShipping');
                    }

                    members.gotoStage('shipping');
                });

                $('.payment-summary .edit-button', plugin).on('click', function () {
                    members.gotoStage('payment');
                });

                //
                // remember stage (e.g. shipping)
                //
                updateUrl(members.currentStage);

                //
                // Listen for foward/back button press and move to correct checkout-stage
                //
                $(window).on('popstate', function (e) {
                    //
                    // Back button when event state less than current state in ordered
                    // checkoutStages array.
                    //
                    if (e.state === null ||
                         checkoutStages.indexOf(e.state) < members.currentStage) {
                        members.handlePrevStage(false);
                    } else if (checkoutStages.indexOf(e.state) > members.currentStage) {
                        // Forward button  pressed
                        members.handleNextStage(false);
                    }
                });

                //
                // Set the form data
                //
                plugin.data('formData', formData);
                members.updatemulticlickflag();
            },

            /**
             * The next checkout state step updates the css for showing correct buttons etc...
             */
            nextStage: function () {
                var promise = members.updateStage();

                promise.done(function () {
                    // Update UI with new stage
                    members.handleNextStage(true);
                    members.submittedCurrentStage = true;
                });

                promise.fail(function (data) {
                    // show errors
                    if (data) {
                        if (data.errorStage) {
                            members.gotoStage(data.errorStage.stage);

                            if (data.errorStage.step === 'billingAddress') {
                                var $billingAddressSameAsShipping = $(
                                    'input[name$="_shippingAddressUseAsBillingAddress"]'
                                );
                                if ($billingAddressSameAsShipping.is(':checked')) {
                                    $billingAddressSameAsShipping.prop('checked', false);
                                }
                            }
                            if (data.requirecc) {
                                $('body').trigger('zerodollar:require-cc');
                            }
                        }

                        if (data.errorMessage) {
                            $('.error-message').show();
                            $('.error-message-text').text(data.errorMessage);
                        }
                        members.submittedCurrentStage = true;
                        // GTM - event
                        require('gtm/gtm/gtm').itemsRestrictedPlaceOrder(data);
                    }
                });

                promise.always(function () {
                    members.submittedCurrentStage = true;
                });
            },

            /**
             * The next checkout state step updates the css for showing correct buttons etc...
             *
             * @param {boolean} bPushState - boolean when true pushes state using the history api.
             */
            handleNextStage: function (bPushState) {
                if (members.currentStage < checkoutStages.length - 1) {
                    // move stage forward
                    members.currentStage++;

                    //
                    // show new stage in url (e.g.payment)
                    //
                    if (bPushState) {
                        updateUrl(members.currentStage);
                    }
                }

                // Set the next stage on the DOM
                $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
            },

            /**
             * Previous State
             */
            handlePrevStage: function () {
                if (members.currentStage > 0) {
                    // move state back
                    members.currentStage--;
                    updateUrl(members.currentStage);
                }
                members.submittedCurrentStage = true;

                $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
            },

            /**
             * Use window history to go to a checkout stage
             * @param {string} stageName - the checkout state to goto
             */
            gotoStage: function (stageName) {
                members.currentStage = checkoutStages.indexOf(stageName);
                updateUrl(members.currentStage);
                members.submittedCurrentStage = true;
                $(plugin).attr('data-checkout-stage', checkoutStages[members.currentStage]);
            },

            updatemulticlickflag: function () {
                $('body').on('click', '#adddress-suggestion .close, #restricted-product-overlay .close, #restricted-product-overlay .close-restricted, #foid-product-overlay .close, #foid-product-overlay .decline-foid', function () {
                    members.submittedCurrentStage = true;
                });
            }
        };

        //
        // Initialize the checkout
        //
        members.initialize();

        return this;
    };
}(jQuery));

var exports = {
    initialize: function () {
        $('#checkout-main').checkout();
    },

    updateShippingAddressSelector: function () {
        $('body').on('checkout:updateShippingAddressSelector', function () {
            var customerType = $('#checkout-main').data('customer-type');
            if (customerType !== 'guest') {
                var $stateField = $('.shipping-form.shipping-section').find('select[name$="shippingAddress_addressFields_states_stateCode"]');
                var $zipField = $('.shipping-form.shipping-section').find('input[name$="shippingAddress_addressFields_postalCode"]');
                var $shippingMethodBlock = $('.shipping-form .shipping-method-block').find('.shipping-method-list');
                var addressSelector = $('.shipping-form.shipping-section').find('.addressSelector').val();
                if (addressSelector !== 'new') {
                    $('.shipping-form.shipping-section').find('.shipping-address-block').addClass('d-none');
                }
                if ($stateField.hasClass('field-selection-update')) {
                    $('.shipping-form.shipping-section').find('.shipping-address-block').removeClass('d-none');
                    $stateField.removeClass('field-selection-update');
                } else if ($zipField.hasClass('field-selection-update')) {
                    $('.shipping-form.shipping-section').find('.shipping-address-block').removeClass('d-none');
                    $zipField.removeClass('field-selection-update');
                } else if ($shippingMethodBlock.hasClass('field-selection-update')) {
                    $('.shipping-form.shipping-section').find('.shipping-address-block').removeClass('d-none');
                    $shippingMethodBlock.removeClass('field-selection-update');
                }
            }
        });
        $('body').trigger('checkout:updateShippingAddressSelector');
    },
    updateCheckoutView: function () {
        $('body').on('checkout:updateCheckoutView', function (e, data) {
            shippingHelpers.methods.updateMultiShipInformation(data.order);
            summaryHelpers.updateTotals(data.order);
            data.order.shipping.forEach(function (shipping) {
                shippingHelpers.methods.updateShippingInformation(
                    shipping,
                    data.order,
                    data.customer,
                    data.options
                );
                if (shipping.isStoreShipment) {
                    $('.shipping-form.store-shipment').find('.view-address-block').empty().html(shipping.storeHtml);
                } else if (!shipping.isStoreShipment && data.order.shipping.length === 1) {
                    $('.bypass-ffldealer-selection ').addClass('d-none');
                }
            });
            billingHelpers.methods.updateBillingInformation(
                data.order,
                data.customer,
                data.options
            );
            billingHelpers.methods.updatePaymentInformation(data.order, data.options);
            summaryHelpers.updateOrderProductSummaryInformation(data.order, data.options);
        });
    },
    zeroDollarInit: function () {
        $('body').on('zerodollar:require-cc', function () {
            $('.cc-fields').each(function () { $(this).removeClass('d-none'); });
            if ($('.stored-form-remove-cc').hasClass('hasSavedCards')) {
                $('.stored-form-remove-cc').addClass('checkout-hidden');
            }
            $('#checkout-main').attr('data-requirecc', true);
        });
        $('body').on('zerodollar:no-require-cc', function () {
            $('.cc-fields').each(function () { $(this).addClass('d-none'); });
            $('.stored-form-remove-cc').removeClass('checkout-hidden');
            $('#checkout-main').attr('data-requirecc', false);
        });
    },
    // handler for "edit" click of payment in CO
    edit: function () {
        $('.payment-summary .edit-button').on('click', function () {
            var hasSavedCards = $('.saved-payment-instrument').length;
            var savedElement = $('.user-payment-instruments');
            if (hasSavedCards > 0) {
                savedElement.removeClass('checkout-hidden');
                $('.payment-information').data('is-new-payment', false);
                $('.stored-form-remove-cc').addClass('checkout-hidden');
            }
            $('input[name$="_cardNumber"]').closest('.form-group').find('label').removeClass('input-focus');
            $('#expirationMonth').val('');
            $('#expirationMonth').closest('.form-group').find('label').removeClass('input-focus');
            $('#expirationYear').val('');
            $('#expirationYear').closest('.form-group').find('label').removeClass('input-focus');
            $('input[name$="_securityCode"]').val('').closest('.form-group').find('label')
                .removeClass('input-focus');
            formFields.updateSelect();
        });
    },
    checkoutEmitterHandlerInit: function () {
        $('body').on('shipping:updateShippingSummaryInformation', function (e, data) {
            $('[data-shipment-summary=' + data.shipping.UUID + ']').each(function (i, el) {
                var $container = $(el);
                if ($container.length > 0) {
                    $container.find('.shipping-email').text(data.order.orderEmail);
                    if (data.shipping.shippingAddress && data.shipping.shippingAddress.companyName) {
                        $container.find('.companyName').text(data.shipping.shippingAddress.companyName);
                    }
                }
                var hasMultiShipmentContainer = $container.hasClass('shipping-address-multi-address');
                var address = data.shipping.shippingAddress;
                if (hasMultiShipmentContainer && address) {
                    var nameLine = address.firstName ? address.firstName + ' ' : '';
                    if (address.lastName) nameLine += address.lastName;
                    $('.ship-to-name', $container).text(nameLine);
                    $('.ship-to-address1', $container).text(address.address1);
                    if (address.address2) {
                        $('.ship-to-address2', $container).text(address.address2);
                    } else {
                        $('.ship-to-address2', $container).text('');
                    }
                    if (address.stateCode && address.postalCode && address.city) {
                        $('.ship-to-city-st-zip', $container).text(address.city + ', ' + address.stateCode + ' ' + address.postalCode);
                    }
                    $('.ship-to-phone', $container).text(address.phone);

                    if (address.companyName) {
                        $('.ship-to-companyName', $container).text(address.companyName);
                    } else {
                        $('.ship-to-companyName', $container).text('');
                    }
                    if (!data.shipping.isStoreShipment) {
                        $('.ship-to-email', $container).text(data.order.orderEmail);
                    }
                    if ($('#checkout-main').hasClass('multi-ship')) {
                        $container.removeClass('d-none');
                        $('.multi-ship-title').removeClass('d-none');
                    } else {
                        $container.removeClass('d-none');
                        $('.multi-ship-title').addClass('d-none');
                    }
                }
            });
        });
    },

    initializeFormFields: function () {
        $('body').on('shipping:selectSingleShipping', function () {
            $('body').trigger('checkout:updateShippingAddressSelector');
            formFields.updateSelect();
        });
    },
    triggerCustomSelect: function () {
        $('body').on('triggerCustomSelect', function () {
            formFields.updateSelect();
        });
    },
    nextButtonStickyElement: function () {
        if (($(window).width() <= 1023) && ($('#checkout-main').length > 0)) {
            var totalHeight = $('.next-step-button').outerHeight();
            $('#checkout-main').siblings('footer').css('padding-bottom', totalHeight);
        }
    },

    nextButtonResize: function () {
        $(window).resize(function () {
            if (($(window).width() <= 1023) && ($('#checkout-main').length > 0)) {
                var totalHeight = $('.next-step-button').outerHeight();
                $('#checkout-main').siblings('footer').css('padding-bottom', totalHeight);
            }
        });
    }
};

exports.giftCard = giftCard();

[billingHelpers, shippingHelpers, addressHelpers].forEach(function (library) {
    Object.keys(library).forEach(function (item) {
        if (typeof library[item] === 'object') {
            exports[item] = $.extend({}, exports[item], library[item]);
        } else {
            exports[item] = library[item];
        }
    });
});

module.exports = exports;
