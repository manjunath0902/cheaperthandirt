'use strict';

var baseBilling = require('base/checkout/billing');
var formFields = require('../formFields/formFields');
var addressHelpers = require('./address');

/**
 * clears the credit card form
 */
function clearCreditCardForm() {
    $('input[name$="_cardNumber"]').data('cleave').setRawValue('');
    $('select[name$="_expirationMonth"]').val('');
    $('select[name$="_expirationYear"]').val('');
    $('input[name$="_securityCode"]').val('');
}

/**
 * clears the input focus from add-payment
 */
function clearInputFocusForm() {
    $('input[name$="_cardNumber"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_securityCode"]').val('').closest('.form-group').find('label')
        .removeClass('input-focus');
    $('select[name$=expirationMonth]').closest('.form-group').find('label')
        .removeClass('input-focus');
    $('select[name$=expirationYear]').closest('.form-group').find('label')
        .removeClass('input-focus');
}

/**
 * clears the input focus from form
 */
function clearBillingFocusForm() {
    $('input[name$="_firstName"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_lastName"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_address1"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_address2"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_city"]').closest('.form-group').find('label').removeClass('input-focus');
    $('select[name$="_stateCode"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_postalCode"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_phone"]').closest('.form-group').find('label').removeClass('input-focus');
}

/**
 * updates the billing address selector within billing forms
 * @param {Object} order - the order model
 * @param {Object} customer - the customer model
 */
function updateBillingAddressSelector(order, customer) {
    var shippings = order.shipping;

    var form = $('form[name$=billing]')[0];
    var $billingAddressSelector = $('.addressSelector', form);
    var hasSelectedAddress = false;
    if ($billingAddressSelector && $billingAddressSelector.length === 1) {
        $billingAddressSelector.empty();
        $billingAddressSelector.closest('.custom-select').find('.selected-option').remove();
        $billingAddressSelector.closest('.custom-select').find('.selection-list').remove();

        // Add New Address option
        $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
            null,
            false,
            order,
            { type: 'billing' }));

        var updatedLabel = true;
        shippings.forEach(function (aShipping) {
            if (!aShipping.selectedShippingMethod || !aShipping.selectedShippingMethod.storePickupEnabled) {
                if (updatedLabel) {
                    // Separator -
                    $billingAddressSelector.append(
                        addressHelpers.methods.optionValueForAddress(order.resources.shippingAddresses, false, order, { type: 'billing' })
                    );
                    updatedLabel = false;
                }
                var isSelected = order.billing.matchingAddressId === aShipping.UUID;
                hasSelectedAddress = hasSelectedAddress || isSelected;
                // Shipping Address option
                $billingAddressSelector.append(
                    addressHelpers.methods.optionValueForAddress(aShipping, isSelected, order, { type: 'billing' })
                );
            }
        });

        if (customer.addresses && customer.addresses.length > 0) {
            $billingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
                order.resources.accountAddresses, false, order));
            customer.addresses.forEach(function (address) {
                var isSelected = order.billing.matchingAddressId === address.ID;
                hasSelectedAddress = hasSelectedAddress || isSelected;
                // Customer Address option
                $billingAddressSelector.append(
                    addressHelpers.methods.optionValueForAddress({
                        UUID: 'ab_' + address.ID,
                        shippingAddress: address
                    }, isSelected, order, { type: 'billing' })
                );
            });
        }
        formFields.updateSelect();
    }

    if (hasSelectedAddress
        || (!order.billing.matchingAddressId && order.billing.billingAddress.address)) {
        // show
        $(form).attr('data-address-mode', 'edit');
    } else {
        $(form).attr('data-address-mode', 'new');
    }

    $billingAddressSelector.show();
}


baseBilling.addNewPaymentInstrument = function () {
    $('.btn.add-payment').on('click', function (e) {
        e.preventDefault();
        $('.payment-information').data('is-new-payment', true);
        clearCreditCardForm();
        $('.credit-card-form').removeClass('checkout-hidden');
        $('.user-payment-instruments').addClass('checkout-hidden');
        var hasSavedCards = $('.saved-payment-instrument').length;
        if (hasSavedCards > 0) {
            $('.cancel-new-payment').removeClass('checkout-hidden').removeClass('d-none');
        }
        formFields.updateSelect();
        clearInputFocusForm();
    });
};

baseBilling.cancelNewPayment = function () {
    $('.cancel-new-payment').on('click', function (e) {
        e.preventDefault();
        $('.payment-information').data('is-new-payment', false);
        clearCreditCardForm();
        $('.user-payment-instruments').removeClass('checkout-hidden');
        $('.credit-card-form').addClass('checkout-hidden');
    });
};


baseBilling.selectBillingAddress = function () {
    $('.payment-form .addressSelector').on('change', function () {
        var form = $(this).parents('form')[0];
        var selectedOption = $('option:selected', this);
        var optionID = selectedOption[0].value;

        if (optionID === 'new') {
            // Show Address
            $(form).attr('data-address-mode', 'new');
            clearBillingFocusForm();
        } else {
            // Hide Address
            $(form).attr('data-address-mode', 'shipment');
        }

        // Copy fields
        var attrs = selectedOption.data();
        var element;
        if (attrs.countryCode !== null && attrs.countryCode !== undefined) {
            attrs.countryCode = 'US';
        }
        Object.keys(attrs).forEach(function (attr) {
            element = attr === 'countryCode' ? 'country' : attr;
            if (element === 'cardNumber') {
                $('.cardNumber').data('cleave').setRawValue(attrs[attr]);
            } else {
                $('[name$=' + element + ']', form).val(attrs[attr]);
            }
        });
        formFields.updateSelect();
        formFields.adjustForAutofill();
    });
};

baseBilling.selectSavedPaymentInstrument = function () {
    $(document).on('click', '.saved-payment-instrument', function (e) {
        if ($(e.target).hasClass('cvv-label')) {
            $(e.target).closest('.security-code-input').find('input.saved-payment-security-code').focus();
        }
        if ($(this).hasClass('selected-payment')) {
            return false;
        }
        e.preventDefault();
        $('.saved-payment-security-code').val('');
        $('.saved-payment-security-code').closest('.form-group').find('.form-control-label').removeClass('input-focus');
        $('.saved-payment-instrument').removeClass('selected-payment');
        $(this).addClass('selected-payment');
        $('.saved-payment-instrument .card-image').removeClass('checkout-hidden');
        $('.saved-payment-instrument .security-code-input').addClass('checkout-hidden');
        $('.saved-payment-instrument.selected-payment' +
            ' .card-image').addClass('checkout-hidden');
        $('.saved-payment-instrument.selected-payment ' +
            '.security-code-input').removeClass('checkout-hidden');
        return true;
    });
};

/**
 * Updates the payment information in checkout, based on the supplied order model
 * @param {Object} order - checkout model to use as basis of new truth
 */
function updatePaymentInformation(order) {
    // update payment details
    var $paymentSummary = $('.payment-details');
    var htmlToAppend = '';
    var paymentInstruments = order.billing.payment.selectedPaymentInstruments;
    if (order.billing.payment && order.billing.payment.selectedPaymentInstruments
        && order.billing.payment.selectedPaymentInstruments.length > 0) {
        paymentInstruments.forEach(function (payment) {
            if (payment.paymentMethod === 'CREDIT_CARD') {
                htmlToAppend += '<span>' + order.resources.cardType + ' '
                + payment.type
                + '</span><div>'
                + payment.maskedCreditCardNumber
                + '</div><div><span>'
                + order.resources.cardEnding + ' '
                + payment.expirationMonth
                + '/' + payment.expirationYear
                + '</span></div>';
            }
        });
    }
    if (order !== null && order.requireCCPayment !== null && !order.requireCCPayment) {
        $('.payment-info-label').addClass('d-none');
    } else if (order !== null && order.requireCCPayment !== null && order.requireCCPayment) {
        $('.payment-info-label').removeClass('d-none');
    }

    $paymentSummary.empty().append(htmlToAppend);
}

/**
 * updates the billing address form values within payment forms
 * @param {Object} order - the order model
 */
function updateBillingAddressFormValuesWithoutEmail(order) {
    var billing = order.billing;
    if (!billing.billingAddress || !billing.billingAddress.address) return;

    var form = $('form[name=dwfrm_billing]');
    if (!form) return;

    $('input[name$=_firstName]', form).val(billing.billingAddress.address.firstName);
    $('input[name$=_lastName]', form).val(billing.billingAddress.address.lastName);
    $('input[name$=_address1]', form).val(billing.billingAddress.address.address1);
    $('input[name$=_address2]', form).val(billing.billingAddress.address.address2);
    $('input[name$=_city]', form).val(billing.billingAddress.address.city);
    $('input[name$=_postalCode]', form).val(billing.billingAddress.address.postalCode);
    $('select[name$=_stateCode],input[name$=_stateCode]', form)
        .val(billing.billingAddress.address.stateCode);
    $('select[name$=_country]', form).val('US').change();

    if (billing.payment && billing.payment.selectedPaymentInstruments
        && billing.payment.selectedPaymentInstruments.length > 0) {
        $('select[name$=expirationMonth]', form).val('').change();
        $('select[name$=expirationMonth]').closest('.form-group').find('label').removeClass('input-focus');
        $('select[name$=expirationYear]', form).val('').change();
        $('select[name$=expirationYear]').closest('.form-group').find('label').removeClass('input-focus');
        // Force security code and card number clear
        $('input[name$=securityCode]', form).val('');
        $('input[name$=securityCode]').closest('.form-group').find('label').removeClass('input-focus');
        $('input[name$=cardNumber]').data('cleave').setRawValue('');
        $('input[name$=cardNumber]').closest('.form-group').find('label').removeClass('input-focus');
    }
}

/**
 * updates the billing address form values within payment forms
 * @param {Object} order - the order model
 */
function updateBillingAddressFormValues(order) {
    var billing = order.billing;
    if (!billing.billingAddress || !billing.billingAddress.address) return;

    var form = $('form[name=dwfrm_billing]');
    if (!form) return;

    $('input[name$=_firstName]', form).val(billing.billingAddress.address.firstName);
    $('input[name$=_lastName]', form).val(billing.billingAddress.address.lastName);
    $('input[name$=_address1]', form).val(billing.billingAddress.address.address1);
    $('input[name$=_address2]', form).val(billing.billingAddress.address.address2);
    $('input[name$=_city]', form).val(billing.billingAddress.address.city);
    $('input[name$=_postalCode]', form).val(billing.billingAddress.address.postalCode);
    $('select[name$=_stateCode],input[name$=_stateCode]', form)
        .val(billing.billingAddress.address.stateCode);
    $('select[name$=_country]', form).val('US').change();
    if (billing.billingAddress.address.phone !== undefined) {
        $('input[name$=_phone]', form).val(billing.billingAddress.address.phone);
        $('input[name$=_phone]', form).closest('.form-group').find('label').addClass('input-focus');
    }
    if (order.orderEmail !== undefined && order.orderEmail !== null) {
        $('input[name$=_email]', form).val(order.orderEmail);
        $('input[name$=_email]', form).closest('.form-group').find('label').addClass('input-focus');
    }
    if (billing.payment && billing.payment.selectedPaymentInstruments
        && billing.payment.selectedPaymentInstruments.length > 0) {
        $('select[name$=expirationMonth]', form).val('').change();
        $('select[name$=expirationMonth]').closest('.form-group').find('label').removeClass('input-focus');
        $('select[name$=expirationYear]', form).val('').change();
        $('select[name$=expirationYear]').closest('.form-group').find('label').removeClass('input-focus');
        // Force security code and card number clear
        $('input[name$=securityCode]', form).val('');
        $('input[name$=securityCode]').closest('.form-group').find('label').removeClass('input-focus');
        $('input[name$=cardNumber]').data('cleave').setRawValue('');
        $('input[name$=cardNumber]').closest('.form-group').find('label').removeClass('input-focus');
    }
}

/**
 * Updates the billing information in checkout, based on the supplied order model
 * @param {Object} order - checkout model to use as basis of new truth
 * @param {Object} customer - customer model to use as basis of new truth
 * @param {Object} [options] - options
 */
function updateBillingInformation(order, customer) {
    updateBillingAddressSelector(order, customer);

    // update billing address form
    updateBillingAddressFormValues(order);

    // update billing address summary
    addressHelpers.methods.populateAddressSummary('.billing .address-summary',
        order.billing.billingAddress.address);
    if (order.onlyGCInCart || order.onlyStoreShipment || (order.hasGCInCart && order.shipping.length > 1)) {
        // update billing parts of order summary
        $('.order-summary-email').text(order.orderEmail);

        if (order.billing.billingAddress.address) {
            $('.order-summary-phone').text(order.billing.billingAddress.address.phone);
        }
    } else {
        $('.order-summary-email').text('');
        $('.order-summary-phone').text('');
    }
}
/* eslint-disable */
/**
 * clears the billing address form values
 */
function clearBillingAddressFormValues() {
    updateBillingAddressFormValues({
        billing: {
            billingAddress: {
                address: {
                    countryCode: {}
                }
            }
        }
    });
}
/* eslint-enable */
/**
 * clears the billing address form values
 */
function clearBillingAddressFormValuesWithoutEmail() {
    updateBillingAddressFormValuesWithoutEmail({
        billing: {
            billingAddress: {
                address: {
                    countryCode: {}
                }
            }
        }
    });
}


baseBilling.clearBillingForm = function () {
    $('body').on('checkout:clearBillingForm', function () {
        clearBillingAddressFormValuesWithoutEmail();
    });
};

baseBilling.clearCVV = function () {
    $('.saved-payment-security-code, #securityCode').on('keyup', function (e) {
        e.preventDefault();
        this.value = this.value.replace(/[^0-9]/g, '');
    });
};

baseBilling.methods.updateBillingInformation = updateBillingInformation;
baseBilling.methods.updateBillingAddressFormValues = updateBillingAddressFormValues;
baseBilling.methods.updatePaymentInformation = updatePaymentInformation;
baseBilling.methods.updateBillingAddressSelector = updateBillingAddressSelector;

module.exports = baseBilling;
