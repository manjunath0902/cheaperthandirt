'use strict';

var base = require('instorepickup/checkout/shipping');
var ups = require('ups/checkout/shipping');
var addressHelpers = require('base/checkout/address');
var formFields = require('../formFields/formFields');
var formHelpers = require('base/checkout/formErrors');
var PerfectScrollbar = require('perfect-scrollbar/dist/perfect-scrollbar');

/**
 * Expands the ship to home
 */
function eventsOnMultiShipment() {
    $('body').find('.btn-enter-multi-ship').trigger('click');
    $('body').find('form.store-shipment.has-multiple-shipment .save-shipment').trigger('click');
}

/**
 * Hide and show to appropriate elements that allows the user to edit multi ship address information
 * @param {jQuery} element - The shipping content
 */
function hideCancelifNewAddress(element) {
    // Show
    element.find('.shipping-address').removeClass('d-none');
    element.find('.btn-save-multi-ship.save-shipment').removeClass('d-none');

    // Hide
    element.find('.view-address-block').addClass('d-none');
    element.find('.btn-enter-multi-ship').addClass('d-none');
    element.find('.btn-edit-multi-ship').addClass('d-none');
    element.find('.multi-ship-address-actions').addClass('d-none');
}


/**
 * Hide and show to appropriate elements that allows the user to edit multi ship address information
 * @param {jQuery} element - The shipping content
 */
function editMultiShipAddress(element) {
    // Show
    element.find('.shipping-address').removeClass('d-none');
    element.find('.btn-save-multi-ship.save-shipment').removeClass('d-none');

    // Hide
    element.find('.view-address-block').addClass('d-none');
    element.find('.btn-enter-multi-ship').addClass('d-none');
    element.find('.btn-edit-multi-ship').addClass('d-none');
    element.find('.multi-ship-address-actions').addClass('d-none');

    $('body').trigger('shipping:editMultiShipAddress', { element: element, form: element.find('.shipping-form') });
}

/**
 * perform the proper actions once a user has clicked enter address or edit address for a shipment
 * @param {jQuery} element - The shipping content
 * @param {string} mode - the address mode
 */
function editOrEnterMultiShipInfo(element, mode) {
    var form = $(element).closest('form');
    var root = $(element).closest('.shipping-content');

    $('body').trigger('shipping:updateDataAddressMode', { form: form, mode: mode });

    editMultiShipAddress(root);

    var addressInfo = addressHelpers.methods.getAddressFieldsFromUI(form);

    var savedState = {
        UUID: $('input[name=shipmentUUID]', form).val(),
        shippingAddress: addressInfo
    };

    root.data('saved-state', JSON.stringify(savedState));
}

/**
 * Does Ajax call to create a server-side shipment w/ pliUUID & URL
 * @param {string} url - string representation of endpoint URL
 * @param {Object} shipmentData - product line item UUID
 * @returns {Object} - promise value for async call
 */
function createNewShipment(url, shipmentData) {
    $.spinner().start();
    return $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: shipmentData
    });
}

/**
 * clears the shipping Address input focus
 */
function clearShippingAddressFields() {
    $('input[name$="_firstName"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_companyName"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_lastName"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_address1"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_address2"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_city"]').closest('.form-group').find('label').removeClass('input-focus');
    $('select[name$="_stateCode"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_postalCode"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_phone"]').closest('.form-group').find('label').removeClass('input-focus');
}

/**
 * Update list of available shipping methods whenever user modifies shipping address details.
 * @param {jQuery} $shippingForm - current shipping form
 */
function updateShippingMethodList($shippingForm) {
    // delay for autocomplete!
    setTimeout(function () {
        var $shippingMethodList = $shippingForm.find('.shipping-method-list');
        var urlParams = addressHelpers.methods.getAddressFieldsFromUI($shippingForm);
        var shipmentUUID = $shippingForm.find('[name=shipmentUUID]').val();
        var url = $shippingMethodList.data('actionUrl');
        urlParams.shipmentUUID = shipmentUUID;

        $shippingMethodList.spinner().start();
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: urlParams,
            success: function (data) {
                if (data.error) {
                    window.location.href = data.redirectUrl;
                } else {
                    $('body').trigger('checkout:updateCheckoutView',
                        {
                            order: data.order,
                            customer: data.customer,
                            options: { keepOpen: true }
                        });
                    $('body').trigger('checkout:updateShippingAddressSelector');
                    $shippingMethodList.spinner().stop();
                }
            }
        });
    }, 300);
}

base.editMultiShipInfo = function () {
    $('.btn-edit-multi-ship').on('click', function (e) {
        e.preventDefault();
        editOrEnterMultiShipInfo($(this), 'edit');
        var addressSaved = $('.shiptohome-shipment.save-shipment').is(':visible') || $('.btn-save-multi-ship.save-shipment').is(':visible');
        $('button.submit-shipping').attr('disabled', addressSaved);
        formFields.updateSelect();
    });
};

base.selectSingleShipAddress = function () {
    $('.single-shipping .addressSelector').on('change', function () {
        var form = $(this).parents('form')[0];
        var selectedOption = $('option:selected', this);
        var attrs = selectedOption.data();
        if (attrs.countryCode !== null && attrs.countryCode !== undefined) {
            attrs.countryCode = 'US';
        }
        if (attrs.companyName === null || attrs.companyName === undefined || attrs.companyName === 'null') {
            attrs.companyName = '';
        }
        var shipmentUUID = selectedOption[0].value;
        var originalUUID = $('input[name=shipmentUUID]', form).val();
        var element;

        Object.keys(attrs).forEach(function (attr) {
            element = attr === 'countryCode' ? 'country' : attr;
            $('[name$=' + element + ']', form).val(attrs[attr]);
            if (attr === 'companyName' && attrs[attr] === '') {
                $('input[name$="_companyName"]').closest('.form-group').find('label').removeClass('input-focus');
            }
        });

        $('[name$=stateCode]', form).trigger('change');

        if ($('[name$=stateCode]').hasClass('field-selection-update')) {
            $('[name$=stateCode]').removeClass('field-selection-update');
        }

        if (shipmentUUID === 'new') {
            $(form).attr('data-address-mode', 'new');
            clearShippingAddressFields();
        } else if (shipmentUUID === originalUUID) {
            $(form).attr('data-address-mode', 'shipment');
        } else if (shipmentUUID.indexOf('ab_') === 0) {
            $(form).attr('data-address-mode', 'customer');
        } else {
            $(form).attr('data-address-mode', 'edit');
        }
        formFields.adjustForAutofill();
    });
};

base.selectMultiShipAddress = function () {
    $('.multi-shipping .addressSelector').on('change', function () {
        var form = $(this).closest('form');
        var selectedOption = $('option:selected', this);
        var attrs = selectedOption.data();
        var shipmentUUID = selectedOption[0].value;
        var originalUUID = $('input[name=shipmentUUID]', form).val();
        var pliUUID = $('input[name=productLineItemUUID]', form).val();

        var element;
        Object.keys(attrs).forEach(function (attr) {
            if (attr === 'isGift') {
                $('[name$=' + attr + ']', form).prop('checked', attrs[attr]);
                $('[name$=' + attr + ']', form).trigger('change');
            } else {
                if (attr === 'companyName' && (attrs[attr] === null || attrs[attr] === 'null' || attrs[attr] === undefined)) {
                    attrs[attr] = '';
                }
                element = attr === 'countryCode' ? 'country' : attr;
                $('[name$=' + element + ']', form).val(attrs[attr]);
                if (attr === 'companyName' && attrs[attr] === '') {
                    $('input[name$="_companyName"]').closest('.form-group').find('label').removeClass('input-focus');
                }
            }
        });

        if (shipmentUUID === 'new') {
            var createShipmentUrl = $(this).attr('data-create-shipment-url');
            createNewShipment(createShipmentUrl, { productLineItemUUID: pliUUID })
                .done(function (response) {
                    $.spinner().stop();
                    if (response.error) {
                        if (response.redirectUrl) {
                            window.location.href = response.redirectUrl;
                        }
                        return;
                    }

                    $('body').trigger('checkout:updateCheckoutView',
                        {
                            order: response.order,
                            customer: response.customer,
                            options: { keepOpen: true }
                        }
                    );
                    $('input[name$=_firstName]', form).val('');
                    $('input[name$=_lastName]', form).val('');
                    $('input[name$=_address1]', form).val('');
                    $('input[name$=_address2]', form).val('');
                    $('input[name$=_companyName]', form).val('');
                    $('input[name$=_city]', form).val('');
                    $('input[name$=_postalCode]', form).val('');
                    $('select[name$=_stateCode],input[name$=_stateCode]', form)
                        .val('');
                    $('select[name$=_country]', form).val('US');
                    $('input[name$=_phone]', form).val('');
                    $('.shipping-form.shipping-section').find('.shipping-address-block').removeClass('d-none');
                    clearShippingAddressFields();
                    $(form).attr('data-address-mode', 'new');
                })
                .fail(function () {
                    $.spinner().stop();
                });
        } else if (shipmentUUID === originalUUID) {
            $('select[name$=stateCode]', form).trigger('change');
            if ($('select[name$=stateCode]').hasClass('field-selection-update')) {
                $('select[name$=stateCode]').removeClass('field-selection-update');
            }
            $(form).attr('data-address-mode', 'shipment');
            $('.shipping-form.shipping-section').find('.shipping-address-block').addClass('d-none');
            formFields.updateSelect();
            formFields.adjustForAutofill();
        } else if (shipmentUUID.indexOf('ab_') === 0) {
            var url = $(this).attr('data-create-shipment-url');
            createNewShipment(url, {})
                .done(function (response) {
                    $.spinner().stop();
                    if (response.error) {
                        if (response.redirectUrl) {
                            window.location.href = response.redirectUrl;
                        }
                        return;
                    }


                    $(form).attr('data-address-mode', 'customer');
                    var $rootEl = $(form).closest('.shipping-content');
                    editMultiShipAddress($rootEl);
                    updateShippingMethodList($(form));
                    $('.shipping-form.shipping-section').find('.shipping-address-block').addClass('d-none');
                })
                .fail(function () {
                    $.spinner().stop();
                });
        } else {
            var updatePLIShipmentUrl = $(form).attr('action');
            var serializedAddress = $(form).serialize();
            createNewShipment(updatePLIShipmentUrl, serializedAddress)
                .done(function (response) {
                    $.spinner().stop();
                    if (response.error) {
                        if (response.redirectUrl) {
                            window.location.href = response.redirectUrl;
                        }
                        return;
                    }

                    $('body').trigger('checkout:updateCheckoutView',
                        {
                            order: response.order,
                            customer: response.customer,
                            options: { keepOpen: true }
                        }
                    );

                    $(form).attr('data-address-mode', 'edit');
                })
                .fail(function () {
                    $.spinner().stop();
                });
            clearShippingAddressFields();
        }
        formFields.adjustForAutofill();
    });
};

/**
 * Create an alert to display the error message
 * @param {Object} message - Error message to display
 */
function createErrorNotification(message) {
    var errorHtml = '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
    'fade show" role="alert">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
    '<span aria-hidden="true">&times;</span>' +
    '</button>' + message + '</div>';

    $('.shipping-error').append(errorHtml);
}

/**
 * Creat the modal to display a restricted items
 * @param {string} modalHeadMsg for the modal
 * @param {string} type of modal
 * @return {string} htmlString html content string
 */
function createHtmlString(modalHeadMsg, type) {
    if ($('#restricted-product-overlay').length !== 0) {
        $('#restricted-product-overlay').remove();
    } else if ($('#foid-product-overlay').length !== 0) {
        $('#foid-product-overlay').remove();
    }

    var htmlString = '';
    if (type === 'restrict') {
        htmlString = '<!-- Modal -->' +
        '<div class="modal fade" id="restricted-product-overlay" class="restricted-product-overlay" style="display: block;" role="dialog">' +
        '<div class="modal-dialog ups-shipping-dialog">' +
        '<!-- Modal content-->' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<span class="modal-title text-uppercase">INFORMATION REGARDING YOUR ORDER</span>' +
        '<button type="button" class="close pull-right" data-dismiss="modal">' +
        '<span class="svg-25-Close_Button_Black svg-25-Close_Button_Black-dims d-inline-block" aria-hidden="true"></span>' +
        '</button>' +
        '</div>' +
        '<div class="modal-header-description">' +
        '<span class="b3-black">' + modalHeadMsg + '</span>' +
        '<div class="modal-body">' +
        '</div>' +
        '</div>' +
        '<div class="modal-footer">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    } else {
        htmlString = '<!-- Modal -->' +
        '<div class="modal fade" id="foid-product-overlay" class="foid-product-overlay" style="display: block;" role="dialog">' +
        '<div class="modal-dialog ups-shipping-dialog">' +
        '<!-- Modal content-->' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<span class="modal-title text-uppercase">INFORMATION REGARDING YOUR ORDER</span>' +
        '<button type="button" class="close pull-right" data-dismiss="modal">' +
        '<span class="svg-25-Close_Button_Black svg-25-Close_Button_Black-dims d-inline-block" aria-hidden="true"></span>' +
        '</button>' +
        '</div>' +
        '<div class="modal-header-description">' +
        '<span class="b3-black">' + modalHeadMsg + '</span>' +
        '<div class="modal-body">' +
        '</div>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<div class="button-wrapper">' +
        '<button class="affirm-foid btn btn-primary button-primary btn-block"> YES ' +
        '</button>' +
        '<button class="decline-foid btn btn-primary button-secondary btn-block"> NO ' +
        '</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    }
    return htmlString;
}

/**
 * Handle response from the server for valid or invalid form fields.
 *  valid model data.
 */
function initializeRestrictedProdModal() {
    $('body').on('click', '.remove-restricted', function (e) {
        var url = $(this).data('action-url');
        e.preventDefault();
        var data = {
            postalCode: $('.shipping-form.shipping-section').find('#shippingZipCode').val(),
            stateCode: $('.shipping-form.shipping-section').find('#shippingState').val()
        };
        $('body').spinner().start();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (res) {
                $('body').spinner().stop();
                window.location.href = res.responseURL;
            },
            error: function (res) {
                $('body').spinner().stop();
                window.location.href = res.responseURL;
            }
        });
    });
    $('body').on('click', '.close-restricted', function (e) {
        e.preventDefault();
        $('div#restricted-product-overlay').modal('hide');
        $('div#restricted-product-overlay').remove();
        $('body').removeClass('modal-open').removeAttr('style');
        $('.modal-backdrop').remove();
    });

    $('body').on('click', '.affirm-foid', function (e) {
        e.preventDefault();
        $('body').find('.byPassFOIDCheck').attr('value', 'true');
        $('body').find('.byPassAddressValidation').attr('value', 'true');
        if ($('#checkout-main').hasClass('multi-ship')) {
            $('.shiptohome-shipment.save-shipment').trigger('click');
        } else {
            $('.submit-shipping').trigger('click');
        }
        setTimeout(function () {
            $('body').find('.byPassFOIDCheck').attr('value', 'false');
            $('body').find('.byPassAddressValidation').attr('value', 'false');
        }, 1000);
        $('div#foid-product-overlay').modal('hide');
        $('div#foid-product-overlay').remove();
        $('body').removeClass('modal-open').removeAttr('style');
        $('.modal-backdrop').remove();
    });

    $('body').on('click', '.decline-foid', function (e) {
        e.preventDefault();
        $('div#foid-product-overlay').modal('hide');
        $('div#foid-product-overlay').remove();
        $('body').removeClass('modal-open').removeAttr('style');
        $('.modal-backdrop').remove();
    });
}
/**
 * Handle response from the server for valid or invalid form fields.
 *  valid model data.
 */
function restrictedProductsResponse() {
    $('body').on('restricted:ProductPurchase', function (e, data) {
        var body = data.restrictItemsHtml;
        var modalHeadMsg = data.restrictAssetHTML;

        // Modal Container
        var htmlString = createHtmlString(modalHeadMsg, 'restrict');

        $('body').append(htmlString);
        $('div#restricted-product-overlay').modal('show');
        $('#restricted-product-overlay').find('.modal-body').html(body);
        setTimeout(function () {
            if ($('.product-container-scrollbar').hasClass('perfect-scrollbar')) {
                var resultsDiv = '.perfect-scrollbar';
                var perfectScrollbar = new PerfectScrollbar(resultsDiv);
                perfectScrollbar.update();
            }
        }, 500);
        initializeRestrictedProdModal();

        // GTM - event
        require('gtm/gtm/gtm').itemsRestricted();
    });
    $('body').on('restricted:foidConsent', function (e, data) {
        var body = data.FOIDItemsHtml;
        var modalHeadMsg = data.foidAssetHTML;

        // Modal Container
        var htmlString = createHtmlString(modalHeadMsg, 'foid');

        $('body').append(htmlString);
        $('div#foid-product-overlay').modal('show');
        $('#foid-product-overlay').find('.modal-body').html(body);
        setTimeout(function () {
            if ($('.product-container-scrollbar').hasClass('perfect-scrollbar')) {
                var resultsDiv = '.perfect-scrollbar';
                var perfectScrollbar = new PerfectScrollbar(resultsDiv);
                perfectScrollbar.update();
            }
        }, 500);
        initializeRestrictedProdModal();

        // GTM - event
        require('gtm/gtm/gtm').itemsRestricted();
    });
}

/**
 * Handle response from the server for valid or invalid form fields.
 * @param {Object} defer - the deferred object which will resolve on success or reject.
 * @param {Object} data - the response data with the invalid form fields or
 *  valid model data.
 */
function shippingFormResponse(defer, data) {
    var isMultiShip = $('#checkout-main').hasClass('multi-ship');
    var formSelector = isMultiShip
        ? '.multi-shipping .active form'
        : '.single-shipping form';

    // highlight fields with errors
    if (data.error && data.hasRestrictedItems !== undefined && data.hasRestrictedItems !== false) {
        $('body').trigger('restricted:ProductPurchase', data);
    } else if (data.error && data.hasFOIDItems !== undefined && data.hasFOIDItems !== false) {
        $('body').trigger('restricted:foidConsent', data);
    } else if (data.error && data.upsAddressReturned) {
        $('body').trigger('ups:validateAddress', data);
    } else if (data.error) {
        if (data.fieldErrors.length) {
            data.fieldErrors.forEach(function (error) {
                if (Object.keys(error).length) {
                    formHelpers.loadFormErrors(formSelector, error);
                }
            });
            defer.reject(data);
        }

        if (data.serverErrors && data.serverErrors.length) {
            $.each(data.serverErrors, function (index, element) {
                createErrorNotification(element);
            });

            defer.reject(data);
        }

        if (data.cartError) {
            window.location.href = data.redirectUrl;
            defer.reject();
        }
    } else {
        // Populate the Address Summary
        var hasSavedCards = $('.saved-payment-instrument').length;
        var savedElement = $('.user-payment-instruments');
        if (hasSavedCards > 0) {
            savedElement.removeClass('checkout-hidden');
            $('.payment-information').data('is-new-payment', false);
            $('.stored-form-remove-cc').addClass('checkout-hidden');
        }
        $('body').trigger('checkout:updateCheckoutView', {
            order: data.order,
            customer: data.customer
        });

        defer.resolve(data);
    }
}

/**
 * updates the shipping address selector within shipping forms
 * @param {Object} productLineItem - the productLineItem model
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order model
 * @param {Object} customer - the customer model
 */
function updateShippingAddressSelector(productLineItem, shipping, order, customer) {
    var shippings = order.shipping;

    var form = shipping.isStoreShipment ? $('body').find('.shipping-form.store-shipment') : $('body').find('.shipping-form.shipping-section');
    var $shippingAddressSelector;
    var hasSelectedAddress = false;

    if (form && form.length > 0) {
        $shippingAddressSelector = $('.addressSelector', form);
    }
    var previousSelectedValue;
    if ($shippingAddressSelector !== null && $shippingAddressSelector !== undefined) {
        previousSelectedValue = $shippingAddressSelector.val();
    }
    if ($shippingAddressSelector && $shippingAddressSelector.length === 1) {
        $shippingAddressSelector.empty();
        $shippingAddressSelector.closest('.custom-select').find('.selected-option').remove();
        $shippingAddressSelector.closest('.custom-select').find('.selection-list').remove();
        // Add New Address option
        $shippingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
            null,
            false,
            order));
        // Separator -
        if (shipping.shippingAddress !== null && shipping.shippingAddress !== undefined) {
            $shippingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
                order.resources.shippingAddresses, false, order, { className: 'multi-shipping' }
            ));
        }


        shippings.forEach(function (aShipping) {
            if (aShipping.shippingAddress !== null && aShipping.shippingAddress !== undefined && (!aShipping.selectedShippingMethod || !aShipping.selectedShippingMethod.storePickupEnabled)) {
                var isSelected = shipping.UUID === aShipping.UUID;
                hasSelectedAddress = hasSelectedAddress || isSelected;

                var addressOption = addressHelpers.methods.optionValueForAddress(
                        aShipping,
                        isSelected,
                        order,
                        { className: 'multi-shipping' }
                );
                var newAddress = addressOption.html() === order.resources.addNewAddress;
                var matchingUUID = aShipping.UUID === shipping.UUID;
                if ((newAddress && matchingUUID) || (!newAddress && matchingUUID) || (!newAddress && !matchingUUID)) {
                    $shippingAddressSelector.append(addressOption);
                }
                if (newAddress && !matchingUUID) {
                    $(addressOption[0]).remove();
                }
            }
        });
        if (customer.addresses && customer.addresses.length > 0) {
            $shippingAddressSelector.append(addressHelpers.methods.optionValueForAddress(
                order.resources.accountAddresses, false, order));
            customer.addresses.forEach(function (address) {
                var isSelected = shipping.matchingAddressId === address.ID;
                $shippingAddressSelector.append(
                    addressHelpers.methods.optionValueForAddress({
                        UUID: 'ab_' + address.ID,
                        shippingAddress: address
                    }, isSelected, order)
                );
            });
        }
        if (previousSelectedValue !== null && previousSelectedValue !== undefined && previousSelectedValue !== '') {
            $shippingAddressSelector.val(previousSelectedValue);
        }
        formFields.updateSelect();
    }

    if (!hasSelectedAddress) {
        // show
        $(form).addClass('hide-details');
    } else {
        $(form).removeClass('hide-details');
    }
}

/**
 * Handle response from the server for valid or invalid form fields.
 *  valid model data.
 */
function saveMultiShipInfo() {
    $('.btn-save-multi-ship').on('click', function (e) {
        e.preventDefault();

        // Save address to checkoutAddressBook
        var form = $(this).closest('form');
        var $rootEl = $(this).closest('.shipping-content');
        var data = $(form).serialize();
        var url = $(form).attr('action');

        var checkedShippingMethod = $('input[name=dwfrm_shipping_shippingAddress_shippingMethodID]:checked', form);
        var isStorePickUpMethod = checkedShippingMethod.attr('data-pickup');
        var storeId = $("input[name='storeId']", form).val();
        var errorMsg = 'Before you can continue to the next step, you must select a store.';

        if (isStorePickUpMethod === 'true' && (storeId === undefined)) {
            base.methods.createErrorNotification(errorMsg);
        } else {
            $rootEl.spinner().start();
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: data
            })
            .done(function (response) {
                formHelpers.clearPreviousErrors(form);
                // highlight fields with errors
                if (response.error && response.hasRestrictedItems !== undefined && response.hasRestrictedItems !== false) {
                    $('body').trigger('restricted:ProductPurchase', response);
                } else if (response.error && response.hasFOIDItems !== undefined && response.hasFOIDItems !== false) {
                    $('body').trigger('restricted:foidConsent', response);
                } else if (response.error && response.upsAddressReturned) {
                    $('body').trigger('ups:validateAddress', response);
                } else if (response.error) {
                    if (response.fieldErrors && response.fieldErrors.length) {
                        response.fieldErrors.forEach(function (error) {
                            if (Object.keys(error).length) {
                                formHelpers.loadFormErrors(form, error);
                            }
                        });
                    } else if (response.serverErrors && response.serverErrors.length) {
                        $.each(response.serverErrors, function (index, element) {
                            base.methods.createErrorNotification(element);
                        });
                    }
                } else {
                    // Update UI from response
                    $('body').trigger('checkout:updateCheckoutView',
                        {
                            order: response.order,
                            customer: response.customer
                        }
                    );

                    base.methods.viewMultishipAddress($rootEl);
                }

                if (response.order && response.order.shippable) {
                    if (response.order.usingMultiShipping) {
                        var addressSaved = $('.shiptohome-shipment.save-shipment').is(':visible') || $('.btn-save-multi-ship.save-shipment').is(':visible');
                        $('button.submit-shipping').attr('disabled', addressSaved);
                    } else {
                        $('button.submit-shipping').attr('disabled', null);
                    }
                }
                $('body').trigger('checkout:updateShippingAddressSelector');
                $rootEl.spinner().stop();
            })
            .fail(function (err) {
                if (err.responseJSON.redirectUrl) {
                    window.location.href = err.responseJSON.redirectUrl;
                }

                $rootEl.spinner().stop();
            });
        }
        return false;
    });
}

/**
 * updates the order shipping summary for an order shipment model
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order model
 */
function updateShippingSummaryInformation(shipping, order) {
    $('[data-shipment-summary=' + shipping.UUID + ']').each(function (i, el) {
        var $container = $(el);
        var $shippingAddressLabel = $container.find('.shipping-addr-label');
        var $addressContainer = $container.find('.address-summary');
        var $shippingPhone = $container.find('.shipping-phone');
        var $methodTitle = $container.find('.shipping-method-title');
        var $methodArrivalTime = $container.find('.shipping-method-arrival-time');
        var $methodPrice = $container.find('.shipping-method-price');
        var $shippingSummaryLabel = $container.find('.shipping-method-label');
        var $summaryDetails = $container.find('.row.summary-details');
        var giftMessageSummary = $container.find('.gift-summary');

        var address = shipping.shippingAddress;
        var selectedShippingMethod = shipping.selectedShippingMethod;
        var isGift = shipping.isGift;
        if (shipping.storeID === null) {
            addressHelpers.methods.populateAddressSummary($addressContainer, address);
            // multi shipment
            if ($methodTitle.length === 0) {
                $methodTitle = $container.parent('.view-address-block').find('.shipping-method-title');
            }
            if ($methodArrivalTime.length === 0) {
                $methodArrivalTime = $container.parent('.view-address-block').find('.shipping-method-arrival-time');
            }
            if ($methodPrice.length === 0) {
                $methodPrice = $container.parent('.view-address-block').find('.shipping-method-price');
            }
        }
        if (shipping.storeID !== null && shipping.storeID !== 'ByPass') {
            $('.shipping .bypass-ffldealer-selection').addClass('d-none');
            $('.shipping .address-summary').removeClass('d-none');
            $('.shipping-form.store-shipment').find('.view-address-block .bypass-ffldealer-selection').addClass('d-none');
            $('.shipping-form.store-shipment').find('.view-address-block .store-details').removeClass('d-none');
            addressHelpers.methods.populateAddressSummary($addressContainer, address);
        } else if (shipping.storeID !== null) {
            $('.shipping .bypass-ffldealer-selection').removeClass('d-none');
            $('.shipping .address-summary').addClass('d-none');
            $('.shipping-form.store-shipment').find('.view-address-block .bypass-ffldealer-selection').removeClass('d-none');
            $('.shipping-form.store-shipment').find('.view-address-block .store-details').addClass('d-none');
        }

        if (address && address.phone) {
            $shippingPhone.text(address.phone);
        } else {
            $shippingPhone.empty();
        }

        if (selectedShippingMethod) {
            var addressLabelText = selectedShippingMethod.storePickupEnabled ? order.resources.selectedFFL : order.resources.shippingAddress;
            $shippingAddressLabel.text(addressLabelText);
            $shippingSummaryLabel.show();
            $summaryDetails.show();
            $methodTitle.text(selectedShippingMethod.displayName);
            if (selectedShippingMethod.estimatedArrivalTime) {
                $methodArrivalTime.text(
                    '( ' + selectedShippingMethod.estimatedArrivalTime + ' )'
                );
            } else {
                $methodArrivalTime.empty();
            }
            $methodPrice.text(selectedShippingMethod.shippingCost);
        }

        if (isGift) {
            giftMessageSummary.find('.gift-message-summary').text(shipping.giftMessage);
            giftMessageSummary.removeClass('d-none');
        } else {
            giftMessageSummary.addClass('d-none');
        }
    });

    $('body').trigger('shipping:updateShippingSummaryInformation', { shipping: shipping, order: order });
}

/**
 * updates the shipping address form values within shipping forms
 * @param {Object} shipping - the shipping (shipment model) model
 */
function updateShippingAddressFormValues(shipping) {
    var addressObject = $.extend({}, shipping.shippingAddress);

    if (!addressObject) {
        addressObject = {
            firstName: null,
            lastName: null,
            address1: null,
            address2: null,
            companyName: null,
            city: null,
            postalCode: null,
            stateCode: null,
            countryCode: null,
            phone: null
        };
    }

    addressObject.isGift = shipping.isGift;
    addressObject.giftMessage = shipping.giftMessage;

    $('input[value=' + shipping.UUID + ']').each(function (formIndex, el) {
        var form = el.form;
        if (!form) return;
        var isNew = $('.shipping-form.shipping-section').find('select[name="shipmentSelector"] option:selected').val();
        if (isNew === 'new') return;
        var countryCode = addressObject.countryCode;

        $('input[name$=_firstName]', form).val(addressObject.firstName);
        $('input[name$=_lastName]', form).val(addressObject.lastName);
        $('input[name$=_address1]', form).val(addressObject.address1);
        $('input[name$=_address2]', form).val(addressObject.address2);
        $('input[name$=_companyName]', form).val(addressObject.companyName);
        $('input[name$=_city]', form).val(addressObject.city);
        $('input[name$=_postalCode]', form).val(addressObject.postalCode);
        $('select[name$=_stateCode],input[name$=_stateCode]', form)
            .val(addressObject.stateCode);

        if (countryCode && typeof countryCode === 'object') {
            $('select[name$=_country]', form).val('US');
        } else {
            $('select[name$=_country]', form).val('US');
        }

        $('input[name$=_phone]', form).val(addressObject.phone);

        $('input[name$=_isGift]', form).prop('checked', addressObject.isGift);
        $('textarea[name$=_giftMessage]', form).val(addressObject.isGift && addressObject.giftMessage ? addressObject.giftMessage : '');
    });

    $('body').trigger('shipping:updateShippingAddressFormValues', { shipping: shipping });
}


/**
 * Update the shipping UI for a single shipping info (shipment model)
 * @param {Object} shipping - the shipping (shipment model) model
 * @param {Object} order - the order/basket model
 * @param {Object} customer - the customer model
 * @param {Object} [options] - options for updating PLI summary info
 * @param {Object} [options.keepOpen] - if true, prevent changing PLI view mode to 'view'
 */
function updateShippingInformation(shipping, order, customer, options) {
    // First copy over shipmentUUIDs from response, to each PLI form
    order.shipping.forEach(function (aShipping) {
        aShipping.productLineItems.items.forEach(function (productLineItem) {
            base.methods.updateProductLineItemShipmentUUIDs(productLineItem, aShipping);
        });
    });

    // Now update shipping information, based on those associations
    base.methods.updateShippingMethods(shipping);
    updateShippingAddressFormValues(shipping);
    updateShippingSummaryInformation(shipping, order);

    // And update the PLI-based summary information as well
    shipping.productLineItems.items.forEach(function (productLineItem) {
        updateShippingAddressSelector(productLineItem, shipping, order, customer);
        base.methods.updatePLIShippingSummaryInformation(productLineItem, shipping, order, options);
    });

    $('body').trigger('shipping:updateShippingInformation', {
        order: order,
        shipping: shipping,
        customer: customer,
        options: options
    });
}

/**
 * Clear out all the shipping form values and select the new address in the drop down
 * @param {Object} order - the order object
 */
function clearShippingForms(order) {
    order.shipping.forEach(function (shipping) {
        $('input[value=' + shipping.UUID + ']').each(function (formIndex, el) {
            var form = el.form;
            if (!form) return;

            $('input[name$=_firstName]', form).val(null);
            $('input[name$=_lastName]', form).val(null);
            $('input[name$=_address1]', form).val(null);
            $('input[name$=_address2]', form).val(null);
            $('input[name$=_companyName]', form).val(null);
            $('input[name$=_city]', form).val(null);
            $('input[name$=_postalCode]', form).val(null);
            $('select[name$=_stateCode],input[name$=_stateCode]', form).val(null);
            $('select[name$=_country]', form).val('US');

            $('input[name$=_phone]', form).val(null);

            $('input[name$=_isGift]', form).prop('checked', false);
            $('textarea[name$=_giftMessage]', form).val('');
            $(form).find('.gift-message').addClass('d-none');

            $(form).attr('data-address-mode', 'new');
            var addressSelectorDropDown = $('.addressSelector option[value=new]', form);
            $(addressSelectorDropDown).prop('selected', true);
        });
    });

    $('body').trigger('shipping:clearShippingForms', { order: order });
}

base.updateDataAddressMode = function () {
    $('body').on('shipping:updateDataAddressMode', function (e, data) {
        if ($('.addressSelector option:selected', data.form).val() !== 'new' && !$('#checkout-main').hasClass('multi-ship')) {
            data.mode = 'edit'; // eslint-disable-line
        }
        $(data.form).attr('data-address-mode', data.mode);
    });
};


base.cancelMultiShipAddress = function () {
    $('.btn-cancel-multi-ship-address').on('click', function (e) {
        e.preventDefault();

        var form = $(this).closest('form');
        var $rootEl = $(this).closest('.shipping-content');
        var restoreState = $rootEl.data('saved-state');

        // Should clear out changes / restore previous state
        if (restoreState) {
            var restoreStateObj = JSON.parse(restoreState);
            var originalStateCode = restoreStateObj.shippingAddress.stateCode;
            var stateCode = $('[name$=_stateCode]', form).val();

            if (stateCode === '') {
                hideCancelifNewAddress($rootEl);
                return false;
            }
            updateShippingAddressFormValues(restoreStateObj);
            if (stateCode !== originalStateCode) {
                $('[data-action=save]', form).trigger('click');
            } else {
                $(form).attr('data-address-mode', 'edit');
                editMultiShipAddress($rootEl);
            }
        }

        return false;
    });
};

/**
 * Does Ajax call to select shipping method
 * @param {string} url - string representation of endpoint URL
 * @param {Object} urlParams - url params
 * @param {Object} el - element that triggered this call
 */
function selectShippingMethodAjax(url, urlParams, el) {
    $.spinner().start();
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: urlParams
    })
        .done(function (data) {
            if (data.error) {
                window.location.href = data.redirectUrl;
            } else {
                $('body').trigger('checkout:updateCheckoutView',
                    {
                        order: data.order,
                        customer: data.customer,
                        options: { keepOpen: true },
                        urlParams: urlParams
                    }
                );
                $('body').trigger('checkout:postUpdateCheckoutView',
                    {
                        el: el
                    }
                );
                $('body').trigger('checkout:updateShippingAddressSelector');
            }
            $.spinner().stop();
        })
        .fail(function () {
            $.spinner().stop();
        });
}

/**
 * Does Ajax call to select shipping method
 */
function selectShippingMethod() {
    $('.shipping-method-list').change(function () {
        var $shippingForm = $(this).parents('form');
        var methodID = $(':checked', this).val();
        var shipmentUUID = $shippingForm.find('[name=shipmentUUID]').val();
        var urlParams = addressHelpers.methods.getAddressFieldsFromUI($shippingForm);
        var $shippingAddressBlock = $('.shipping-form.shipping-section').find('.shipping-address-block');
        urlParams.shipmentUUID = shipmentUUID;
        urlParams.methodID = methodID;
        urlParams.isGift = $shippingForm.find('.gift').prop('checked');
        urlParams.giftMessage = $shippingForm.find('textarea[name$=_giftMessage]').val();

        var url = $(this).data('select-shipping-method-url');
        selectShippingMethodAjax(url, urlParams, $(this));

        if (!$(this).hasClass('field-selection-update') && $shippingAddressBlock.is(':visible')) {
            $(this).addClass('field-selection-update');
        }
    });
}

/**
 * Does Ajax call to select shipping method
 */
function updateShippingList() {
    var $shippingAddressBlock = $('.shipping-form.shipping-section').find('.shipping-address-block');

    $('select[name$="shippingAddress_addressFields_states_stateCode"]')
        .on('change', function (e) {
            updateShippingMethodList($(e.currentTarget.form));
            if (!($(this).hasClass('field-selection-update')) && $shippingAddressBlock.is(':visible')) {
                $(this).addClass('field-selection-update');
            }
        });

    $('input[name$="shippingAddress_addressFields_postalCode"]')
        .on('focusout', function (e) {
            updateShippingMethodList($(e.currentTarget.form));
            if (!$(this).hasClass('field-selection-update')) {
                $(this).addClass('field-selection-update');
            }
        });
}


base.updateShippingList = updateShippingList;
base.methods.updateShippingMethodList = updateShippingMethodList;
base.selectShippingMethod = selectShippingMethod;
base.methods.selectShippingMethodAjax = selectShippingMethodAjax;
base.methods.updateShippingAddressSelector = updateShippingAddressSelector;
base.methods.updateShippingAddressFormValues = updateShippingAddressFormValues;
base.methods.clearShippingForms = clearShippingForms;
base.methods.updateShippingSummaryInformation = updateShippingSummaryInformation;
base.methods.updateShippingInformation = updateShippingInformation;
base.saveMultiShipInfo = saveMultiShipInfo;
base.restrictedProductsResponse = restrictedProductsResponse;
base.methods.shippingFormResponse = shippingFormResponse;
base.eventsOnMultiShipment = eventsOnMultiShipment;
base.upsAddressValidation = ups.upsAddressValidation;
base.createUPSModalHtmlString = ups.createHtmlString;
base.createUPSModalBody = ups.createModalBody;
module.exports = base;
