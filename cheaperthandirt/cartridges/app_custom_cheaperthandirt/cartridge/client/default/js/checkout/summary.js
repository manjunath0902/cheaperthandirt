'use strict';

/**
 * updates the order product shipping summary for an order model
 * @param {Object} order - the order model
 */
function updateOrderProductSummaryInformation(order) {
    var $productSummary = $('<div />');
    order.shipping.forEach(function (shipping) {
        shipping.productLineItems.items.forEach(function (lineItem) {
            var pli = $('[data-product-line-item=' + lineItem.UUID + ']');
            $productSummary.append(pli);
        });
        if (!shipping.onlyGCInShipment) {
            var address = shipping.shippingAddress || {};
            var selectedMethod = shipping.selectedShippingMethod;

            var nameLine = address.firstName ? address.firstName + ' ' : '';
            if (address.lastName) nameLine += address.lastName;

            var address1Line = address.address1;
            var address2Line = address.address2;

            var phoneLine = address.phone;

            var shippingCost = selectedMethod ? selectedMethod.shippingCost : '';
            var methodNameLine = selectedMethod ? selectedMethod.displayName : '';
            var methodArrivalTime = selectedMethod && selectedMethod.estimatedArrivalTime
                ? '( ' + selectedMethod.estimatedArrivalTime + ' )'
                : '';

            var tmpl = $('#pli-shipping-summary-template').clone();

            if (shipping.productLineItems.items && shipping.productLineItems.items.length > 1) {
                $('h5 > span').text(' - ' + shipping.productLineItems.items.length + ' '
                    + order.resources.items);
            } else {
                $('h5 > span').text('');
            }

            var stateRequiredAttr = $('#shippingState').attr('required');
            var isRequired = stateRequiredAttr !== undefined && stateRequiredAttr !== false;
            var stateExists = (shipping.shippingAddress && shipping.shippingAddress.stateCode)
                ? shipping.shippingAddress.stateCode
                : false;
            var stateBoolean = false;
            if ((isRequired && stateExists) || (!isRequired)) {
                stateBoolean = true;
            }

            var shippingForm = $('.multi-shipping input[name="shipmentUUID"][value="' + shipping.UUID + '"]').parent();
            if (!shipping.isStoreShipment) {
                $('.ship-to-name', tmpl).text(nameLine);
                $('.ship-to-address1', tmpl).text(address1Line);
                $('.ship-to-address2', tmpl).text(address2Line);
                $('.ship-to-city', tmpl).text(address.city);
                if (address.stateCode) {
                    $('.ship-to-st', tmpl).text(address.stateCode);
                }
                $('.ship-to-zip', tmpl).text(address.postalCode);
                $('.ship-to-phone', tmpl).text(phoneLine);

                if (!address2Line) {
                    $('.ship-to-address2', tmpl).hide();
                }

                if (!phoneLine) {
                    $('.ship-to-phone', tmpl).hide();
                }

                shippingForm.find('.ship-to-message').text('');
            }

            if (shipping.shippingAddress
                && shipping.shippingAddress.firstName
                && shipping.shippingAddress.address1
                && shipping.shippingAddress.city
                && stateBoolean
                && shipping.shippingAddress.countryCode
                && shipping.productLineItems.items[0].fromStoreId !== 'ByPass') {
                $('.ship-to-name', tmpl).text(nameLine);
                $('.ship-to-address1', tmpl).text(address1Line);
                $('.ship-to-address2', tmpl).text(address2Line);
                $('.ship-to-city', tmpl).text(address.city);
                if (address.stateCode) {
                    $('.ship-to-st', tmpl).text(address.stateCode);
                }
                $('.ship-to-zip', tmpl).text(address.postalCode);
                $('.ship-to-phone', tmpl).text(phoneLine);

                if (!address2Line) {
                    $('.ship-to-address2', tmpl).hide();
                }

                if (!phoneLine) {
                    $('.ship-to-phone', tmpl).hide();
                }
                if (address.companyName) {
                    $('.ship-to-companyName', tmpl).text(address.companyName);
                }
                shippingForm.find('.ship-to-message').text('');
                if (!shipping.isStoreShipment) {
                    $('.ship-to-email', tmpl).text(order.orderEmail);
                }
            } else {
                $('.ship-to-message-bypass', tmpl).text(order.resources.byPassAddress);
            }

            if (shipping.isGift) {
                $('.gift-message-summary', tmpl).text(shipping.giftMessage);
            } else {
                $('.gift-summary', tmpl).addClass('d-none');
            }

            // checking h5 title shipping to or pickup
            var $shippingAddressLabel = $('.shipping-header-text', tmpl);
            $('body').trigger('shipping:updateAddressLabelText',
                { selectedShippingMethod: selectedMethod, resources: order.resources, shippingAddressLabel: $shippingAddressLabel });

            if (shipping.selectedShippingMethod && !shipping.selectedShippingMethod.storePickupEnabled) {
                $('.display-name', tmpl).text(methodNameLine);
                $('.arrival-time', tmpl).text(methodArrivalTime);
                $('.price', tmpl).text(shippingCost);
                $('.shipping-method-label', tmpl).append('<h5>' + $('#shipping-method-label-msg').text() + '</h5>');
            }

            var $shippingSummary = $('<div class="multi-shipping" data-shipment-summary="'
                + shipping.UUID + '" />');
            $shippingSummary.html(tmpl.html());
            $productSummary.append($shippingSummary);
        }
    });

    $('.product-summary-block').html($productSummary.html());
}

/**
 * re-renders the order totals and the number of items in the cart
 * @param {Object} data - AJAX response from the server
 */
function updateTotals(data) {
    $('.shipping-total-cost').empty().append(data.totals.totalShippingCost);
    $('.tax-total').empty().append(data.totals.totalTax);
    $('.grand-total-sum').empty().append(data.finalOrderTotal);
    $('.sub-total').empty().append(data.totals.subTotal);

    if (data.giftCards.giftCardTotal.value > 0) {
        $('.giftcard-discount').removeClass('hide-order-discount');
        $('.order-giftcard-total').empty()
            .append('- ' + data.giftCards.giftCardTotal.formatted);
    } else {
        $('.giftcard-discount').addClass('hide-order-discount');
    }

    if (data.giftCards.giftCardTotal.value > 0) {
        $('.gift-card-remaining').removeClass('giftcard-empty');
        $('.remaining-order-total').empty()
            .append('  ' + data.finalOrderTotal);
    } else {
        $('.gift-card-remaining').addClass('giftcard-empty');
    }
    if (data.requireCCPayment) {
        $('body').trigger('zerodollar:require-cc');
    } else {
        $('body').trigger('zerodollar:no-require-cc');
    }

    $('.coupons-and-promos').empty().append(data.totals.discountsHtml);
    $('.gift-card-displays').empty().append(data.giftCards.giftCardsHTML);
    if (data.giftCards.giftCards.length < 3) {
        $('body').find('.apply-giftcard-toggle').removeClass('d-none');
        $('body').find('.gift-max-message').addClass('d-none');
        $('body').find('.gift-card-info').removeClass('d-none');
    }

    if (data.totals.totalDiscount > 0) {
        $('.savings-total').removeClass('d-none');
        $('.grand-total-discount').empty()
            .append(data.totals.totalDiscountFormatted);
    } else {
        $('.savings-total').addClass('d-none');
    }

    if (data.totals.orderLevelDiscountTotal.value > 0) {
        $('.order-discount').removeClass('hide-order-discount');
        $('.order-discount-total').empty()
            .append('- ' + data.totals.orderLevelDiscountTotal.formatted);
    } else {
        $('.order-discount').addClass('hide-order-discount');
    }

    if (data.totals.shippingLevelDiscountTotal.value > 0) {
        $('.shipping-discount').removeClass('hide-shipping-discount');
        $('.shipping-discount-total').empty().append('- ' +
            data.totals.shippingLevelDiscountTotal.formatted);
    } else {
        $('.shipping-discount').addClass('hide-shipping-discount');
    }
}

module.exports = {
    updateTotals: updateTotals,
    updateOrderProductSummaryInformation: updateOrderProductSummaryInformation
};
