'use strict';

/**
 * Read more
 */
function readMore() {
    $('.read-more').click(function () {
        $('.read-less-content').toggleClass('d-none');
        $('.read-more-content').toggleClass('d-none');
    });
}

/**
 * slick
 */
function initializeEvents() {
    $('.subcategory-cover-content .row').slick({
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        cssEase: 'ease-in-out',
        arrows: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2.3,
                slidesToScroll: 1
            }
        }
        ]
    });

    $('.fire-content').slick({
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        cssEase: 'ease-in-out',
        arrows: true,
        prevArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-prev"></span>',
        nextArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-next"></span>',
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2.5,
                slidesToScroll: 1,
                arrows: false
            }
        }
        ]
    });

    $('.shop-brandlist .anchor-link a').on('click', function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var scrollPosition = $('#' + href).offset().top;
        $('html, body').animate({
            /* eslint-disable */
            'scrollTop': scrollPosition
            /* eslint-enable */
        }, 500);
    });
}

module.exports = {
    readMore: readMore,
    initializeEvents: initializeEvents
};
