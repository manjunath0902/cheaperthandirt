'use strict';

var formValidation = require('base/components/formValidation');
var cleave = require('base/components/cleave');
var base = require('base/paymentInstruments/paymentInstruments');

var url;

module.exports = {
    removePayment: function () {
        $('.remove-payment').on('click', function (e) {
            e.preventDefault();
            url = $(this).data('url') + '?UUID=' + $(this).data('id');
            $('.payment-to-remove').empty().append($(this).data('card'));

            $('.delete-confirmation-btn').click(function (f) {
                f.preventDefault();
                $('.remove-payment').trigger('payment:remove', f);
                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            $('#uuid-' + data.UUID).remove();
                            if ($('.card-obj').length < 10) {
                                $('.add-button').removeClass('d-none');
                            }
                        }
                        if (data.message) {
                            $('.paymentInstruments').html('');
                            var toInsert = '<div><h3 class="no-saved-card-message">' +
                            data.message +
                            '</h3><div>';
                            $('.paymentInstruments').html(toInsert);
                        }
                    },
                    error: function (err) {
                        if (err.responseJSON.redirectUrl) {
                            window.location.href = err.responseJSON.redirectUrl;
                        }
                        $.spinner().stop();
                    }
                });
            });
        });
    },


    submitPayment: function () {
        $('form.payment-form').submit(function (e) {
            var $form = $(this);
            e.preventDefault();
            url = $form.attr('action');
            $form.spinner().start();
            $('form.payment-form').trigger('payment:submit', e);

            var formData = cleave.serializeData($form);

            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: formData,
                success: function (data) {
                    $form.spinner().stop();
                    if (!data.success) {
                        formValidation($form, data);
                        if (data.errorMessage) {
                            $('.service-error').remove();
                            $form.prepend('<div class="service-error alert-danger">'
                                   + data.errorMessage + '</div>');
                        }
                    } else {
                        location.href = data.redirectUrl;
                    }
                },
                error: function (err) {
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    }
                    $form.spinner().stop();
                }
            });
            return false;
        });
    },

    handleCreditCardNumber: base.handleCreditCardNumber
};
