'use strict';

var recaptcha = require('../recaptcha/recaptcha');

/**
 * update the token and submit the form
 * @param {string} token - to update the hidden field
 * @param {Object} $form - current form
 */
function sendToFriendSubmit(token, $form) {
    $form.find('input[name="recaptcha_token"]').val(token);

    $form.find('.sendtofriendFormSubmit').trigger('click');
}

module.exports = {
    submitSendToFriendForm: function () {
        $('#recaptchaSubmit').on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            var $form = $('.sendToFriend-form');
            $('.focusout-validate').each(function () {
                $(this).trigger('blur');
            });
            if ($('.sendToFriend-form-fields').find('.is-invalid').length === 0) {
                recaptcha('sendToFriend', $form, sendToFriendSubmit);
            }
        });
    }
};
