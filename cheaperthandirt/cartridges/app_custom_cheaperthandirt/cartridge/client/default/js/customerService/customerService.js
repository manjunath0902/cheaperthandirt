'use strict';

var formValidation = require('base/components/formValidation');
var recaptcha = require('../recaptcha/recaptcha');

var items = $('.timeline li');

/**
 * update the token and submit the form
 * @param {string} token - to update the hidden field
 * @param {Object} $form - current form
 */
function contactUsCallback(token, $form) {
    // update the token to the form hidden field
    $form.find('input[name="recaptcha_token"]').val(token);

    // make the ajax call
    var url = $form.attr('action');
    $('.cu-alert-message').remove();
    $form.find('.form-fields').spinner().start();
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: $form.serialize(),
        success: function (data) {
            $form.find('.form-fields').spinner().stop();
            if (data.success) {
                window.location.href = data.redirectUrl;
            } else if (!data.success) {
                formValidation($form, data);
                $('.contact-us-response-msg').append(
                        '<div class="alert cu-alert-message text-center" role="alert">'
                        + data.message
                        + '</div>'
                    );
            }
        },
        error: function () {
            $form.find('.form-fields').spinner().stop();
        }
    });
}

/**
 * update the token and submit the form
 * @param {string} token - to update the hidden field
 * @param {Object} $form - current form
 */
function rmaCallback(token, $form) {
    // update the token to the form hidden field
    $form.find('input[name="recaptcha_token"]').val(token);

    // make the ajax call
    var url = $form.attr('action');
    $('.rma-alert-message').remove();
    $form.find('.form-fields').spinner().start();
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: $form.serialize(),
        success: function (data) {
            $form.find('.form-fields').spinner().stop();
            if (!data.success) {
                formValidation($form, data);
            } else if (data.success) {
                window.location.href = data.redirectUrl;
            }
        },
        error: function () {
            $form.find('.form-fields').spinner().stop();
        }
    });
}

/**
 * Retrieves the value associated with the scroll of the browser
 * @param {jquery} el - DOM container for the relevant element
 * @return {string} - value found in the quantity input
 */
function elementInViewport(el) {
    var $el = el;
    var top = $el.offsetTop;
    var left = $el.offsetLeft;
    var width = $el.offsetWidth;
    var height = $el.offsetHeight;

    while ($el.offsetParent) {
        $el = $el.offsetParent;
        top += $el.offsetTop;
        left += $el.offsetLeft;
    }

    return (
        top >= window.pageYOffset &&
        left >= window.pageXOffset &&
        (top + height) <= (window.pageYOffset + window.innerHeight) &&
        (left + width) <= (window.pageXOffset + window.innerWidth)
    );
}

/**
 * Scroll callback function for to show the items in the list
 */
function scrollCallbackFunction() {
    var count;
    for (count = 0; count < items.length; count++) {
        if (elementInViewport(items[count])) {
            items[count].classList.add('in-view');
        }
    }
}

module.exports = {
    submitContactForm: function () {
        $('form.contact-form').submit(function (e) {
            e.preventDefault();
            var $form = $(this);
            recaptcha('contactUs', $form, contactUsCallback);
        });
    },

    submitReturnItemForm: function () {
        $('form.rma-form').submit(function (e) {
            e.preventDefault();
            var $form = $(this);
            recaptcha('contactUs', $form, rmaCallback);
        });
    },

    customCode: function () {
        $(window).scroll(function () {
            scrollCallbackFunction();
        });

        $(window).resize(function () {
            scrollCallbackFunction();
        });
    }
};
