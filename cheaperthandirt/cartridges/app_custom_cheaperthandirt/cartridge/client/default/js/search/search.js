'use strict';

var catLanding = require('../catLanding/catLanding');
var base = require('base/search/search');
var formFields = require('../formFields/formFields');

/**
 * Update sort option URLs from Ajax response
 *
 * @param {string} response - Ajax response HTML code
 * @return {undefined}
 */
function updateSortOptions(response) {
    var $tempDom = $('<div>').append($(response));
    var sortOptions = $tempDom.find('.grid-footer').data('sort-options').options;
    sortOptions.forEach(function (option) {
        $('option.' + option.id).val(option.url);
    });
}

/**
 * AR page product slick
 */
function topSlickARPage() {
    if ($('.top-products').hasClass('slick-initialized')) {
        $('.top-products').slick('unslick');
    }
    $('.top-products').slick({
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        cssEase: 'ease-in-out',
        arrows: true,
        rows: 0,
        prevArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-prev"></span>',
        nextArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-next"></span>',
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                dots: true,
                arrows: false
            }
        }
        ]
    });
}

/**
 * AR page product slick re-position height
 */
function topSlickRepositionHeight() {
    $('.top-products').on('setPosition', function () {
        $(this).find('.product-tile').height('auto');

        var slickTrack = $(this).find('.slick-track');
        var slickTrackHeight = $(slickTrack).height();
        $(this).find('.product-tile').css('height', slickTrackHeight + 'px');
    });
}

/**
 * calling function for read more
 */
function initalize() {
    catLanding.readMore();
    topSlickARPage();
    topSlickRepositionHeight();
    if (window.matchMedia('(min-width: 1024px)').matches) {
        $('.refinement-bar').find('.card').addClass('active');
    }
}


/**
 * Update DOM elements with Ajax results
 *
 * @param {Object} $results - jQuery DOM element
 * @param {string} selector - DOM element to look up in the $results
 * @return {undefined}
 */
function updateDom($results, selector) {
    var $updates = $results.find(selector);
    $(selector).empty().html($updates.html());
}

/**
 * Keep refinement panes expanded/collapsed after 'A-jax' refresh
 * @param {Object} $results - jQuery DOMelement
 * @return {undefined}
 */
function handleRefinements($results) {
    $('.refinement.active').each(function () {
        $(this).removeClass('active');

        $results
            .find('.' + $(this)[0].className.replace(/ /g, '.'))
            .addClass('active');
    });

    updateDom($results, '.refinements');
}

/**
 * Parse Ajax results and updated select DOM elements
 *
 * @param {string} response - Ajax response HTML code
 * @return {undefined}
 */
function parseResults(response) {
    var $results = $(response);
    var specialHandlers = {
        '.refinements': handleRefinements
    };

    // Update DOM elements that do not require special handling
    [
        '.grid-header',
        '.sort-selection',
        '.header-bar',
        '.header.page-title',
        '.product-grid',
        '.show-more',
        '.filter-bar'
    ].forEach(function (selector) {
        updateDom($results, selector);
        if (selector === '.sort-selection') {
            formFields.updateSelect();
        }
    });

    Object.keys(specialHandlers).forEach(function (selector) {
        specialHandlers[selector]($results);
    });
}

module.exports = {

    initalize: initalize,

    customShowMore: function () {
        $('body').on('click', '.ShowMore', function () {
            $(this).closest('.refinement').find('.refinement-showmore-section').removeClass('d-none');
            $(this).closest('.refinement').find('.refinement-showless-button').removeClass('d-none');
            $(this).closest('.refinement').find('.refinement-showmore-button').addClass('d-none');
        });
    },

    filter: function () {
        // Display refinements bar when Menu icon clicked
        $('.container').on('click', 'button.filter-results', function () {
            $('.refinement-bar, .modal-background').show();
            $('.refinement-bar, .modal-background').addClass('refine-active');
        });
    },

    closeRefinments: function () {
        // Refinements close button
        $('.container').on('click', '.refinement-bar button.close, .modal-background', function () {
            $('.refinement-bar, .modal-background').hide();
            $('.refinement-bar, .modal-background').removeClass('refine-active');
        });
    },

    resize: function () {
        // Close refinement bar and hide modal background if user resizes browser
        $(window).resize(function () {
            $('.refinement-bar, .modal-background').hide();
            if (window.matchMedia('(min-width: 1024px)').matches) {
                $('.refinement-bar').find('.card').addClass('active');
            } else {
                $('.refinement-bar').find('.card').removeClass('active');
            }
        });
    },

    showLess: function () {
        $('body').on('click', '.ShowLess', function () {
            $(this).closest('.refinement').find('.refinement-showmore-section').addClass('d-none');
            $(this).closest('.refinement').find('.refinement-showmore-button').removeClass('d-none');
            $(this).closest('.refinement').find('.refinement-showless-button').addClass('d-none');
        });
    },

    showContentTab: base.showContentTab,

    inputCheck: function () {
        var mainsrc = $('.ar15Image').attr('src');
        var $slotBannerHTML = $('.secondPositionSlot');
        $('.custom-checkbox input').change(function () {
            $.spinner().start();
            if (this.checked) {
                var imgsrc = $(this).closest('.level4-main').attr('data-level4image');
                var $childcontent = $(this).closest('.category-level-content');
                $('.ar15Image').attr('src', imgsrc);
                $childcontent.addClass('level-checked').find('.levelFiveGrid').removeClass('d-none');
                $childcontent.find('.levelFiveGrid.levelFiveGrid-1').after($slotBannerHTML);
                $childcontent.siblings().removeClass('level-checked').find('input[type=checkbox]').prop('checked', false);
                $childcontent.siblings().find('.levelFiveGrid').addClass('d-none');
                $childcontent.siblings().find('.secondPositionSlot').remove();
            } else {
                $(this).closest('.category-level-content').removeClass('level-checked').siblings()
                .find('.levelFiveGrid')
                .removeClass('d-none');
                $(this).closest('.category-level-content').siblings().find('.secondPositionSlot')
                .remove();
                $(this).closest('.category-level-content').find('.secondPositionSlot').remove();
                $('.category-level-content').eq(0).find('.levelFiveGrid.levelFiveGrid-1').after($slotBannerHTML);
                $('.ar15Image').attr('src', mainsrc);
            }
            setTimeout(function () {
                topSlickARPage();
                topSlickRepositionHeight();
                $.spinner().stop();
            }, 1000);
        });
    }
};

base.applyFilter = function () {
        // Handle refinement value selection and reset click
    $('.container').on(
            'click',
            '.refinements li a, .refinement-bar a.reset, .filter-value a, .swatch-filter a',
            function (e) {
                e.preventDefault();
                e.stopPropagation();

                var url = new URL(e.currentTarget.href);
                var params = new URLSearchParams(url.search);
                var srule = $('select[name="sort-order"] option:selected').data('id');
                if (params.has('srule')) {
                    params.delete('srule');
                }
                params.append('srule', srule);
                var filterURL = url.origin + url.pathname + '?' + params.toString();

                $.spinner().start();
                $(this).trigger('search:filter', e);
                $.ajax({
                    url: filterURL,
                    data: {
                        page: $('.grid-footer').data('page-number'),
                        selectedUrl: e.currentTarget.href
                    },
                    method: 'GET',
                    success: function (response) {
                        parseResults(response);
                        $.spinner().stop();
                    },
                    error: function () {
                        $.spinner().stop();
                    }
                });

                // GTM - event
                require('gtm/gtm/gtm').facet($(this));
            });
};

base.showMore = function () {
    // Show more products
    $('.container').on('click', '.show-more button', function (e) {
        e.stopPropagation();
        var showMoreUrl = $(this).data('url');

        e.preventDefault();

        $.spinner().start();
        // $(this).trigger('search:showMore', e);
        $.ajax({
            url: showMoreUrl,
            data: { selectedUrl: showMoreUrl },
            method: 'GET',
            success: function (response) {
                $('.grid-footer').replaceWith(response);
                updateSortOptions(response);
                $.spinner().stop();
            },
            error: function () {
                $.spinner().stop();
            }
        });
    });
};

base.sort = function () {
    // Handle sort order menu selection
    $('.container').on('change', '[name=sort-order]', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.spinner().start();
        var $selectField = $('select[name="sort-order"] option:selected');
        var sortUrl = $selectField.data('url');
        var url = new URL(sortUrl);
        var params = new URLSearchParams(url.search);
        var srule = $selectField.data('id');
        if (params.has('srule')) {
            params.delete('srule');
        }
        params.append('srule', srule);
        params.append('start', 0);
        params.append('sz', 24);
        params.append('isAjax', true);
        sortUrl = url.origin + url.pathname + '?' + params.toString();

        $.ajax({
            url: sortUrl,
            data: { selectedUrl: sortUrl },
            method: 'GET',
            success: function (response) {
                parseResults(response);
                $.spinner().stop();
            },
            error: function () {
                $.spinner().stop();
            }
        });

        // GTM - event
        require('gtm/gtm/gtm').sort($(this).find('option:selected').text());
    });
};
