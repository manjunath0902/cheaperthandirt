'use strict';

var catLanding = require('../catLanding/catLanding');
var Mark = require('mark.js/dist/jquery.mark.min');

/**
 * slickContent for AR-15 Build Page
 */
function slickContent() {
    $('.tab-content .slick-content').slick({
        dots: false,
        infinite: false,
        arrows: true,
        speed: 500,
        slidesToShow: 5.65,
        slidesToScroll: 1,
        cssEase: 'ease-in-out',
        prevArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-prev"></span>',
        nextArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-next"></span>',
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3.5,
                slidesToScroll: 1,
                arrows: false
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2.5,
                slidesToScroll: 1,
                arrows: false
            }
        }
        ]
    });

    $('.parts-content').on('click', '.part-image', function () {
        var partImage = $(this).data('part-image');
        var partText = $(this).closest('.part-tile').find('span').text();
        $(this).closest('.tab-pane').find('.slick-banner-image').attr('src', partImage);
        $(this).closest('.tab-pane').find('.text-section').addClass('d-none');
        $(this).closest('.tab-pane').find('.text-section.slick-text-update').removeClass('d-none')
        .text(partText);
    });
}

/**
 * contentHeight for AR-15 Build Page
 */
function contentHeight() {
    $('.build-lower-receiver .top-content').each(function () {
        var contentOuterHeight = $(this).outerHeight() + 40;
        $(this).closest('.build-lower-receiver').siblings('.content-container').find('ul')
        .css('top', contentOuterHeight);
    });
}

/**
 * calling function for read more
 */
function initalizeEvents() {
    catLanding.readMore();
    contentHeight();

    $('body').on('click', '.btnNext', function () {
        $('.nav-tabs > .active').next('a')
        .trigger('click');
    });

    $('body').on('click', '.btnPrevious', function () {
        $('.nav-tabs > .active').prev('a')
        .trigger('click');
    });

    $('input[type="radio"]').on('click', function () {
        $('.box').hide();
        $('.' + this.value).show();
    });

    $('.outside-link').on('click', function () {
        $('.nav-tabs a').removeClass('active');
        $($(this).attr('data-toggle-content')).trigger('click');
    });

    $('.search-term-button').on('click', function () {
        $(this).closest('.device').find('.form-group').toggleClass('d-none');
    });

    var mark = function () {
        // Read the keyword
        var keywordValue = $('input[name="keyword"]').val();

        // Remove previous marked elements and mark
        // the new keyword inside the context
        var contextTerm = $('#searchTerms');
        var instanceMark = new Mark(contextTerm);
        var options = {
            /* eslint-disable */
            'element': '',
            'className': '',
            'exclude': [],
            'separateWordSearch': true,
            'accuracy': 'partially',
            'diacritics': true,
            'synonyms': {},
            'iframes': false,
            'iframesTimeout': 5000,
            'acrossElements': false,
            'caseSensitive': false,
            'ignoreJoiners': false,
            'ignorePunctuation': [],
            'wildcards': 'disabled',
            'filter': function () {
                // textNode is the text node which contains the found term
                // foundTerm is the found search term
                // totalCounter is a counter indicating the total number of all marks
                // at the time of the function call
                // counter is a counter indicating the number of marks for the found term
                return true; // must return either true or false
            },
            'debug': false,
            'log': window.console
            /* eslint-enable */
        };
        instanceMark.unmark(options);
        instanceMark.mark(keywordValue, options);
    };

    $('input[name="keyword"]').on('input', mark);
}

module.exports = {
    slickContent: slickContent,
    initalizeEvents: initalizeEvents,
    youtubeVideo: function () {
        $('.containerAR15-body').find('.youtube-icon').off('click').on('click', function () {
            $(this).closest('.image-section').find('iframe').css({
                /* eslint-disable */
                'width': $(this).closest('.image-section').find('.video-link').width(),
                'height': $(this).closest('.image-section').find('.video-link').height()
                /* eslint-enable */
            });
            $(this).closest('.image-section').addClass('hide-background-icon');
            var autovideo = $(this).closest('.image-section').find('iframe')[0].src.replace('autoplay=0', 'autoplay=1');
            $(this).closest('.image-section').find('iframe').attr('src', autovideo);
        });
    },
    updateSelect: function () {
        $('.containerAR15-body').find('.content-container ul li').off('click').on('click', function () {
            var $this = $(this);
            var dataIframe = $this.data('iframe-youtube');
            var dataIframeContent = $this.attr('id');
            var dataIframeText = $this.html($this.html());
            var dataIframeImage = $this.closest('.containerAR15-body').find('.' + dataIframeContent).data('iframe-image');

            if ($this.hasClass('update-content')) {
                $this.closest('.content-container').toggleClass('show-dropdown');
            } else {
                $.spinner().start();
                $this.closest('.containerAR15-body').find('.image-section').removeClass('hide-background-icon');
                $this.closest('.containerAR15-body').find('.image-section iframe').attr('src', dataIframe);
                if (dataIframeImage !== '' && dataIframeImage !== undefined) {
                    $this.closest('.containerAR15-body').find('.image-section .video-link').attr('src', dataIframeImage);
                }
                $this.closest('.content-container').removeClass('show-dropdown');
                $this.closest('.containerAR15-body').find('.video-player').removeClass('active');
                $this.closest('.containerAR15-body').find('.' + dataIframeContent).addClass('active');
                $this.closest('.content-container').find('li').removeClass('update-content');
                dataIframeText.addClass('update-content').prependTo($this.closest('.content-container').find('ul'));
                setTimeout(function () {
                    $.spinner().stop();
                }, 1000);
            }
        });
    },
    customTabs: function () {
        $('.custom-nav-tabs li, .custom-tab-content .panel-title a').on('click', function () {
            $.spinner().start();
            $('.tab-pane').find('.text-section').removeClass('d-none');
            $('.tab-pane').find('.text-section.slick-text-update').addClass('d-none');
            $('.slick-image-section .slick-banner-image').each(function () {
                var updateSlickBanner = $(this).data('slick-image');
                var currentSlickBanner = $(this).attr('src');
                if (currentSlickBanner !== updateSlickBanner) {
                    $(this).attr('src', updateSlickBanner);
                }
            });

            if ($('.tab-content .slick-content').hasClass('slick-initialized')) {
                $('.tab-content .slick-content').slick('unslick');
            }
            setTimeout(function () {
                slickContent();
                $.spinner().stop();
            }, 2000);
        });
    },
    resizeContentHeight: function () {
        $(window).resize(function () {
            contentHeight();
        });
    },
    setVideoURLs: function () {
        var i = 0;
        $('body').find('.video-url').each(function () {
            $(this).closest('.containerAR15-body').find('.video-player-' + i).attr('data-iframe-image', $(this).attr('data-iframe-url'));
            i++;
        });
    },
    selectbrandlist: function () {
        $('.select-brands').click(function () {
            $('.selection-lists').toggle();
            $(this).toggleClass('dropdown-enable');
        });

        $(document).on('click', function (e) {
            if (!$('.select-brands').is(e.target)) {
                $('.selection-lists').hide();
                $('.select-brands').removeClass('dropdown-enable');
            }
        });
    }
};
