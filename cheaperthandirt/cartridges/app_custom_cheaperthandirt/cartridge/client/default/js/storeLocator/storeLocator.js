/* globals google */
'use strict';

var base = require('base/storeLocator/storeLocator');
var PerfectScrollbar = require('perfect-scrollbar/dist/perfect-scrollbar');

/**
 * appends params to a url
 * @param {string} url - Original url
 * @param {Object} params - Parameters to append
 * @returns {string} result url with appended parameters
 */
function appendToUrl(url, params) {
    var newUrl = url;
    newUrl += (newUrl.indexOf('?') !== -1 ? '&' : '?') + Object.keys(params).map(function (key) {
        return key + '=' + encodeURIComponent(params[key]);
    }).join('&');

    return newUrl;
}

/**
 * Uses google maps api to render a map
 */
function maps() {
    var map;
    var infowindow = new google.maps.InfoWindow();

    // Init U.S. Map in the center of the viewport
    var latitude = $('#USlatitude').val() !== '' ? $('#USlatitude').val() : 37.09024;
    var longitude = $('#USlongitude').val() !== '' ? $('#USlongitude').val() : -95.712891;
    var latlng = new google.maps.LatLng(latitude, longitude);
    var mapOptions = {
        scrollwheel: false,
        zoom: 4,
        center: latlng
    };

    map = new google.maps.Map($('.map-canvas')[0], mapOptions);
    var mapdiv = $('.map-canvas').attr('data-locations');

    mapdiv = JSON.parse(mapdiv);

    var bounds = new google.maps.LatLngBounds();

    // Customized google map marker icon with svg format
    var markerImg = {
        path: 'M17,22 L13,27 L9,22 L0,22 L0,0 L26,0 L26,22 L17,22 Z',
        fillColor: '#C62128',
        fillOpacity: 1,
        scale: 1.1,
        strokeColor: 'white',
        strokeWeight: 1,
        anchor: new google.maps.Point(13, 30),
        labelOrigin: new google.maps.Point(12, 12)
    };

    Object.keys(mapdiv).forEach(function (key) {
        var item = mapdiv[key];
        var lable = parseInt(key, 10) + 1;
        var storeLocation = new google.maps.LatLng(item.latitude, item.longitude);
        var marker = new google.maps.Marker({
            position: storeLocation,
            map: map,
            title: item.name,
            icon: markerImg,
            label: { text: lable.toString(), color: 'white', fontSize: '17px' }
        });

        marker.addListener('click', function () {
            infowindow.setOptions({
                content: item.infoWindowHtml
            });
            infowindow.open(map, marker);
        });

        // Create a minimum bound based on a set of storeLocations
        bounds.extend(marker.position);
    });
    // Fit the all the store marks in the center of a minimum bounds when any store has been found.
    if (mapdiv && mapdiv.length !== 0) {
        map.fitBounds(bounds);
    }
}

/**
 * Renders the results of the search and updates the map
 * @param {Object} data - Response from the server
 */
function updateStoresResults(data) {
    var $resultsDiv = $('.results');
    var $mapDiv = $('.map-canvas');
    var hasResults = data.stores.length > 0;

    if (!hasResults) {
        $('.store-locator-no-results').show();
        $('.store-page-button').addClass('hide-in-page');
        $('.store-locator-right-content').removeClass('stores-visible');
    } else {
        $('.store-locator-no-results').hide();
        $('.store-page-button').removeClass('hide-in-page');
        $('.store-locator-right-content').addClass('stores-visible');
    }

    $resultsDiv.empty()
        .data('has-results', hasResults)
        .data('radius', data.radius)
        .data('search-key', data.searchKey);

    $mapDiv.attr('data-locations', data.locations);

    if ($mapDiv.data('has-google-api')) {
        maps();
    } else {
        $('.store-locator-no-apiKey').show();
    }

    if (data.storesResultsHtml) {
        $resultsDiv.append(data.storesResultsHtml);
    }
    $('.select-store').prop('disabled', true);
    if ($('input.select-store-input').length > 0) {
        $('input.select-store-input:first').trigger('click');
        $('.select-store').prop('disabled', false);
        $('input.select-store-input:first').closest('.card-body').addClass('background-color');
    }
    $('.select-dealer-one').removeClass('d-lg-none');
}

/**
 * Search for stores with new zip code
 * @param {HTMLElement} element - the target html element
 * @returns {boolean} false to prevent default event
 */
function search(element) {
    var dialog = element.closest('.in-store-inventory-dialog');
    var spinner = dialog.length ? dialog.spinner() : $.spinner();
    spinner.start();
    var $form = element.closest('.store-locator');
    var radius = $('.results').data('radius');
    var url = $form.attr('action');
    var urlParams = { radius: radius };

    var payload = $form.is('form') ? $form.serialize() : { postalCode: $form.find('[name="postalCode"]').val() };

    url = appendToUrl(url, urlParams);

    $.ajax({
        url: url,
        type: $form.attr('method'),
        data: payload,
        dataType: 'json',
        success: function (data) {
            spinner.stop();
            updateStoresResults(data);
        }
    });

    // GTM - event
    require('gtm/gtm/gtm').storeLocatorSearch(element);

    return false;
}

module.exports = {
    init: function () {
        if ($('.map-canvas').data('has-google-api')) {
            maps();
        } else {
            $('.store-locator-no-apiKey').show();
        }

        if (!$('.results').data('has-results')) {
            $('.store-locator-no-results').show();
        }
    },

    detectLocation: function () {
        // clicking on detect location.
        $('.detect-location').on('click', function () {
            // GTM - event
            require('gtm/gtm/gtm').storeLocatorSearch($(this));

            $.spinner().start();
            if (!navigator.geolocation) {
                $.spinner().stop();
                return;
            }

            navigator.geolocation.getCurrentPosition(function (position) {
                var $detectLocationButton = $('.detect-location');
                var url = $detectLocationButton.data('action');
                var radius = $('.results').data('radius');
                var urlParams = {
                    radius: radius,
                    lat: position.coords.latitude,
                    long: position.coords.longitude
                };

                url = appendToUrl(url, urlParams);
                $.ajax({
                    url: url,
                    type: 'get',
                    dataType: 'json',
                    success: function (data) {
                        $.spinner().stop();
                        updateStoresResults(data);
                    }
                });
            });
        });
    },

    search: function () {
        $('.store-locator-container form.store-locator').submit(function (e) {
            e.preventDefault();
            search($(this));
        });
        $('.store-locator-container .btn-storelocator-search[type="button"]').click(function (e) { // eslint-disable-line
            e.preventDefault();
            // To restrict function call if zipcode value is 0 in CO page for ffl prod.
            if ($(this).closest('div.store-locator').find('.form-group #store-postal-code').val().length === 0) {
                /* eslint-disable */
                return false;
                /* eslint-enable */
            }
            search($(this));
        });
    },

    changeRadius: function () {
        $('.store-locator-container .radius').change(function () {
            var radius = $(this).val();
            var searchKeys = $('.results').data('search-key');
            var url = $('.radius').data('action-url');
            var urlParams = {};

            if (searchKeys.postalCode) {
                urlParams = {
                    radius: radius,
                    postalCode: searchKeys.postalCode
                };
            } else if (searchKeys.lat && searchKeys.long) {
                urlParams = {
                    radius: radius,
                    lat: searchKeys.lat,
                    long: searchKeys.long
                };
            }

            url = appendToUrl(url, urlParams);
            var dialog = $(this).closest('.in-store-inventory-dialog');
            var spinner = dialog.length ? dialog.spinner() : $.spinner();
            spinner.start();
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    spinner.stop();
                    updateStoresResults(data);
                }
            });
        });
    },

    selectStore: function () {
        $('.store-locator-container').on('click', '.select-store', (function (e) {
            e.preventDefault();
            var selectedStore = $(':checked', '.results-card .results');
            var data = {
                storeID: selectedStore.val(),
                searchRadius: $('#radius').val(),
                searchPostalCode: $('.results').data('search-key').postalCode,
                storeDetailsHtml: selectedStore.siblings('label').find('.store-details').html(),
                event: e,
                storeInfo: selectedStore.data('store-info')
            };
            $('body').trigger('store:selected', data);
            $('.bypass-ffldealer-selection').addClass('display-none');

            // GTM - event
            require('gtm/gtm/gtm').selectFFL();
            if (window.location.hash === '#ffldealer') {
                window.location.hash = '';
            }
        }));
    },
    updateSelectStoreButton: base.updateSelectStoreButton,

    selectStoreWithInventory: function () {
        $('body').on('store:selected', function (e, data) {
            var url = $('.storeID').data('action-url');
            var requestData = {};
            requestData.storeID = data.storeID;

            $.ajax({
                url: url,
                data: requestData,
                method: 'GET',
                success: function () {
                    $('.store-locator-container .btn-storelocator-search[type="button"]').trigger('click');
                }
            });
        });
    },

    showMoreStores: function () {
        $('body').on('click', '.show-more-stores', function () {
            $(this).closest('.results.striped').find('.card-body').removeClass('show-more');
            $(this).addClass('d-none');
        });
    },

    ajaxCompleteFunction: function () {
        $(document).ajaxComplete(function () {
            if ($('input.select-store-input').length > 0) {
                $('input.select-store-input:first').trigger('click');
                $('input.select-store-input:first').closest('.card-body').addClass('background-color');
            }
            if (window.matchMedia('(min-width: 1024px)').matches && $('.results.striped').length > 0) {
                var resultsDiv = '.perfect-scrollbar';
                var perfectScrollbar = new PerfectScrollbar(resultsDiv);
                perfectScrollbar.update();
            }
        });
    },

    storeSelectedInPage: function () {
        $('body').on('change', '.select-store-input', function () {
            $('.card-body').removeClass('background-color');
            if ($(this).is(':checked')) {
                $(this).closest('.card-body').addClass('background-color');
            }
        });
    }
};
