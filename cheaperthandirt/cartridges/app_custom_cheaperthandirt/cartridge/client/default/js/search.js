'use strict';

var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./search/search'));
    processInclude(require('./product/quickView'));
    processInclude(require('./recommendation'));
    processInclude(require('./product/wishlistHeart'));
    processInclude(require('custom/search/searchcustom'));
    processInclude(require('./product/pdpInstoreInventory'));
});
