'use strict';

module.exports = function () {
    $('.home-banner-slide').slick({
        dots: true,
        infinite: false,
        arrows: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: 'ease-in-out'
    });

    $('.rangeday-friday .image-section .row').slick({
        dots: false,
        infinite: false,
        arrows: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        cssEase: 'ease-in-out',
        prevArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-prev"></span>',
        nextArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-next"></span>',
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: true,
                arrows: false
            }
        },
        {
            breakpoint: 769,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: false,
                centerMode: true
            }
        }
        ]
    });
};
