window.jQuery = window.$ = require('jquery');
window.slick = window.Slick = require('slick-carousel/slick/slick');
var processInclude = require('base/util');

$(document).ready(function () {
    processInclude(require('./components/menu'));
    processInclude(require('base/components/cookie'));
    processInclude(require('./components/footer'));
    // base > wishlist
    processInclude(require('./components/miniCart'));
    processInclude(require('base/components/collapsibleItem'));
    processInclude(require('./components/search'));
    processInclude(require('./components/clientSideValidation'));
    processInclude(require('base/components/countrySelector'));
    processInclude(require('./formFields/formFields'));
});

require('base/thirdParty/bootstrap');
require('base/components/spinner');
