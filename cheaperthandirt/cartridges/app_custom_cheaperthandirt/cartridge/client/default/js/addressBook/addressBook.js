'use strict';

var base = require('base/addressBook/addressBook');
var formFields = require('../formFields/formFields');
var formValidation = require('base/components/formValidation');
var upsAddressBook = require('ups/addressBook/addressBook');

/**
 * this is a call back function
 */
function updateAddressSuggestion() {
    $('body').on('address:updateAddressSuggestion', function (e, data) {
        var suggestedAddressArray = data.suggestedAddressArray;
        var $form = $(this).find('form.address-form');
        var url = $form.attr('action');
        if (url === 'default') {
            $('#adddress-suggestion .modal-body').remove();
            $('#adddress-suggestion').remove();
            $form.spinner().stop();
        }
        var radioButton = $('.modal-dialog .modal-body').find('input:radio[name="option"]:checked');

        if (radioButton.length === 1) {
            var arrayIndex = radioButton.data('count');
            if (arrayIndex !== 'default') {
                $form.find('input[name$="_address1"]').val(suggestedAddressArray[arrayIndex].address1);
                $form.find('input[name$="_address2"]').val(suggestedAddressArray[arrayIndex].address2);
                $form.find('input[name$="_city"]').val(suggestedAddressArray[arrayIndex].city);
                $form.find('select[name$="_country"]').val(suggestedAddressArray[arrayIndex].countryCode);
                $form.find('input[name$="_postalCode"]').val(suggestedAddressArray[arrayIndex].postalCode);
                $form.find('select[name$="_stateCode"]').val(suggestedAddressArray[arrayIndex].stateCode);
                formFields.updateSelect();
            }

            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: $form.serialize(),
                success: function (res) {
                    $form.spinner().stop();
                    if (!res.success) {
                        formValidation($form, res);
                    } else {
                        location.href = res.redirectUrl;
                    }
                },
                error: function (err) {
                    $form.spinner().stop();
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    }
                }
            });
        }
    });
}

base.submitAddress = upsAddressBook.submitAddress;
base.updateAddressSuggestion = updateAddressSuggestion;
module.exports = base;
