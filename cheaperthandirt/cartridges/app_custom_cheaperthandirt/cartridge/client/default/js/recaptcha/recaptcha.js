'use strict';

module.exports = function (action, $form, callback) {
    /* eslint-disable */
    grecaptcha.ready(function() {
        var reCAPTCHA_site_key = $form.find('input[name="recaptcha_site_key"]').val();
        grecaptcha.execute(reCAPTCHA_site_key, {action: action}).then(function(token) {
            callback(token, $form);
        });
    });
    /* eslint-enable */
};
