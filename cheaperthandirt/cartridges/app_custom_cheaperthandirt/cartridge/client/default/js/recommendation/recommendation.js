'use strict';

/**
 * Global recommendation slickContent
 */
function recommendationSlick() {
    var slidesToShow;
    if ($('.container').hasClass('cat-inner-container') || $('.container').hasClass('AR-15-build-page') || $('.container').hasClass('cart-page') || $('.container').hasClass('cart-empty')) {
        slidesToShow = 4;
    } else if ($('.container').hasClass('product-detail')) {
        slidesToShow = 5;
    } else if ($('.container').hasClass('ar15-history-container')) {
        slidesToShow = 3;
    } else {
        slidesToShow = 6;
    }

    $('.homepage-product-listing').slick({
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: slidesToShow,
        slidesToScroll: 1,
        cssEase: 'ease-in-out',
        arrows: true,
        rows: 0,
        prevArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-prev"></span>',
        nextArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-next"></span>',
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
                dots: true,
                arrows: false
            }
        },
        {
            breakpoint: 769,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: true,
                arrows: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: true,
                arrows: false
            }
        }
        ]
    }).on('setPosition', function () {
        $('.homepage-product-listing').find('.slick-slide').height('auto');

        var slickTrack = $('.homepage-product-listing').find('.slick-track');
        var slickTrackHeight = $(slickTrack).height();
        $('.homepage-product-listing').find('.slick-slide').css('height', slickTrackHeight + 'px');
    });
}

/**
 * Global recommendation slickContent for Last visiting section
 */
function recommendationSlickLast() {
    $('.product-last-visiting').slick({
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        cssEase: 'ease-in-out',
        arrows: true,
        rows: 0,
        prevArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-prev"></span>',
        nextArrow: '<span class="svg-04-Caro_Chevron_Right_-_inactive svg-04-Caro_Chevron_Right_-_inactive-dims d-inline-block slick-next"></span>',
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
                dots: true,
                arrows: false
            }
        },
        {
            breakpoint: 769,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                dots: true,
                arrows: false
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: true,
                arrows: false
            }
        }
        ]
    }).on('setPosition', function () {
        $('.product-last-visiting').find('.slick-slide').height('auto');

        var slickTrack = $('.product-last-visiting').find('.slick-track');
        var slickTrackHeight = $(slickTrack).height();
        $('.product-last-visiting').find('.slick-slide').css('height', slickTrackHeight + 'px');
    });
}

module.exports = {
    recommendationSlick: recommendationSlick,
    recommendationSlickLast: recommendationSlickLast
};
