'use strict';

/**
 * Decorate product with product tile information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} productType - Product type information
 *
 * @returns {Object} - Decorated product model
 */
var base = module.superModule;
var PromotionMgr = require('dw/campaign/PromotionMgr');

module.exports = function productTile(product, apiProduct, productType) {
    base.call(this, product, apiProduct, productType);
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var categoryName = '';
    if (product.category !== null && product.category !== undefined) {
        var category = CatalogMgr.getCategory(product.category);
        categoryName = category.getDisplayName();
    }
    Object.defineProperty(product, 'categoryName', {
        value: categoryName
    });

    Object.defineProperty(product, 'tag', {
        enumerable: true,
        value: apiProduct.custom.tag
    });
    Object.defineProperty(product, 'promotions', {
        value: PromotionMgr.activeCustomerPromotions.getProductPromotions(apiProduct)
    });
    return product;
};
