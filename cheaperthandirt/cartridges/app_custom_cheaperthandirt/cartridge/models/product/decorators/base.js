'use strict';

var baseSuper = module.superModule;

/**
 * Decorate product with product tile information
 * @param {dw.catalog.Product} product - Product information returned by the script API
 * @returns {string} defaultVariantId - the default variant id for the product
 */
function getDefaultVariant(product) {
    var defaultVariant;
    if (product.master) {
        if (product.variationModel.defaultVariant) {
            defaultVariant = product.variationModel.defaultVariant;
        } else {
            defaultVariant = product.variationModel.variants[0];
        }
    } else {
        defaultVariant = product;
    }
    return defaultVariant;
}

/**
 * Decorate product with product tile information
 * @param {Object} object - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {string} type - Product type information
 */
function base(object, apiProduct, type) {
    baseSuper.call(this, object, apiProduct, type);

    Object.defineProperty(object, 'manufacturerName', {
        value: apiProduct.getManufacturerSKU() ? apiProduct.getManufacturerSKU() : '',
        enumerable: true
    });

    Object.defineProperty(object, 'UPC', {
        value: apiProduct.getUPC() ? apiProduct.getUPC() : '',
        enumerable: true
    });

    Object.defineProperty(object, 'isFirearm', {
        value: 'availableForInStorePickup' in getDefaultVariant(apiProduct).custom ? getDefaultVariant(apiProduct).custom.availableForInStorePickup : false,
        enumerable: true
    });

    Object.defineProperty(object, 'videoURL', {
        value: 'videoStorage' in apiProduct.custom ? apiProduct.custom.videoStorage : null,
        enumerable: true
    });
    Object.defineProperty(object, 'videoTitle', {
        value: 'videoTitle' in apiProduct.custom ? apiProduct.custom.videoTitle : null,
        enumerable: true
    });
    Object.defineProperty(object, 'giftcardProduct', {
        value: 'giftcardProduct' in apiProduct.custom ? apiProduct.custom.giftcardProduct : null,
        enumerable: true
    });
    Object.defineProperty(object, 'giftcardType', {
        value: 'giftcardType' in apiProduct.custom ? apiProduct.custom.giftcardType : null,
        enumerable: true
    });
    Object.defineProperty(object, 'masterProductID', {
        value: apiProduct.variant ? apiProduct.variationModel.master.ID : apiProduct.ID,
        enumerable: true
    });
    Object.defineProperty(object, 'onlineFromDate', {
        value: apiProduct.onlineFrom ? apiProduct.onlineFrom : null,
        enumerable: true
    });
    Object.defineProperty(object, 'defaultVariantID', {
        value: getDefaultVariant(apiProduct).ID,
        enumerable: true
    });
    Object.defineProperty(object, 'restrictGroup', {
        value: {
            prodRestrictedGroup: apiProduct.custom.prodRestrictedGroup,
            restrictItemZip: apiProduct.custom.restrictItemZip,
            restrictItemState: apiProduct.custom.restrictItemState,
            restrictItemShip: apiProduct.custom.restrictItemShip,
            restrictItemFOID: apiProduct.custom.restrictItemFOID,
            restrictItemGroup: apiProduct.custom.restrictItemGroup
        },
        enumerable: true
    });
    Object.defineProperty(object, 'starRating', {
        value: 'StarRatingBase' in apiProduct.custom ? apiProduct.custom.StarRatingBase : 0,
        enumerable: true
    });
    Object.defineProperty(object, 'ratingCount', {
        value: 'StarRatingCount' in apiProduct.custom ? apiProduct.custom.StarRatingCount : 0,
        enumerable: true
    });
    Object.defineProperty(object, 'category', {
    	value: apiProduct.isVariant() ? apiProduct.getMasterProduct().getPrimaryCategory() !== null ? apiProduct.getMasterProduct().getPrimaryCategory().getID(): apiProduct.getPrimaryCategory() !== null ?apiProduct.getPrimaryCategory().getID():'':apiProduct.getPrimaryCategory() !== null ?apiProduct.getPrimaryCategory().getID():'', // eslint-disable-line
        enumerable: true
    });
    Object.defineProperty(object, 'isMaster', {
        value: !apiProduct.isVariant(),
        enumerable: true
    });
}

module.exports = base;
