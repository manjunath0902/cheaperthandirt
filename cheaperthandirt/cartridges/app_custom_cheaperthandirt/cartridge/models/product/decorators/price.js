'use strict';
var priceFactory = require('*/cartridge/scripts/factories/price');

/**
 * Convert price to an object
 * @param {dw.value.Money} price - Price object returned from the API
 * @param {string} currency - currency code for the current price model
 * @returns {Object} price formatted as a simple object
 */
function toPriceModel(price, currency) {
    return {
        value: price,
        currency: currency
    };
}

/**
 * Calculate savings per product with percentage
 * @param {dw.catalog.Product|dw.catalog.productSearchHit} product - API object for a product
 * @param {boolean} useSimplePrice - Flag as to whether a simple price should be used, used for
 *     product tiles and cart line items.
 * @param {dw.util.Collection<dw.campaign.Promotion>} promotions - Promotions that apply to this
 *                                                                 product
 * @param {dw.catalog.ProductOptionModel} currentOptions - The product's option model
 * @return {TieredPrice|RangePrice|DefaultPrice} - The product's price
 */
function calculateSaving(product, useSimplePrice, promotions, currentOptions) {
    var price = priceFactory.getPrice(product, null, useSimplePrice, promotions, currentOptions);
    var salesPrice;
    var listPrice;

    if (price.sales && price.sales.value != null) {
        salesPrice = parseFloat(price.sales.value);
    }

    if (price.list && price.list.value != null) {
        listPrice = parseFloat(price.list.value);
    }

    if (salesPrice != null && listPrice != null && listPrice !== salesPrice) {
        price.savings = toPriceModel(listPrice - salesPrice, price.sales.currency);
        price.savePercentage = ((price.savings.value / listPrice) * 100).toFixed();
    }

    return price;
}

module.exports = function (object, product, promotions, useSimplePrice, currentOptions) {
    Object.defineProperty(object, 'price', {
        enumerable: true,
        value: calculateSaving(product, useSimplePrice, promotions, currentOptions)
    });
};
