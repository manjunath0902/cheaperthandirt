'use strict';
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var StringUtils = require('dw/util/StringUtils');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var File = require('dw/io/File');
var Status = require('dw/system/Status');
var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');
var Calendar = require('dw/util/Calendar');

/**
 * Reads the Custom Objects and prepares a txt file for the BISN notifications.
 * @returns {status} status - Status of the job
 */
function sendNotifications() {
    var timeStamp = StringUtils.formatCalendar(new Calendar(), "yyyy-MM-dd'T'HH:mm:ss'Z'");
    var bisnObjects = CustomObjectMgr.getAllCustomObjects('Back-In-Stock-Notifications');
    var params = arguments[0];
    var path = '/src/Back-in-Stock-Notifications';
    /*
     * If no parameters are configured on the step, return Error
     */
    if (params == null || params.ImpexLocation == null) {
        path = params.ImpexLocation;
    }
    var fileSystem = new File(File.IMPEX + path);
    if (!fileSystem.exists()) {
        fileSystem.mkdir();
    }
    var file = new File(fileSystem.getFullPath() + '/Back-in-Stock-Notifications_' + timeStamp + '.txt');
    var fileWriter = new FileWriter(file);
    var csvWriter = new CSVStreamWriter(fileWriter, '	');
    var currentObject;
    var JSONString = [];
    JSONString[0] = 'Email';
    JSONString[1] = 'PartNumber';
    JSONString[2] = 'MasterProductID';
    csvWriter.writeNext(JSONString);
    try {
        Transaction.wrap(function () {
            while (bisnObjects.hasNext()) {
                currentObject = bisnObjects.next();
                if (currentObject.custom.emailID != null && currentObject.custom.partNumber != null && currentObject.custom.masterProductID != null) {
                    JSONString[0] = currentObject.custom.emailID;
                    JSONString[1] = currentObject.custom.partNumber;
                    JSONString[2] = currentObject.custom.masterProductID;
                    csvWriter.writeNext(JSONString);
                    CustomObjectMgr.remove(currentObject);
                }
            }
        });
    } catch (e) {
        var errorMsg = e.fileName + '| line#:' + e.lineNumber + '| Message:' + e.message + '| Stack:' + e.stack;
        Logger.error(errorMsg);
    } finally {
        fileWriter.flush();
        csvWriter.close();
        fileWriter.close();
    }
    return new Status(Status.OK);
}

exports.SendNotifications = sendNotifications;
