'use strict';

var base = module.superModule;
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var Resource = require('dw/web/Resource');
var ShippingHelpers = require('*/cartridge/scripts/checkout/shippingHelpers');

/**
 * @constructor
 * @classdesc CartModel class that represents the current basket
 *
 * @param {dw.order.Basket} basket - Current users's basket
 */
function CartModel(basket) {
    base.call(this, basket);
    var shipToHomeShipmentObj;
    if (basket) {
        this.onlyStoreShipment = COHelpers.onlyStoreShipment(basket.getShipments());
        var shipToHomeShipment = COHelpers.getShipToHomeShipment(basket);
        if (shipToHomeShipment !== null && shipToHomeShipment !== undefined) {
            var shippingModel = ShippingHelpers.getShippingModel(shipToHomeShipment, null, 'basket');
            if (shippingModel) {
                var result = {};
                result.shippingMethods = shippingModel.applicableShippingMethods;
                if (shippingModel.selectedShippingMethod) {
                    result.selectedShippingMethod = shippingModel.selectedShippingMethod.ID;
                }
                shipToHomeShipmentObj = result;
            }
        }
        if (shipToHomeShipmentObj !== null && shipToHomeShipmentObj !== undefined) {
            this.shipToHomeShipments = shipToHomeShipmentObj;
        }
    }
    var label = this.numItems === 1 ? 'label.number.items.in.cart.single' : 'label.number.items.in.cart';
    this.resources = {
        numberOfItems: Resource.msgf(label, 'cart', null, this.numItems),
        emptyCartMsg: Resource.msg('info.cart.empty.msg', 'cart', null)
    };
}

CartModel.prototype = Object.create(base.prototype);

module.exports = CartModel;
