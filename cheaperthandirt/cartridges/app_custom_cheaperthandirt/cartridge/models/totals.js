'use strict';
var base = module.superModule;
var formatMoney = require('dw/util/StringUtils').formatMoney;


/**
 * Accepts a total object and formats the value
 * @param {dw.value.Money} total - Total price of the cart
 * @returns {string} the formatted money value
 */
function getTotals(total) {
    return !total.available ? '-' : formatMoney(total);
}


/**
 * @constructor
 * @classdesc totals class that represents the order totals of the current line item container
 *
 * @param {dw.order.lineItemContainer} lineItemContainer - The current user's line item container
 */
function totals(lineItemContainer) {
    base.call(this, lineItemContainer);
    this.orderTotalValue = lineItemContainer.totalGrossPrice.value;
    var collections = require('*/cartridge/scripts/util/collections');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var ShippingMgr = require('dw/order/ShippingMgr');
    var hasFirearm = false;
    this.hasBulkFee = false;
    var shipments = lineItemContainer.getShipments();
    var Money = require('dw/value/Money');
    var bulkFee = new Money(0.0, lineItemContainer.getCurrencyCode());
    var firearmFee = getTotals(new Money(0.0, lineItemContainer.getCurrencyCode()));
    var totalShippingCost = getTotals(new Money(0.0, lineItemContainer.getCurrencyCode()));

    collections.forEach(shipments, function (shipment) {
        var shippingMethod = shipment.getShippingMethod();
        if (shippingMethod !== null && !shippingMethod.custom.enable_ship_rate_ups) {
            var ShipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment);
            var shippingCost = ShipmentShippingModel.getShippingCost(shipment.getShippingMethod()).amount;
            var adjustedShippingTotal = shipment.adjustedShippingTotalPrice;
            var shipmentBulkFee = adjustedShippingTotal.subtract(shippingCost);
            bulkFee = bulkFee.add(shipmentBulkFee);
            if (shipment.custom.shipmentType === 'instore') {
                hasFirearm = true;
                firearmFee = getTotals(shippingCost);
            } else {
                totalShippingCost = getTotals(shippingCost);
            }
        } else {
            totalShippingCost = getTotals(shipment.shippingTotalNetPrice);
        }
    });
    this.hasFirearm = hasFirearm;
    this.firearmHandlingFee = firearmFee;
    this.totalShippingCost = totalShippingCost;
    if (bulkFee.value > 0) {
        this.hasBulkFee = true;
        this.bulkFee = getTotals(bulkFee);
    }
    var totalExcludingOrderDiscount = lineItemContainer.getAdjustedMerchandizeTotalPrice(false);
    var totalIncludingOrderDiscount = lineItemContainer.getAdjustedMerchandizeTotalPrice(true);
    var orderDiscount = totalExcludingOrderDiscount.subtract(totalIncludingOrderDiscount);

    var totalExcludingShippingDiscount = lineItemContainer.shippingTotalPrice;
    var totalIncludingShippingDiscount = lineItemContainer.adjustedShippingTotalPrice;
    var shippingDiscount = totalExcludingShippingDiscount.subtract(totalIncludingShippingDiscount);

    var totalDiscount = orderDiscount.add(shippingDiscount);
    this.totalDiscount = totalDiscount.value;
    this.totalDiscountFormatted = formatMoney(totalDiscount);

    //    get the updated final total
    var giftCardTotals = COHelpers.getRemainingTotal(lineItemContainer);
    if (giftCardTotals) {
        this.grandTotal = getTotals(giftCardTotals);
    }
}

module.exports = totals;
