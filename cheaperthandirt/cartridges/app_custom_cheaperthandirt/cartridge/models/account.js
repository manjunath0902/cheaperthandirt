'use strict';

var base = module.superModule;

var AddressModel = require('*/cartridge/models/address');
var URLUtils = require('dw/web/URLUtils');
var StringUtils = require('dw/util/StringUtils');
/**
 * Creates an array of plain object that contains address book addresses, if any exist
 * @param {dw.customer.Customer} addressBook - target customer
 * @returns {Array<Object>} an array of customer addresses
 */
function getAddresses(addressBook) {
    var result = [];
    if (addressBook) {
        for (var i = 0, ii = addressBook.addresses.length; i < ii; i++) {
            result.push(new AddressModel(addressBook.addresses[i]).address);
        }
    }

    return result;
}

/**
 * Creates a plain object that contains the customer's preferred address
 * @param {dw.customer.Customer} addressBook - target customer
 * @returns {Object} an object that contains information about current customer's preferred address
 */
function getPreferredAddress(addressBook) {
    var result = null;
    if (addressBook && addressBook.preferredAddress) {
        result = new AddressModel(addressBook.preferredAddress).address;
    }

    return result;
}
/**
 * Creates a plain object that contains payment instrument information
 * @param {Object} wallet - current customer's wallet
 * @returns {Object} object that contains info about the current customer's payment instrument
 */
function getPayment(wallet) {
    if (wallet) {
        var paymentInstruments = wallet.paymentInstruments;
        var paymentInstrument = paymentInstruments[0];

        if (paymentInstrument) {
            var result = {
                maskedCreditCardNumber: paymentInstrument.maskedCreditCardNumber,
                creditCardType: paymentInstrument.creditCardType,
                creditCardExpirationMonth: StringUtils.formatNumber(paymentInstrument.creditCardExpirationMonth, '00'),
                creditCardExpirationYear: paymentInstrument.creditCardExpirationYear
            };
            result.cardTypeImage = {
                src: URLUtils.staticURL('/images/' +
                    paymentInstrument.creditCardType.toLowerCase().replace(/\s/g, '') +
                    '-dark.svg'),
                alt: paymentInstrument.creditCardType
            };
            return result;
        }
    }
    return null;
}

/**
 * Creates a plain object that contains payment instrument information
 * @param {Object} userPaymentInstruments - current customer's paymentInstruments
 * @returns {Object} object that contains info about the current customer's payment instruments
 */
function getCustomerPaymentInstruments(userPaymentInstruments) {
    var paymentInstruments;

    paymentInstruments = userPaymentInstruments.map(function (paymentInstrument) {
        var result = {
            creditCardHolder: paymentInstrument.creditCardHolder,
            maskedCreditCardNumber: paymentInstrument.maskedCreditCardNumber,
            creditCardType: paymentInstrument.creditCardType,
            creditCardExpirationMonth: StringUtils.formatNumber(paymentInstrument.creditCardExpirationMonth, '00'),
            creditCardExpirationYear: paymentInstrument.creditCardExpirationYear,
            UUID: paymentInstrument.UUID
        };

        result.cardTypeImage = {
            src: URLUtils.staticURL('/images/' +
                paymentInstrument.creditCardType.toLowerCase().replace(/\s/g, '') +
                '-dark.svg'),
            alt: paymentInstrument.creditCardType
        };

        return result;
    });

    return paymentInstruments;
}

/**
 * Account class that represents the current customer's profile dashboard
 * @param {dw.customer.Customer} currentCustomer - Current customer
 * @param {Object} addressModel - The current customer's preferred address
 * @param {Object} orderModel - The current customer's order history
 * @constructor
 */
function account(currentCustomer, addressModel, orderModel) {
    base.call(this, currentCustomer, addressModel, orderModel);
    var CustomerModel = require('*/cartridge/models/customer.js');
    var customerObj = new CustomerModel(currentCustomer.raw);
    this.addresses = getAddresses(customerObj.addressBook);
    this.preferredAddress = addressModel || getPreferredAddress(customerObj.addressBook);
    this.payment = getPayment(currentCustomer.wallet);
}

account.getCustomerPaymentInstruments = getCustomerPaymentInstruments;

module.exports = account;
