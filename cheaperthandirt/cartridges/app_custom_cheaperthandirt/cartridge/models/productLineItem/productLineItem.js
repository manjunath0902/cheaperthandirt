'use strict';

var base = module.superModule;

/**
 * Decorate product with product line item information
 * @param {Object} product - Product Model to be decorated
 * @param {dw.catalog.Product} apiProduct - Product information returned by the script API
 * @param {Object} options - Options passed in from the factory
 * @returns {Object} - Decorated product model
 */
function productLineItem(product, apiProduct, options) {
    base.call(this, product, apiProduct, options);
    var CatalogMgr = require('dw/catalog/CatalogMgr');
    var lineItem = options.lineItem;
    Object.defineProperty(product, 'giftcard', {
        value: {
            giftCardRecipientEmail: lineItem.custom.giftCardRecipientEmail,
            giftCardRecipient: lineItem.custom.giftCardRecipient
        }
    });
    var categoryName = '';
    if (product.category !== null && product.category !== undefined) {
        var category = CatalogMgr.getCategory(product.category);
        categoryName = category.getDisplayName();
    }
    Object.defineProperty(product, 'categoryName', {
        value: categoryName
    });
    return product;
}

module.exports = productLineItem;
