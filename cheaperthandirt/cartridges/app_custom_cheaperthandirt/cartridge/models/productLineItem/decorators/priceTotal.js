'use strict';

var formatMoney = require('dw/util/StringUtils').formatMoney;

var collections = require('*/cartridge/scripts/util/collections');
var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

/**
 * Adds discounts to a discounts object
 * @param {dw.util.Collection} collection - a collection of price adjustments
 * @param {Object} discounts - an object of price adjustments
 * @returns {Object} an object of price adjustments
 */
function createDiscountObject(collection, discounts) {
    var result = discounts;
    collections.forEach(collection, function (item) {
        if (!item.basedOnCoupon) {
            result[item.UUID] = {
                UUID: item.UUID,
                lineItemText: item.lineItemText,
                price: formatMoney(item.price),
                type: 'promotion',
                callOutMsg: item.promotion.calloutMsg
            };
        }
    });

    return result;
}

/**
 * creates an array of discounts.
 * @param {dw.order.LineItemCtnr} lineItem - the current line item container
 * @returns {Array} an array of objects containing promotion and coupon information
 */
function getDiscounts(lineItem) {
    var discounts = {};

    collections.forEach(lineItem.priceAdjustments, function (priceAdjustment) {
        var couponLineItem = priceAdjustment.couponLineItem;
        if (couponLineItem !== null && couponLineItem !== undefined) {
            var priceAdjustments = collections.map(
                couponLineItem.priceAdjustments, function (adjustment) {
                    return { callOutMsg: adjustment.promotion.calloutMsg };
                });
            discounts[couponLineItem.UUID] = {
                type: 'coupon',
                UUID: couponLineItem.UUID,
                couponCode: couponLineItem.couponCode,
                applied: couponLineItem.applied,
                valid: couponLineItem.valid,
                relationship: priceAdjustments
            };
        }
    });

    discounts = createDiscountObject(lineItem.priceAdjustments, discounts);

    return Object.keys(discounts).map(function (key) {
        return discounts[key];
    });
}

/**
 * get the total price for the product line item
 * @param {dw.order.ProductLineItem} lineItem - API ProductLineItem instance
 * @returns {Object} an object containing the product line item total info.
 */
function getTotalPrice(lineItem) {
    var context;
    var price;
    var result = {};
    var template = 'checkout/productCard/productCardProductRenderedTotalPrice';

    if (lineItem.priceAdjustments.getLength() > 0) {
        result.nonAdjustedPrice = formatMoney(lineItem.getPrice());

        result.discounts = getDiscounts(lineItem);
    }

    price = lineItem.adjustedPrice;

    // The platform does not include prices for selected option values in a line item product's
    // price by default.  So, we must add the option price to get the correct line item total price.
    collections.forEach(lineItem.optionProductLineItems, function (item) {
        price = price.add(item.adjustedNetPrice);
    });

    result.price = formatMoney(price);
    result.priceValue = price.getValueOrNull();
    context = { lineItem: { priceTotal: result } };

    result.renderedPrice = renderTemplateHelper.getRenderedHtml(context, template);
    result.productLevelDiscountTotal = formatMoney(lineItem.getPrice().subtract(price));

    return result;
}


module.exports = function (object, lineItem) {
    Object.defineProperty(object, 'priceTotal', {
        enumerable: true,
        value: getTotalPrice(lineItem)
    });
};
