'use strict';

var seoHelper = require('*/cartridge/scripts/helpers/seoHelper');

/**
 * Get the breadcrumb JSON object
 * @param {Array} breadcrumbs - the array of breadcrumbs
 * @return {Object} pageBreadCrumbs - The constructed array of JSON objects of breadcrumbs
 */
function getSeoBreadCrumbs(breadcrumbs) {
    var pageBreadCrumbs = null;
    if (breadcrumbs != null) {
        pageBreadCrumbs = seoHelper.getBreadcrumbs(breadcrumbs);
    }
    return pageBreadCrumbs;
}

/**
 * Get the product details JSON object for SEO
 * @param {Object} seoproduct - The created Product object
 * @param {Object} params - The request parameters for the page.
 * @return {Object} productdetails - The constructed array of JSON objects of product details.
 */
function getProductSeoDetails(seoproduct, params) {
    var productdetails = null;
    if (seoproduct != null) {
        productdetails = seoHelper.getProductDetails(seoproduct, params);
    }
    return productdetails;
}

/**
 * Get the video details JSON object for SEO
 * @return {Object} videoSeodetails - The constructed JSON object of AR15 build videos.
 */
function getBuildVideoSeoDetails() {
    var videoSeoDetails = null;
    videoSeoDetails = seoHelper.getVideoDetails();
    return videoSeoDetails;
}

exports.getSeoBreadCrumbs = getSeoBreadCrumbs;
exports.getProductSeoDetails = getProductSeoDetails;
exports.getBuildVideoSeoDetails = getBuildVideoSeoDetails;
