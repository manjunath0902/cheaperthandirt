'use strict';
var base = module.superModule;
/**
 * Address class that represents an orderAddress
 * @param {dw.order.OrderAddress} addressObject - User's address
 * @constructor
 */
function address(addressObject) {
    base.call(this, addressObject);
    if (this.address && addressObject) {
        this.address.UUID = addressObject.UUID;
    }
}

module.exports = address;
