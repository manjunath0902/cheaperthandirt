'use strict';

var collections = require('*/cartridge/scripts/util/collections');
var urlHelper = require('*/cartridge/scripts/helpers/urlHelpers');
var Site = require('dw/system/Site');

var ACTION_ENDPOINT = 'Search-UpdateGrid';
var currentsite = Site.getCurrent();


/**
 * Retrieves sorting options
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search instance
 * @param {dw.util.List.<dw.catalog.SortingOption>} sortingOptions - List of sorting rule options
 * @param {dw.web.PagingModel} pagingModel - The paging model for the current search context
 * @return {SortingOption} - Sorting option
 */
function getSortingOptions(productSearch, sortingOptions, pagingModel) {
    return collections.map(sortingOptions, function (option) {
        var baseUrl = productSearch.urlSortingRule(ACTION_ENDPOINT, option.sortingRule);
        var pagingParams = {
            start: '0',
            sz: pagingModel.end + 1
        };
        return {
            displayName: option.displayName,
            id: option.ID,
            url: urlHelper.appendQueryParams(baseUrl.toString(), pagingParams).toString()
        };
    });
}

/**
 * Retrieves refined or default category sort ID
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search instance
 * @param {dw.catalog.Category} rootCategory - Catalog's root category
 * @return {string} - Sort rule ID
 */
function getSortRuleDefault(productSearch, rootCategory) {
    var category = productSearch.category ? productSearch.category : rootCategory;
    return category.defaultSortingRule.ID;
}

/**
 * Removes the globally excluded sorting options
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search instance
 * @param {dw.util.List} sortOptions - Catalog sorting options
 * @return {dw.util.List} - List of filtered Sorting Options
 */
function getFilteredSortOptions(productSearch, sortOptions) {
    var globalExcludeSortOptions = currentsite.getCustomPreferenceValue('globalExcludeSortOptions');
    var ArrayList = require('dw/util/ArrayList');
    this.filteredSortOptions = new ArrayList();
    try {
        collections.forEach(sortOptions, function (option) {
            if (globalExcludeSortOptions.indexOf(option.ID) < 0) {
                this.filteredSortOptions.push(option);
            } else if ((globalExcludeSortOptions.indexOf(option.ID) >= 0) && ('overrideGlobalSortExclusions' in productSearch.category.custom && productSearch.category.custom.overrideGlobalSortExclusions.indexOf(option.ID) >= 0)) {
                this.filteredSortOptions.push(option);
            }
        }, this);
    } catch (e) {
        return sortOptions;
    }
    return this.filteredSortOptions;
}


/**
 * @constructor
 * @classdesc Model that encapsulates product sort options
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search instance
 * @param {string|null} sortingRuleId - HTTP Param srule value
 * @param {dw.util.List.<dw.catalog.SortingOption>} sortingOptions - Sorting rule options
 * @param {dw.catalog.Category} rootCategory - Catalog's root category
 * @param {dw.web.PagingModel} pagingModel - The paging model for the current search context
 */
function ProductSortOptions(
    productSearch,
    sortingRuleId,
    sortingOptions,
    rootCategory,
    pagingModel
) {
    var filteredSortOptions = getFilteredSortOptions(productSearch, sortingOptions);
    this.options = getSortingOptions(productSearch, filteredSortOptions, pagingModel);
    this.ruleId = sortingRuleId || getSortRuleDefault(productSearch, rootCategory);
}

module.exports = ProductSortOptions;
