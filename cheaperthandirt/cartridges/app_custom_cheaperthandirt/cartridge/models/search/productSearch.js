'use strict';


var base = module.superModule;
var collections = require('*/cartridge/scripts/util/collections');
var searchRefinementsFactory = require('*/cartridge/scripts/factories/searchRefinements');
var ContentMgr = require('dw/content/ContentMgr');
var URLUtils = require('dw/web/URLUtils');
var DEFAULT_PAGE_SIZE = 12;
var PAGE_LOAD_SIZE = 24;
var ACTION_ENDPOINT = 'Search-Show';

/**
 * Configures and returns a PagingModel instance
 *
 * @param {dw.util.Iterator} productHits - Iterator for product search results
 * @param {number} count - Number of products in search results
 * @param {number} pageSize - Number of products to display
 * @param {number} startIndex - Beginning index value
 * @return {dw.web.PagingModel} - PagingModel instance
 */
function getPagingModel(productHits, count, pageSize, startIndex) {
    var PagingModel = require('dw/web/PagingModel');
    var paging = new PagingModel(productHits, count);

    paging.setStart(startIndex || 0);
    paging.setPageSize(pageSize || DEFAULT_PAGE_SIZE);

    return paging;
}

/**
 * Generates URL for [Show] More button
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {Object} httpParams - HTTP query parameters
 * @return {string} - More button URL
 */
function getShowMoreUrl(productSearch, httpParams) {
    var showMoreEndpoint = 'Search-UpdateGrid';
    var currentStart = httpParams.start || 0;
    var pageSize = httpParams.sz || DEFAULT_PAGE_SIZE;
    var hitsCount = productSearch.count;
    var nextStart;

    var paging = getPagingModel(
        productSearch.productSearchHits,
        hitsCount,
        DEFAULT_PAGE_SIZE,
        currentStart
    );

    if (pageSize >= hitsCount) {
        return '';
    } else if (pageSize > DEFAULT_PAGE_SIZE) {
        nextStart = pageSize;
    } else {
        var endIdx = paging.getEnd();
        nextStart = endIdx + 1 < hitsCount ? endIdx + 1 : null;

        if (!nextStart) {
            return '';
        }
    }

    paging.setStart(nextStart);

    var baseUrl = productSearch.url(showMoreEndpoint);
    var finalUrl = paging.appendPaging(baseUrl);
    return finalUrl;
}

/**
 * Generates URL that removes refinements, essentially resetting search criteria
 *
 * @param {dw.catalog.ProductSearchModel} search - Product search object
 * @param {Object} httpParams - Query params
 * @param {string} [httpParams.q] - Search keywords
 * @param {string} [httpParams.cgid] - Category ID
 * @return {string} - URL to reset query to original search
 */
function getResetLink(search, httpParams) {
    return search.categorySearch
        ? URLUtils.abs(ACTION_ENDPOINT, 'cgid', httpParams.cgid)
        : URLUtils.abs(ACTION_ENDPOINT, 'q', httpParams.q);
}

/**
 * Retrieves banner image URL
 *
 * @param {dw.catalog.Category} category - Subject category
 * @return {string} - Banner's image URL
 */
function getBannerImageUrl(category) {
    var url = null;

    if (category.custom && 'slotBannerImage' in category.custom &&
        category.custom.slotBannerImage) {
        url = category.custom.slotBannerImage.getURL();
    } else if (category.image) {
        url = category.image.getURL();
    }

    return url;
}

/**
 * Retrieves next prev URL
 *
 * @param {dw.web.PagingModel} pagingModel - pagingModel object
 * @param {dw.catalog.Category} category - Subject category
 * @return {Object} links - JSON object for prev & next links
 */
function getRelLinks(pagingModel, category) {
    var current = pagingModel.start;
    var pageSize = pagingModel.pageSize;
    var pageURL = URLUtils.url('Search-Show', 'cgid', category.ID);
    var currentPage = pagingModel.currentPage;
    var maxPage = pagingModel.maxPage;
    var links = {};
    links.prevURL = null;
    links.nextURL = null;
    if (maxPage >= 1) {
        if (currentPage > 0) {
            links.prevURL = decodeURI(pagingModel.appendPaging(pageURL, current - pageSize));
        }
        if (currentPage !== maxPage) {
            links.nextURL = decodeURI(pagingModel.appendPaging(pageURL, current + pageSize));
        }
    }
    return links;
}
/* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["httpParams"] }]*/
/**
 * @constructor
 * @classdesc ProductSearch class
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {Object} httpParams - HTTP query parameters
 * @param {string} sortingRule - Sorting option rule ID
 * @param {dw.util.ArrayList.<dw.catalog.SortingOption>} sortingOptions - Options to sort search
 *     results
 * @param {dw.catalog.Category} rootCategory - Search result's root category if applicable
 */
function ProductSearch(productSearch, httpParams, sortingRule, sortingOptions, rootCategory) {
    var Site = require('dw/system/Site');
    var currentsite = Site.getCurrent();
    var searchBannerPath = currentsite.getCustomPreferenceValue('searchBannerPath');

    /* On page load, we are displaying 24 products. If sz is null, it means it is first page load, so we are setting its value to 24.
     * If sz is not null, it means the load more button has been clicked. In this case, the value will be 12, since we are showing 12 products on load more.
     */
    if (httpParams.sz == null) {
        httpParams.sz = PAGE_LOAD_SIZE;
    }

    base.call(this, productSearch, httpParams, sortingRule, sortingOptions, rootCategory);

    var currentStart = httpParams.start || 0;

    this.contextObject = productSearch.category;
    this.contentAssetID = productSearch.category != null && 'categoryPromoContentId' in productSearch.category.custom ? productSearch.category.custom.categoryPromoContentId : null;
    this.contentAssetBody = (this.contentAssetID != null) ? ContentMgr.getContent(this.contentAssetID) : null;

    if (searchBannerPath && searchBannerPath !== null) {
        var searchBannerUrl = URLUtils.absStatic(URLUtils.CONTEXT_LIBRARY, null, searchBannerPath);
        this.bannerImageUrl = productSearch.category ? getBannerImageUrl(productSearch.category) : searchBannerUrl;
    }

    var pagingModel = getPagingModel(
        productSearch.productSearchHits,
        productSearch.count,
        httpParams.sz,
        currentStart
    );
    if (pagingModel && productSearch.category) {
        this.links = getRelLinks(pagingModel, productSearch.category);
    }
    this.resetLink = getResetLink(productSearch, httpParams);
    this.productDisplayCount = pagingModel.getEnd() + 1;
    this.showMoreUrl = getShowMoreUrl(productSearch, httpParams);
}

/**
 * Retrieves search refinements
 *
 * @param {dw.catalog.ProductSearchModel} productSearch - Product search object
 * @param {dw.catalog.ProductSearchRefinements} refinements - Search refinements
 * @param {ArrayList.<dw.catalog.ProductSearchRefinementDefinition>} refinementDefinitions - List of
 *     product serach refinement definitions
 * @return {Refinement[]} - List of parsed refinements
 */
function getRefinements(productSearch, refinements, refinementDefinitions) {
    return collections.map(refinementDefinitions, function (definition) {
        var refinementValues = refinements.getAllRefinementValues(definition);
        var values = searchRefinementsFactory.get(productSearch, definition, refinementValues);

        return {
            displayName: definition.displayName,
            isCategoryRefinement: definition.categoryRefinement,
            isAttributeRefinement: definition.attributeRefinement,
            isPriceRefinement: definition.priceRefinement,
            values: values
        };
    });
}

/**
 * Returns the refinement values that have been selected
 *
 * @param {Array.<CategoryRefinementValue|AttributeRefinementValue|PriceRefinementValue>}
 *     refinements - List of all relevant refinements for this search
 * @return {Object[]} - List of selected filters
 */
function getSelectedFilters(refinements) {
    var selectedFilters = [];
    var selectedValues = [];

    refinements.forEach(function (refinement) {
        selectedValues = refinement.values.filter(function (value) {
            return value.selected;
        });
        if (selectedValues.length) {
            selectedFilters.push.apply(selectedFilters, selectedValues);
        }
    });

    return selectedFilters;
}

// We are copying line 16 to line 92 since the objects extended using Oject.definePorperty arent available inside for loops
Object.defineProperty(ProductSearch.prototype, 'refinements', {
    get: function () {
        if (!this.cachedRefinements) {
            this.cachedRefinements = getRefinements(
                this.productSearch,
                this.productSearch.refinements,
                this.productSearch.refinements.refinementDefinitions
            );
        }

        return this.cachedRefinements;
    }
});

Object.defineProperty(ProductSearch.prototype, 'selectedFilters', {
    get: function () {
        return getSelectedFilters(this.refinements);
    }
});

Object.defineProperty(ProductSearch.prototype, 'lengthofRefinements', {
    get: function () {
        return getSelectedFilters(this.refinements).length;
    }
});

module.exports = ProductSearch;
