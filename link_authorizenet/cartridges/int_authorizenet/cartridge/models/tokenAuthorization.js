'use strict';

var authorizeServiceHelper = require('*/cartridge/scripts/helpers/authorizeServiceHelper.js');
var authorizeService = require('*/cartridge/scripts/init/authorizeService.js');
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');
var enableLoggers = Site.getCurrent().getCustomPreferenceValue('addAuthLoggers');


/**
 * @constructor
 * @classdesc TokenAuthorization class
 * @param {dw.order.Order} order - Product search object
 * @param {request} req - request object
 * @param {paymentInstrument} paymentInstrument - paymentInstrument object
 */
function TokenAuthorization(order, req, paymentInstrument) {
    var tokenAuthorizeService = authorizeService.get();
    var paymentObj = authorizeServiceHelper.getPaymentCardDetails(paymentInstrument);
    var customer = req.currentCustomer.raw;

    var requestObj = {};
    requestObj.createTransactionRequest = {};
    requestObj.createTransactionRequest.merchantAuthentication = authorizeServiceHelper.getMerchantAuthentication();

    requestObj.createTransactionRequest.refId = order.orderNo;

    requestObj.createTransactionRequest.transactionRequest = {};
    requestObj.createTransactionRequest.transactionRequest.transactionType = authorizeServiceHelper.getTransactionType('authorize');

    requestObj.createTransactionRequest.transactionRequest.amount = paymentObj.amount;

    requestObj.createTransactionRequest.transactionRequest.profile = authorizeServiceHelper.getPaymentProfileDetails(paymentInstrument, customer);

    requestObj.createTransactionRequest.transactionRequest.order = authorizeServiceHelper.getOrderInfo(order);

    requestObj.createTransactionRequest.transactionRequest.lineItems = {};
    requestObj.createTransactionRequest.transactionRequest.lineItems.lineItem = authorizeServiceHelper.getProductLineItems(order.getProductLineItems());

    requestObj.createTransactionRequest.transactionRequest.tax = authorizeServiceHelper.getTaxDetails(order);
    // we are not sending the duty charges
    // requestObj.createTransactionRequest.transactionRequest.duty = authorizeServiceHelper.getDutyDetails(order);

    requestObj.createTransactionRequest.transactionRequest.shipping = authorizeServiceHelper.getShippingDetails(order);

    requestObj.createTransactionRequest.transactionRequest.poNumber = authorizeServiceHelper.getShippingZipCode(order);

    requestObj.createTransactionRequest.transactionRequest.customer = {};
    requestObj.createTransactionRequest.transactionRequest.customer.id = customer.authenticated ? 'SF' + customer.profile.customerNo : customer.ID.substring(0, 5);

    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var shipToHomeShipment = COHelpers.getShipToHomeShipment(order);
    if (shipToHomeShipment === null || shipToHomeShipment === undefined) {
        shipToHomeShipment = order.defaultShipment;
    }

    requestObj.createTransactionRequest.transactionRequest.shipTo = authorizeServiceHelper.getAddress(shipToHomeShipment.shippingAddress);


    requestObj.createTransactionRequest.transactionRequest.customerIP = req.remoteAddress;

    var responseObj = tokenAuthorizeService.call(requestObj);

    try {
        if (enableLoggers) {
            Transaction.wrap(function () {
                var cardNumber = requestObj.createTransactionRequest.transactionRequest.profile.paymentProfile.paymentProfileId;
                if (cardNumber !== null) {
                    cardNumber = '************' + cardNumber.substring(cardNumber.length - 4);
                    requestObj.createTransactionRequest.transactionRequest.profile.paymentProfile.paymentProfileId = cardNumber;
                }
                order.custom.authRequest = JSON.stringify(requestObj); // eslint-disable-line
                var responseString = responseObj.object.text;

                order.custom.authResponse = responseString; // eslint-disable-line
            });
        }
    } catch (e) {
        var errorMsg = e.fileName + "| line#:"+ e.lineNumber + "| Message:" + e.message+ "| Stack:" + e.stack;
        Transaction.wrap(function () {
            order.custom.authErrorLog = errorMsg;
        });
    }

    var result = authorizeServiceHelper.handleServiceResponse(responseObj);

    if (!result.error) {
        if (result.transactionResponse.responseCode === '2' || result.transactionResponse.responseCode === '3') {
            result.error = true;
            result.message = authorizeServiceHelper.getRejectMsg();
            return result;
        }
        if (result.transactionResponse.responseCode === '1' || result.transactionResponse.responseCode === '4') {
            result.error = false;
            Transaction.wrap(function () {
                authorizeServiceHelper.updateTransactionRefOnPI(result, paymentInstrument);
            });
            // Validate CVV 2 codes
            if (authorizeServiceHelper.validateCVV2Codes(result.transactionResponse.cvvResultCode)) { 
                result.error = true;
                result.message = authorizeServiceHelper.getRejectMsg();
            }
        }
    }
    return result;
}

module.exports = TokenAuthorization;
