'use strict';

var authorizeServiceHelper = require('*/cartridge/scripts/helpers/authorizeServiceHelper.js');
var authorizeService = require('*/cartridge/scripts/init/authorizeService.js');

/**
 * @constructor
 * @classdesc VoidAuthorization class
 * @param {dw.order.Order} order - Product search object
 * @param {paymentInstrument} paymentInstrument - paymentInstrument object
 */
function VoidAuthorization(order, paymentInstrument) {
    var voidAuthorizationService = authorizeService.get();
    var requestObj = {};
    requestObj.createTransactionRequest = {};
    requestObj.createTransactionRequest.merchantAuthentication = authorizeServiceHelper.getMerchantAuthentication();

    requestObj.createTransactionRequest.refId = order.orderNo;

    requestObj.createTransactionRequest.transactionRequest = {};
    requestObj.createTransactionRequest.transactionRequest.transactionType = authorizeServiceHelper.getTransactionType('void');
    requestObj.createTransactionRequest.transactionRequest.refTransId = paymentInstrument.paymentTransaction.custom.transId;

    var responseObj = voidAuthorizationService.call(requestObj);

    var result = authorizeServiceHelper.handleServiceResponse(responseObj);
    return result;
}

module.exports = VoidAuthorization;
