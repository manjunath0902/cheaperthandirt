'use strict';

var authorizeServiceHelper = require('*/cartridge/scripts/helpers/authorizeServiceHelper.js');
var authorizeService = require('*/cartridge/scripts/init/authorizeService.js');
var Transaction = require('dw/system/Transaction');
var Site = require('dw/system/Site');
var enableLoggers = Site.getCurrent().getCustomPreferenceValue('addAuthLoggers');

/**
 * @constructor
 * @classdesc AuthorizeCreditCard class
 * @param {dw.order.Order} order - Product search object
 * @param {request} req - request object
 * @param {paymentInstrument} paymentInstrument - paymentInstrument object
 */
function AuthorizeCreditCard(order, req, paymentInstrument) {
    var authorizeCreditCardService = authorizeService.get();
    var paymentObj = authorizeServiceHelper.getPaymentCardDetails(paymentInstrument);
    var customer = req.currentCustomer.raw;

    var requestObj = {};
    requestObj.createTransactionRequest = {};
    requestObj.createTransactionRequest.merchantAuthentication = authorizeServiceHelper.getMerchantAuthentication();

    requestObj.createTransactionRequest.refId = order.orderNo;

    requestObj.createTransactionRequest.transactionRequest = {};
    requestObj.createTransactionRequest.transactionRequest.transactionType = authorizeServiceHelper.getTransactionType('authorize');

    requestObj.createTransactionRequest.transactionRequest.amount = paymentObj.amount;
    requestObj.createTransactionRequest.transactionRequest.payment = paymentObj.payment;

    requestObj.createTransactionRequest.transactionRequest.profile = {};
    requestObj.createTransactionRequest.transactionRequest.profile.createProfile = customer.authenticated;

    requestObj.createTransactionRequest.transactionRequest.order = authorizeServiceHelper.getOrderInfo(order);

    requestObj.createTransactionRequest.transactionRequest.lineItems = {};
    requestObj.createTransactionRequest.transactionRequest.lineItems.lineItem = authorizeServiceHelper.getProductLineItems(order.getProductLineItems());

    requestObj.createTransactionRequest.transactionRequest.tax = authorizeServiceHelper.getTaxDetails(order);
    // we are not sending duty charges
    // requestObj.createTransactionRequest.transactionRequest.duty = authorizeServiceHelper.getDutyDetails(order);

    requestObj.createTransactionRequest.transactionRequest.shipping = authorizeServiceHelper.getShippingDetails(order);

    requestObj.createTransactionRequest.transactionRequest.poNumber = authorizeServiceHelper.getShippingZipCode(order);

    requestObj.createTransactionRequest.transactionRequest.customer = {};
    requestObj.createTransactionRequest.transactionRequest.customer.id = customer.ID.substring(0, 5);

    requestObj.createTransactionRequest.transactionRequest.billTo = authorizeServiceHelper.getAddress(order.getBillingAddress());

    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var shipToHomeShipment = COHelpers.getShipToHomeShipment(order);
    if (shipToHomeShipment === null || shipToHomeShipment === undefined) {
        shipToHomeShipment = order.defaultShipment;
    }
    requestObj.createTransactionRequest.transactionRequest.shipTo = authorizeServiceHelper.getAddress(shipToHomeShipment.getShippingAddress());

    requestObj.createTransactionRequest.transactionRequest.customerIP = req.remoteAddress;

    var responseObj = authorizeCreditCardService.call(requestObj);
    try {
        if (enableLoggers) {
            Transaction.wrap(function () {
                var cardNumber = requestObj.createTransactionRequest.transactionRequest.payment.creditCard.cardNumber;
                if (cardNumber !== null && cardNumber !== undefined) {
                    cardNumber = '************' + cardNumber.substring(cardNumber.length - 4);
                    requestObj.createTransactionRequest.transactionRequest.payment.creditCard.cardNumber = cardNumber;
                }
                order.custom.authRequest = JSON.stringify(requestObj); // eslint-disable-line
                order.custom.authResponse = responseObj.object.text; // eslint-disable-line
            });
        }
    } catch(e) {
        var errorMsg = e.fileName + "| line#:"+ e.lineNumber + "| Message:" + e.message+ "| Stack:" + e.stack;
        Transaction.wrap(function () {
            order.custom.authErrorLog = errorMsg;
        });
    }
    var result = authorizeServiceHelper.handleServiceResponse(responseObj);
    // Approved response codes vs failure response codes

    if (!result.error) {
        if (result.transactionResponse.responseCode === '2' || result.transactionResponse.responseCode === '3') {
            result.error = true;
            result.message = authorizeServiceHelper.getRejectMsg();
            return result;
        }
        if (result.transactionResponse.responseCode === '1' || result.transactionResponse.responseCode === '4') {
            result.error = false;
            Transaction.wrap(function () {
                authorizeServiceHelper.updateTransactionRefOnPI(result, paymentInstrument);
            });
            if (authorizeServiceHelper.validateCVV2Codes(result.transactionResponse.cvvResultCode)) {
                result.error = true;
                result.message = authorizeServiceHelper.getRejectMsg();
            }
        }
    }
    return result;
}

module.exports = AuthorizeCreditCard;
