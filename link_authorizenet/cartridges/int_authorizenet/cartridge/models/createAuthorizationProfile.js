'use strict';

var authorizeServiceHelper = require('*/cartridge/scripts/helpers/authorizeServiceHelper.js');
var authorizeService = require('*/cartridge/scripts/init/authorizeService.js');

/**
 * @constructor
 * @classdesc CreateAuthorizationProfile class
 * @param {dw.customer.Customer} customer - Native Customer Object
 * @param {Object} formInfo - Form Data for the payment
 * @return {Object} result - Profile creation response
 */
function CreateAuthorizationProfile(customer, formInfo) {
    var createAuthorizationProfileService = authorizeService.get();
    var requestObj = {};

    requestObj.createCustomerProfileRequest = {};
    requestObj.createCustomerProfileRequest.merchantAuthentication = authorizeServiceHelper.getMerchantAuthentication();

    requestObj.createCustomerProfileRequest.profile = {};
    requestObj.createCustomerProfileRequest.profile = authorizeServiceHelper.getProfileDetails(customer, formInfo);

    requestObj.createCustomerProfileRequest.validationMode = authorizeServiceHelper.getValidateMode();

    var responseObj = createAuthorizationProfileService.call(requestObj);

    var result = authorizeServiceHelper.handleServiceResponse(responseObj);
    return result;
}

module.exports = CreateAuthorizationProfile;
