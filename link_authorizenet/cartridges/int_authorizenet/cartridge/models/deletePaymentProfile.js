'use strict';

var authorizeServiceHelper = require('*/cartridge/scripts/helpers/authorizeServiceHelper.js');
var authorizeService = require('*/cartridge/scripts/init/authorizeService.js');

/**
 * @constructor
 * @classdesc DeletePaymentProfile class
 * @param {Object} params - Product search object
 * @return {Object} response - Delete Payment Profile response
 */
function DeletePaymentProfile(params) {
    var deletePaymentProfileService = authorizeService.get();
    var requestObj = {};
    requestObj.deleteCustomerPaymentProfileRequest = {};
    requestObj.deleteCustomerPaymentProfileRequest.merchantAuthentication = authorizeServiceHelper.getMerchantAuthentication();

    requestObj.deleteCustomerPaymentProfileRequest.customerProfileId = params.authorizeProfileID;
    requestObj.deleteCustomerPaymentProfileRequest.customerPaymentProfileId = params.paymentID;

    var responseObj = deletePaymentProfileService.call(requestObj);

    var result = authorizeServiceHelper.handleServiceResponse(responseObj);
    return result;
}

module.exports = DeletePaymentProfile;
