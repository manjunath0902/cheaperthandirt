'use strict';

var authorizeServiceHelper = require('*/cartridge/scripts/helpers/authorizeServiceHelper.js');
var authorizeService = require('*/cartridge/scripts/init/authorizeService.js');

/**
 * @constructor
 * @classdesc CreatePaymentProfile class
 * @param {dw.customer.Customer} customer - Native Customer Object
 * @param {Object} formInfo - Form Data for the payment
 * @param {string} authorizeProfileID - Authorize.net Profile ID
 * @return {Object} result - Profile Payment creation response
 */
function CreatePaymentProfile(customer, formInfo, authorizeProfileID) {
    var createPaymentProfileService = authorizeService.get();
    var requestObj = {};
    requestObj.createCustomerPaymentProfileRequest = {};

    requestObj.createCustomerPaymentProfileRequest.merchantAuthentication = authorizeServiceHelper.getMerchantAuthentication();
    requestObj.createCustomerPaymentProfileRequest.customerProfileId = authorizeProfileID;

    requestObj.createCustomerPaymentProfileRequest.paymentProfile = {};

    requestObj.createCustomerPaymentProfileRequest.paymentProfile.payment = {};

    var creditCard = authorizeServiceHelper.getProfileDetails(customer, formInfo).paymentProfiles.payment.creditCard;

    requestObj.createCustomerPaymentProfileRequest.paymentProfile.payment.creditCard = creditCard;
    requestObj.createCustomerPaymentProfileRequest.validationMode = authorizeServiceHelper.getValidateMode();

    var responseObj = createPaymentProfileService.call(requestObj);

    var result = authorizeServiceHelper.handleServiceResponse(responseObj);
    return result;
}


module.exports = CreatePaymentProfile;
