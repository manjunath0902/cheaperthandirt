'use strict';

var collections = require('*/cartridge/scripts/util/collections');

var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var PaymentStatusCodes = require('dw/order/PaymentStatusCodes');
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');

var AuthorizeCreditCard = require('*/cartridge/models/authorizeCreditCard.js');
var VoidAuthorization = require('*/cartridge/models/voidAuthorization.js');

/**
 * Creates a token. This should be replaced by utilizing a tokenization provider
 * @param {dw.customer.Customer} customer - Native Customer object
 * @param {Object} formInfo - Payment form for the user input details
 * @returns {Object} tokenResponse - Returns the token response
 */
function createToken(customer, formInfo) {
    var tokenResponse;
    if (customer.authenticated) {
        var CreateAuthorizationProfile = require('*/cartridge/models/createAuthorizationProfile.js');
        var CreatePaymentProfile = require('*/cartridge/models/createPaymentProfile.js');
        var authorizeProfileID = customer.getProfile().custom.authorizeProfileID;
        if (authorizeProfileID === null) {
            tokenResponse = new CreateAuthorizationProfile(customer, formInfo);
            if (!tokenResponse.error) {
                tokenResponse.customerPaymentProfileId = tokenResponse.customerPaymentProfileIdList[0];
                Transaction.wrap(function () {
                    customer.getProfile().custom.authorizeProfileID = tokenResponse.customerProfileId; // eslint-disable-line
                });
            }
        } else {
            tokenResponse = new CreatePaymentProfile(customer, formInfo, authorizeProfileID);
        }
    }
    return tokenResponse;
}

/**
 * Verifies that entered credit card information is a valid card. If the information is valid a
 * credit card payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {Object} paymentInformation - the payment information
 * @return {Object} returns an error object
 */
function Handle(basket, paymentInformation) {
    var currentBasket = basket;
    var cardErrors = {};
    var cardNumber = paymentInformation.cardNumber.value;
    var cardSecurityCode = paymentInformation.securityCode.value;
    var expirationMonth = paymentInformation.expirationMonth.value;
    var expirationYear = paymentInformation.expirationYear.value;
    var serverErrors = [];
    var creditCardStatus;

    var cardType = paymentInformation.cardType.value;
    var paymentCard = PaymentMgr.getPaymentCard(cardType);

    if (!paymentInformation.creditCardToken) {
        if (paymentCard) {
            creditCardStatus = paymentCard.verify(
                expirationMonth,
                expirationYear,
                cardNumber,
                cardSecurityCode
            );
        } else {
            cardErrors[paymentInformation.cardNumber.htmlName] =
                Resource.msg('error.invalid.card.number', 'creditCard', null);

            return {
                fieldErrors: [cardErrors],
                serverErrors: serverErrors,
                error: true
            };
        }

        if (creditCardStatus.error) {
            collections.forEach(creditCardStatus.items, function (item) {
                switch (item.code) {
                    case PaymentStatusCodes.CREDITCARD_INVALID_CARD_NUMBER:
                        cardErrors[paymentInformation.cardNumber.htmlName] =
                            Resource.msg('error.invalid.card.number', 'creditCard', null);
                        break;

                    case PaymentStatusCodes.CREDITCARD_INVALID_EXPIRATION_DATE:
                        cardErrors[paymentInformation.expirationMonth.htmlName] =
                            Resource.msg('error.expired.credit.card', 'creditCard', null);
                        cardErrors[paymentInformation.expirationYear.htmlName] =
                            Resource.msg('error.expired.credit.card', 'creditCard', null);
                        break;

                    case PaymentStatusCodes.CREDITCARD_INVALID_SECURITY_CODE:
                        cardErrors[paymentInformation.securityCode.htmlName] =
                            Resource.msg('error.invalid.security.code', 'creditCard', null);
                        break;
                    default:
                        serverErrors.push(
                            Resource.msg('error.card.information.error', 'creditCard', null)
                        );
                }
            });

            return {
                fieldErrors: [cardErrors],
                serverErrors: serverErrors,
                error: true
            };
        }
    }

    Transaction.wrap(function () {
        var paymentInstruments = currentBasket.getPaymentInstruments(
            PaymentInstrument.METHOD_CREDIT_CARD
        );

        collections.forEach(paymentInstruments, function (item) {
            currentBasket.removePaymentInstrument(item);
        });

        var paymentInstrument = currentBasket.createPaymentInstrument(
            PaymentInstrument.METHOD_CREDIT_CARD, currentBasket.totalGrossPrice
        );

        paymentInstrument.setCreditCardHolder(currentBasket.billingAddress.fullName);
        paymentInstrument.setCreditCardNumber(cardNumber);
        paymentInstrument.setCreditCardType(cardType);
        paymentInstrument.setCreditCardExpirationMonth(expirationMonth);
        paymentInstrument.setCreditCardExpirationYear(expirationYear);
        if (paymentInformation.creditCardToken) {
            paymentInstrument.setCreditCardToken(paymentInformation.creditCardToken);
        }
    });

    return {
        fieldErrors: cardErrors,
        serverErrors: serverErrors,
        error: false
    };
}


/**
 * Authorizes a payment using a credit card. Customizations may use other processors and custom
 *      logic to authorize credit card payment.
 * @param {dw.order.order} order - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @param {request} req - The current r
 * @return {Object} returns an error object
 */
function Authorize(order, paymentInstrument, paymentProcessor, req) {
    var errorMessage;
    var fieldErrors = {};
    var error = false;
    var TokenAuthorization = require('*/cartridge/models/tokenAuthorization.js');
    var authorizeServiceHelper = require('*/cartridge/scripts/helpers/authorizeServiceHelper.js');
    try {
        var authResponse;
        if (paymentInstrument.getCreditCardToken()) {
            authResponse = new TokenAuthorization(order, req, paymentInstrument);
        } else {
            authResponse = new AuthorizeCreditCard(order, req, paymentInstrument);
        }

        Transaction.wrap(function () {
            paymentInstrument.paymentTransaction.setTransactionID(order.orderNo);
            paymentInstrument.paymentTransaction.setPaymentProcessor(paymentProcessor);
        });
        error = authResponse.error;
        errorMessage = authorizeServiceHelper.getRejectMsg();
        Transaction.wrap(function () {
            order.custom.authErrorLog = authResponse.message !== undefined ? authResponse.message : authResponse.responseMsg
        });
    } catch (e) {
        var errorMsg = e.fileName + "| line#:"+ e.lineNumber + "| Message:" + e.message+ "| Stack:" + e.stack;
        Transaction.wrap(function () {
            order.custom.authErrorLog = errorMsg;
        });
        error = true;
        errorMessage = authorizeServiceHelper.getTechnicalErrorMsg();
    }

    return {
        fieldErrors: fieldErrors,
        errorMessage: errorMessage,
        error: error
    };
}


/**
 * Voids a payment using a credit card. Customizations may use other processors and custom
 *      logic to authorize credit card payment.
 * @param {dw.order.order} order - The current order's number
 * @param {dw.order.PaymentInstrument} paymentInstrument -  The payment instrument to authorize
 * @param {dw.order.PaymentProcessor} paymentProcessor -  The payment processor of the current
 *      payment method
 * @param {request} req - The current request
 * @return {Object} returns an error object
 */
function Void(order, paymentInstrument) {
    var fieldErrors = {};
    var serverErrors = [];
    var error = true;
    try {
        var voidResponse = new VoidAuthorization(order, paymentInstrument);
        error = voidResponse.error;
    } catch (e) {
        error = true;
        serverErrors.push(
            Resource.msg('error.technical', 'checkout', null)
        );
    }

    return {
        fieldErrors: fieldErrors,
        serverErrors: serverErrors,
        error: error
    };
}

exports.Handle = Handle;
exports.Authorize = Authorize;
exports.Void = Void;
exports.createToken = createToken;
