'use strict';

/**
 * Initialize  HTTP service
 */

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
 *
 * Authorize.net Payment Service - http Services
 *
 * @return {HTTPService} Authorize REST Service
 */
function authorizeService() {
    return LocalServiceRegistry.createService('int.post.authorizenetapi', {
        createRequest: function (svc, requestBody) {
            svc.setRequestMethod('POST');
            svc.addHeader('Content-Type', 'application/json');
            return JSON.stringify(requestBody);
        },

        parseResponse: function (service, response) {
            return response;
        }
    });
}

module.exports = {
    get: authorizeService
};
