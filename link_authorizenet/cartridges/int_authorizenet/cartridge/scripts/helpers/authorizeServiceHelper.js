/**
 * Description of the module and the logic it provides
 *
 * @module cartridge/scripts/helpers/authorizeServiceHelper
 */
'use strict';
var server = require('server');

var Site = require('dw/system/Site');
var collections = require('*/cartridge/scripts/util/collections');
var ShippingMgr = require('dw/order/ShippingMgr');
var StringUtils = require('dw/util/StringUtils');

/**
 * Creates the breadcrumbs object
 * @returns {json} json object which contains the Merchant Authentication details
 */
function getMerchantAuthentication() {
    var transactionKey = Site.getCurrent().getCustomPreferenceValue('authTransactionKey');
    var name = Site.getCurrent().getCustomPreferenceValue('authMerchantName');
    var merchantAuthenticationObj = {};
    if (transactionKey != null && name != null) {
        merchantAuthenticationObj.name = name;
        merchantAuthenticationObj.transactionKey = transactionKey;
    }
    return merchantAuthenticationObj;
}

/**
 * Return type of the transaction
 * @return {string} mode of validation
 */
function getValidateMode() {
    var mode = Site.getCurrent().getCustomPreferenceValue('authValidateMode');
    return mode;
}

/**
 * Return type of the transaction
 * @return {string} Reject Message
 */
function getRejectMsg() {
    var mode = Site.getCurrent().getCustomPreferenceValue('authRejectMsg');
    return mode;
}

/**
 * Return type of the transaction
 * @return {string} Technical Error Message
 */
function getTechnicalErrorMsg() {
    var mode = Site.getCurrent().getCustomPreferenceValue('authServiceTechError');
    return mode;
}

/**
 * Return type of the transaction
 * @return {string} authTrasactionType
 */
function getAuthTransactionType() {
    var authTrasactionType = Site.getCurrent().getCustomPreferenceValue('authServiceTransactionType');
    return authTrasactionType;
}

/**
 * Return type of the transaction
 * @return {Object} CC details object
 */
function getCreditCardNumber() {
    var card = {};
    var paymentForm = server.forms.getForm('billing');
    card.cNumber = paymentForm.creditCardFields.cardNumber.value;
    card.cardSecurityCode = paymentForm.creditCardFields.securityCode.value;
    return card;
}

/**
 * Return type of the transaction
 * @param  {string} transaction - transaction type
 * @return {string} transaction type accepted by Authorize.net
 */
function getTransactionType(transaction) {
    var result;
    if (transaction === 'authorize') {
        result = getAuthTransactionType(); // 'authOnlyTransaction';
    } else if (transaction === 'void') {
        result = 'voidTransaction';
    } else if (transaction === 'tokenAuthorize') {
        result = 'authCaptureTransaction';
    }
    return result;
}

/**
 * Return type of the transaction
 * @param  {Collection} lineitems - Current Basket LineItems
 * @return {Array} lineItemArr lineItemArr object accepted by Authorize.net
 */
function getProductLineItems(lineitems) {
    var lineItemArr = [];
    collections.forEach(lineitems, function (lineItem) {
        var lineItemJsonObj = {};
        lineItemJsonObj.itemId = lineItem.product.ID;
        lineItemJsonObj.name = lineItem.product.name.substring(0, 25);
        lineItemJsonObj.description = lineItem.product.name;
        lineItemJsonObj.quantity = lineItem.quantityValue;
        var unitPrice = lineItem.getAdjustedNetPrice().value / lineItemJsonObj.quantity;
        lineItemJsonObj.unitPrice = unitPrice.toFixed(2);
        lineItemArr.push(lineItemJsonObj);
    });
    return lineItemArr;
}

/**
 * Creates the Address object
 * @param  {Object} customerAddress - Native Address object
 * @returns {Object} BillingAddress - Customers Billing Address details
 */
function getAddress(customerAddress) {
    var address = {};
    address.firstName = customerAddress.firstName;
    address.lastName = customerAddress.lastName;
    address.company = customerAddress.companyName != null ? customerAddress.companyName : '';
    address.address = customerAddress.address1;
    address.city = customerAddress.city;
    address.state = customerAddress.stateCode;
    address.zip = customerAddress.postalCode;
    address.country = 'US';
    return address;
}

/**
 * Creates the Payment Card  object
 * @param  {PaymentInstrument} paymentInstrument - Native paymentInstrument object
 * @returns {Object} paymentInstrument - paymentInstrument details in the authorize.net accepted format
 */
function getPaymentCardDetails(paymentInstrument) {
    var result = {};
    var cardDetails = getCreditCardNumber();
    result.payment = {};
    result.payment.creditCard = {};
    result.payment.creditCard.cardNumber = cardDetails.cNumber;
    result.payment.creditCard.expirationDate = paymentInstrument.creditCardExpirationYear + '-' + StringUtils.formatNumber(paymentInstrument.creditCardExpirationMonth, '00');
    result.payment.creditCard.cardCode = cardDetails.cardSecurityCode;
    result.amount = paymentInstrument.paymentTransaction.getAmount().value;
    return result;
}

/**
 * Creates the Payment Card  based on the paymentProfileId
 * @param  {PaymentInstrument} paymentInstrument - Native paymentInstrument object
 * @param  {dw.customer.Customer} customer - Native Customer object
 * @returns {Object} payment - payment profile details in the authorize.net accepted format
 */
function getPaymentProfileDetails(paymentInstrument, customer) {
    var result = {};
    result.customerProfileId = customer.getProfile().custom.authorizeProfileID;
    result.paymentProfile = {};
    result.paymentProfile.paymentProfileId = paymentInstrument.getCreditCardToken();
    var cardDetails = getCreditCardNumber();
    if (cardDetails.cardSecurityCode !== undefined && cardDetails.cardSecurityCode !== null) {
        result.paymentProfile.cardCode = cardDetails.cardSecurityCode;
    }

    return result;
}

/**
 * Creates the order object
 * @param  {Object} order - Native Order object
 * @returns {Object} order - Authorize.net accepted format order object
 */
function getOrderInfo(order) {
    var orderObj = {};
    orderObj.invoiceNumber = order.orderNo;
    orderObj.description = 'Cheaper Than Dirt Order';
    return orderObj;
}

/**
 * Creates the tax details object
 * @param  {Object} order - Native Order object
 * @returns {Object} tax - Authorize.net accepted format tax object
 */
function getTaxDetails(order) {
    var tax = {};
    tax.amount = order.getTotalTax().value;
    tax.name = 'Tax amount';
    tax.description = 'Tax amount';
    return tax;
}

/**
 * Creates the shipping details object
 * @param  {Object} order - Native Order object
 * @returns {Object} shipping - Authorize.net accepted format shipping object
 */
function getShippingDetails(order) {
    var shipping = {};
    shipping.amount = order.adjustedShippingTotalPrice !== null ? order.adjustedShippingTotalPrice.value.toFixed(2) : 0.00;
    shipping.name = 'Shipping Charges';
    shipping.description = 'Shipping Charges';
    return shipping;
}

/**
 * Creates the shipping zipCode object
 * @param  {Object} order - Native Order object
 * @returns {Object} shippingZipCode - Authorize.net accepted format poNumber
 */
function getShippingZipCode(order) {
    var shipment = order.defaultShipment;
    var shippingAddress = shipment.getShippingAddress();
    var zipCode = shippingAddress.postalCode;
    return zipCode;
}


/**
 * Creates the shipping details object
 * @param  {Object} order - Native Order object
 * @returns {Object} shipping - Authorize.net accepted format shipping object
 */
function getDutyDetails(order) {
    var duty = {};
    var shipment = order.defaultShipment;
    var shippingMethod = shipment.getShippingMethod();
    var shippingCost = ShippingMgr.getShippingCost(shippingMethod, order.getTotalGrossPrice()).value;
    duty.amount = shippingCost;
    duty.name = 'Duty amount';
    duty.description = 'Duty amount ';
    return duty;
}

/**
 * Create the Profile Objects
 * @param  {dw.customer.Customer} Customer - Native Customer object
 * @param  {Form} formInfo - formInfo object
 * @returns {Object} profile - Authorize.net accepted format profile object
 */
function getProfileDetails(Customer, formInfo) {
    var profile = {};
    profile.merchantCustomerId = 'SF' + Customer.profile.customerNo;
    profile.description = '';
    profile.email = Customer.profile.email;
    profile.paymentProfiles = {};
    profile.paymentProfiles.customerType = 'individual';
    profile.paymentProfiles.payment = {};
    profile.paymentProfiles.payment.creditCard = {};
    profile.paymentProfiles.payment.creditCard.cardNumber = formInfo.cardNumber;
    var expirationDate = formInfo.expirationYear + '-' + StringUtils.formatNumber(formInfo.expirationMonth, '00');
    profile.paymentProfiles.payment.creditCard.expirationDate = expirationDate;
    return profile;
}

/**
 * Stores the auth response on the paymentInstrument
 * @param  {response} response - Authorize response object
 * @param  {PaymentInstrument} paymentInstrument - paymentInstrument object
 */
function updateTransactionRefOnPI(response, paymentInstrument) {
    var paymentTransaction = paymentInstrument.paymentTransaction;
    paymentTransaction.custom.responseCode = response.transactionResponse.responseCode;
    paymentTransaction.custom.authCode = response.transactionResponse.authCode;
    paymentTransaction.custom.avsResultCode = response.transactionResponse.avsResultCode;
    paymentTransaction.custom.cvvResultCode = response.transactionResponse.cvvResultCode;
    paymentTransaction.custom.cavvResultCode = response.transactionResponse.cavvResultCode;
    paymentTransaction.custom.transId = response.transactionResponse.transId;
    paymentTransaction.custom.refTransID = response.transactionResponse.refTransID;
    paymentTransaction.custom.testRequest = response.transactionResponse.testRequest;
    paymentTransaction.custom.accountNumber = response.transactionResponse.accountNumber;
    paymentTransaction.custom.accountType = response.transactionResponse.accountType;
    paymentTransaction.custom.messages = JSON.stringify(response.transactionResponse.messages);
    paymentTransaction.custom.transHashSha2 = response.transactionResponse.transHashSha2;
    paymentTransaction.custom.supplementalDataQualificationIndicator = response.transactionResponse.SupplementalDataQualificationIndicator;
}

/**
 * Handle the Authorize.net Service response
 * @param  {dw.svc.Result} response - Service response object
 * @returns {Object} result - Service JSON object
 */
function handleServiceResponse(response) {
    var result = {};
    if (response.status === 'SERVICE_UNAVAILABLE') {
        result.error = true;
        result.responseMsg = Site.getCurrent().getCustomPreferenceValue('authServiceUnavailable');
    } else if (response.error) {
        result.error = true;
        result.responseMsg = Site.getCurrent().getCustomPreferenceValue('authServiceTechError');
    } else if (response.ok) {
        var responseString = response.object.text;
        responseString = responseString.replace(/^\uFEFF/, '').trim();
        result = JSON.parse(responseString);
        result.error = false;
        if (result.messages.resultCode.toLowerCase() === 'error') {
            result.error = true;
            result.responseMsg = result.messages.message[0].text + 'Error Code: ' + result.messages.message[0].code;
            result.errorCode = result.messages.message[0].code;
        }
    }
    return result;
}


/**
 * Handle the CVV 2 validation
 * @param  {string} cvvResponse - Service response object
 * @returns {Object} result - Service JSON object
 */
function validateCVV2Codes(cvvResponse) {
    var validCVV2Codes = Site.getCurrent().getCustomPreferenceValue('authValidCVVCodes');
    var valid = false;
    if (validCVV2Codes !== null && cvvResponse !== null && validCVV2Codes.indexOf(cvvResponse) === -1) {
        valid = true;
    }
    return valid;
}

module.exports = {
    getDutyDetails: getDutyDetails,
    getShippingDetails: getShippingDetails,
    getTaxDetails: getTaxDetails,
    getOrderInfo: getOrderInfo,
    getPaymentCardDetails: getPaymentCardDetails,
    getAddress: getAddress,
    getProductLineItems: getProductLineItems,
    getTransactionType: getTransactionType,
    getShippingZipCode: getShippingZipCode,
    getMerchantAuthentication: getMerchantAuthentication,
    getProfileDetails: getProfileDetails,
    getPaymentProfileDetails: getPaymentProfileDetails,
    getValidateMode: getValidateMode,
    getRejectMsg: getRejectMsg,
    updateTransactionRefOnPI: updateTransactionRefOnPI,
    handleServiceResponse: handleServiceResponse,
    validateCVV2Codes: validateCVV2Codes,
    getTechnicalErrorMsg: getTechnicalErrorMsg
};
