'use strict';


var GiftCardService = require('*/cartridge/scripts/init/valuetecService.js');
var valuetecHelper = require('*/cartridge/scripts/helpers/valutecHelper.js');

/**
 * Verifies that entered credit card information is a valid card. If the information is valid a
 * credit card payment instrument is created
 * @param {string} cardNumber - the gift card number of the customer
 * @return {Object} returns an error object
 */
function checkBalance(cardNumber) {
    var Site = require('dw/system/Site');
    var giftCardResponse;
    try {
        var giftCardService = GiftCardService.get();
        var requestObj = valuetecHelper.getCardBalance(cardNumber);
        var result = giftCardService.call(requestObj);
        giftCardResponse = valuetecHelper.handleResponse(result);
    } catch (e) {
        giftCardResponse = {};
        giftCardResponse.error = true;
        giftCardResponse.responseMsg = Site.getCurrent().getCustomPreferenceValue('valueTecServiceTechError');
    }
    return giftCardResponse;
}


/**
 * Verifies that entered gift card information is a valid card. If the information is valid a
 * gift card payment instrument is created
 * @param {dw.order.Basket} basket Current users's basket
 * @param {Object} cardNumber - the gift card number entered by user
 * @return {Object} returns an error object
 */
function Handle(basket, cardNumber) {
    var currentBasket = basket;
    var arrayHelper = require('*/cartridge/scripts/util/array');
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var Transaction = require('dw/system/Transaction');
    var paymentInstruments = currentBasket.getPaymentInstruments();
    var result = {};
    result.error = true;

    var existingPaymentInstrument = arrayHelper.find(paymentInstruments, function (paymentInstrument) {
        return paymentInstrument.getPaymentTransaction().custom.giftCardNumber === cardNumber;
    });

    if (!existingPaymentInstrument) {
        var checkBalanceResponse = checkBalance(cardNumber);
        if (checkBalanceResponse.error) {
            result = checkBalanceResponse;
            result.type = 'CARD_NOT_FOUND';
        }
        if (!checkBalanceResponse.error && checkBalanceResponse.balance <= 0) {
            result.type = 'NO_BALANCE';
        } else if (!checkBalanceResponse.error) {
            var total = COHelpers.getNonGiftCardAmount(currentBasket, checkBalanceResponse.balance);
            if (total.giftCardAmountToApply > 0) {
                Transaction.wrap(function () {
                    var paymentInstrument = currentBasket.createPaymentInstrument(
                        'GiftCard', total.giftCardAmountToApply
                    );
                    var paymentTransaction = paymentInstrument.getPaymentTransaction();
                    paymentTransaction.custom.giftCardNumber = cardNumber;
                    result.error = false;
                    result.cardNumber = cardNumber;
                    result.type = 'APPLIED';
                    result.amountApplied = paymentInstrument.getPaymentTransaction().getAmount();
                });
            } else {
                result.type = 'NO_CARD_REQUIRED';
            }
        }
    } else {
        result.type = 'CARD_ALREADY_IN_CART';
    }
    return result;
}


exports.Handle = Handle;
exports.CheckBalance = checkBalance;
