'use strict';

var webreference = webreferences.Valuetec;  // eslint-disable-line
var Site = require('dw/system/Site');

/**
 * Creates the merchant details object
 * @returns {json} json object which contains the Merchant Authentication details
 */
function getMerchantDetails() {
    var obj = {};
    obj.clientKey = Site.getCurrent().getCustomPreferenceValue('valutecClientKey');
    obj.terminalID = Site.getCurrent().getCustomPreferenceValue('valutecTerminalID');
    return obj;
}

/**
 * Creates the breadcrumbs object
 * @param {string}cardNumber - The gift card number
 * @returns {transactionCardBalance} transactionCardBalance object which contains the card balance request
 */
function getCardBalance(cardNumber) {
    var transactionCardBalance = new webreference.Transaction_CardBalance();
    var merchDetails = getMerchantDetails();
    var ProgramType = webreference.ProgramType;
    var fromValue = ProgramType.fromValue('Gift');

    transactionCardBalance.setClientKey(merchDetails.clientKey);
    transactionCardBalance.setTerminalID(merchDetails.terminalID);
    transactionCardBalance.setProgramType(fromValue);
    transactionCardBalance.setCardNumber(cardNumber);

    return transactionCardBalance;
}
/**
 * Handle the Authorize.net Service response
 * @param  {dw.svc.Result} response - Service response object
 * @returns {Object} result - Service JSON object
 */
function handleResponse(response) {
    var result = {};
    if (response.status === 'SERVICE_UNAVAILABLE') {
        result.error = true;
        result.responseMsg = Site.getCurrent().getCustomPreferenceValue('valueTecServiceUnavailable');
    } else if (response.error) {
        result.error = true;
        result.responseMsg = Site.getCurrent().getCustomPreferenceValue('valueTecServiceTechError');
    } else if (response.ok) {
        var transactionCardResponse = response.object.transaction_CardBalanceResult;
        if (transactionCardResponse.errorMsg.length === 0) {
            result.balance = transactionCardResponse.balance;
            result.responseMsg = 'Available Balance: ';
            result.error = false;
        } else {
            result.responseMsg = transactionCardResponse.errorMsg;
            result.error = true;
        }
    }
    return result;
}


module.exports = {
    getCardBalance: getCardBalance,
    handleResponse: handleResponse
};
