'use strict';

var base = module.superModule;

/**
 * Sets the payment transaction amount
 * @param {dw.order.Basket} currentBasket - The current basket
 * @param {string} cardBalance - Card Balance of the Gift Card
 * @returns {Object} an error object
 */
function getNonGiftCardAmount(currentBasket, cardBalance) {
    var collections = require('*/cartridge/scripts/util/collections');
    var Money = require('dw/value/Money');
    var currencyCode = currentBasket.getCurrencyCode();
    var giftCardsTotal = new Money(0.0, currencyCode);
    var result = {};
    result.error = false;
    try {
        var paymentInstruments = currentBasket.getPaymentInstruments();
        collections.forEach(paymentInstruments, function (paymentInstrument) {
            if (paymentInstrument.getPaymentMethod() === 'GiftCard') {
                giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
            }
        });
        // Gets the order total.
        var orderTotal = currentBasket.totalGrossPrice;

        // Calculates the amount to charge for the payment instrument.
        // This is the remaining open order total that must be paid.
        var amountOpen = orderTotal.subtract(giftCardsTotal);

        if (amountOpen.value > cardBalance) {
            result.giftCardAmountToApply = new Money(cardBalance, currencyCode);
        } else {
            result.giftCardAmountToApply = amountOpen;
        }
    } catch (e) {
        result.error = true;
    }

    return result;
}

/**
 * Sets the payment transaction amount
 * @param {dw.order.LineItemContainer} lineItemContainer - The current basket
 * @param {Object} totals - The current basket totals
 * @param {Object} giftCards - The current basket gift cards
 * @returns {dw.value.Money}finalOrderTotal - Order Total
 */
function getFinalOrderTotal(lineItemContainer, totals, giftCards) {
    var formatMoney = require('dw/util/StringUtils').formatMoney;
    var Money = require('dw/value/Money');
    var finalOrderTotal;
    if (totals && giftCards) {
        if (giftCards.hasGC) {
            var totalsMinusGC = totals.orderTotalValue - giftCards.giftCardTotal.value;
            finalOrderTotal = formatMoney(new Money(totalsMinusGC, lineItemContainer.getCurrencyCode()));
        } else {
            finalOrderTotal = formatMoney(new Money(totals.orderTotalValue, lineItemContainer.getCurrencyCode()));
        }
    }
    return finalOrderTotal;
}


module.exports = {
    getFirstNonDefaultShipmentWithProductLineItems: base.getFirstNonDefaultShipmentWithProductLineItems,
    ensureNoEmptyShipments: base.ensureNoEmptyShipments,
    getProductLineItem: base.getProductLineItem,
    isShippingAddressInitialized: base.isShippingAddressInitialized,
    prepareShippingForm: base.prepareShippingForm,
    prepareBillingForm: base.prepareBillingForm,
    copyCustomerAddressToShipment: base.copyCustomerAddressToShipment,
    copyCustomerAddressToBilling: base.copyCustomerAddressToBilling,
    copyShippingAddressToShipment: base.copyShippingAddressToShipment,
    copyBillingAddressToBasket: base.copyBillingAddressToBasket,
    validateFields: base.validateFields,
    validateShippingForm: base.validateShippingForm,
    validateBillingForm: base.validateBillingForm,
    validatePayment: base.validatePayment,
    validateCreditCard: base.validateCreditCard,
    calculatePaymentTransaction: base.calculatePaymentTransaction,
    recalculateBasket: base.recalculateBasket,
    handlePayments: base.handlePayments,
    createOrder: base.createOrder,
    placeOrder: base.placeOrder,
    savePaymentInstrumentToWallet: base.savePaymentInstrumentToWallet,
    getRenderedPaymentInstruments: base.getRenderedPaymentInstruments,
    sendConfirmationEmail: base.sendConfirmationEmail,
    ensureValidShipments: base.ensureValidShipments,
    setGift: base.setGift,
    onlyStoreShipment: base.onlyStoreShipment,
    getNonGiftCardAmount: getNonGiftCardAmount,
    getFinalOrderTotal: getFinalOrderTotal,
    getShipToHomeShipment: base.getShipToHomeShipment,
    getShipToStoreShipment: base.getShipToStoreShipment,
    calculateUPSShipRate: base.calculateUPSShipRate
};

