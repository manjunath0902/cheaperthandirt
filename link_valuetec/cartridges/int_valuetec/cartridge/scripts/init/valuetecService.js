'use strict';

/**
 * Initialize  SOAP service
 */

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var webreference = webreferences.Valuetec; // eslint-disable-line

/**
 *
 * ValuteTec Service Payment Service - SOAP Services
 *
 * @return {HTTPService} Authorize REST Service
 */
function valutecService() {
    return LocalServiceRegistry.createService('int.soap.valuetec', {
        initServiceClient: function () {
            var serviceClient = webreference.getService('ValutecWS', 'ValutecWSSoap');
            return serviceClient;
        },
        createRequest: function (svc, requestBody) {
            return requestBody;
        },
        execute: function (svc, requestBody) {
            return svc.getServiceClient().transaction_CardBalance(requestBody);
        },
        parseResponse: function (service, response) {
            return response;
        }
    });
}

module.exports = {
    get: valutecService
};
