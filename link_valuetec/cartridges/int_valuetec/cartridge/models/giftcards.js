'use strict';

var formatMoney = require('dw/util/StringUtils').formatMoney;
var collections = require('*/cartridge/scripts/util/collections');
var HashMap = require('dw/util/HashMap');
var Template = require('dw/util/Template');
var Transaction = require('dw/system/Transaction');

/**
 * Gets the gift Card total amount on the basket
 * @param {collection} paymentInstruments - Current users's basket payment Instrument
 * @param {string} currencyCode - Basket currencyCode
 * @returns {Object} an object that contains the value and formatted value of the order gift card total
 */
function getGiftCardTotal(paymentInstruments, currencyCode) {
    var Money = require('dw/value/Money');
    var giftCardsTotal = new Money(0.0, currencyCode);
    collections.forEach(paymentInstruments, function (paymentInstrument) {
        if (paymentInstrument.getPaymentMethod() === 'GiftCard') {
            giftCardsTotal = giftCardsTotal.add(paymentInstrument.getPaymentTransaction().getAmount());
        }
    });
    return {
        value: giftCardsTotal.value,
        formatted: formatMoney(giftCardsTotal)
    };
}


/**
 * Decides Basket has a Gift Card payment Instrument
 * @param {collection} paymentInstruments - Current users's basket
 * @returns {boolean} hasGC - Decides whether basket has a Gift Card
 */
function hasGiftCard(paymentInstruments) {
    var arrayHelper = require('*/cartridge/scripts/util/array');
    var hasGC = arrayHelper.find(paymentInstruments, function (paymentInstrument) {
        return paymentInstrument.getPaymentMethod() === 'GiftCard';
    });
    return !hasGC;
}

/**
 * creates an array of giftCards.
 * @param {paymentInstruments} paymentInstruments - the current line item container
 * @param {dw.value.Money} grandTotal - To check whether the order total is more utilized
 * @param {dw.order.LineItemContainer} lineItemContainer - The Basket or order
 * @returns {Array} an array of objects containing promotion and coupon information
 */
function getGiftCards(paymentInstruments, grandTotal, lineItemContainer) {
    var orderTotal = grandTotal;
    var amountOpen;
    var giftCards = {}; // eslint-disable-line
    var lastCardUpdated = true;
    collections.forEach(paymentInstruments, function (paymentInstrument) {
        if (paymentInstrument.getPaymentMethod() === 'GiftCard') {
            lastCardUpdated = true;
            if (orderTotal.value > 0.0) {
                lastCardUpdated = false;
                var giftCardAmount = paymentInstrument.getPaymentTransaction().getAmount();
                if (giftCardAmount.value > orderTotal.value) {
                    amountOpen = orderTotal;
                } else if (giftCardAmount.value === orderTotal.value) {
                    amountOpen = giftCardAmount;
                } else if (giftCardAmount.value < orderTotal.value) {
                    amountOpen = giftCardAmount;
                }
                Transaction.wrap(function () {
                    var giftCardNumber = paymentInstrument.getPaymentTransaction().custom.giftCardNumber;
                    lineItemContainer.removePaymentInstrument(paymentInstrument);
                    var newPaymentInstrument = lineItemContainer.createPaymentInstrument(
                        'GiftCard', amountOpen
                    );
                    var paymentTransaction = newPaymentInstrument.getPaymentTransaction();
                    paymentTransaction.custom.giftCardNumber = giftCardNumber;
                    giftCards[giftCardNumber] = {
                        type: 'giftcard',
                        UUID: giftCardNumber,
                        amount: amountOpen.value
                    };
                    orderTotal = orderTotal.subtract(amountOpen);
                });
            }

            if (orderTotal.value <= 0.0 && lastCardUpdated) {
                Transaction.wrap(function () {
                    lineItemContainer.removePaymentInstrument(paymentInstrument);
                });
            }
        }
    });

    return Object.keys(giftCards).map(function (key) {
        return giftCards[key];
    });
}

/**
 * create the discount results html
 * @param {Array} giftcards - an array of objects that contains gift cards
 * information
 * @returns {string} The rendered HTML
 */
function getGiftCardHtml(giftcards) {
    var context = new HashMap();
    var object = {
        giftCards: {
            giftCards: giftcards
        }
    };

    Object.keys(object).forEach(function (key) {
        context.put(key, object[key]);
    });

    var template = new Template('cart/cartGiftCardDisplay');
    return template.render(context).text;
}

/**
 * @constructor
 * @classdesc totals class that represents the order totals of the current line item container
 * @param {dw.order.lineItemContainer} lineItemContainer - The current user's line item container
 * @param {Object} totals - The current user's line item container totals
 */
function giftCards(lineItemContainer) {
    if (lineItemContainer) {
        var currentBasket = lineItemContainer;
        var grandTotal = lineItemContainer.totalGrossPrice;
        var currencyCode = currentBasket.getCurrencyCode();
        this.giftCards = getGiftCards(currentBasket.getPaymentInstruments(), grandTotal, lineItemContainer);
        this.giftCardsHTML = getGiftCardHtml(this.giftCards);
        this.giftCardTotal = getGiftCardTotal(currentBasket.getPaymentInstruments(), currencyCode);
        this.hasGC = !(hasGiftCard(currentBasket.getPaymentInstruments()));
    } else {
        this.giftCardTotal = '-';
        this.giftCards = '-';
        this.giftCardsHTML = '-';
        this.hasGC = false;
    }
}

module.exports = giftCards;
