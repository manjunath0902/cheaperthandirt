'use strict';

var base = module.superModule;
var GiftCardsModel = require('*/cartridge/models/giftcards');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var formatMoney = require('dw/util/StringUtils').formatMoney;
var Money = require('dw/value/Money');

/**
 * Accepts a total object and formats the value
 * @param {dw.value.Money} total - Total price of the cart
 * @returns {string} the formatted money value
 */
function getTotals(total) {
    return !total.available ? '-' : formatMoney(total);
}

/**
 * @constructor
 * @classdesc CartModel class that represents the current basket
 *
 * @param {dw.order.Basket} basket - Current users's basket
 */
function CartModel(basket) {
    base.call(this, basket);
    this.giftCards = new GiftCardsModel(basket);
    var totals = this.totals;
    if (totals) {
        this.finalOrderTotal = COHelpers.getFinalOrderTotal(basket, totals, this.giftCards);
    }
    if (this.giftCards && totals) {
        var orderDiscount = new Money(totals.totalDiscount, basket.getCurrencyCode());
        var totalDiscount = orderDiscount;
        this.totalDiscount = totalDiscount.value;
        this.totalDiscountFormatted = formatMoney(totalDiscount);
    }
    this.hasDiscount = false;
    if (this.totalDiscount > 0) {
        this.hasDiscount = true;
    }
    var requireCCPayment = COHelpers.validateIfOrderRequirePayment(basket);
    this.requireCCPayment = requireCCPayment;
    var giftCardTotals = COHelpers.getRemainingTotal(basket);
    if (giftCardTotals) {
        this.grandTotal = getTotals(giftCardTotals);
    }
}

CartModel.prototype = Object.create(base.prototype);

module.exports = CartModel;
