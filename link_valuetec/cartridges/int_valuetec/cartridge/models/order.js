'use strict';

var base = module.superModule;
var GiftCardsModel = require('*/cartridge/models/giftcards');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var formatMoney = require('dw/util/StringUtils').formatMoney;
var Money = require('dw/value/Money');

/**
 * Order class that represents the current order
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket/order
 * @param {Object} options - Object to help configure the orderModel
 * @constructor
 */
function OrderModel(lineItemContainer, options) {
    base.call(this, lineItemContainer, options);
    var totals = this.totals;
    this.giftCards = new GiftCardsModel(lineItemContainer);
    this.finalOrderTotal = COHelpers.getFinalOrderTotal(lineItemContainer, totals, this.giftCards);
    this.hasDiscount = false;
    if (this.giftCards && totals) {
        var giftCardTotal = this.giftCards.giftCardTotal.value;
        var giftCardMoney = new Money(giftCardTotal, lineItemContainer.getCurrencyCode());
        var orderDiscount = new Money(totals.totalDiscount, lineItemContainer.getCurrencyCode());
        var totalDiscount = giftCardMoney.add(orderDiscount);
        if (totalDiscount.value > 0) {
            this.hasDiscount = true;
        }
        this.totalDiscount = formatMoney(totalDiscount);
    }
}

OrderModel.prototype = Object.create(base.prototype);

module.exports = OrderModel;
