'use strict';

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var recaptcha = require('*/cartridge/scripts/services/recaptcha');
var Resource = require('dw/web/Resource');
var ContentMgr = require('dw/content/ContentMgr');
var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');

server.get('Show', cache.applyDefaultCache, function (req, res, next) {
    var apiContent = ContentMgr.getContent('seo-giftcard-landing');
    if (apiContent) {
        pageMetaHelper.setPageMetaData(req.pageMetaData, apiContent);
        pageMetaHelper.setPageMetaTags(req.pageMetaData, apiContent);
    }
    res.render('/gift/giftcardLanding');
    next();
});


server.post('CheckBalance', function (req, res, next) {
    var HookMgr = require('dw/system/HookMgr');
    var giftNumber = req.form.giftNumber;
    var result = recaptcha.validate(req);
    if (result !== null && result.success) {
        var checkBalanceResponse = HookMgr.callHook(
                'app.payment.processor.gift_card',
                'CheckBalance',
                giftNumber
            );
        res.json({
            data: checkBalanceResponse
        });
    } else {
        var data = {};
        data.error = true;
        data.responseMsg = Resource.msg('error.message.reccaptcha.failed', 'customerService', null);
        res.json({
            data: data
        });
    }
    next();
});

server.get(
    'AddGiftCard',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var BasketMgr = require('dw/order/BasketMgr');
        var Transaction = require('dw/system/Transaction');
        var URLUtils = require('dw/web/URLUtils');
        var CartModel = require('*/cartridge/models/cart');
        var HookMgr = require('dw/system/HookMgr');
        var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

        var currentBasket = BasketMgr.getCurrentBasket();

        if (!currentBasket) {
            res.setStatusCode(500);
            res.json({
                error: true,
                redirectUrl: URLUtils.url('Cart-Show').toString()
            });

            return next();
        }

        if (!currentBasket) {
            res.setStatusCode(500);
            res.json({
                errorMessage: Resource.msg('error.add.giftcard', 'checkout', null)
            });
            return next();
        }

        var error = false;
        var errorMessage;
        var giftCardNumber = req.querystring.giftCard.trim();
        var applyCardResponse;

        try {
            Transaction.wrap(function () {
                applyCardResponse = HookMgr.callHook(
                    'app.payment.processor.gift_card',
                    'Handle',
                    currentBasket,
                    giftCardNumber
                );
            });
        } catch (e) {
            error = true;
        }

        if (error || applyCardResponse.error) {
            var errorCodes = {
                CARD_NOT_FOUND: 'error.giftcard.not.found',
                CARD_ALREADY_IN_CART: 'error.giftcard.already.in.cart',
                NO_BALANCE: 'error.giftcard.cannot.be.combined',
                NO_CARD_REQUIRED: 'error.giftcard.cannot.be.applied',
                default: 'giftcard.default.error'
            };
            var errorMessageKey = errorCodes[applyCardResponse.type] || errorCodes.default;
            errorMessage = Resource.msg(errorMessageKey, 'checkout', null);
            res.json({
                error: applyCardResponse.error || error,
                errorMessage: errorMessage
            });
            return next();
        }

        Transaction.wrap(function () {
            basketCalculationHelpers.calculateTotals(currentBasket);
        });

        var basketModel = new CartModel(currentBasket);

        res.json(basketModel);
        return next();
    }
);

server.get('RemoveGiftCard', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var CartModel = require('*/cartridge/models/cart');
    var arrayHelper = require('*/cartridge/scripts/util/array');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.setStatusCode(500);
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });

        return next();
    }

    var existingPaymentInstrument;
    var paymentInstruments = currentBasket.getPaymentInstruments();

    if (currentBasket && req.querystring.uuid) {
        existingPaymentInstrument = arrayHelper.find(paymentInstruments, function (paymentInstrument) {
            return paymentInstrument.getPaymentTransaction().custom.giftCardNumber === req.querystring.uuid;
        });

        if (existingPaymentInstrument) {
            Transaction.wrap(function () {
                currentBasket.removePaymentInstrument(existingPaymentInstrument);
                basketCalculationHelpers.calculateTotals(currentBasket);
            });

            var basketModel = new CartModel(currentBasket);

            res.json(basketModel);
            return next();
        }
    }

    res.setStatusCode(500);
    res.json({
        errorMessage: Resource.msg('error.cannot.remove.giftcard', 'checkout', null)
    });
    return next();
});


module.exports = server.exports();

