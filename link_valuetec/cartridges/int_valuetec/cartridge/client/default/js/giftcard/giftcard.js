'use strict';
var recaptcha = require('customcore/recaptcha/recaptcha');
/**
 * update the token and submit the form
 * @param {string} token - to update the hidden field
 * @param {Object} $form - current form
 */
function checkBalanceCallback(token, $form) {
    $form.find('input[name="recaptcha_token"]').val(token);
    var url = $form.attr('action');
    $form.spinner().start();
    $.ajax({
        url: url,
        type: 'post',
        data: $form.serialize(),
        success: function (response) {
            $form.spinner().stop();
            $form.find('.gift-card-balance').attr('disabled', false);
            $form.find('.available-balance').removeClass('d-none');
            $form.find('.available-balance span.label').text(response.data.responseMsg);
            if (!response.data.error) {
                $form.find('.available-balance span.value').text('$' + response.data.balance);
                $form.find('.available-balance span.label').removeClass('error');
            } else {
                $form.find('.available-balance span.value').text('');
                $form.find('.available-balance span.label').addClass('error');
            }
        }
    });
}

module.exports = {
    checkBalance: function () {
        $('.gift-card-balance').on('click', function (e) {
            e.preventDefault();
            $(this).attr('disabled', true);
            var $form = $(this).closest('form');
            recaptcha('giftcardCheckBalance', $form, checkBalanceCallback);
        });
    }
};
