'use strict';

/**
 * appends params to a url
 * @param {string} url - Original url
 * @param {Object} params - Parameters to append
 * @returns {string} result url with appended parameters
 */
function appendToUrl(url, params) {
    var newUrl = url;
    newUrl += (newUrl.indexOf('?') !== -1 ? '&' : '?') + Object.keys(params).map(function (key) {
        return key + '=' + encodeURIComponent(params[key]);
    }).join('&');

    return newUrl;
}

/**
 * Checks whether the basket is valid. if invalid displays error message and disables
 * checkout button
 * @param {Object} data - AJAX response from the server
 */
function validateBasket(data) {
    if (data.valid.error) {
        if (data.valid.message) {
            var errorHtml = '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
                'fade show" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button>' + data.valid.message + '</div>';

            $('.cart-error').append(errorHtml);
        } else {
            $('.cart').empty().append('<div class="row"> ' +
                '<div class="col-12 text-center"> ' +
                '<h1>' + data.resources.emptyCartMsg + '</h1> ' +
                '</div> ' +
                '</div>'
            );
            $('.number-of-items').empty().append(data.resources.numberOfItems);
            $('.minicart-quantity').empty().append(data.numItems);
            $('.minicart .popover').empty();
            $('.minicart .popover').removeClass('show');
        }

        $('.checkout-btn').addClass('disabled');
    } else {
        $('.checkout-btn').removeClass('disabled');
    }
}

/**
 * re-renders the order totals and the number of items in the cart
 * @param {Object} message - Error message to display
 */
function createErrorNotification(message) {
    var errorHtml = '<div class="alert alert-danger alert-dismissible valid-cart-error ' +
        'fade show" role="alert">' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button>' + message + '</div>';

    $('.cart-error').append(errorHtml);
}

/**
 * re-renders the order totals and the number of items in the cart
 * @param {Object} data - AJAX response from the server
 */
function updateOrderTotals(data) {
    $('.shipping-total-cost').empty().append(data.totals.totalShippingCost);
    $('.tax-total').empty().append(data.totals.totalTax);
    $('.grand-total-sum').empty().append(data.finalOrderTotal);
    $('.sub-total').empty().append(data.totals.subTotal);

    if (data.giftCards.giftCardTotal.value > 0) {
        $('.giftcard-discount').removeClass('hide-order-discount');
        $('.order-giftcard-total').empty()
            .append('- ' + data.giftCards.giftCardTotal.formatted);
    } else {
        $('.giftcard-discount').addClass('hide-order-discount');
    }

    if (data.giftCards.giftCardTotal.value > 0) {
        $('.gift-card-remaining').removeClass('giftcard-empty');
        $('.remaining-order-total').empty()
            .append('  ' + data.finalOrderTotal);
    } else {
        $('.gift-card-remaining').addClass('giftcard-empty');
    }
    if (data.requireCCPayment) {
        $('body').trigger('zerodollar:require-cc');
    } else {
        $('body').trigger('zerodollar:no-require-cc');
    }

    if (data.totalDiscount > 0) {
        $('.savings-total').removeClass('d-none');
        $('.grand-total-discount').empty()
            .append(data.totalDiscountFormatted);
    } else {
        $('.savings-total').addClass('d-none');
    }

    if (data.totals.orderLevelDiscountTotal.value > 0) {
        $('.order-discount').removeClass('hide-order-discount');
        $('.order-discount-total').empty()
            .append('- ' + data.totals.orderLevelDiscountTotal.formatted);
    } else {
        $('.order-discount').addClass('hide-order-discount');
    }

    if (data.totals.shippingLevelDiscountTotal.value > 0) {
        $('.shipping-discount').removeClass('hide-shipping-discount');
        $('.shipping-discount-total').empty().append('- ' +
            data.totals.shippingLevelDiscountTotal.formatted);
    } else {
        $('.shipping-discount').addClass('hide-shipping-discount');
    }
}

module.exports = function () {
    $('.gift-card-form').submit(function (e) {
        e.preventDefault();
        $.spinner().start();
        $('.giftcard-missing-error').hide();
        $('.giftcard-error-message').empty();
        if (!$('.gift-card-field').val()) {
            $('.gift-card-form .form-control').addClass('is-invalid');
            $('.giftcard-missing-error').show();
            $.spinner().stop();
            return false;
        }
        var $form = $('.gift-card-form');
        $('.gift-card-form .form-control').removeClass('is-invalid');
        $('.giftcard-error-message').empty();

        $.ajax({
            url: $form.attr('action'),
            type: 'GET',
            dataType: 'json',
            data: $form.serialize(),
            success: function (data) {
                if (data.error) {
                    $('.gift-card-form .form-control').addClass('is-invalid');
                    $('.giftcard-error-message').empty().append(data.errorMessage);
                } else {
                    $('.gift-card-displays').empty().append(data.giftCards.giftCardsHTML);
                    if (data.giftCards.giftCards.length >= 3) {
                        $('body').find('.apply-giftcard').addClass('d-none');
                        $('body').find('.gift-max-message').removeClass('d-none');
                        $('body').find('.apply-giftcard-toggle').addClass('d-none');
                        $('body').find('.gift-card-info').addClass('d-none');
                    }
                    updateOrderTotals(data);
                    validateBasket(data);
                }
                $('.gift-card-field').val('');
                $('.gift-card-field').closest('.form-group').find('label').removeClass('input-focus');
                $('.gift-card-field').closest('.form-group').find('.promo-code-btn').attr('disabled', true);
                $.spinner().stop();
            },
            error: function (err) {
                if (err.responseJSON.redirectUrl) {
                    window.location.href = err.responseJSON.redirectUrl;
                } else {
                    createErrorNotification(err.errorMessage);
                    $.spinner().stop();
                }
            }
        });
        return false;
    });

    $('body').on('blur', 'input.gift-card-field', function () {
        if ($(this).val().length > 0) {
            $(this).closest('.form-group').find('.promo-code-btn').attr('disabled', false);
        } else {
            $(this).closest('.form-group').find('.promo-code-btn').attr('disabled', true);
        }
    });

    $('body').on('click', '.apply-giftcard-toggle', function () {
        $('.apply-giftcard').toggleClass('d-none');
        $('.gift-card-remaining').toggleClass('d-none');
    });

    $('body').on('click', '.remove-giftcard', function (e) {
        e.preventDefault();

        var uuid = $(this).data('uuid');
        var $deleteConfirmBtn = $('.delete-giftcard-confirmation-btn');
        var $productToRemoveSpan = $('.giftcard-to-remove');

        $deleteConfirmBtn.data('uuid', uuid);
        $deleteConfirmBtn.data('code', uuid);

        $productToRemoveSpan.empty().append(uuid);
    });


    $('body').on('click', '.delete-giftcard-confirmation-btn', function (e) {
        e.preventDefault();

        var url = $(this).data('action');
        var uuid = $(this).data('uuid');
        var couponCode = $(this).data('uuid');
        var urlParams = {
            code: couponCode,
            uuid: uuid
        };

        url = appendToUrl(url, urlParams);

        $('body > .modal-backdrop').remove();

        $.spinner().start();
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            success: function (data) {
                $('.gift-card-displays').empty().append(data.giftCards.giftCardsHTML);
                updateOrderTotals(data);
                validateBasket(data);
                if (data.giftCards.giftCards.length < 3) {
                    $('body').find('.apply-giftcard').removeClass('d-none');
                    $('body').find('.apply-giftcard-toggle').removeClass('d-none');
                    $('body').find('.gift-max-message').addClass('d-none');
                    $('body').find('.gift-card-info').removeClass('d-none');
                }
                $.spinner().stop();
            },
            error: function (err) {
                if (err.responseJSON.redirectUrl) {
                    window.location.href = err.responseJSON.redirectUrl;
                } else {
                    createErrorNotification(err.responseJSON.errorMessage);
                    $.spinner().stop();
                }
            }
        });
    });
};
