'use strict';

/**
 * @function
 * @description Extracts all parameters from a given query string into an object
 * @param {string} qs The query string from which the parameters will be extracted
 * @returns {Object} Query string in key/value pair format.
 */
var getQueryStringParams = function () {
    var qs = location.search.substring(1);
    if (!qs || qs.length === 0) { return {}; }
    var params = {};
    var unescapedQS = decodeURIComponent(qs);
    // Use the String::replace method to iterate over each
    // name-value pair in the string.
    unescapedQS.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'),
        function ($0, $1, $2, $3) {
            params[$1] = $3;
        }
    );
    return params;
};

module.exports = {
    updateStage: function (stage) {
        var dl = JSON.parse(JSON.stringify(window.globalData));
        var checkoutStages = ['shipping', 'payment', 'placeOrder'];
        if (dl.ecommerce.checkout.actionField.step - 2 === stage) {
            return;
        }
        switch (checkoutStages[stage]) {
            case 'shipping':
                dl.event = 'checkoutShipping';
                dl.pageName = 'checkout: shipping';
                dl.ecommerce.checkout.actionField.step = 2;
                break;
            case 'payment':
                dl.event = 'checkoutBilling';
                dl.pageName = 'checkout: billing';
                dl.ecommerce.checkout.actionField.step = 3;
                break;
            case 'placeOrder':
                dl.event = 'checkoutReview';
                dl.pageName = 'checkout: order review';
                dl.ecommerce.checkout.actionField.step = 4;
                break;
            default:
                // Do nothing
        }

        // Push global data to GTM
        delete dl.ecommerce.checkout.products;
        dataLayer.push(dl);
    },

    loginEvent: function () {
        var registration;
        try {
            var URLString = window.location.href;
            var url = new URL(URLString);
            registration = url.searchParams.get('registration');
        } catch (e) {
            // URL is not support on the IE version. This blocks the user from checkout.
            registration = getQueryStringParams().registration;
        }
        if (registration === 'false') {
            dataLayer.push({
                event: 'accountLogin'
            });
        }
    },

    checkoutEmailSignup: function (data) {
        if (data.signupForEmail === 'true') {
            dataLayer.push({
                emailSignupLocation: 'checkout', // the location of the signup.  E.g. footer, checkout, popup, etc.
                event: 'emailSignup'
            });
        }
    },

    sort: function (sortOption) {
        dataLayer.push({
            event: 'pageSort',
            sort_selected: $.trim(sortOption.replace(/(?:\r\n|\r|\n)/g, ''))
        });
    },

    facet: function ($this) {
        if ($this.parents('.refinement').length === 0) {
            return;
        }

        var facetCategory = $.trim($this.parents('.refinement').find('.card-header').text().replace(/(?:\r\n|\r|\n)/g, ''));
        var facetSelection = $.trim($this.text().replace(/(?:\r\n|\r|\n)/g, '')) ? $.trim($this.text().replace(/(?:\r\n|\r|\n)/g, '')) : $this.attr('title');
        dataLayer.push({
            event: 'facetEngagement',
            facetCategory: facetCategory, // the facet category
            facetSelection: facetSelection // the selection made within the category
        });
    },

    navigationClick: function () {
        var navObject = {
            navL1: '', // the level 1 link text clicked.
            navL2: '', // the level 2 link text clicked.  If top level is clicked leave empty.
            navL3: '', // the level 3 link text clicked.  If top level or second level is clicked leave empty.
            event: 'navClick'
        };

        $('.main-menu ul[role="menu"] li a').click(function () {
            var navLevels = $(this).parents('li[role="menuitem"]');
            if (navLevels.length === 3) {
                navObject.navL3 = navLevels[0].innerText;
                navObject.navL2 = $(navLevels[1]).find('a:first span:first').text();
                navObject.navL1 = $(navLevels[2]).find('a:first span:first').text();
            } else if (navLevels.length === 2) {
                navObject.navL2 = $(navLevels[0]).find('a:first span:first').text();
                navObject.navL1 = $(navLevels[1]).find('a:first span:first').text();
            } else {
                navObject.navL1 = $(navLevels[0]).find('a:first span:first').text();
            }
            dataLayer.push(navObject);
        });
    },

    headerClicks: function () {
        $('.header .header-box .brand a').click(function () {
            dataLayer.push({
                linktext: 'brand logo',
                event: 'headerClick'
            });
        });

        $('.header .header-box .user-authenticated a, .header .header-box .user-unauthenticated a').click(function () {
            dataLayer.push({
                linktext: $(this).find('.live-chatmsg').text() || $(this).find('.user-message').text(),
                event: 'headerClick'
            });
        });

        $('.header .header-box .minicart .minicart-link').click(function () {
            dataLayer.push({
                linktext: $(this).find('.minicart-msg').text(),
                event: 'headerClick'
            });
        });
    },

    gtmPDPEvents: function () {
        $('.product-details-main').on('click', '.social-links li a', function () {
            dataLayer.push({
                productID: $('.product-details-main').data('master'),
                socialNetwork: $(this).data('share'),
                event: 'socialShare'
            });
        });

        $('.product-details-main').on('click', '#send-to-friend', function () {
            dataLayer.push({
                event: 'referFriend'
            });
        });

        $('.product-details-main').on('click', '.description-and-detail .nav-tab-main .nav-link', function () {
            dataLayer.push({
                productID: $('.product-details-main').data('master'),
                productName: $('h1.product-name').text(),
                productTab: $(this).text(),
                event: 'productTabClick'
            });
        });
    },

    pdpAddToWishlist: function () {
        dataLayer.push({
            productID: $('.product-details-main').data('master'),
            productName: $('h1.product-name').text(),
            event: 'addToWishlist'
        });
    },

    cartAddToWishlist: function ($this) {
        dataLayer.push({
            productID: $this.data('master'),
            productName: $this.data('name'),
            event: 'addToWishlist'
        });
    },

    plpAddToWishlist: function ($this) {
        dataLayer.push({
            productID: $this.closest('.product').data('pid'),
            productName: $this.parents('.product-tile').find('.pdp-link a').attr('title'),
            event: 'addToWishlist'
        });
    },

    estimateShipRate: function () {
        dataLayer.push({
            event: 'estimateShipping'
        });
    },

    applyCoupon: function (data) {
        dataLayer.push({
            promoCodeApplied: data.couponCode,
            promoCodeFail: data.error ? 'yes' : 'no',
            event: 'promoAppliedInCart'
        });
    },

    itemsRestricted: function () {
        dataLayer.push({
            event: 'itemsCannotBeShipped'
        });
    },

    itemsRestrictedPlaceOrder: function (data) {
        if (data.errorClass === 'shipping-restricted') {
            dataLayer.push({
                event: 'itemsCannotBeShipped'
            });
        }
    },

    removeFromCart: function (pid) {
        var $this = $('body .remove-line-item button[data-pid="' + pid + '"]:first');
        dataLayer.push({
            event: 'removeFromCart',
            ecommerce: {
                currencyCode: $('#gtm-attributes').data('currency-code'),
                remove: {
                    products: [{
                        name: $this.data('name'),
                        id: $this.data('master'),
                        price: $this.data('price'),
                        brand: $this.data('brand'),
                        category: $this.data('category')
                    }]
                }
            }
        });
    },

    storeLocatorSearch: function (element) {
        var e = (element.parents('.modal-dialog').length > 0 || element.parents('.checkout-main').length > 0) ? 'FFLSearch' : 'storeLocatorSearch';
        dataLayer.push({
            event: e
        });
    },

    selectFFL: function () {
        dataLayer.push({
            event: 'FFLSelect'
        });
    },

    bypassFFL: function () {
        dataLayer.push({
            event: 'FFLBypass'
        });
    },

    recommendations: function () {
        var recZone = $('.product-impressions');
        if (recZone.length === 0) {
            return;
        }
        recZone.each(function () {
            var dl = {};
            var $this = $(this);
            dl.event = 'productImpressions';
            dl.ecommerce = {};
            dl.ecommerce.currencyCode = $('#gtm-attributes').data('currency-code');
            var impressions = [];
            $this.find('.product-impression').each(function () {
                impressions.push({
                    name: $(this).find('.product-tile').find('.pdp-link a').attr('title'),
                    id: $(this).find('.product-tile').data('master'),
                    price: $.trim($(this).find('.product-tile').find('.price .sales:first .value').text()
                        .replace(/(?:\r\n|\r|\n)/g, '')).substr(1),
                    brand: $(this).find('.product-tile').data('brand'),
                    category: $.trim($(this).find('.product-tile').data('category')),
                    list: $this.data('gtm-title'),
                    position: $(this).data('index')
                });
            });
            dl.ecommerce.impressions = impressions;
            dataLayer.push(dl);
        });
    },

    productClick: function () {
        $('.container').on('click', '.product-tile .pdp-link a, .product-tile .product-anchor', function (e) {
            e.stopPropagation();
            var $this = $(this).parents('.product-impression');
            var dl = {};
            dl.event = 'productClick';
            dl.ecommerce = {};
            dl.ecommerce.currencyCode = $('#gtm-attributes').data('currency-code');
            dl.ecommerce.click = {};
            dl.ecommerce.click.actionField = {};
            dl.ecommerce.click.products = [];
            dl.ecommerce.click.products.push({
                name: $this.find('.product-tile').find('.pdp-link a').attr('title'),
                id: $this.find('.product-tile').data('master'),
                price: $.trim($this.find('.product-tile').find('.price .sales:first .value').text().replace(/(?:\r\n|\r|\n)/g, '')).substr(1),
                brand: $this.find('.product-tile').data('brand'),
                category: $.trim($this.find('.product-tile').data('category')),
                list: $this.parents('.product-impressions').data('gtm-title'),
                position: $this.data('index')
            });
            dataLayer.push(dl);
        });
    },

    quickView: function (data) {
        var product = data.product;
        var dl = {};
        dl.event = 'productQuickview';
        dl.ecommerce = {};
        dl.ecommerce.currencyCode = $('#gtm-attributes').data('currency-code');
        dl.ecommerce.detail = {};
        dl.ecommerce.detail.actionField = {};
        dl.ecommerce.detail.actionField.list = '';
        dl.ecommerce.detail.products = [];

        var priceObj = product.price.type === 'range' ? product.price.min : product.price;
        var listPrice = (priceObj.list ? priceObj.list.value.toFixed(2) : priceObj.sales.value.toFixed(2));
        var price = priceObj.sales.value.toFixed(2);
        var discountAmount = (parseInt(listPrice, 10) - parseInt(price, 10)).toFixed(2);

        dl.ecommerce.detail.products.push({
            name: product.productName ? product.productName : '',
            id: product.masterProductID,
            price: price,
            brand: (product.brand ? product.brand : ''),
            category: product.category,
            listPrice: listPrice,
            variant: product.id,
            upc: product.UPC,
            mfrNumber: product.manufacturerName,
            availability: product.available ? 'In Stock' : 'Out of Stock',
            discountAmount: discountAmount
        });
        dataLayer.push(dl);
    },

    addToCart: function (data, quantity) {
        var product = data.cart.items.filter(function (item) {
            return item.UUID === data.pliUUID;
        })[0];

        var priceObj = product.price.type === 'range' ? product.price.min : product.price;
        var listPrice = (priceObj.list ? priceObj.list.value.toFixed(2) : priceObj.sales.value.toFixed(2));
        var price = priceObj.sales.value.toFixed(2);
        var discountAmount = (parseInt(listPrice, 10) - parseInt(price, 10)).toFixed(2);

        dataLayer.push({
            event: 'addToCart',
            ecommerce: {
                currencyCode: $('#gtm-attributes').data('currency-code'),
                add: {
                    products: [{
                        name: product.productName ? product.productName : '',
                        id: product.masterProductID,
                        price: price,
                        brand: (product.brand ? product.brand : ''),
                        category: product.category,
                        listPrice: listPrice,
                        variant: product.id,
                        upc: product.UPC,
                        mfrNumber: product.manufacturerName,
                        availability: product.available ? 'In Stock' : 'Out of Stock',
                        discountAmount: discountAmount,
                        quantity: parseInt(quantity, 10)
                    }]
                }
            }
        });
    }
};
