'use strict';

module.exports = {
    onSubmit: function () {
        $('body').on('click', '.pr-btn-review', function () {
            var URLString = window.location.href;
            var url = new URL(URLString);
            var productID = url.searchParams.get('pr_page_id');
            dataLayer.push({
                productID: productID,
                productName: $('.pr-header-product-name').text(),
                event: 'writeReview'
            });
        });
    }
};
