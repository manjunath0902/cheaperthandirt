'use strict';

/**
 * Controller for Google Tag Manager
 *
 * @module controllers/GoogleTagManager
 */

var server = require('server');

/**
 * Prepare and render GTM global data.
 */
server.get('GlobalData', function (req, res, next) {
    var globalData = require('~/cartridge/scripts/datalayer/DataLayer.js').getGlobalData(req);
    res.render('googleTagManager/gtmGlobalData', {
        GlobalData: globalData
    });
    next();
});

module.exports = server.exports();
