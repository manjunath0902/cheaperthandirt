/* API Includes */
var MessageDigest = require('dw/crypto/MessageDigest');
var Encoding = require('dw/crypto/Encoding');
var CatalogMgr = require('dw/catalog/CatalogMgr');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');

var server = require('server');

var updateCustomerAndGeoData = function (obj) {
    // Customer information
    /* eslint-disable no-param-reassign */
    if (customer.isAuthenticated()) {
        var messageDigest = new MessageDigest(MessageDigest.DIGEST_MD5);
        obj.userEmail = customer.getProfile().getEmail();
        obj.userHashedEmail = Encoding.toBase64(messageDigest.digestBytes(Encoding.fromBase64(obj.userEmail)));
        obj.userID = customer.getProfile().getCustomerNo();
        obj.userLoginState = 'registered';
    } else {
        obj.userEmail = '';
        obj.userHashedEmail = '';
        obj.userID = '';
        obj.userLoginState = 'guest';
    }

    // Customer geo location info
    var geoLocation = request.getGeolocation();
    obj.userCountry = geoLocation.getCountryCode();
    obj.userState = geoLocation.getRegionCode();
    /* eslint-enable no-param-reassign */
};

var getCategoryName = function (categoryID) {
    try {
        var category = CatalogMgr.getCategory(categoryID);
        var displayName = category.displayName;
    } catch (e) {
        displayName = categoryID;
    }
    return displayName;
};

var getProductLevelCoupons = function (pli) {
    var collections = require('*/cartridge/scripts/util/collections');
    var priceAdjustments = pli.getPriceAdjustments();
    var couponArr = [];
    collections.forEach(priceAdjustments, function (priceAdjustment) {
        if (priceAdjustment.isBasedOnCoupon()) {
            var coupon = priceAdjustment.getCouponLineItem();
            couponArr.push(coupon.getCouponCode());
        }
    });
    return couponArr.join('|');
};

var getProductLevelCouponValues = function (pli) {
    var collections = require('*/cartridge/scripts/util/collections');
    var priceAdjustments = pli.getPriceAdjustments();
    var couponArr = [];
    collections.forEach(priceAdjustments, function (priceAdjustment) {
        if (priceAdjustment.isBasedOnCoupon()) {
            var price = priceAdjustment.getPrice();
            couponArr.push((price.value).toString().substring(1));
        }
    });
    return couponArr.join('|');
};

var getOrderLevelCoupons = function (order) {
    var suc = [],
    couponLineItem;
    var couponLineItems = order.getCouponLineItems().iterator();
    while (couponLineItems.hasNext()) {
        couponLineItem = couponLineItems.next();
        var priceAdjustments = couponLineItem.priceAdjustments.iterator();
        while (priceAdjustments.hasNext()) {
          var priceAdjustment= priceAdjustments.next();
          if (priceAdjustment) {
              var promotion = priceAdjustment.promotion;
              if (promotion && promotion.promotionClass === 'ORDER') {
                  suc.push(couponLineItem.getCouponCode());
              }
          }
        }
    }
    return suc.join('|');
};

var getOrderLevelCouponValues = function (order) {
    var suc = [],
    couponLineItem;
    var couponLineItems = order.getCouponLineItems().iterator();
    while (couponLineItems.hasNext()) {
        couponLineItem = couponLineItems.next();
        var priceAdjustments = couponLineItem.priceAdjustments.iterator();
        while (priceAdjustments.hasNext()) {
          var priceAdjustment= priceAdjustments.next();
          if (priceAdjustment) {
              var promotion = priceAdjustment.promotion;
              if (promotion && promotion.promotionClass === 'ORDER') {
                  suc.push((priceAdjustment.getPrice().value).toString().substring(1));
              }
          }
        }
    }
    return suc.join('|');
};

var getShippingMethods = function (order) {
    var shipments = {};
    shipments.shipToHomeShipment = COHelpers.getShipToHomeShipment(order);
    shipments.shipToStoreShipment = COHelpers.getShipToStoreShipment(order);
    return shipments;
};

exports.getGlobalData = function (req) {
    var globalData = {};

    // pageType and pageName
    switch (req.querystring.action) {
        case 'Sites-CTDDOTCOM-Site':
        case 'Sites-CTDDOTNET-Site':
        case 'Default-Start':
        case 'Home-Show':
            globalData.pageType = 'home';
            globalData.pageName = globalData.pageType + ': homepage';
            break;

        case 'Page-Show':
            globalData.pageType = 'content';
            globalData.pageName = globalData.pageType + ': ' + req.querystring.cid;
            break;

        case 'Search-Show':
            if (req.querystring.cgid) {
                var category = CatalogMgr.getCategory(req.querystring.cgid);
                globalData.pageType = category.isTopLevel() ? 'category landing' : 'category grid';
                globalData.pageName = globalData.pageType + ': ' + CatalogMgr.getCategory(req.querystring.cgid).getDisplayName();
            } else {
                globalData.pageType = 'search';
                globalData.pageName = globalData.pageType + ': search results';
            }
            break;

        case 'Login-Show':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': log in';
            break;

        case 'Account-Show':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': account overview';
            break;

        case 'Account-EditProfile':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': edit profile';
            break;

        case 'Wishlist-Show':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': wishlist';
            break;

        case 'PaymentInstruments-AddPayment':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': add payment cards';
            break;

        case 'PaymentInstruments-List':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': payment cards';
            break;

        case 'Account-EditPassword':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': edit password';
            break;

        case 'LoyaltyProgram-ShowDashboard':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': reward dashboard';
            break;

        case 'Address-AddAddress':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': add address';
            break;

        case 'Address-List':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': address book';
            break;

        case 'Address-EditAddress':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': edit address';
            break;

        case 'Order-History':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': order history';
            break;

        case 'Order-Details':
            globalData.pageType = 'account';
            globalData.pageName = globalData.pageType + ': order details';
            break;

        case 'Product-Show':
            globalData.pageType = 'pdp';
            globalData.pageName = globalData.pageType + ': <product name>';
            break;

        case 'Cart-Show':
            globalData.pageType = 'cart';
            globalData.pageName = globalData.pageType + ': shopping cart';
            break;

        case 'Checkout-Login':
            globalData.pageType = 'checkout';
            globalData.pageName = globalData.pageType + ': login';
            break;

        case 'Checkout-Begin':
            var event = '';
            var title = '';
            switch (req.querystring.stage) {
                case 'shipping':
                    event = 'checkoutShipping';
                    title = 'shipping';
                    break;
                case 'payment':
                    event = 'checkoutBilling';
                    title = 'billing';
                    break;
                case 'placeOrder':
                    event = 'checkoutReview';
                    title = 'order review';
                    break;
                default:
                    // Do Nothing
            }

            globalData.event = event;
            globalData.pageType = 'checkout';
            globalData.pageName = globalData.pageType + ': ' + title;
            break;

        case 'Order-Confirm':
            globalData.pageType = 'order confirmation';
            globalData.pageName = 'checkout: order confirmation';
            break;

        case 'RedirectURL-Start':
            globalData.pageType = 'error';
            globalData.pageName = globalData.pageType + ': 404';
            break;

        case 'Home-ErrorNotFound':
            globalData.pageType = 'error';
            globalData.pageName = globalData.pageType + ': 404';
            break;

        default:
            // Do nothing
    }

    // Customer and Geo-location data
    updateCustomerAndGeoData(globalData);

    return globalData;
};


exports.getPageData = function (pdict) {
    var pageData = {};
    var i = 0;
    var items;
    var item;
    var listPrice;
    var price;
    var discountAmount;
    var priceObj;
    switch (pdict.action) {
        case 'Search-Show':
            if (pdict.CurrentHttpParameterMap.q.stringValue) {
                pageData.searchTerm = pdict.productSearch.searchKeywords;
                pageData.searchResults = pdict.productSearch.count.toString();
                if (pdict.productSearch.count === 0) {
                    pageData.pageName = 'search: null search results';
                }
            }
            break;

        case 'Account-Show':
            if (pdict.CurrentHttpParameterMap.registration.stringValue === 'false') {
                pageData.event = 'accountLogin';
            } else if (pdict.CurrentHttpParameterMap.registration.stringValue === 'submitted') {
                pageData.event = 'accountCreated';
            }
            break;

        case 'EmailSubscription-SubmitRequest':
            pageData.emailSignupLocation = 'footer';
            pageData.event = 'emailSignup';
            break;

        case 'Product-Show':
            var product = pdict.product;

            pageData.pageName = 'pdp: ' + product.productName;

            pageData.ecommerce = {};
            pageData.ecommerce.detail = {};
            priceObj = product.price.type === 'range' ? product.price.min : product.price;
            listPrice = (priceObj.list ? priceObj.list.value.toFixed(2) : priceObj.sales.value.toFixed(2));
            price = priceObj.sales.value.toFixed(2);
            discountAmount = (parseInt(listPrice, 10) - parseInt(price, 10)).toFixed(2);
            pageData.ecommerce.detail.products = [{
                name: product.productName ? product.productName : '',
                id: product.masterProductID,
                price: price,
                brand: (product.brand ? product.brand : ''),
                category: getCategoryName(product.category),
                listPrice: listPrice,
                variant: product.id,
                upc: product.UPC,
                mfrNumber: product.manufacturerName,
                availability: product.available ? 'In Stock' : 'Out of Stock',
                discountAmount: discountAmount
            }];
            break;

        case 'Cart-Show':
            pageData.cartedProducts = [];
            items = pdict.items;
            for (i = 0; i < items.length; i++) {
                item = items[i];
                pageData.cartedProducts.push({
                    name: item.productName,
                    id: item.masterProductID,
                    price: (item.priceTotal.priceValue / item.quantity).toFixed(2),
                    brand: (item.brand ? item.brand : ''),
                    category: getCategoryName(item.category)
                });
            }
            break;

        case 'Checkout-Login':
            pageData.ecommerce = {
                currencyCode: session.getCurrency().getCurrencyCode(),
                checkout: {
                    actionField: {
                        step: 1
                    }
                }
            };
            break;

        case 'Checkout-Begin':
            var step = 2;
            switch (pdict.currentStage) {
                case 'shipping':
                    step = 2;
                    break;
                case 'payment':
                    step = 3;
                    break;
                case 'placeOrder':
                    step = 4;
                    break;
                default:
                    // Do nothing
            }

            pageData.ecommerce = {
                currencyCode: session.getCurrency().getCurrencyCode(),
                checkout: {
                    actionField: {
                        step: step
                    }
                }
            };

            pageData.ecommerce.checkout.products = [];
            items = pdict.order.items.items;
            for (i = 0; i < items.length; i++) {
                item = items[i];
                listPrice = (item.price.list ? item.price.list.value.toFixed(2) : item.price.sales.value.toFixed(2));
                price = (item.priceTotal.priceValue / item.quantity).toFixed(2);
                discountAmount = (parseInt(listPrice, 10) - parseInt(price, 10)).toFixed(2);
                pageData.ecommerce.checkout.products.push({
                    name: item.productName ? item.productName : '',
                    id: item.masterProductID,
                    price: (item.priceTotal.priceValue / item.quantity).toFixed(2),
                    brand: (item.brand ? item.brand : ''),
                    category: getCategoryName(item.category),
                    listPrice: listPrice,
                    variant: item.id,
                    upc: item.UPC,
                    mfrNumber: item.manufacturerName,
                    availability: item.available ? 'In Stock' : 'Out of Stock',
                    discountAmount: discountAmount
                });
            }

            break;

        case 'Order-Confirm':
            var order = pdict.order;
            var OrderMgr = require('dw/order/OrderMgr');
            var dwOrder = OrderMgr.getOrder(order.orderNumber);
            var shippingMethods = getShippingMethods(dwOrder);
    
            pageData.ecommerce = {};
            pageData.ecommerce.currencyCode = session.getCurrency().getCurrencyCode();
            pageData.ecommerce.purchase = {};
            pageData.ecommerce.purchase.actionField = {};
            pageData.ecommerce.purchase.actionField.id = order.orderNumber;
            pageData.ecommerce.purchase.actionField.revenue = order.totals.subTotal.substr(1);
            pageData.ecommerce.purchase.actionField.tax = order.totals.totalTax.substr(1);
            pageData.ecommerce.purchase.actionField.shipping = order.totals.totalShippingCost !== undefined && order.totals.totalShippingCost !== null ? order.totals.totalShippingCost.substring(1) : '0.00';
            pageData.ecommerce.purchase.actionField.coupon = getOrderLevelCoupons(dwOrder);
            if (order.totals.hasFirearm) {
                pageData.ecommerce.purchase.actionField.firearmHandingCharge = order.totals.firearmHandlingFee.substring(1);
            }
            if (order.totals.hasBulkFee) {
                pageData.ecommerce.purchase.actionField.bulkFeeCharge = order.totals.bulkFee.substring(1);
            }
            pageData.ecommerce.purchase.actionField.couponValue = getOrderLevelCouponValues(dwOrder);

            pageData.ecommerce.purchase.products = [];
            items = pdict.order.items.items;
            for (i = 0; i < items.length; i++) {
                item = items[i];
                listPrice = (item.price.list ? item.price.list.value.toFixed(2) : item.price.sales.value.toFixed(2));
                price = (item.priceTotal.priceValue / item.quantity).toFixed(2);
                discountAmount = (parseInt(listPrice, 10) - parseInt(price, 10)).toFixed(2);
                var pliIter = dwOrder.getProductLineItems(item.id).iterator();
                var pli;
                while (pliIter.hasNext()) {
                   pli = pliIter.next();
                   break;
                }
                var couponCode = getProductLevelCoupons(pli);
                var couponCodeValues = getProductLevelCouponValues(pli);
                pageData.ecommerce.purchase.products.push({
                    coupon: couponCode,
                    couponValue: couponCodeValues,
                    name: item.productName ? item.productName : '',
                    id: item.masterProductID,
                    price: (item.priceTotal.priceValue / item.quantity).toFixed(2),
                    brand: (item.brand ? item.brand : ''),
                    category: getCategoryName(item.category),
                    listPrice: listPrice,
                    variant: item.id,
                    upc: item.UPC,
                    mfrNumber: item.manufacturerName,
                    availability: item.available ? 'In Stock' : 'Out of Stock',
                    discountAmount: discountAmount,
                    shippingMethod: 'fromStoreId' in pli.custom && pli.custom.fromStoreId !== null ? shippingMethods.shipToStoreShipment.shippingMethod.ID : shippingMethods.shipToHomeShipment.shippingMethod.ID
                });
            }

            break;

        case 'Error-Start':
        case 'Home-ErrorNotFound':
            pageData.pageType = 'error';
            pageData.pageName = pageData.pageType + ': 404';
            updateCustomerAndGeoData(pageData);
            break;

        default:
            // Do Nothing
    }

    return pageData;
};

exports.onLoadAdditionalEvents = function (pdict) {
    var onLoadAdditionalEvents = {};
    onLoadAdditionalEvents.events = [];
    switch (pdict.action) {
        case 'Account-Show':
            var registrationForm = server.forms.getForm('profile');
            if (pdict.CurrentHttpParameterMap.registration.stringValue === 'submitted' && registrationForm.customer.addtoemaillist.value) {
                onLoadAdditionalEvents.events.push({
                    emailSignupLocation: 'account registration',
                    event: 'emailSignup'
                });
            }
            break;

        default:
            // Do Nothing
    }
    return onLoadAdditionalEvents;
};
