'use strict';

var PaymentInstrument = require('dw/order/PaymentInstrument');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');
var Site = require('dw/system/Site');
var Cipher = require('dw/crypto/Cipher');
var CertificateRef = require('dw/crypto/CertificateRef');
var ShippingMgr = require('dw/order/ShippingMgr');
var PriceAdjustment = require('dw/order/PriceAdjustment');
module.exports = {

    getCustomer: function (order) {
        if (order.getCustomer().getProfile()) {
            return order.getCustomer().getProfile().getCustomerNo();
        }
        return '';
    },

    getShipTotal: function(shipment, totalsModel) {
        var adjustedShippingTotal = shipment.adjustedShippingTotalPrice;
        var shipTotal = adjustedShippingTotal.getValue();
        if (shipment.custom.shipmentType === 'instore') {
            var ShipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment);
            var shippingCost = ShipmentShippingModel.getShippingCost(shipment.getShippingMethod()).amount;
            shipTotal = adjustedShippingTotal.subtract(shippingCost).getValue();
        } 
        return shipTotal;
    },

    getShipWeight: function (shipment) {
        var shipWeight = 0,
            productLineItem;
        var productLineItems = shipment.getProductLineItems().iterator();
        while (productLineItems.hasNext()) {
            productLineItem = productLineItems.next();
            shipWeight += productLineItem.getProduct().custom.unitWeight * productLineItem.getQuantityValue();
        }
        return shipWeight;
    },

    getSC: function (order) {
        var sc = [],
            lineItem;
        var allLineItems = order.getAllLineItems().iterator();
        while (allLineItems.hasNext()) {
            lineItem = allLineItems.next();
            if (lineItem instanceof PriceAdjustment && lineItem.getPromotionID()) {
                sc.push(lineItem.getPromotionID());
            }

        }
        return sc.join('|');
    },

    getSUC: function (order) {
        var suc = [],
            couponLineItem;
        var couponLineItems = order.getCouponLineItems().iterator();
        while (couponLineItems.hasNext()) {
            couponLineItem = couponLineItems.next();
            suc.push(couponLineItem.getCouponCode());
        }
        return suc.join('|');
    },

    isPOBoxAddress: function (address) {
        var regExp = /([\w\s*\W]*(P(OST)?.?\s*((O(FF(ICE)?)?)?.?\s*(B(IN|OX|.?))|B(IN|OX))+))[\w\s*\W]*/;
        return regExp.test(address);
    },

    getItems: function (shipment) {
        var items = [];
        var item, product, productLineItem;;
        var productLineItems = shipment.getProductLineItems().iterator();
        while (productLineItems.hasNext()) {
            productLineItem = productLineItems.next();
            product = productLineItem.getProduct();

            item = [];

            //ID
            item.push('');

            //SKU
            if (product.isVariant()) {
                item.push(product.getMasterProduct().getID());
            } else {
                item.push(product.getID());
            }

            //DESC
            item.push('');

            //STATUS
            item.push('');

            //PRICE1
            var price1 = (productLineItem.getAdjustedPrice().getValueOrNull()/productLineItem.getQuantityValue()).toFixed(2)
            item.push(price1);
            // to send a unit price

            //PRICE2
            item.push(product.getPriceModel().getPrice().getValueOrNull());
            // to send only base price
            //PRICE3
            item.push('');

            //QUANTITY
            item.push(productLineItem.getQuantityValue());

            //SHIPPED
            item.push('');

            //NUMBER
            item.push(product.getID());

            //OTHER1
            item.push('');

            //OTHER2
            item.push('');

            //OTHER3
            item.push('');

            //OTHER4
            item.push('');

            //OTHER5
            item.push('');

            //OTHER6
            item.push('');

            //OTHER7
            item.push('');

            //OTHER8
            item.push('');

            //GIFTEMAIL
            if (productLineItem.custom.giftCardRecipientEmail) {
                item.push(productLineItem.custom.giftCardRecipientEmail);
            } else {
                item.push('');
            }

            //GIFTNAME
            if (productLineItem.custom.giftCardSender) {
                item.push(productLineItem.custom.giftCardSender);
            } else {
                item.push('');
            }

            items.push(item.join('~'));
        }

        return items.join('|');
    },

    getPayments: function (order) {
        var payments = [];
        var payment, paymentMethod, paymentInstrument;
        var paymentInstruments = order.getPaymentInstruments().iterator();
        while (paymentInstruments.hasNext()) {
            paymentInstrument = paymentInstruments.next();
            paymentMethod = paymentInstrument.getPaymentMethod();

            payment = [];

            //ID
            payment.push('');

            //METHODNAME
            payment.push(this.getMethodName(paymentMethod));

            //PAYAMOUNT
            payment.push(paymentInstrument.getPaymentTransaction().getAmount().getValueOrNull());

            //SUBNAME
            payment.push(this.getSubName(paymentMethod));

            if (paymentMethod === PaymentInstrument.METHOD_CREDIT_CARD) {

                //CARDTYPE
                payment.push(paymentInstrument.getCreditCardType());

                //ACCOUNT
                payment.push('');

                //CC
                payment.push(this.getCCNumber(paymentInstrument));

                //EXPMM
                if (paymentInstrument.getCreditCardExpirationMonth().toString().length === 1) {
                    payment.push('0' + paymentInstrument.getCreditCardExpirationMonth());
                } else {
                    payment.push(paymentInstrument.getCreditCardExpirationMonth());
                }

                //EXPYY
                payment.push(paymentInstrument.getCreditCardExpirationYear().toString().substr(-2));

            } else if (paymentMethod === 'GiftCard') {
                //GC NUMBER
                payment.push(paymentInstrument.getPaymentTransaction().custom.giftCardNumber);

                //GC Amount
                payment.push(paymentInstrument.getPaymentTransaction().getAmount().getValueOrNull());

                //CC
                payment.push('');

                //EXPMM
                payment.push('');

                //EXPYY
                payment.push('');
            } else {

                //CARDTYPE/GC NUMBER
                payment.push('');

                //ACCOUNT/GC Amount
                payment.push('');

                //CC
                payment.push('');

                //EXPMM
                payment.push('');

                //EXPYY
                payment.push('');
            }

            payments.push(payment.join('~'));
        }
        return payments.join('|');
    },

    getAuthInfo: function (order) {
        var CCPaymentInstruments = order.getPaymentInstruments(dw.order.PaymentInstrument.METHOD_CREDIT_CARD);
        if (CCPaymentInstruments.size() === 0) {
            return new Array(8).join('~');
        }
        var CCPaymentInstrument = CCPaymentInstruments.iterator().next();

        var paymentTransaction = CCPaymentInstrument.getPaymentTransaction();
        var authInfo = [];

        //Push CUSTTOKEN if customer is authenticated.
        if (order.getCustomer().getProfile() && CCPaymentInstrument.getCreditCardToken()) {
            authInfo.push(order.getCustomer().getProfile().custom.authorizeProfileID);
        } else {
            authInfo.push('');
        }

        //Push PAYTOKEN
        if (CCPaymentInstrument.getCreditCardToken()) {
            authInfo.push(CCPaymentInstrument.getCreditCardToken());
        } else {
            authInfo.push('');
        }

        //Push CC4
        authInfo.push(CCPaymentInstrument.getCreditCardNumberLastDigits());

        //TRANID
        authInfo.push(paymentTransaction.custom.refTransID);

        //APPOVAL
        authInfo.push(paymentTransaction.custom.authCode);

        //AVS
        authInfo.push(paymentTransaction.custom.avsResultCode);

        //CODE2
        authInfo.push('');

        //AUTHID
        authInfo.push(paymentTransaction.custom.transId);

        return authInfo.join('~');
    },

    getCCNumber: function (paymentInstrument) {
        if (paymentInstrument.getCreditCardToken()) {
            return '';
        }
        var certRef = new CertificateRef(Site.getCurrent().getCustomPreferenceValue('cc_certificateAliasName'));
        return paymentInstrument.getEncryptedCreditCardNumber(dw.order.PaymentInstrument.ENCRYPTION_ALGORITHM_RSA, certRef);
    },

    getMethodName: function (paymentMethod) {
        var methodName = paymentMethod;
        switch (paymentMethod) {
            case 'CREDIT_CARD':
                methodName = 'CREDITCARD';
                break;
            case 'GiftCard':
                methodName = 'GIFTCERT';
                break;
            default:
                methodName = paymentMethod;
                break;
        }
        return methodName;
    },

    getSubName: function (paymentMethod) {
        var subName;
        switch (paymentMethod) {
            case 'CREDIT_CARD':
                subName = 'CREDIT CARD';
                break;
            case 'GiftCard':
                subName = 'GIFT CERTIFICATE';
                break;
            default:
                subName = paymentMethod;
                break;
        }
        return subName;
    },

    getOrdersForExport: function (ordernumbers) {
    	var orderQueryRefinement = '';
    	if(ordernumbers){
            var orderQueryRefinements = !empty(ordernumbers)?ordernumbers.split("|"):[];            

            for (var i = 0; i < orderQueryRefinements.length; i++) {
                if (i === 0) {
                    orderQueryRefinement = "orderNo LIKE '" + orderQueryRefinements[i] + "'";
                } else {
                    orderQueryRefinement += " OR orderNo LIKE '" + orderQueryRefinements[i] + "'";
                }
            }
    	}
    	
    	var querySQL;
        
        if(empty(orderQueryRefinement)){
        	querySQL = "((exportStatus={0} AND exportStatus!={1}) AND exportStatus!={2} AND status!={3})";
        } else {
        	querySQL = "((exportStatus={0} AND exportStatus!={1}) AND exportStatus!={2} AND status!={3}) AND (" + orderQueryRefinement + ")";
        }
    	
    	return SystemObjectMgr.querySystemObjects('Order', querySQL,
                'orderNo asc',
                dw.order.Order.EXPORT_STATUS_READY,
                dw.order.Order.EXPORT_STATUS_NOTEXPORTED,
                dw.order.Order.EXPORT_STATUS_FAILED,
                dw.order.Order.ORDER_STATUS_CANCELLED);      
    },

    getCSVStreamWriter: function () {
        var directory = new File(File.IMPEX + File.SEPARATOR + 'src/orderexport/');
        if (!directory.exists()) {
            directory.mkdirs();
        }

        var file = new File(File.IMPEX + File.SEPARATOR + 'src/orderexport/orderexport_' + Site.getCurrent().getID().toLowerCase() + '_' + StringUtils.formatCalendar(new Calendar(), 'yyyyMMdd_HHmmss'));

        //Create new file.
        file.createNewFile();

        var fileWriter = new FileWriter(file, 'UTF-8');
        var csvStreamWriter = CSVStreamWriter(fileWriter, '\\t');
        return csvStreamWriter;
    },

    getCSVHeader: function () {
        return ['ORDER',
            'CUSTOMER',
            'STATUS',
            'SUBTOTAL',
            'TAX',
            'SHIPTOTAL',
            'SHIPCOST',
            'SHIPMETHOD',
            'SHIPWEIGHT',
            'SHIPLOC',
            'GIFTWRAP',
            'ADD_ADDRES',
            'ADD_CHARGE',
            'WEIGHTSUR',
            'TOTAL',
            'MERCH',
            'SC',
            'SUC',
            'DEFERRED',
            'MULTIADD',
            'DELETED',
            'ORDERDATE',
            'BILLCODE',
            'BILLFIRST',
            'BILLLAST',
            'BILLADD1',
            'BILLADD2',
            'BILLPO',
            'BILLCITY',
            'BILLSTATE',
            'BILLCOUNTR',
            'BILLZIP',
            'BILLCOMP',
            'BILLPHONE',
            'BILLCOMP2',
            'BILLEMAIL',
            'ORDERSHIP',
            'ORDERSHIPD',
            'SHIPCODE',
            'SHIPFIRST',
            'SHIPLAST',
            'SHIPADD1',
            'SHIPADD2',
            'SHIPPO',
            'SHIPCITY',
            'SHIPSTATE',
            'SHIPCOUNTR',
            'SHIPZIP',
            'SHIPCOMP',
            'SHIPPHONE',
            'SHIPCOMP2',
            'SHIPEMAIL',
            'ORDSTATUS',
            'ORDSHIPCOD',
            'ORDSHIPNAM',
            'ORDSHIPDES',
            'ORDSUB',
            'ORDTOTAL',
            'ORDSHIPTOT',
            'ORDSHIPCOS',
            'ORDSHIPME',
            'ORDSHIPWGT',
            'ORDSHIPTAX',
            'ORDSHIPGIF',
            'ORDSHIPADD',
            'ORDWGTSUR',
            'ORDMERCTOT',
            'ORDDELETE',
            'BULKFEE',
            'ITEMS',
            'PAYMENTS',
            'AUTHINFO'
        ];
    }
};
