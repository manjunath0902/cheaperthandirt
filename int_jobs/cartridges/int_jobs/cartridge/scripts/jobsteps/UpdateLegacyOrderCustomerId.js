'use strict';

var Status = require('dw/system/Status');
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var CSVStreamReader = require('dw/io/CSVStreamReader');
var OrderMgr = require('dw/order/OrderMgr');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Logger = require('dw/system/Logger').getLogger('legacyorder-customer-update');
var CustomerMgr = require('dw/customer/CustomerMgr');

var process = function(args) {

    try {

        //Directory
        var directory = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'legacyorderupdate');
        var files = directory.listFiles().iterator();

        if (!files.hasNext()) {
            Logger.info('UpdateLegacyOrderCustomerId.js : No files found for import!');
            return new Status(Status.OK, 'OK');
        }

        var file, csvStreamReader, fileReader, order, nextRecord;
        while (files.hasNext()) {
            file = files.next();
            if (file.isDirectory()) {
                continue;
            }

            Logger.info('UpdateLegacyOrderCustomerId.js : Processing the file...' + file.getName());

            fileReader = new FileReader(file, 'UTF-8');
            csvStreamReader = new CSVStreamReader(fileReader, '\t');

            //Skip header.
            csvStreamReader.readNext();

            while (nextRecord = csvStreamReader.readNext()) {
                order = OrderMgr.getOrder(nextRecord[0]);
                if (order) {
                    Logger.info('UpdateLegacyOrderCustomerId.js : Processing the order...' + order.getOrderNo());
                } else {
                    Logger.info('UpdateLegacyOrderCustomerId.js : Skipping the order...' + nextRecord[1] + '. Order does not exists!.');
                    continue;
                }
              //Update the customer object. 
                var customer = CustomerMgr.getCustomerByCustomerNumber(nextRecord[2]);
                if (customer !== null){
                    order.setCustomer(customer);
                    order.setCustomerNo(nextRecord[2]);
                } else {
                	order.setCustomerNo(nextRecord[2]);
                }
            }

            //Close reader.
            csvStreamReader.close();
            fileReader.close();

            //Move the file to archive.
            var archiveDirectory = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'legacyorderupdate' + File.SEPARATOR + 'archive' + File.SEPARATOR + StringUtils.formatCalendar(new Calendar(), 'yyyy-MM-dd'));
            archiveDirectory.mkdirs();
            var archiveFile = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'legacyorderupdate' + File.SEPARATOR + 'archive' + File.SEPARATOR + StringUtils.formatCalendar(new Calendar(), 'yyyy-MM-dd') + File.SEPARATOR + file.getName());
            file.renameTo(archiveFile);
        }

        return new Status(Status.OK, 'OK');

    } catch (e) {
        Logger.error('UpdateLegacyOrderCustomerId.js : Error while importing legacy order customer id...' + e.message);
        return new Status(Status.ERROR, 'ERROR', e.message);
    }
};

/*
 * Job exposed methods
 */
/** Imports order status to SFCC.
 * @see module:steps/UpdateLegacyOrderCustomerId~process */
exports.process = process;
