'use strict';

var orderExportHelper = require('*/cartridge/scripts/helpers/orderExportHelper');
var OrderExportModel = require('*/cartridge/scripts/models/orderExport');

var Status = require('dw/system/Status');
var Site = require('dw/system/Site');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var RandomAccessFileReader = require('dw/io/RandomAccessFileReader');
var FileReader = require('dw/io/FileReader');
var OrderMgr = require('dw/order/OrderMgr');
var Order = require('dw/order/Order');
var Logger = require('dw/system/Logger').getLogger('order-export');

var generate = function(args) {

    try {
        //Query orders for export
        var ordersForExport = orderExportHelper.getOrdersForExport(args.ExportOrderRange);
        if (ordersForExport.getCount() === 0) {
            return new Status(Status.OK, 'OK', 'No orders found for export!');
        }

        //Max number of orders per file.
        var maximumOrdersPerFile = args.MaxOrdersPerFile;
        if (!maximumOrdersPerFile) {
            maximumOrdersPerFile = ordersForExport.getCount();
        }
        
        if (maximumOrdersPerFile > ordersForExport.getCount()){
        	maximumOrdersPerFile = ordersForExport.getCount();
        }

        //Create directory
        var directory = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'orderexport');
        directory.mkdirs();

        // Prefix the file name
        var fileName = 'order_export_';
        if (args.FileNamePrefix) {
            fileName = args.FileNamePrefix + '_order_export_';
        }
        
      var tmpMaximumOrdersPerFile = maximumOrdersPerFile;
      
      while (ordersForExport.hasNext() && maximumOrdersPerFile > 0) {
            var successfulOrders;
	        if (maximumOrdersPerFile === tmpMaximumOrdersPerFile) { 
		    	//Create file
		        var file = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'orderexport' + File.SEPARATOR + fileName + Site.getCurrent().getID().toLowerCase() + '_' + StringUtils.formatCalendar(new Calendar(), 'yyyyMMdd_HHmmssSSS') + '' + '.tsv');
		        file.createNewFile();
		
		        //Get Stream Writer
		        var fileWriter = new FileWriter(file, 'UTF-8');
		        var csvStreamWriter = new CSVStreamWriter(fileWriter, '\t');
	
		        //Write header
		        var csvHeader = orderExportHelper.getCSVHeader();
		        csvStreamWriter.writeNext(csvHeader);
		        
		        successfulOrders = [];
	
	    	} // end start index check
	        //Write order content to TSV file.
	        var order, orderExportModelObject, orderRecord, shipment, shipments;
	        
	            order = ordersForExport.next();
	
	            Logger.info('ExportOrders.js : Processing the order...' + order.getOrderNo());
	
	            /*For the order with multiple shipments we need to send the same order totals for both the rows and 
	              we will send payment information only for the first row and send blank for the second row.
	             */
	            shipments = order.getShipments().iterator();
	            while (shipments.hasNext()) {
	                shipment = shipments.next();
	                orderExportModelObject = new OrderExportModel(order, shipment);
	                orderRecord = [];
	                for (var index in csvHeader) {
	                    orderRecord.push(orderExportModelObject[csvHeader[index]]);
	                }
	                csvStreamWriter.writeNext(orderRecord);
	            }
	
	          //Flush file content
	          fileWriter.flush();
	
	          successfulOrders.push(order.getOrderNo());
	
	          maximumOrdersPerFile--;
	          if (maximumOrdersPerFile <= 0) {
		          //Close the writer.
		            csvStreamWriter.close();
		            fileWriter.close();
		            
		            // Encrypt file
		            if (args.EnableFileEncryption) {
		                encryptFile(file, directory);
		            }
		
		            //Mark orders as exported.
		            for (let i = 0; i < successfulOrders.length > 0; i++) {
			                order = OrderMgr.getOrder(successfulOrders[i]);
			                order.setExportStatus(Order.EXPORT_STATUS_EXPORTED);
		            }		           
		            maximumOrdersPerFile = tmpMaximumOrdersPerFile;
	          } // end if
        }       

        return new Status(Status.OK, 'OK');

    } catch (e) {
        Logger.error('ExportOrders.js : Error while exporting orders...' + e.message);
        return new Status(Status.ERROR, 'ERROR', e.message);
    }
};

/**
 * Method to encrypt order export file
 * @input orderXML : String
 * @param orderfileencryptkey: String Encryption key configured in Business manager
 */
function encryptFile(orderExportFile, orderExportDir) {

	var tmpExportFile = orderExportFile.name + ".tmp";
	var tmpExportFilePath = "";
	tmpExportFilePath = orderExportDir.fullPath + tmpExportFile;

	var tmpFile = new File(tmpExportFilePath);
	var reader = new RandomAccessFileReader(orderExportFile);
	//var reader = new FileReader(orderExportFile);
	var writer = new FileWriter(tmpFile,"UTF-8");
	var done = false;
    var publicKeyRef = new dw.crypto.CertificateRef(Site.getCurrent().getCustomPreferenceValue('orderExport_certificateAliasName'));
	try {
		// generate a random session key (32 bytes) and an initialization vector (16 bytes)
		var random = new dw.crypto.SecureRandom();
		var sessionKey = random.nextBytes(32);
		var IV = random.nextBytes(16);
		var sessionKeyString = dw.crypto.Encoding.toBase64(sessionKey);
		var IVString = dw.crypto.Encoding.toBase64(IV);

		// encrypt the session key with RSA using the client's public key
		var encryptedSessionKey = new dw.crypto.Cipher().encryptBytes(sessionKey,publicKeyRef,'RSA','',0);
		var encryptedSessionKeyString = dw.crypto.Encoding.toBase64(encryptedSessionKey);
		
		// encrypt the orders file with AES 256, Cipher feedback mode (CFB), no padding
		var transform = "AES/CBC/PKCS5Padding";
		var BLOCK_SIZE = 8 * (dw.util.Bytes.MAX_BYTES / 10);
		var orders;
			// write out base64 encoded encrypted session key, IV, and the encrypted orders
			writer.write(encryptedSessionKeyString);
			writer.write(IVString);
			while (!done) {
				try {
					/** Throw error java.lang.StringIndexOutOfBoundsException: String index out of range: -1**/
					orders = reader.readBytes(BLOCK_SIZE);
				} catch (e) {
					Logger.error('ExportOrders.js : Error occured while reading bytes from file:' + e.message);
				}
				if (empty(orders)) {
					done = true;
				} else {
					var encryptedOrders = new dw.crypto.Cipher().encrypt(orders.toString(),sessionKeyString,transform,IVString,1);
					//var encryptedOrdersString = dw.crypto.Encoding.toBase64(dw.util.Bytes(encryptedOrders));
					writer.write(encryptedOrders);
				}
			}
	} catch (e) {
		Logger.error('ExportOrders.js: Error occurred while encrypting file: ' +e.message);
		return new status(Status.ERROR);
	} finally {
		writer.flush();
		writer.close();
		reader.close();
	}

	orderExportFile.remove();
    tmpFile.renameTo(orderExportFile);
}

/*
 * Job exposed methods
 */
/** Triggers the generation of order feed. 
 * @see module:steps/ExportOrders~generate */
exports.generate = generate;
