'use strict';

var Status = require('dw/system/Status');
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var CSVStreamReader = require('dw/io/CSVStreamReader');
var OrderMgr = require('dw/order/OrderMgr');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Logger = require('dw/system/Logger').getLogger('order-status-import');

var process = function(args) {

    try {

        //Directory
        var directory = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'orderstatus');
        var files = directory.listFiles().iterator();

        if (!files.hasNext()) {
            Logger.info('OrderStatusImport.js : No files found for import!');
            return new Status(Status.OK, 'OK');
        }

        var file, csvStreamReader, fileReader, order, nextRecord;
        while (files.hasNext()) {
            file = files.next();
            if (file.isDirectory()) {
                continue;
            }

            Logger.info('OrderStatusImport.js : Processing the file...' + file.getName());

            fileReader = new FileReader(file, 'UTF-8');
            csvStreamReader = new CSVStreamReader(fileReader, '\t');

            //Skip header.
            csvStreamReader.readNext();

            while (nextRecord = csvStreamReader.readNext()) {
                 try {
                     order = OrderMgr.getOrder(nextRecord[1]);
                     if (order) {
                         Logger.info('OrderStatusImport.js : Processing the order...' + order.getOrderNo());
                     } else {
                         Logger.info('OrderStatusImport.js : Skipping the order...' + nextRecord[1] + '. Order does not exists!.');
                         continue;
                     }

                     //Update MOM Order. 
                     order.custom.MOMOrder = nextRecord[0];

                     //Update Notation.
                     var orderStatus = nextRecord[2];
                     order.custom.customOrderStatus = orderStatus;


                     //Update SFState.
                     var shipStatus = getShipStatus(nextRecord[19]);
                     order.setShippingStatus(shipStatus);

                     //Set shipConfirmationEmailSent
                     if (shipStatus === 2) {
                         order.custom.shipConfirmationEmailSent = true;
                     }
                     try {
                         // trim the value and convert to lowercase to match all possible values.
                         orderStatus = orderStatus.toLowerCase().replace(/ /g, '');
                         if (orderStatus.indexOf('canceled') > -1) {
                            order.setStatus(6); //  cancel the order
                         }
                     } catch (e) {
                         Logger.info('Order Status not set for Order : ' + order.orderNo);
                     }
                 } catch (e) {
                     var errorMsg = e.fileName + "| line#:"+ e.lineNumber + "| Message:" + e.message+ "| Stack:" + e.stack;
                     Logger.info('OrderStatusImport.js : Skipping the order...' + nextRecord[1] + '. Due to exception!.');
                     Logger.info('Stack Trace : ' + errorMsg);
                 }
            }

            //Close reader.
            csvStreamReader.close();
            fileReader.close();

            //Move the file to archive.
            var archiveDirectory = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'orderstatus' + File.SEPARATOR + 'archive' + File.SEPARATOR + StringUtils.formatCalendar(new Calendar(), 'yyyy-MM-dd'));
            archiveDirectory.mkdirs();
            var archiveFile = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'orderstatus' + File.SEPARATOR + 'archive' + File.SEPARATOR + StringUtils.formatCalendar(new Calendar(), 'yyyy-MM-dd') + File.SEPARATOR + file.getName());
            file.renameTo(archiveFile);
        }

        return new Status(Status.OK, 'OK');

    } catch (e) {
        Logger.error('OrderStatusImport.js : Error while importing order status...' + e.message);
        return new Status(Status.ERROR, 'ERROR', e.message);
    }
};

var getShipStatus = function(SFState) {
    var shipStatus = 0;
    SFState = SFState.toLowerCase().replace(/ /g, ''); // eslint-disable-line
    switch (SFState) {
        case 'notshipped':
            shipStatus = 0;
            break;
        case 'shipped':
            shipStatus = 2;
            break;
    }
    return shipStatus;
}

/*
 * Job exposed methods
 */
/** Imports order status to SFCC.
 * @see module:steps/OrderStatusImport~process */
exports.process = process;
