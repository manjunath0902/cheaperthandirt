'use strict';

var CustomerMgr = require('dw/customer/CustomerMgr');
var File = require('dw/io/File');
var FileWriter = require('dw/io/FileWriter');
var CSVStreamWriter = require('dw/io/CSVStreamWriter');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Status = require('dw/system/Status');

var Logger = require('dw/system/Logger').getLogger('customer-legacy-export');

var generate = function(args) {
	 
	//Create file
		var file = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'legacycustomerexport' + File.SEPARATOR + 'legacycustomerexp' + '-' + StringUtils.formatCalendar(new Calendar(), 'yyyyMMdd_HHmmssSSS') + '' + '.csv');
		file.createNewFile();
		
		//Get Stream Writer
		var fileWriter = new FileWriter(file, 'UTF-8');
		var csvStreamWriter = new CSVStreamWriter(fileWriter, '\t');
    try {
        var customer;
		var customers = CustomerMgr.searchProfiles('', 'creationDate asc');
		
	
		//Write header
		 var csvHeader = ['CUSTOMERID','EMAILID','LOGINID'];
		 csvStreamWriter.writeNext(csvHeader);
		while (customers.hasNext()) { 
            var customer = customers.next();
            csvStreamWriter.writeNext([customer.customerNo,customer.email,customer.credentials.login]);
            
          }
        return new Status(Status.OK, 'OK');

    } catch (e) {
        Logger.error('ExportCustomer.js : Error while exporting customers...' + e.message);
        return new Status(Status.ERROR, 'ERROR', e.message);
    } finally {
    	
		fileWriter.flush();
		fileWriter.close();		
	}
};



/*
 * Job exposed methods
 */
/** Triggers the generation of order feed. 
 * @see module:steps/ExportCustomers~generate */
exports.generate = generate;
