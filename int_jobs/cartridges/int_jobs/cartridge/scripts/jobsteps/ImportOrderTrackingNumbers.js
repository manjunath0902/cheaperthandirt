'use strict';

var Status = require('dw/system/Status');
var File = require('dw/io/File');
var FileReader = require('dw/io/FileReader');
var CSVStreamReader = require('dw/io/CSVStreamReader');
var OrderMgr = require('dw/order/OrderMgr');
var StringUtils = require('dw/util/StringUtils');
var Calendar = require('dw/util/Calendar');
var Logger = require('dw/system/Logger').getLogger('order-status-import');

var process = function(args) {

    try {

        //Directory
        var directory = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'ordertrackingnumbers');
        var files = directory.listFiles().iterator();

        if (!files.hasNext()) {
            Logger.info('ImportOrderTrackingNumbers.js : No files found for import!');
            return new Status(Status.OK, 'OK');
        }

        var file, csvStreamReader, fileReader, order, nextRecord, trackingNumberData, currentTrackingNumberData;
        while (files.hasNext()) {
            file = files.next();
            if (file.isDirectory()) {
                continue;
            }

            Logger.info('ImportOrderTrackingNumbers.js : Processing the file...' + file.getName());

            fileReader = new FileReader(file, 'UTF-8');
            csvStreamReader = new CSVStreamReader(fileReader, '\t');

            //Skip header.
            csvStreamReader.readNext();

            while (nextRecord = csvStreamReader.readNext()) {
                try {
                    order = OrderMgr.getOrder(nextRecord[5]);
                    if (order) {
                        Logger.info('ImportOrderTrackingNumbers.js : Processing the order...' + order.getOrderNo());
                    } else {
                        Logger.info('ImportOrderTrackingNumbers.js : Skipping the order...' + nextRecord[5] + '. Order does not exists!.');
                        continue;
                    }

                    //Update trackingNumber.
                    if (order.custom.trackingNumber) {
                        order.custom.trackingNumber += ',' + nextRecord[7];
                    } else {
                        order.custom.trackingNumber = nextRecord[7];
                    }

                    //Update trackingNumberData.
                    trackingNumberData = {};
                    trackingNumberData.trackNo = nextRecord[7];
                    trackingNumberData.shipmethod = nextRecord[4];
                    trackingNumberData.shipemailsent = 'N';
                    trackingNumberData.shipmentType = getShipmentType(order, nextRecord);
                    if (order.custom.trackingNumberData) {
                        currentTrackingNumberData = JSON.parse(order.custom.trackingNumberData);
                        currentTrackingNumberData.push(trackingNumberData);
                        order.custom.trackingNumberData = JSON.stringify(currentTrackingNumberData);
                    } else {
                        order.custom.trackingNumberData = JSON.stringify([trackingNumberData]);
                    }

                    //Set newTrackingNumAvailable
                    order.custom.newTrackingNumAvailable = true;
                } catch (e) {
                    var errorMsg = e.fileName + "| line#:"+ e.lineNumber + "| Message:" + e.message+ "| Stack:" + e.stack;
                    Logger.info('ImportOrderTrackingNumbers.js : Order Tracking Number Job : Skipping the order...' + nextRecord[5] + '. Due to exception!.');
                    Logger.info('Stack Trace : ' + errorMsg);
                }
            }

            //Close reader.
            csvStreamReader.close();
            fileReader.close();

            //Move the file to archive.
            var archiveDirectory = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'ordertrackingnumbers' + File.SEPARATOR + 'archive' + File.SEPARATOR + StringUtils.formatCalendar(new Calendar(), 'yyyy-MM-dd'));
            archiveDirectory.mkdirs();
            var archiveFile = new File(File.IMPEX + File.SEPARATOR + 'src' + File.SEPARATOR + 'ordertrackingnumbers' + File.SEPARATOR + 'archive' + File.SEPARATOR + StringUtils.formatCalendar(new Calendar(), 'yyyy-MM-dd') + File.SEPARATOR + file.getName());
            file.renameTo(archiveFile);
        }

        return new Status(Status.OK, 'OK');

    } catch (e) {
        Logger.error('ImportOrderTrackingNumbers.js : Error while importing order tracking numbers...' + e.message);
        return new Status(Status.ERROR, 'ERROR', e.message);
    }
};


var getShipmentType = function(order, nextRecord) {
    var shipmentType = '';
    var shipment;
    var address;
    var shipments = order.getShipments().iterator();
    while (shipments.hasNext()) {
        shipment = shipments.next();
        address = shipment.getShippingAddress();
        if (address.getAddress1().equalsIgnoreCase(nextRecord[11]) && address.getCity().equalsIgnoreCase(nextRecord[13])) {
            if (shipment.custom.shipmentType === 'instore') {
                shipmentType = 'InStore';
            } else {
                shipmentType = 'ShipToHome';
            }
            break;
        }
    }
    return shipmentType;
};

/*
 * Job exposed methods
 */
/** Imports order status to SFCC.
 * @see module:steps/ImportOrderTrackingNumbers~process */
exports.process = process;
