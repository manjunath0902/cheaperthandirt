'use strict';

var orderExportHelper = require('*/cartridge/scripts/helpers/orderExportHelper');
var TotalsModel = require('*/cartridge/models/totals');

/**
 * OrderExport class that represents the order record in the export file.
 * @param {dw.order.Order} order object
 * @constructor
 */
function orderExport (order, shipment) {

    var billingAddress = order.getBillingAddress();
    var shippingAddress = shipment.getShippingAddress();

    var totalsModel = new TotalsModel(order);

    //Prepare order export object
    this.ORDER = (shipment.getID() === 'me') ? order.getOrderNo() : (order.getOrderNo() + '_1'); //Assumes maximum two shipments at any given point in time. 
    this.CUSTOMER = orderExportHelper.getCustomer(order);
    this.STATUS = '';
    this.SUBTOTAL = totalsModel.subTotal.substr(1); //.substr(1) skips currency symbol i.e $.
    this.TAX = totalsModel.totalTax.substr(1);
    this.SHIPTOTAL = orderExportHelper.getShipTotal(shipment);
    this.SHIPCOST = '';
    this.SHIPMETHOD = '';
    this.SHIPWEIGHT = orderExportHelper.getShipWeight(shipment);
    this.SHIPLOC = '';
    this.GIFTWRAP = '';
    this.ADD_ADDRES = '';
    this.ADD_CHARGE = '';
    this.WEIGHTSUR = '';
    this.TOTAL = totalsModel.orderTotalValue;
    this.MERCH = totalsModel.subTotal.substr(1);
    this.SC = orderExportHelper.getSC(order);
    this.SUC = orderExportHelper.getSUC(order);
    this.DEFERRED = '';
    this.MULTIADD = '';
    this.DELETED = '';
    this.ORDERDATE = order.getCreationDate().toISOString();
    this.BILLCODE = '';
    this.BILLFIRST = billingAddress.getFirstName();
    this.BILLLAST = billingAddress.getLastName();
    this.BILLADD1 = billingAddress.getAddress1();
    this.BILLADD2 = billingAddress.getAddress2();
    this.BILLPO = orderExportHelper.isPOBoxAddress(billingAddress.getAddress1());
    this.BILLCITY = billingAddress.getCity();
    this.BILLSTATE = billingAddress.getStateCode();
    this.BILLCOUNTR = billingAddress.getCountryCode().getValue();
    this.BILLZIP = billingAddress.getPostalCode();
    this.BILLCOMP = billingAddress.getCompanyName();
    this.BILLPHONE = billingAddress.getPhone();
    this.BILLCOMP2 = '';
    this.BILLEMAIL = order.getCustomerEmail();
    this.ORDERSHIP = '';
    this.ORDERSHIPD = '';
    this.SHIPCODE = '';
    this.SHIPFIRST = shippingAddress.getFirstName();
    this.SHIPLAST = shippingAddress.getLastName();
    this.SHIPADD1 = shippingAddress.getAddress1();
    this.SHIPADD2 = shippingAddress.getAddress2();
    this.SHIPPO = orderExportHelper.isPOBoxAddress(shippingAddress.getAddress1());
    this.SHIPCITY = shippingAddress.getCity();
    this.SHIPSTATE = shippingAddress.getStateCode();
    this.SHIPCOUNTR = shippingAddress.getCountryCode().getValue();
    this.SHIPZIP = shippingAddress.getPostalCode();
    this.SHIPCOMP = shippingAddress.getCompanyName();
    this.SHIPPHONE = shippingAddress.getPhone();
    this.SHIPCOMP2 = '';
    this.SHIPEMAIL = order.getCustomerEmail();
    this.ORDSTATUS = '';
    this.ORDSHIPCOD = shipment.getShippingMethodID();
    this.ORDSHIPNAM = '';
    this.ORDSHIPDES = '';
    this.ORDSUB = totalsModel.subTotal.substr(1);
    this.ORDTOTAL = totalsModel.orderTotalValue;
    this.ORDSHIPTOT = '';
    this.ORDSHIPCOS = '';
    this.ORDSHIPME = '';
    this.ORDSHIPWGT = orderExportHelper.getShipWeight(shipment);
    this.ORDSHIPTAX = '';
    this.ORDSHIPGIF = '';
    this.ORDSHIPADD = shipment.custom.shipmentType === 'instore' && totalsModel.hasFirearm ? totalsModel.firearmHandlingFee.substr(1) : '';
    this.ORDWGTSUR = '';
    this.ORDMERCTOT = totalsModel.subTotal.substr(1);
    this.ORDDELETE = '';
    this.BULKFEE = totalsModel.hasBulkFee ? totalsModel.bulkFee.substr(1) : '';
    this.ITEMS = orderExportHelper.getItems(shipment);
    this.PAYMENTS = (shipment.getID() === 'me') ? orderExportHelper.getPayments(order) : '';
    this.AUTHINFO = (shipment.getID() === 'me') ? orderExportHelper.getAuthInfo(order) : '';
}

module.exports = orderExport;
