'use strict';

/**
 *
 * @module services/upsAddress
 */

// service
var UPSServiceObject = require('*/cartridge/scripts/init/UPSService.js');
var upsHelpers = require('*/cartridge/scripts/helpers/upsHelpers');

var svcResult = require('dw/svc/Result');
var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');


/**
 * @return {Object} service status
 * @param {Object} basket - basket object
 * @param {Object} userAddress - address object
 * @param {boolean} multipleShippingMethods - true then collect all methods for creating request
 */
function UPSShipRate(basket, userAddress, multipleShippingMethods) {
    var service;
    var result;
    var responseObj = {};
    var currentBasket = basket;

    var configs = upsHelpers.getHeadersConfigs();
    var params = {};
    params.headers = {
        accessKey: configs.upsAccessKey,
        userName: configs.upsUserName,
        password: configs.upsPassword
    };

    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var shippingMethods = upsHelpers.getEligibleShipRateMethods(currentBasket, multipleShippingMethods);
    var shipToHomeShipment = COHelpers.getShipToHomeShipment(currentBasket);

    params.body = upsHelpers.createShiprateSvcObj(currentBasket, userAddress, shippingMethods);

    service = UPSServiceObject.get('shiprate');

    // Execute the service
    result = service.call(params);

    // Check for service unavailability.
    if (result.status === svcResult.SERVICE_UNAVAILABLE) {
        responseObj.error = true;
        responseObj.errorType = 'service unavailable';
        Logger.error('UPS ship rate service is unavailable at the momment');
        return responseObj;
    }

    if (result !== null && result.error) {
        return result;
    }

    // Parse the response
    try {
        var shipRateResponseJSON;
        var responseStatusCode;

        shipRateResponseJSON = JSON.parse(result.object.response);
        responseStatusCode = shipRateResponseJSON.RateResponse.Response.ResponseStatus.Code.toString();

        if (responseStatusCode === '1') {
            var ratedShipments = shipRateResponseJSON.RateResponse.RatedShipment;
            var shipRates = {};
            for (var i = 0; i < ratedShipments.length; i++) {
                var upsShipment = ratedShipments[i];
                for (var j = 0; j < shippingMethods.length; j++) {
                    var method = shippingMethods[j];

                    if (upsShipment.Service.Code === method.custom.service_code_ups) {
                        shipRates[upsShipment.Service.Code] = upsShipment.TotalCharges.CurrencyCode + '|' + upsShipment.TotalCharges.MonetaryValue;
                    }
                }
            }
            delete session.custom.notEligibleShipMethod; // eslint-disable-line
            for (var k = 0; k < shippingMethods.length; k++) {
                var shipMethod = shippingMethods[k];
                if (!('service_code_ups' in shipMethod.custom && shipRates[shipMethod.custom.service_code_ups])) {
                    session.custom.notEligibleShipMethod += shipMethod.ID; // eslint-disable-line
                }
            }
            Transaction.wrap(function () {
                shipToHomeShipment.custom.calculatedShipRate = JSON.stringify(shipRates);
            });
            responseObj.error = false;
        } else {
            session.custom.restrictedShipMethods = true; // eslint-disable-line
        }
    } catch (e) {
        session.custom.restrictedShipMethods = true; // eslint-disable-line
        Logger.error('Error occurred while executing UPSRequest: ' + e.message);
        responseObj.error = true;
    }

    return responseObj;
}

module.exports = UPSShipRate;
