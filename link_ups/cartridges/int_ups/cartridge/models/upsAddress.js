'use strict';

/**
 *
 * @module services/upsAddress
 */

// service
var UPSServiceObject = require('*/cartridge/scripts/init/UPSService.js');
var upsHelpers = require('*/cartridge/scripts/helpers/upsHelpers');

var svcResult = require('dw/svc/Result');
var ArrayList = require('dw/util/ArrayList');
var Logger = require('dw/system/Logger');


/**
 * @return {JSON} service status
 * @param {Object} reqAddress - address object
 */
function UPSAddress(reqAddress) {
    var service;
    var result;
    var responseObj = {};

    var configs = upsHelpers.getHeadersConfigs();
    var params = {};
    params.headers = {
        accessKey: configs.upsAccessKey,
        userName: configs.upsUserName,
        password: configs.upsPassword
    };

    service = UPSServiceObject.get('address');

    params.body = upsHelpers.createAddressValidationSvcObj(service, reqAddress);

    // Execute the service
    result = service.call(params);

    // Check for service unavailability.
    if (result.status === svcResult.SERVICE_UNAVAILABLE) {
        responseObj.IsMultiAddress = 'true';
        responseObj.IsExactAddress = false;
        responseObj.error = true;
        responseObj.errorType = 'service unavailable';
        Logger.error('UPS address validation service is unavailable at the momment');
        return responseObj;
    }

    if (result !== null && result.error) {
        responseObj.IsMultiAddress = 'true';
        responseObj.IsExactAddress = false;
        responseObj.error = true;
        responseObj.errorType = 'server error';
        return responseObj;
    }

    // Create result Object
    responseObj.RequestAddress = reqAddress;

    // Parse the response
    try {
        var addressValidationResponseJSON;
        var responseStatusCode;

        // JSON response
        addressValidationResponseJSON = JSON.parse(result.object.response);
        responseStatusCode = addressValidationResponseJSON.XAVResponse.Response.ResponseStatus.Code.toString();

        if (responseStatusCode == 1) {    // eslint-disable-line
            var responseAddrs = new ArrayList();
            var candidates = addressValidationResponseJSON.XAVResponse.Candidate;
            var addressItem;
            for (var j = 0; j < Object.keys(candidates).length; j++) {
                addressItem = candidates[j];
                var addrItem;
                if (Object.keys(candidates).length === 1 && addressItem === undefined) {
                    addressItem = candidates.AddressKeyFormat;
                    addrItem = upsHelpers.addressItem(addressItem.AddressLine, addressItem.PoliticalDivision2, addressItem.PoliticalDivision1, addressItem.PostcodePrimaryLow, addressItem.PostcodeExtendedLow, addressItem.CountryCode);
                    responseAddrs.add(addrItem);
                } else {
                    // without PO BOX
                    if (typeof addressItem.AddressLine !== 'undefined') {
                        if (responseAddrs.size() < 10) {
                            addrItem = upsHelpers.addressItem(addressItem.AddressLine, addressItem.PoliticalDivision2, addressItem.PoliticalDivision1, addressItem.PostcodePrimaryLow, addressItem.PostcodeExtendedLow, addressItem.CountryCode);
                            responseAddrs.add(addrItem);
                        } else {
                            break;
                        }
                    }

                    // PO BOX
                    if (typeof addressItem.AddressKeyFormat !== 'undefined') {
                        if (responseAddrs.size() < 10) {
                            addrItem = upsHelpers.addressItem(addressItem.AddressKeyFormat.AddressLine, addressItem.AddressKeyFormat.PoliticalDivision2, addressItem.AddressKeyFormat.PoliticalDivision1, addressItem.AddressKeyFormat.PostcodePrimaryLow, addressItem.AddressKeyFormat.PostcodeExtendedLow, addressItem.AddressKeyFormat.CountryCode);
                            responseAddrs.add(addrItem);
                        } else {
                            break;
                        }
                    }
                }
            }

            if (responseAddrs.size() === 1) {
                var address = reqAddress;
                var postalCode = addressItem.PostcodePrimaryLow + (addressItem.PostcodeExtendedLow ? '-' + addressItem.PostcodeExtendedLow : '');
                if (addressItem.AddressLine.toLowerCase() === address.address1.toLowerCase() && addressItem.PoliticalDivision2.toLowerCase() === address.city.toLowerCase() && addressItem.PoliticalDivision1.toLowerCase() === address.stateCode.toLowerCase() && postalCode === address.postalCode.substring(0, 10)) {
                    responseObj.IsExactAddress = true;
                }
            }

            if (responseAddrs.size() > 1) {
                responseObj.IsMultiAddress = 'true';
            } else {
                responseObj.IsMultiAddress = 'false';
            }
            responseObj.AddressList = responseAddrs;
            responseObj.error = false;
        } else {
            var error = addressValidationResponseJSON.Error;
            if (error != null) {
                responseObj.errorDescription = error.ErrorDescription != null ? error.ErrorDescription.toString() : null;
                responseObj.errorCode = error.ErrorCode != null ? error.ErrorCode.toString() : null;
                responseObj.errorSeverity = error.ErrorSeverity != null ? error.ErrorSeverity.toString() : null;
            }
            Logger.error('ERROR: ' + responseObj.errorCode + '   ' + responseObj.errorDescription);
            responseObj.error = true;
        }
        return responseObj;
    } catch (e) {
        Logger.error('Error occurred while executing UPSRequest: ' + e.message);
        responseObj.error = true;
        responseObj.IsExactAddress = true;
        return responseObj;
    }
}

module.exports = UPSAddress;
