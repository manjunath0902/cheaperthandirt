'use strict';

var base = module.superModule;

var ShippingMgr = require('dw/order/ShippingMgr');

var formatCurrency = require('*/cartridge/scripts/util/formatting').formatCurrency;

/**
 * Returns shippingCost property for a specific Shipment / ShippingMethod pair
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} shipment - a shipment of the current basket
 * @returns {string} String representation of Shipping Cost
 */
function getShippingCost(shippingMethod, shipment) {
    var shipmentShippingModel = ShippingMgr.getShipmentShippingModel(shipment);
    var shippingCost = {};
    var upsRates = shipment.custom.calculatedShipRate;
    var shippingRateEnabled = shippingMethod.custom.enable_ship_rate_ups;
    var upsCodeForShippingMethod = shippingMethod.custom.service_code_ups;
    if (upsRates !== null && shippingRateEnabled) {
        upsRates = JSON.parse(upsRates);
        if (upsRates !== null && Object.prototype.hasOwnProperty.call(upsRates, upsCodeForShippingMethod)) {
            shippingCost.amount = {
                value: parseFloat(upsRates[upsCodeForShippingMethod].split('|')[1]),
                currencyCode: upsRates[upsCodeForShippingMethod].split('|')[0]
            };
        } else {
            shippingCost = shipmentShippingModel.getShippingCost(shippingMethod);
        }
    } else {
        shippingCost = shipmentShippingModel.getShippingCost(shippingMethod);
    }


    return formatCurrency(shippingCost.amount.value, 'USD');
}

/**
 * Plain JS object that represents a DW Script API dw.order.ShippingMethod object
 * @param {dw.order.ShippingMethod} shippingMethod - the default shipment of the current basket
 * @param {dw.order.Shipment} [shipment] - a Shipment
 */
function ShippingMethodModel(shippingMethod, shipment) {
    base.call(this, shippingMethod, shipment);
    if (shipment) {
        // Optional model information available with 'shipment' parameter
        this.shippingCost = getShippingCost(shippingMethod, shipment);
    }
}

module.exports = ShippingMethodModel;
