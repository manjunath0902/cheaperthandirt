'use strict';

var base = module.superModule;

/**
 * Identifies if it is only store shipment
 * @param {dw.util.Collection} shipments - Current Basket shipments
 * @returns {boolean} Returns true if it is only store shipment
 */
function onlyStoreShipment(shipments) {
    var storeShipment = null;
    var isStoreShipmentOnly = false;
    try {
        var arrayHelper = require('*/cartridge/scripts/util/array');
        storeShipment = arrayHelper.find(shipments, function (shipment) {
            return shipment.custom.shipmentType === 'instore';
        });
        if (storeShipment && shipments.getLength() === 1) {
            isStoreShipmentOnly = true;
        }
    } catch (e) {
        isStoreShipmentOnly = false;
    }
    return isStoreShipmentOnly;
}

/**
 * Gets the Ship to Home Shipment
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {shipment} Shipment Object
 */
function getShipToHomeShipment(currentBasket) {
    var arrayHelper = require('*/cartridge/scripts/util/array');
    var existingShipment = null;
    if (currentBasket) {
        var shipments = currentBasket.getShipments();
        if (shipments.length) {
            existingShipment = arrayHelper.find(shipments, function (shipment) {
                return shipment.custom.shipmentType !== 'instore';
            });
        }
    }
    return existingShipment;
}

/**
 * Gets the Ship to Home Shipment
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {shipment} Shipment Object
 */
function getShipToStoreShipment(currentBasket) {
    var arrayHelper = require('*/cartridge/scripts/util/array');
    var existingShipment = null;
    if (currentBasket) {
        var shipments = currentBasket.getShipments();
        if (shipments.length) {
            existingShipment = arrayHelper.find(shipments, function (shipment) {
                return shipment.custom.shipmentType === 'instore';
            });
        }
    }
    return existingShipment;
}

/**
 * Gets the Ship to Home Shipment
 * @param {dw.order.Basket} currentBasket - The current basket
 * @param {preferredAddress} preferredAddress = preferredAddress object
 */
function calculateUPSShipRate(currentBasket, preferredAddress) {
    var shipToHomeShipment = getShipToHomeShipment(currentBasket);
    if (shipToHomeShipment !== null && shipToHomeShipment !== undefined) {
        var UPSShipRate = require('*/cartridge/models/upsShipRate');
        var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
        var Transaction = require('dw/system/Transaction');
        // Passing false as the third parameter
        // to get the ship rate for the selected shipping method only.
        var address = shipToHomeShipment.shippingAddress;
        if (preferredAddress !== null && preferredAddress !== undefined) {
            address = preferredAddress;
        }

        var upsResult = new UPSShipRate(currentBasket, address, true);
        if (upsResult !== undefined && !upsResult.error && shipToHomeShipment.shippingMethod !== null && shipToHomeShipment.shippingMethod.custom.enable_ship_rate_ups) {
            var upsRates = 'calculatedShipRate' in shipToHomeShipment.custom && shipToHomeShipment.custom.calculatedShipRate ? shipToHomeShipment.custom.calculatedShipRate : null;
            var upsCodeForShippingMethod = shipToHomeShipment.shippingMethod.custom.service_code_ups;
            if (upsRates != null) {
                upsRates = JSON.parse(upsRates);
            }
            Transaction.wrap(function () {
                if (upsRates !== null && upsRates[upsCodeForShippingMethod] !== null && upsRates[upsCodeForShippingMethod] !== undefined) {
                    var price = parseFloat(upsRates[upsCodeForShippingMethod].split('|')[1]);
                    if (price !== null) {
                        shipToHomeShipment.standardShippingLineItem.setPriceValue(price);
                    }
                }

                basketCalculationHelpers.calculateTotals(currentBasket);
            });
        }
    }
}

module.exports = {
    getFirstNonDefaultShipmentWithProductLineItems: base.getFirstNonDefaultShipmentWithProductLineItems,
    ensureNoEmptyShipments: base.ensureNoEmptyShipments,
    getProductLineItem: base.getProductLineItem,
    isShippingAddressInitialized: base.isShippingAddressInitialized,
    prepareShippingForm: base.prepareShippingForm,
    prepareBillingForm: base.prepareBillingForm,
    copyCustomerAddressToShipment: base.copyCustomerAddressToShipment,
    copyCustomerAddressToBilling: base.copyCustomerAddressToBilling,
    copyShippingAddressToShipment: base.copyShippingAddressToShipment,
    copyBillingAddressToBasket: base.copyBillingAddressToBasket,
    validateFields: base.validateFields,
    validateShippingForm: base.validateShippingForm,
    validateBillingForm: base.validateBillingForm,
    validatePayment: base.validatePayment,
    validateCreditCard: base.validateCreditCard,
    calculatePaymentTransaction: base.calculatePaymentTransaction,
    recalculateBasket: base.recalculateBasket,
    handlePayments: base.handlePayments,
    createOrder: base.createOrder,
    placeOrder: base.placeOrder,
    savePaymentInstrumentToWallet: base.savePaymentInstrumentToWallet,
    getRenderedPaymentInstruments: base.getRenderedPaymentInstruments,
    sendConfirmationEmail: base.sendConfirmationEmail,
    ensureValidShipments: base.ensureValidShipments,
    setGift: base.setGift,
    onlyStoreShipment: onlyStoreShipment,
    getShipToHomeShipment: getShipToHomeShipment,
    getShipToStoreShipment: getShipToStoreShipment,
    calculateUPSShipRate: calculateUPSShipRate
};
