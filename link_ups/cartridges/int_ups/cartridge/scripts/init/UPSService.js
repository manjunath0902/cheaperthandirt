'use strict';

/**
 * Initialize  HTTP service
 */

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
var upsHelpers = require('*/cartridge/scripts/helpers/upsHelpers');
var Site = require('dw/system/Site');
var enableLoggers = Site.getCurrent().getCustomPreferenceValue('enableRequestLoggers');
var Logger = require('dw/system/Logger');
var upsLogger = Logger.getLogger('UPS', 'UPS');

/**
 * Set the service as per the request type
 * @param {string} type - type of the service
 * @return {string} string - service profile
 */
function getServiceType(type) {
    if (type === 'address') {
        return 'ups.http.api';
    } else if (type === 'shiprate') {
        return 'ups.http.ship.rate';
    }
    return null;
}
/**
 *
 * UPS http Services
 *
 * @param {string} serviceType - type of the service
 * @return {HTTPService} the service
 */
function upsService(serviceType) {
    return LocalServiceRegistry.createService(getServiceType(serviceType), {

        createRequest: function (svc, params) {
            svc.setRequestMethod('POST');
            svc.addHeader('AccessLicenseNumber', params.headers.accessKey);
            svc.addHeader('Username', params.headers.userName);
            svc.addHeader('Password', params.headers.password);

            svc.addHeader('Content-Type', 'application/json');
            svc.addHeader('Accept', 'application/json');
            if (enableLoggers) {
                upsLogger.error('UPS Request-' + JSON.stringify(params.body));
            }
            return JSON.stringify(params.body);
        },

        parseResponse: function (service, response) {
            return upsHelpers.handleValidateAddressResponse(response);
        },

        /*eslint-disable */
        mockCall: function (service, request) {
            return {
                success: true
            };
        },
        /* eslint-enable */

        filterLogMessage: function (msg) {
            return msg;
        }
    });
}

module.exports = {
    get: upsService
};
