'use strict';

var Encoding = require('dw/crypto/Encoding');
var MessageDigest = require('dw/crypto/MessageDigest');
var Bytes = require('dw/util/Bytes');

/**
 * Creates the Json token object
 * @param {string} params - parametrs to convert
 * @returns {string} jsonToken - stringified value
 */
function createJsonToken(params) {
    var jsonToken = '';

    if (params !== null) {
        jsonToken += JSON.stringify(params);
    }

    return jsonToken;
}

/**
 * Creates hashed object
 * @param {string} params - parametrs for verification
 * @param {Object} req - request object
 * @returns {boolean} - if success then return true
 */
function validateUpdateUPSAddressHashing(params, req) {
    var encriptor = new MessageDigest('MD5');
    var jsonToken = createJsonToken(params);

    var hash = Encoding.toHex(encriptor.digestBytes(new Bytes(jsonToken)));

    var upsAddressHashing = req.session.privacyCache.get('upsAddressHashing');
    if (upsAddressHashing === null || upsAddressHashing !== hash) {
        req.session.privacyCache.set('upsAddressHashing', hash);
        return true;
    }
    return false;
}

/**
 * Creates hashed object
 * @param {string} address - address object
 * @param {string} shippingMethodID - shipping method is
 * @param {string} orderTotal - order total
 * @param {string} pliItem - product line items
 * @param {Object} req - request object
 * @returns {boolean} - if success then return true
 */
function validateUpdateShiprateHashing(address, shippingMethodID, orderTotal, pliItem, req) {
    var encriptor = new MessageDigest('MD5');
    var jsonToken = '';

    if (address !== null) {
        jsonToken += createJsonToken(address);
    }
    if (shippingMethodID !== null) {
        jsonToken += createJsonToken({
            shippingMethodID: shippingMethodID
        });
    }
    if (orderTotal !== null) {
        jsonToken += createJsonToken({
            orderTotal: orderTotal
        });
    }
    if (pliItem !== null) {
        var products = {};
        for (var i = 0; i < pliItem.length; i++) {
            var product = pliItem[i];
            products[product.productID] = product.quantityValue;
        }
        jsonToken += createJsonToken(products);
    }

    var hash = Encoding.toHex(encriptor.digestBytes(new Bytes(jsonToken)));

    var upsShipRateHashing = req.session.privacyCache.get('upsShipRateHashing');
    if (upsShipRateHashing === null || upsShipRateHashing !== hash) {
        req.session.privacyCache.set('upsShipRateHashing', hash);
        return true;
    }
    return false;
}

module.exports = {
    validateUpdateUPSAddressHashing: validateUpdateUPSAddressHashing,
    validateUpdateShiprateHashing: validateUpdateShiprateHashing
};
