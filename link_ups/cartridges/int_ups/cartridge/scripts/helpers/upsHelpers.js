'use strict';

var Logger = require('dw/system/Logger');
var Site = require('dw/system/Site');

var collections = require('*/cartridge/scripts/util/collections');
var arrayHelper = require('*/cartridge/scripts/util/array');
var enableLoggers = Site.getCurrent().getCustomPreferenceValue('enableRequestLoggers');
var upsLogger = Logger.getLogger('UPS', 'UPS');

/**
 * Searches for stores and creates a plain object of the stores returned by the search
 * @param{Object} svc - the service obj
 * @param{Object} address - customer entered address
 * @return {Object} requestObj
 */
function createAddressValidationSvcObj(svc, address) {
    var url;
    var requestOption;
    var regionalRequestIndicator;
    var maximumCandidateListSize;
    var addressLine;
    var customerAddressForm;
    var addressKeyFormat;
    var xavRequest;
    var requestObj;

    try {
        if ('upsRequestOption' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('upsRequestOption')) {
            requestOption = Site.getCurrent().getCustomPreferenceValue('upsRequestOption').value;
        }

        if ('regionalRequestIndicator' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('regionalRequestIndicator')) {
            regionalRequestIndicator = Site.getCurrent().getCustomPreferenceValue('regionalRequestIndicator');
        }

        if ('maximumCandidateListSize' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('maximumCandidateListSize') > 0) {
            maximumCandidateListSize = Site.getCurrent().getCustomPreferenceValue('maximumCandidateListSize');
        }

        if (requestOption != null) {
            url = svc.getURL() + '/' + requestOption;
            svc.setURL(url);
        }

        if (regionalRequestIndicator) {
            svc.addParam('regionalrequestindicator', regionalRequestIndicator);
        }
        if (maximumCandidateListSize > 0) {
            svc.addParam('maximumcandidatelistsize', maximumCandidateListSize);
        }

        addressLine = [];
        customerAddressForm = address;

        addressKeyFormat = {};
        addressKeyFormat.ConsigneeName = '';
        addressKeyFormat.BuildingName = customerAddressForm.address2 !== null ? customerAddressForm.address2 : '';
        addressLine[0] = customerAddressForm.address1;
        addressKeyFormat.AddressLine = addressLine;
        addressKeyFormat.PoliticalDivision2 = customerAddressForm.city;
        addressKeyFormat.PoliticalDivision1 = customerAddressForm.stateCode;
        addressKeyFormat.PostcodePrimaryLow = customerAddressForm.postalCode;
        addressKeyFormat.CountryCode = customerAddressForm.countryCode;

        xavRequest = {};
        xavRequest.AddressKeyFormat = addressKeyFormat;

        requestObj = {};
        requestObj.XAVRequest = xavRequest;

        Logger.debug(JSON.stringify(requestObj));
    } catch (e) {
        Logger.info('UPS address validation error while creating request body', e.message);
    }

    return requestObj;
}

/**
 * @returns {Object} config - configuration obj
 */
function getHeadersConfigs() {
    var upsAccessKey;
    var upsUserName;
    var upsPassword;

    if ('upsAccessKey' in Site.current.preferences.custom &&
        Site.getCurrent().getCustomPreferenceValue('upsAccessKey')) {
        upsAccessKey = Site.getCurrent().getCustomPreferenceValue('upsAccessKey');
    }
    if ('upsUserName' in Site.current.preferences.custom &&
        Site.getCurrent().getCustomPreferenceValue('upsUserName')) {
        upsUserName = Site.getCurrent().getCustomPreferenceValue('upsUserName');
    }
    if ('upsPassword' in Site.current.preferences.custom &&
        Site.getCurrent().getCustomPreferenceValue('upsPassword')) {
        upsPassword = Site.getCurrent().getCustomPreferenceValue('upsPassword');
    }

    return {
        upsAccessKey: upsAccessKey,
        upsUserName: upsUserName,
        upsPassword: upsPassword
    };
}

/**
 * Create a Addres object wrt response address
 * @param {string} address1 - request object
 * @param {string} city - request object
 * @param {string} state - request object
 * @param {string} postalCode1 - request object
 * @param {string} postalCode2 - request object
 * @param {string} country - request object
 * @return {Object} address - address object
 */
function addressItem(address1, city, state, postalCode1, postalCode2, country) {
    var address = {};
    if (typeof address1 === 'object') {
        address.address1 = address1[0];
        address.address2 = address1[1];
    } else {
        address.address1 = address1;
    }

    address.city = city;
    address.state = state;
    address.postalCode = postalCode1 + '-' + postalCode2;
    address.country = country;

    return address;
}

/**
 * @param {Object} response - response from the service
 * @return {Object} result - result object
 */
function handleValidateAddressResponse(response) {
    var result = {};
    result.success = false;
    // Check for service returns error.
    if (response.text.object === null) {
        Logger.error('Error on ups address validation service, Error...' + response.errorMessage);
        result.response = response.text;
        result.error = true;
        result.errorType = 'Technical Error';
    }
    if (response.statusCode === 200) {
        result.success = true;
        result.error = false;
        result.response = response.text;
        result.errorType = null;
        Logger.debug(response.text);
    }
    if (response.text !== null && enableLoggers) {
        upsLogger.error('UPS Response -' + response.text);
    }
    return result;
}

/**
 * @param {Object} customer - current customer
 * @param {Object} configs - configurations
 * @param {Object} shippingMethod - shipping method object
 * @param {number} productItmesWt - items weight
 * @param {Object} userAddress - user entered address
 * @return {Object} shipment - request object
 */
function getShipmentObj(customer, configs, shippingMethod, productItmesWt, userAddress) {
    var shipment;
    var shipmentRatingOptions;
    var shipper;
    var shipperAddress;
    var address;
    var shipTo;
    var shipFrom;
    var service;
    var packageing;
    var packagingType;
    var packageWeight;
    var unitOfMeasurement;

    shipmentRatingOptions = {};
    shipmentRatingOptions.UserLevelDiscountIndicator = 'true';

    shipperAddress = {};
    shipperAddress.AddressLine = configs.shipperAddressLine;
    shipperAddress.City = configs.shipperCity;
    shipperAddress.StateProvinceCode = configs.shipperState;
    shipperAddress.PostalCode = configs.shipperZip;
    shipperAddress.CountryCode = configs.shipperCountry;

    shipper = {};
    shipper.Name = configs.shipperName;
    shipper.ShipperNumber = '';
    shipper.Address = shipperAddress;

    address = {};
    address.PostalCode = userAddress.postalCode;
    address.CountryCode = 'US';

    shipTo = {};
    if (customer.authenticated) {
        shipTo.Name = customer.profile.firstName;
    } else {
        shipTo.Name = '';
    }
    shipTo.Address = address;

    shipFrom = {};
    shipFrom.Name = configs.shipperName;
    shipFrom.Address = shipperAddress;

    service = {};
    service.Code = shippingMethod.custom.service_code_ups;
    service.Description = shippingMethod.custom.service_description_ups;

    packagingType = {};
    packagingType.Code = shippingMethod.custom.package_code_ups;
    packagingType.Description = shippingMethod.custom.package_description_ups;

    unitOfMeasurement = {};
    unitOfMeasurement.Code = configs.unitOfMeasurement;

    packageWeight = {};
    packageWeight.UnitOfMeasurement = unitOfMeasurement;
    packageWeight.Weight = productItmesWt.toString();

    packageing = {};
    packageing.PackagingType = packagingType;
    packageing.PackageWeight = packageWeight;

    shipment = {};
    shipment.ShipmentRatingOptions = shipmentRatingOptions;
    shipment.Shipper = shipper;
    shipment.ShipTo = shipTo;
    shipment.ShipFrom = shipFrom;
    shipment.Service = service;
    shipment.Package = packageing;

    return shipment;
}

/**
 * @param {Object} customer - current customer
 * @param {Object} configs - configurations
 * @param {array} shippingMethods - all shipping methods
 * @param {number} productItmesWt - items weight
 * @param {Object} userAddress - user entered address
 * @return {Object} requestObj - request object
 */
function getShipRateReqBody(customer, configs, shippingMethods, productItmesWt, userAddress) {
    var requestObj;
    var transactionReference;
    var rateRequest;
    var request;
    var shipment = [];

    for (var i = 0; i < shippingMethods.length; i++) {
        var method = shippingMethods[i];
        shipment.push(getShipmentObj(customer, configs, method, productItmesWt, userAddress));
    }

    transactionReference = {};
    if (customer.authenticated) {
        transactionReference.CustomerContext = customer.ID;
    } else {
        transactionReference.CustomerContext = '';
    }

    request = {};
    request.SubVersion = configs.subVersion;
    request.TransactionReference = transactionReference;

    rateRequest = {};
    rateRequest.Request = request;
    rateRequest.Shipment = shipment;

    requestObj = {};
    requestObj.RateRequest = rateRequest;

    Logger.debug(JSON.stringify(requestObj));
    return requestObj;
}

/**
 * @return {Object} configs - all configurations
 */
function getShipRateConfigs() {
    var subVersion;
    var shipperName;
    var shipperNumber;
    var shipperAddressLine;
    var shipperCity;
    var shipperState;
    var shipperZip;
    var shipperCountry;
    var unitOfMeasurement;
    var configs;

    try {
        if ('subVersion_ups' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('subVersion_ups')) {
            subVersion = Site.getCurrent().getCustomPreferenceValue('subVersion_ups');
        }
        if ('shipper_name_ups' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('shipper_name_ups')) {
            shipperName = Site.getCurrent().getCustomPreferenceValue('shipper_name_ups');
        }
        if ('shipper_number_ups' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('shipper_number_ups')) {
            shipperNumber = Site.getCurrent().getCustomPreferenceValue('shipper_number_ups');
        }
        if ('shipper_address1_ups' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('shipper_address1_ups')) {
            shipperAddressLine = Site.getCurrent().getCustomPreferenceValue('shipper_address1_ups');
        }
        if ('shipper_city_ups' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('shipper_city_ups')) {
            shipperCity = Site.getCurrent().getCustomPreferenceValue('shipper_city_ups');
        }
        if ('shipper_state_ups' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('shipper_state_ups')) {
            shipperState = Site.getCurrent().getCustomPreferenceValue('shipper_state_ups');
        }
        if ('shipper_zip_ups' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('shipper_zip_ups')) {
            shipperZip = Site.getCurrent().getCustomPreferenceValue('shipper_zip_ups');
        }
        if ('shipper_country_ups' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('shipper_country_ups')) {
            shipperCountry = Site.getCurrent().getCustomPreferenceValue('shipper_country_ups');
        }
        if ('unit_measurement' in Site.current.preferences.custom &&
            Site.getCurrent().getCustomPreferenceValue('unit_measurement')) {
            unitOfMeasurement = Site.getCurrent().getCustomPreferenceValue('unit_measurement');
        }

        configs = {
            subVersion: subVersion,
            shipperName: shipperName,
            shipperNumber: shipperNumber,
            shipperAddressLine: shipperAddressLine,
            shipperCity: shipperCity,
            shipperState: shipperState,
            shipperZip: shipperZip,
            shipperCountry: shipperCountry,
            unitOfMeasurement: unitOfMeasurement
        };
    } catch (e) {
        Logger.error('Error while fetching site prefs...error: ' + e.message);
    }

    return configs;
}

/**
 * @param {Object} basket - current basket
 * @returns {Object} existingShipment
 */
function getShipToHomeShipment(basket) {
    var existingShipment = null;
    var shipments = basket.getShipments();
    if (shipments.length) {
        existingShipment = arrayHelper.find(shipments, function (shipment) {
            return shipment.custom.shipmentType !== 'instore';
        });
    }
    if (existingShipment === null) {
        existingShipment = basket.getDefaultShipment();
    }
    return existingShipment;
}

/**
 * @param {Object} basket - current basket
 * @param {boolean} multipleShippingMethods - true will capture all methods with ship rate flagged
 * @returns {array} eligibleMethods - all ship rate flagged shipping methods
 */
function getEligibleShipRateMethods(basket, multipleShippingMethods) {
    var ShippingMgr = require('dw/order/ShippingMgr');
    var existingShipment = getShipToHomeShipment(basket);
    var eligibleMethods = [];

    if (!existingShipment) return null;

    var shipmentShippingModel = ShippingMgr.getShipmentShippingModel(existingShipment);

    if (multipleShippingMethods) {
        var shippingMethods = shipmentShippingModel.getApplicableShippingMethods();

        collections.forEach(shippingMethods, function (method) {
            if (method.custom.enable_ship_rate_ups) {
                eligibleMethods.push(method);
            }
        });
    } else {
        eligibleMethods.push(existingShipment.shippingMethod);
    }

    return eligibleMethods;
}

/**
 * @param {Object} basket - current basket
 * @returns {number} totalWt - all shipment product line items weight
 */
function getAllProductItemsWt(basket) {
    var totalWt = 0;
    var existingShipment = getShipToHomeShipment(basket);
    var productLineItems = existingShipment.productLineItems;

    collections.forEach(productLineItems, function (lineItem) {
        totalWt += (lineItem.product.custom.unitWeight * lineItem.quantityValue);
    });
    if (totalWt === 0) {
        totalWt = 1;
    }
    return totalWt;
}

/**
 * Creates the request object for
 * @param {Object} basket - current basket
 * @param {Object} userAddress - user entered address
 * @param {Object} shippingMethods - all shipping methods
 * @returns {Object} requestObj - object to be used for ship rate body
 */
function createShiprateSvcObj(basket, userAddress, shippingMethods) {
    var customer = basket.getCustomer();
    var configs = getShipRateConfigs();
    var productItmesWt = getAllProductItemsWt(basket);

    return getShipRateReqBody(customer, configs, shippingMethods, productItmesWt, userAddress);
}

module.exports = exports = {
    createAddressValidationSvcObj: createAddressValidationSvcObj,
    getHeadersConfigs: getHeadersConfigs,
    addressItem: addressItem,
    handleValidateAddressResponse: handleValidateAddressResponse,
    getEligibleShipRateMethods: getEligibleShipRateMethods,
    createShiprateSvcObj: createShiprateSvcObj
};
