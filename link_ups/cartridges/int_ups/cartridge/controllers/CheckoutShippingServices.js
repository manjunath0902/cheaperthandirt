'use strict';

var server = require('server');
server.extend(module.superModule);

// dw API includes
var BasketMgr = require('dw/order/BasketMgr');
var Locale = require('dw/util/Locale');
var URLUtils = require('dw/web/URLUtils');
var Site = require('dw/system/Site');
var Transaction = require('dw/system/Transaction');

// local API includes
var OrderModel = require('*/cartridge/models/order');
var UPSAddress = require('*/cartridge/models/upsAddress');
var UPSShipRate = require('*/cartridge/models/upsShipRate');
var AccountModel = require('*/cartridge/models/account');
var ShippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
var hashingHelpers = require('*/cartridge/scripts/helpers/hashingHelpers');
var TotalsModel = require('*/cartridge/models/totals');

/**
 * Handle Ajax shipping form submit for ups validation
 */
server.get('UPSSuggestions', function (req, res, next) {
    var form = server.forms.getForm('shipping');
    var result = {};
    var addressList = {};
    var suggestedAddresses = [];
    var upsValidatedAddress = null;
    var addressAffirmURL = URLUtils.url('CheckoutShippingServices-UpdateShippingAddress');
    var addressDeclineURL = URLUtils.url('CheckoutShippingServices-RetainEnteredAddress');
    var message = null;
    var userAddress = [];

    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.json({
            error: true,
            cartError: true,
            fieldErrors: [],
            serverErrors: [],
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        next();
    }

    // verify shipping form data
    var shippingFormErrors = COHelpers.validateShippingForm(form.shippingAddress.addressFields);

    if (Object.keys(shippingFormErrors).length > 0) {
        req.session.privacyCache.set(currentBasket.defaultShipment.UUID, 'invalid');

        res.json({
            form: form,
            fieldErrors: [shippingFormErrors],
            serverErrors: [],
            success: false,
            error: true
        });
        next();
    } else {
        req.session.privacyCache.set(currentBasket.defaultShipment.UUID, 'valid');

        result.address = {
            firstName: form.shippingAddress.addressFields.firstName.value,
            lastName: form.shippingAddress.addressFields.lastName.value,
            address1: form.shippingAddress.addressFields.address1.value,
            address2: form.shippingAddress.addressFields.address2.value,
            city: form.shippingAddress.addressFields.city.value,
            postalCode: form.shippingAddress.addressFields.postalCode.value,
            countryCode: form.shippingAddress.addressFields.country.value,
            phone: form.shippingAddress.addressFields.phone.value
        };

        if (Object.prototype.hasOwnProperty
            .call(form.shippingAddress.addressFields, 'states')) {
            result.address.stateCode =
                form.shippingAddress.addressFields.states.stateCode.value;
        }

        var addressToHash = {
            address1: form.shippingAddress.addressFields.address1.value,
            address2: form.shippingAddress.addressFields.address2.value,
            city: form.shippingAddress.addressFields.city.value,
            postalCode: form.shippingAddress.addressFields.postalCode.value,
            countryCode: form.shippingAddress.addressFields.country.value,
            stateCode: result.address.stateCode
        };

        var newHashCreated = hashingHelpers.validateUpdateUPSAddressHashing(addressToHash, req);

        var exactAddressEntered = false;
        if (newHashCreated) {
            upsValidatedAddress = new UPSAddress(result.address);
        } else {
            exactAddressEntered = true;
        }

        userAddress[0] = result.address;
        if (upsValidatedAddress === null) {
            res.json({
                success: true,
                error: false,
                order: null,
                IsExactAddress: exactAddressEntered,
                RequestAddress: userAddress
            });
            return next();
        } else {  // eslint-disable-line
            if (!upsValidatedAddress.error) {
                for (var i = 0; i < upsValidatedAddress.AddressList.length; i++) {
                    addressList = {};
                    addressList.address1 = upsValidatedAddress.AddressList[i].address1 ? upsValidatedAddress.AddressList[i].address1 : null;
                    addressList.address2 = upsValidatedAddress.AddressList[i].address2 ? upsValidatedAddress.AddressList[i].address2 : null;
                    addressList.city = upsValidatedAddress.AddressList[i].city;
                    addressList.countryCode = upsValidatedAddress.AddressList[i].country;
                    addressList.postalCode = upsValidatedAddress.AddressList[i].postalCode;
                    addressList.stateCode = upsValidatedAddress.AddressList[i].state;
                    suggestedAddresses[i] = addressList;
                }
            } else {
                if (upsValidatedAddress.errorType !== null && upsValidatedAddress.errorType === 'service unavailable') {
                    message = 'upsAddressServiceUnavailableMsg' in Site.current.preferences.custom &&
                        Site.getCurrent().getCustomPreferenceValue('upsAddressServiceUnavailableMsg') ?
                        Site.getCurrent().getCustomPreferenceValue('upsAddressServiceUnavailableMsg') :
                        'Address validation service is unavaialble';
                } else {
                    message = 'upsAddressServiceErrorMsg' in Site.current.preferences.custom &&
                        Site.getCurrent().getCustomPreferenceValue('upsAddressServiceErrorMsg') ?
                        Site.getCurrent().getCustomPreferenceValue('upsAddressServiceErrorMsg') :
                        'Address validation service Error';
                }
                res.json({
                    success: true,
                    error: false,
                    order: null,
                    IsExactAddress: upsValidatedAddress.IsExactAddress,
                    RequestAddress: userAddress,
                    upsServiceMessage: message
                });
                return next();
            }

            result.upsSuggestedAddress = {
                Addresses: suggestedAddresses,
                IsMultiAddress: upsValidatedAddress.IsMultiAddress,
                IsExactAddress: upsValidatedAddress.IsExactAddress,
                AddressAffirmURL: addressAffirmURL,
                AddressDeclineURL: addressDeclineURL
            };

            res.setViewData(result);
        }

        /* eslint-disable no-shadow */
        this.on('route:BeforeComplete', function (req, res) {
            var shippingData = res.getViewData();

            var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
            if (usingMultiShipping === true && currentBasket.shipments.length < 2) {
                req.session.privacyCache.set('usingMultiShipping', false);
                usingMultiShipping = false;
            }

            var currentLocale = Locale.getLocale(req.locale.id);
            var basketModel = new OrderModel(
                currentBasket, {
                    usingMultiShipping: usingMultiShipping,
                    shippable: true,
                    countryCode: currentLocale.country,
                    containerView: 'basket',
                    suggestedAddress: shippingData.upsSuggestedAddress,
                    userEnteredAddress: userAddress,
                    IsExactAddress: shippingData.IsExactAddress
                }
            );

            res.json({
                customer: new AccountModel(req.currentCustomer),
                order: basketModel
            });
        });
        /* eslint-enable no-shadow */
    }
    return next();
});

server.post('UpdateShippingAddress', function (req, res, next) {
    var reqObj = JSON.parse(req.body);

    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        next();
    }

    var shipmentUUID = req.querystring.shipmentUUID || req.form.shipmentUUID;
    var shippingMethodID = reqObj.shippingMethodID;
    var shipment;
    if (shipmentUUID) {
        shipment = ShippingHelper.getShipmentByUUID(currentBasket, shipmentUUID);
    } else {
        shipment = currentBasket.defaultShipment;
    }

    var addressToHash = {
        address1: reqObj.addressData.address1,
        address2: reqObj.addressData.address2,
        city: reqObj.addressData.city,
        postalCode: reqObj.addressData.postalCode,
        countryCode: reqObj.addressData.countryCode,
        stateCode: reqObj.addressData.stateCode
    };

    var upsAddressHashing = req.session.privacyCache.get('upsAddressHashing');
    if (upsAddressHashing !== null) {
        req.session.privacyCache.set('upsAddressHashing', null);
    }
    hashingHelpers.validateUpdateUPSAddressHashing(addressToHash, req);

    var userAddress = {
        address1: reqObj.addressData.address1 || '',
        city: reqObj.addressData.city || '',
        postalCode: reqObj.addressData.postalCode || '',
        stateCode: reqObj.addressData.stateCode || '',
        countryCode: reqObj.addressData.countryCode || ''
    };

    var upsResult;
    var upsRates;
    var totalsModel = new TotalsModel(currentBasket);
    var orderTotal = totalsModel ? totalsModel.grandTotal : null;
    var newHashingMapped = hashingHelpers.validateUpdateShiprateHashing(userAddress, shippingMethodID, orderTotal, currentBasket.productLineItems, req);
    if (newHashingMapped && shipment.shippingMethod.custom.enable_ship_rate_ups) {
        // Passing false as the third parameter
        // to get the ship rate for the selected shipping method only.
        upsResult = new UPSShipRate(currentBasket, userAddress, false);
    }

    if (upsResult !== undefined && upsResult.error) {
        res.json({
            error: true,
            success: false
        });
    }

    upsRates = shipment.custom.calculatedShipRate;
    if (upsRates != null) {
        upsRates = JSON.parse(upsRates);
    }

    var viewData = res.getViewData();
    viewData.address = reqObj.addressData;
    this.on('route:BeforeComplete', function (req, res) { // eslint-disable-line no-shadow
        var shippingData = res.getViewData();
        var address = shippingData.address;

        try {
            Transaction.wrap(function () {
                var shippingAddress = shipment.shippingAddress;

                if (!shippingAddress) {
                    shippingAddress = shipment.createShippingAddress();
                }

                shippingAddress.setAddress1(address.address1 || '');
                shippingAddress.setAddress2(address.address2 || '');
                shippingAddress.setCity(address.city || '');
                shippingAddress.setPostalCode(address.postalCode || '');
                shippingAddress.setStateCode(address.stateCode || '');
                shippingAddress.setCountryCode(address.countryCode || '');

                ShippingHelper.selectShippingMethod(shipment, shippingMethodID);

                if (upsRates !== null && Object.prototype.hasOwnProperty.call(upsRates, shippingMethodID)) {
                    var price = parseFloat(upsRates[shippingMethodID].split('|')[1]);
                    if (price !== null) {
                        shipment.standardShippingLineItem.setPriceValue(price);
                    }
                }

                basketCalculationHelpers.calculateTotals(currentBasket);
            });
        } catch (err) {
            res.setStatusCode(500);
            res.json({
                error: true,
                errorMessage: 'Can\'t update shipping address.'
            });

            return;
        }

        // Loop through all shipments and make sure all are valid
        COHelpers.ensureValidShipments(currentBasket);

        var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
        var currentLocale = Locale.getLocale(req.locale.id);

        var basketModel = new OrderModel(
            currentBasket, {
                usingMultiShipping: usingMultiShipping,
                countryCode: currentLocale.country,
                containerView: 'basket'
            }
        );

        res.json({
            customer: new AccountModel(req.currentCustomer),
            order: basketModel
        });
    });
    next();
});

server.get('RetainEnteredAddress', function (req, res, next) {
    var upsAddressHashing = req.session.privacyCache.get('upsAddressHashing');
    if (upsAddressHashing !== null) {
        req.session.privacyCache.set('upsAddressHashing', null);
    }

    res.json({
        success: true,
        error: false
    });

    next();
});


/**
 * Handle Ajax shipping form submit
 */
server.append(
    'SubmitShipping',
    function (req, res, next) {
        var currentBasket = BasketMgr.getCurrentBasket();
        var storeShipment = false;
        storeShipment = COHelpers.onlyStoreShipment(currentBasket.getShipments());
        var storeId = req.form.storeId;

        if (!storeShipment && !storeId) {
            if (!currentBasket) {
                res.json({
                    error: true,
                    cartError: true,
                    fieldErrors: [],
                    serverErrors: [],
                    redirectUrl: URLUtils.url('Cart-Show').toString()
                });
                next();
            }

            var form = server.forms.getForm('shipping');

            // verify shipping form data
            var shippingFormErrors = COHelpers.validateShippingForm(form.shippingAddress.addressFields);

            if (Object.keys(shippingFormErrors).length > 0) {
                req.session.privacyCache.set(currentBasket.defaultShipment.UUID, 'invalid');

                res.json({
                    form: form,
                    fieldErrors: [shippingFormErrors],
                    serverErrors: [],
                    error: true
                });
            } else {
                req.session.privacyCache.set(currentBasket.defaultShipment.UUID, 'valid');

                var stateCode;
                if (Object.prototype.hasOwnProperty
                    .call(form.shippingAddress.addressFields, 'states')) {
                    stateCode =
                        form.shippingAddress.addressFields.states.stateCode.value;
                }

                var shippingMethodID = form.shippingAddress.shippingMethodID.value;
                var shipment = currentBasket.defaultShipment;

                var userAddress = {
                    address1: form.shippingAddress.addressFields.address1.value || '',
                    city: form.shippingAddress.addressFields.city.value || '',
                    postalCode: form.shippingAddress.addressFields.postalCode.value || '',
                    stateCode: stateCode || '',
                    countryCode: form.shippingAddress.addressFields.country.value || ''
                };

                var upsResult;
                var upsRates;
                var totalsModel = new TotalsModel(currentBasket);
                var orderTotal = totalsModel ? totalsModel.grandTotal : null;
                var newHashingMapped = hashingHelpers.validateUpdateShiprateHashing(userAddress, shippingMethodID, orderTotal, currentBasket.productLineItems, req);
                if (newHashingMapped && shipment.shippingMethod.custom.enable_ship_rate_ups) {
                    // Passing false as the third parameter
                    // to get the ship rate for the selected shipping method only.
                    upsResult = new UPSShipRate(currentBasket, userAddress, false);
                }

                if (upsResult !== undefined && upsResult.error) {
                    res.json({
                        error: true,
                        success: false
                    });
                }

                upsRates = shipment.custom.calculatedShipRate;
                if (upsRates != null) {
                    upsRates = JSON.parse(upsRates);
                }
                this.off('route:BeforeComplete');
                /* eslint-disable no-shadow */
                this.on('route:BeforeComplete', function () {
                    Transaction.wrap(function () {
                        if (upsRates !== null && Object.prototype.hasOwnProperty.call(upsRates, shippingMethodID)) {
                            var price = parseFloat(upsRates[shippingMethodID].split('|')[1]);
                            if (price !== null) {
                                shipment.standardShippingLineItem.setPriceValue(price);
                            }
                        }

                        basketCalculationHelpers.calculateTotals(currentBasket);
                    });
                });
                /* eslint-enable no-shadow */
            }
        }
        next();
    }
);

server.replace('UpdateShippingMethodsList', server.middleware.https, function (req, res, next) {
    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.json({
            error: true,
            cartError: true,
            fieldErrors: [],
            serverErrors: [],
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        return next();
    }

    var shipmentUUID = req.querystring.shipmentUUID || req.form.shipmentUUID;
    var shipment;
    if (shipmentUUID) {
        shipment = ShippingHelper.getShipmentByUUID(currentBasket, shipmentUUID);
    } else {
        shipment = currentBasket.defaultShipment;
    }


    var address = ShippingHelper.getAddressFromRequest(req);

    var shippingMethodID;

    if (shipment.shippingMethod) {
        shippingMethodID = shipment.shippingMethod.ID;
    }

    if ((!('shipmentType' in shipment.custom) || ('shipmentType' in shipment.custom && shipment.custom.shipmentType !== 'instore')) && req.form.stateCode !== null && req.form.stateCode !== undefined && 'AKHI'.indexOf(req.form.stateCode) > -1) {
        var shipMethodsForAKHI = Site.getCurrent().getCustomPreferenceValue('shipMethodsForAKHI');
        var standardMethodForAKHI = Site.getCurrent().getCustomPreferenceValue('standardMethodForAKHI');
        var shipArr = shipMethodsForAKHI.split('|');
        var isARHIMethod = false;
        for (var i = 0; i < shipArr.length; i++) {
            if (shipArr[i] === shippingMethodID) {
                isARHIMethod = true;
                break;
            }
        }
        var allowedOtherMethods = Site.getCurrent().getCustomPreferenceValue('allowedOtherMethodsForAKHI');
        if (!isARHIMethod && allowedOtherMethods.indexOf(shippingMethodID) === -1) {
            shippingMethodID = standardMethodForAKHI;
        }
    }
    var storeShippingMethodID = Site.getCurrent().getCustomPreferenceValue('storeShippingMethodID');
    storeShippingMethodID = storeShippingMethodID ? storeShippingMethodID : 'FFL'; //eslint-disable-line
    if ('shipmentType' in shipment.custom && shipment.custom.shipmentType === 'instore') {
        shippingMethodID = storeShippingMethodID;
    }

    Transaction.wrap(function () {
        var shippingAddress = shipment.shippingAddress;

        if (!shippingAddress) {
            shippingAddress = shipment.createShippingAddress();
        }
        if (req.form.stateCode !== null && req.form.stateCode !== undefined) {
            session.custom.shippingStateCode = req.form.stateCode; // eslint-disable-line
        }
        if (!('shipmentType' in shipment.custom) || ('shipmentType' in shipment.custom && shipment.custom.shipmentType !== 'instore')) {
            Object.keys(address).forEach(function (key) {
                var value = address[key];
                if (value) {
                    shippingAddress[key] = value;
                } else {
                    shippingAddress[key] = null;
                }
            });
        }
        var restrictHelper = require('*/cartridge/scripts/cart/restrictHelper.js');
        var restrictResponse = restrictHelper.validateRestrictGroupAndItem(address, currentBasket);
        session.custom.restrictedShipMethods = restrictResponse.hasRestrictedItems; // eslint-disable-line

        COHelpers.calculateUPSShipRate(currentBasket);

        ShippingHelper.selectShippingMethod(shipment, shippingMethodID);

        basketCalculationHelpers.calculateTotals(currentBasket);
    });

    var usingMultiShipping = req.session.privacyCache.get('usingMultiShipping');
    var currentLocale = Locale.getLocale(req.locale.id);

    var basketModel = new OrderModel(
        currentBasket,
        { usingMultiShipping: usingMultiShipping, countryCode: currentLocale.country, containerView: 'basket' }
    );

    res.json({
        customer: new AccountModel(req.currentCustomer),
        order: basketModel,
        shippingForm: server.forms.getForm('shipping')
    });

    return next();
});

module.exports = server.exports();
