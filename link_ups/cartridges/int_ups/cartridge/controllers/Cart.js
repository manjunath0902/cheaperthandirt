'use strict';

var server = require('server');
server.extend(module.superModule);

var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var UPSShipRate = require('*/cartridge/models/upsShipRate');

server.append('Show', function (req, res, next) {
    var postalForm = server.forms.getForm('addressCheck');
    res.setViewData({
        postalForm: postalForm
    });
    next();
});

server.get(
    'EstimateShipRate',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var BasketMgr = require('dw/order/BasketMgr');
        var Resource = require('dw/web/Resource');
        var Transaction = require('dw/system/Transaction');
        var URLUtils = require('dw/web/URLUtils');
        var CartModel = require('*/cartridge/models/cart');
        var reportingUrlsHelper = require('*/cartridge/scripts/reportingUrls');
        var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
        var renderTemplateHelper = require('*/cartridge/scripts/renderTemplateHelper');

        var upsResult;

        var currentBasket = BasketMgr.getCurrentBasket();

        if (!currentBasket) {
            res.setStatusCode(500);
            res.json({
                error: true,
                redirectUrl: URLUtils.url('Cart-Show').toString()
            });

            return next();
        }

        if (!currentBasket) {
            res.setStatusCode(500);
            res.json({
                errorMessage: Resource.msg('error.retrieve.basket', 'cart', null)
            });
            return next();
        }

        var error = false;
        var errorMessage;
        var zipCode = req.querystring.zipCode;

        var userAddress = {
            address1: '',
            postalCode: zipCode,
            city: '',
            stateCode: '',
            countryCode: 'US'
        };

        try {
            // Passing true as the third parameter
            // to get the ship rate for all the flagged shipping methods.
            upsResult = new UPSShipRate(currentBasket, userAddress, true);
            if (upsResult.error) {
                error = true;
            }
        } catch (e) {
            error = true;
        }

        if (error) {
            res.json({
                error: error,
                errorMessage: errorMessage
            });
            return next();
        }

        Transaction.wrap(function () {
            basketCalculationHelpers.calculateTotals(currentBasket);
        });
        var reportingURLs;
        if (currentBasket && currentBasket.allLineItems.length) {
            reportingURLs = reportingUrlsHelper.getBasketOpenReportingURLs(currentBasket);
        }

        var basketModel = new CartModel(currentBasket);
        basketModel.reportingURLs = reportingURLs;

        var shipRateHTML = basketModel
        ? renderTemplateHelper.getRenderedHtml(basketModel, 'cart/cartShippingMethodSelection')
        : null;

        res.json({
            shipRateHTML: shipRateHTML
        });
        return next();
    }
);

server.replace('SelectShippingMethod', server.middleware.https, function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Resource = require('dw/web/Resource');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var CartModel = require('*/cartridge/models/cart');
    var shippingHelper = require('*/cartridge/scripts/checkout/shippingHelpers');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');

    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.json({
            error: true,
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });

        return next();
    }

    var error = false;

    var shipUUID = req.querystring.shipmentUUID || req.form.shipmentUUID;
    var methodID = req.querystring.methodID || req.form.methodID;
    var shipment;
    if (shipUUID) {
        shipment = shippingHelper.getShipmentByUUID(currentBasket, shipUUID);
    } else {
        shipment = currentBasket.defaultShipment;
    }


    Transaction.wrap(function () {
        shippingHelper.selectShippingMethod(shipment, methodID);

        if (currentBasket && !shipment.shippingMethod) {
            error = true;
            return;
        }

        var upsRates = shipment.custom.calculatedShipRate;
        if (upsRates !== null) {
            upsRates = JSON.parse(upsRates);
            if (Object.prototype.hasOwnProperty.call(upsRates, methodID)) {
                var price = parseFloat(upsRates[methodID].split('|')[1]);
                if (price !== null) {
                    shipment.standardShippingLineItem.setPriceValue(price);
                }
            }
        }

        basketCalculationHelpers.calculateTotals(currentBasket);
    });

    if (!error) {
        var basketModel = new CartModel(currentBasket);

        res.json(basketModel);
    } else {
        res.setStatusCode(500);
        res.json({
            errorMessage: Resource.msg('error.cannot.select.shipping.method', 'cart', null)
        });
    }
    return next();
});

module.exports = server.exports();
