'use strict';

var server = require('server');
server.extend(module.superModule);

server.append('AddNewAddress', function (req, res, next) {
    // dw API includes
    var BasketMgr = require('dw/order/BasketMgr');
    var URLUtils = require('dw/web/URLUtils');
    var Transaction = require('dw/system/Transaction');

    // local API includes
    var COHelpers = require('*/cartridge/scripts/checkout/checkoutHelpers');
    var TotalsModel = require('*/cartridge/models/totals');
    var hashingHelpers = require('*/cartridge/scripts/helpers/hashingHelpers');
    var UPSShipRate = require('*/cartridge/models/upsShipRate');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var currentBasket = BasketMgr.getCurrentBasket();

    if (!currentBasket) {
        res.json({
            error: true,
            cartError: true,
            fieldErrors: [],
            serverErrors: [],
            redirectUrl: URLUtils.url('Cart-Show').toString()
        });
        next();
    }

    var form = server.forms.getForm('shipping');

    // verify shipping form data
    var shippingFormErrors = COHelpers.validateShippingForm(form.shippingAddress.addressFields);

    if (Object.keys(shippingFormErrors).length > 0) {
        req.session.privacyCache.set(currentBasket.defaultShipment.UUID, 'invalid');

        res.json({
            form: form,
            fieldErrors: [shippingFormErrors],
            serverErrors: [],
            error: true
        });
    } else {
        req.session.privacyCache.set(currentBasket.defaultShipment.UUID, 'valid');
        var result = {};
        result.address = {};
        result.address.companyName = form.shippingAddress.addressFields.companyName.value;
        result.email = form.shippingAddress.addressFields.email.value;
        res.setViewData(result);

        var stateCode;
        if (Object.prototype.hasOwnProperty
            .call(form.shippingAddress.addressFields, 'states')) {
            stateCode =
                form.shippingAddress.addressFields.states.stateCode.value;
        }

        var shippingMethodID = form.shippingAddress.shippingMethodID.value;
        var shipment = currentBasket.defaultShipment;

        var userAddress = {
            address1: form.shippingAddress.addressFields.address1.value || '',
            city: form.shippingAddress.addressFields.city.value || '',
            postalCode: form.shippingAddress.addressFields.postalCode.value || '',
            stateCode: stateCode || '',
            countryCode: form.shippingAddress.addressFields.country.value || ''
        };

        var upsResult;
        var upsRates;
        var totalsModel = new TotalsModel(currentBasket);
        var orderTotal = totalsModel ? totalsModel.grandTotal : null;
        var newHashingMapped = hashingHelpers.validateUpdateShiprateHashing(userAddress, shippingMethodID, orderTotal, currentBasket.productLineItems, req);
        if (newHashingMapped && shipment.shippingMethod.custom.enable_ship_rate_ups) {
            upsResult = new UPSShipRate(currentBasket, userAddress, false);
        }

        if (upsResult !== undefined && upsResult.error) {
            res.json({
                error: true,
                success: false
            });
        }

        upsRates = shipment.custom.calculatedShipRate;
        if (upsRates != null) {
            upsRates = JSON.parse(upsRates);
        }
        var oldHandler = this.listeners('route:BeforeComplete');
        this.off('route:BeforeComplete');

        /* eslint-disable no-shadow */
        this.on('route:BeforeComplete', function (req, res) {
            oldHandler[0].call(this, req, res);

            Transaction.wrap(function () {
                if (upsRates !== null && Object.prototype.hasOwnProperty.call(upsRates, shippingMethodID)) {
                    var price = parseFloat(upsRates[shippingMethodID].split('|')[1]);
                    if (price !== null) {
                        shipment.standardShippingLineItem.setPriceValue(price);
                    }
                }
                if (currentBasket && result.email) {
                    currentBasket.setCustomerEmail(result.email);
                }
                basketCalculationHelpers.calculateTotals(currentBasket);
            });
        });
        /* eslint-enable no-shadow */
    }

    next();
});

module.exports = server.exports();
