'use strict';

var server = require('server');
server.extend(module.superModule);

var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');

server.get('UPSSuggestions', userLoggedIn.validateLoggedInAjax, function (req, res, next) {
    var UPSAddress = require('*/cartridge/models/upsAddress');
    var Site = require('dw/system/Site');
    var AccountModel = require('*/cartridge/models/account');
    var Resource = require('dw/web/Resource');

    var result = {};
    var addressList = {};
    var suggestedAddresses = [];
    var userAddress = [];
    var upsValidatedAddress = null;
    var message = null;
    var responseObj;

    var data = res.getViewData();
    if (data && !data.loggedin) {
        res.json();
        return next();
    }

    var formData = server.forms.getForm('address');

    result.address = {
        firstName: formData.firstName.value,
        lastName: formData.lastName.value,
        address1: formData.address1.value,
        address2: formData.address2.value,
        city: formData.city.value,
        postalCode: formData.postalCode.value,
        phone: formData.phone.value
    };

    if (formData.states && formData.states.stateCode) {
        result.address.stateCode = formData.states.stateCode.value;
    }

    if (formData.country) {
        result.address.countryCode = formData.country.value;
    }
    result.address.countryName = Resource.msg(result.address.countryCode.toLowerCase(), 'checkout', null);
    userAddress[0] = result.address;

    upsValidatedAddress = new UPSAddress(result.address);
    var currCustomer = req.currentCustomer.raw;
    var isAddressIDExist = (currCustomer.addressBook) && (currCustomer.addressBook.getAddress(formData.addressId.htmlValue)) && (req.querystring.addressUUID !== currCustomer.addressBook.getAddress(formData.addressId.htmlValue).UUID);
    if (isAddressIDExist) {
        res.json({
            success: false,
            error: true,
            addressIDExists: isAddressIDExist,
            upsServiceMessage: 'Address title already used.'
        });
    } else if (!upsValidatedAddress.error) {
        for (var i = 0; i < upsValidatedAddress.AddressList.length; i++) {
            addressList = {};
            addressList.address1 = upsValidatedAddress.AddressList[i].address1 ? upsValidatedAddress.AddressList[i].address1 : null;
            addressList.address2 = upsValidatedAddress.AddressList[i].address2 ? upsValidatedAddress.AddressList[i].address2 : null;
            addressList.city = upsValidatedAddress.AddressList[i].city;
            addressList.countryCode = upsValidatedAddress.AddressList[i].country;
            addressList.postalCode = upsValidatedAddress.AddressList[i].postalCode;
            addressList.stateCode = upsValidatedAddress.AddressList[i].state;
            addressList.countryName = Resource.msg(addressList.countryCode.toLowerCase(), 'checkout', null);
            suggestedAddresses[i] = addressList;
        }

        responseObj = {
            suggestedAddress: suggestedAddresses,
            IsMultiAddress: upsValidatedAddress.IsMultiAddress,
            userEnteredAddress: userAddress,
            IsExactAddress: upsValidatedAddress.IsExactAddress
        };

        res.json({
            success: true,
            customer: new AccountModel(req.currentCustomer),
            upsResponse: responseObj
        });
    } else {
        if (upsValidatedAddress.errorType !== null && upsValidatedAddress.errorType === 'service unavailable') {
            message = 'upsAddressServiceUnavailableMsg' in Site.current.preferences.custom &&
                Site.getCurrent().getCustomPreferenceValue('upsAddressServiceUnavailableMsg') ?
                Site.getCurrent().getCustomPreferenceValue('upsAddressServiceUnavailableMsg') :
                'Address validation service is unavaialble';
        } else {
            message = 'upsAddressServiceErrorMsg' in Site.current.preferences.custom &&
                Site.getCurrent().getCustomPreferenceValue('upsAddressServiceErrorMsg') ?
                Site.getCurrent().getCustomPreferenceValue('upsAddressServiceErrorMsg') :
                'Address validation service Error';
        }
        responseObj = {
            suggestedAddress: null,
            userEnteredAddress: userAddress,
            IsExactAddress: upsValidatedAddress.IsExactAddress
        };
        res.json({
            success: false,
            error: true,
            upsResponse: responseObj,
            upsServiceMessage: message
        });
    }
    return next();
});

module.exports = server.exports();
