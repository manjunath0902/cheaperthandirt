'use strict';

module.exports = {
    estimateShipRate: function () {
        $('.estimate-ship-form').submit(function (e) {
            e.preventDefault();
            $('.totals').spinner().start();
            var $form = $('.estimate-ship-form');

            // GTM - event
            require('gtm/gtm/gtm').estimateShipRate();

            $.ajax({
                url: $form.attr('action'),
                type: 'GET',
                dataType: 'json',
                data: $form.serialize(),
                success: function (response) {
                    if (response.shipRateHTML !== undefined && response.shipRateHTML !== '') {
                        $('.shipping-selection').empty().html(response.shipRateHTML);
                        $('body').trigger('triggerCustomSelect');
                    }
                    $('.shipping-selection').removeClass('d-none').addClass('d-block');
                    $('.totals').spinner().stop();
                },
                error: function (err) {
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    } else {
                        $('.totals').spinner().stop();
                    }
                }
            });
            return false;
        });
    }
};
