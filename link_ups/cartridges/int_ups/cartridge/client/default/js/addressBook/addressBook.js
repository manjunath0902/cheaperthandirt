'use strict';

var baseAddressBook = require('base/addressBook/addressBook');
var shipping = require('../checkout/shipping');
var PerfectScrollbar = require('perfect-scrollbar/dist/perfect-scrollbar');

baseAddressBook.submitAddress = function () {
    $('form.address-form').submit(function (e) {
        e.preventDefault();
        var $form = $(this);
        var saveAddressURL = $form.attr('action');
        var url = $form.find('input[name="ups-validaition-actionurl"]').val();
        $form.spinner().start();
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            data: $form.serialize()
        }).done(function (data) {
            var body = '';
            var htmlString = '';
            var suggestedAddressArray = null;
            var userEnteredAddress = null;
            var overrideActionPath = saveAddressURL;
            if (data.IsExactAddress) {
                $.spinner().stop();
                return;
            }
            if (data.addressIDExists) {
                $.spinner().stop();
                $('.invalid-feedback.addressError').text(data.upsServiceMessage).show();
                return;
            }
            if (data === null || data.upsResponse === null || data.upsResponse.suggestedAddress === null || data.upsResponse.suggestedAddress.length === 0) {
                body = shipping.createUPSModalBody(data.upsResponse.userEnteredAddress, false);

                // Modal Container
                htmlString = shipping.createUPSModalHtmlString('default', true);

                $('body').append(htmlString);
                $('div#adddress-suggestion').modal('show');
                $('.modal-body').html(body);
                $.spinner().stop();
                $('#entered-address-btn').prop('checked', true);
                $('#adddress-suggestion .button-wrapper button.affirm').removeAttr('disabled');
                $('.suggested-address').find('input[type="radio"]').eq(0).trigger('click');
            } else {
                suggestedAddressArray = data.upsResponse.suggestedAddress;
                userEnteredAddress = data.upsResponse.userEnteredAddress;

                // User entered address
                body = shipping.createUPSModalBody(userEnteredAddress, false);

                // UPS suggested address
                body += shipping.createUPSModalBody(suggestedAddressArray, true);

                // Modal Container
                htmlString = shipping.createUPSModalHtmlString(overrideActionPath, false);

                $('body').append(htmlString);
                $('div#adddress-suggestion').modal('show');
                $('.modal-body').html(body);
                $('#adddress-suggestion .button-wrapper button.affirm').removeAttr('disabled');
                $('.suggested-addr-container').find('input[type="radio"]').eq(0).trigger('click');
                $.spinner().stop();
            }

            $('.modal-dialog input:radio[name="option"]').click(function () {
                $('#adddress-suggestion .button-wrapper button.affirm').removeAttr('disabled');
            });
            $('#adddress-suggestion .button-wrapper button.affirm').click(function (el) {
                el.preventDefault();
                $('body').trigger('address:updateAddressSuggestion', {
                    suggestedAddressArray: suggestedAddressArray
                });
            });
        })
        .fail(function () {
            $form.spinner().stop();
        });
        return false;
    });
};

/**
 * Perfect scroll bar & radio button background class
 */
function initEvents() {
    $(document).ajaxComplete(function () {
        if ($('.modal-body').hasClass('perfect-scrollbar')) {
            var resultsDiv = '.perfect-scrollbar';
            var perfectScrollbar = new PerfectScrollbar(resultsDiv);
            perfectScrollbar.update();
        }
    });

    $('body').on('change', '.selected-address-btn', function () {
        $('.suggested-address').removeClass('selected-address');
        if ($(this).is(':checked')) {
            $(this).closest('.suggested-address').addClass('selected-address');
        }
    });
}

baseAddressBook.initEvents = initEvents;
module.exports = baseAddressBook;
