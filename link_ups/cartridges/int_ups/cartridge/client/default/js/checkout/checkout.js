'use strict';

var baseCheckout = require('base/checkout/checkout');
var baseBilling = require('base/checkout/billing');
var upsShipping = require('./shipping');
var instorepickupCheckout = require('instorepickup/checkout/checkout');

baseCheckout.updateCheckoutView = instorepickupCheckout.updateCheckoutView;

[upsShipping, baseBilling].forEach(function (library) {
    Object.keys(library).forEach(function (key) {
        if (typeof library[key] === 'object') {
            baseCheckout[key] = $.extend({}, baseCheckout[key], library[key]);
        } else {
            baseCheckout[key] = library[key];
        }
    });
});

module.exports = baseCheckout;
