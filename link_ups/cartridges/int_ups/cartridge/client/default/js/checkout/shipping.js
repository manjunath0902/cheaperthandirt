'use strict';

var base = require('base/checkout/shipping');
var PerfectScrollbar = require('perfect-scrollbar/dist/perfect-scrollbar');

/**
 * Creat the modal to display a address
 * @param {string} overrideActionPath url for action
 * @param {boolean} includeWarning - Decides whether or not to include the warning message
 * @return {string} htmlString html content string
 */
function createHtmlString(overrideActionPath, includeWarning) {
    if ($('#adddress-suggestion').length !== 0) {
        $('#adddress-suggestion').remove();
    }
    var message = '';
    if (includeWarning) {
        message = '<span class="b3-black">The address provided was not found in the UPS database or requires additional information.</span>';
    }
    var htmlString = '<!-- Modal -->' +
        '<div class="modal fade" id="adddress-suggestion" class="adddress-suggestion" style="display: block;" role="dialog">' +
        '<div class="modal-dialog ups-shipping-dialog">' +
        '<!-- Modal content-->' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<span class="modal-title text-uppercase">Address Validation</span>' +
        '<button type="button" class="close pull-right" data-dismiss="modal">' +
        '<span class="svg-25-Close_Button_Black svg-25-Close_Button_Black-dims d-inline-block" aria-hidden="true"></span>' +
        '</button>' +
        '</div>' +
        '<div class="modal-header-description">' +
         message +
        '<div class="modal-body perfect-scrollbar">' +
        '</div>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<div class="button-wrapper">' +
        '<button class="affirm btn btn-primary override-address button-primary m-auto" data-url="' + overrideActionPath + '" disabled>' +
        'CONTINUE' +
        '</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    return htmlString;
}

/**
 * Creat the body of the modal to display address
 * @param {Array} address to be displayed
 * @param {boolean} showSuggestedAddress if true then suggested address is in params
 * @return {string} body of the modal
 */
function createModalBody(address, showSuggestedAddress) {
    var body = '';
    var count = 0;
    var addressHtml = '';
    if (address !== undefined && address !== null) {
        if (showSuggestedAddress && address.length !== 0) {
            addressHtml += '<div class="address-suggestion-heading">Use Suggested Address</div>';
        } else if (address.length !== 0) {
            addressHtml += '<div class="address-suggestion-heading">Use Address Entered</div>';
        }
        address.forEach(function (attr) {
            if (showSuggestedAddress) {
                addressHtml += "<div class='suggested-address suggested-addr-container'><div class='custom-radio'><input type='radio' data-address='" + JSON.stringify(attr) + "' class='selected-address-btn' data-count='" + count + "' name='option' /><span class='span-radio'><span class='span-active-radio'></span></span></div>";
                count++;
            } else {
                addressHtml += "<div class='suggested-address entered-address'><div class='custom-radio'><input id='entered-address-btn' type='radio' data-address='" + JSON.stringify(attr) + "' class='selected-address-btn' data-count='default' name='option' /><span class='span-radio'><span class='span-active-radio'></span></span></div>";
            }

            if (attr.address1 !== null) {
                addressHtml += '<div class="address1 address-fields">' +
                    attr.address1 +
                    '</div>';
            }
            if (attr.address2 !== null) {
                addressHtml += '<div class="address2 address-fields">' +
                    attr.address2 +
                    '</div>';
            }
            addressHtml += '<div class="address-fields">' +
                attr.city +
                ',' +
                ' ' +
                attr.stateCode +
                ' ' +
                attr.postalCode +
                '</div>' +
                '<div class="country address-fields">' +
                attr.countryName +
                '</div></div>';
        });
        body += '<div class="address-suggestion">' + addressHtml + '</div>';
    }
    return body;
}

/**
 * clears the input focus for payment fields
 */
function clearInputFocusForm() {
    $('select[name$="_expirationMonth"]').val('').change();
    $('select[name$="_expirationMonth"]').closest('.form-group').find('label').removeClass('input-focus');
    $('select[name$="_expirationYear"]').val('').change();
    $('select[name$="_expirationYear"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_cardNumber"]').val('');
    $('input[name$="_cardNumber"]').closest('.form-group').find('label').removeClass('input-focus');
    $('input[name$="_securityCode"]').val('');
    $('input[name$="_securityCode"]').closest('.form-group').find('label').removeClass('input-focus');
}

/**
 * Does Ajax call to validate address with ups
 */
function upsAddressValidation() {
    $('body').on('ups:validateAddress', function (e, data) {
        var htmlString = '';
        var body = '';
        var suggestedAddressArray;
        var userEnteredAddress;
        var isExactAddress = data.upsSuggestedAddress !== undefined ? data.upsSuggestedAddress.IsExactAddress : data.IsExactAddress;
        $.spinner().stop();
        if (isExactAddress) {
            $('body').find('.byPassAddressValidation').attr('value', 'true');
            $('body').trigger('ups:exactAddress');
            if ($('#checkout-main').hasClass('multi-ship')) {
                $('.shiptohome-shipment.save-shipment').trigger('click');
            } else {
                $('.submit-shipping').trigger('click');
            }
            setTimeout(function () {
                $('body').find('.byPassAddressValidation').attr('value', 'false');
            }, 1000);
            $('div#adddress-suggestion').remove();
            $('body').removeClass('modal-open').removeAttr('style');
            $('.modal-backdrop').remove();
        } else { // eslint-disable-line
            if (data === null || data.upsSuggestedAddress === undefined || data.upsSuggestedAddress.Addresses.length === 0) {  // eslint-disable-line
                body = createModalBody(data.RequestAddress ? data.RequestAddress : data.upsSuggestedAddress.userEnteredAddress, false);

                // Modal Container
                htmlString = createHtmlString('default', true);

                $('body').append(htmlString);
                $('div#adddress-suggestion').modal('show');
                $('#adddress-suggestion').find('.modal-body').html(body);

                $('#entered-address-btn').prop('checked', true);
                $('#entered-address-btn').closest('.suggested-address').addClass('selected-address');
                $('#adddress-suggestion .button-wrapper button.affirm').removeAttr('disabled');
                $('.suggested-address').find('input[type="radio"]').eq(0).trigger('click');
            } else {
                suggestedAddressArray = data.upsSuggestedAddress.Addresses;
                userEnteredAddress = data.upsSuggestedAddress.userEnteredAddress;

                // User entered address
                body = createModalBody(userEnteredAddress, false);

                // UPS suggested address
                body += createModalBody(suggestedAddressArray, true);

                htmlString = createHtmlString('default', false);

                $('body').append(htmlString);

                $('div#adddress-suggestion').modal('show');
                $('#adddress-suggestion').find('.modal-body').html(body);
                $('#adddress-suggestion .button-wrapper button.affirm').removeAttr('disabled');
                $('.suggested-addr-container').find('input[type="radio"]').eq(0).trigger('click');
            }
        }
        $('.modal-dialog input:radio[name="option"]').click(function () {
            $('#adddress-suggestion .button-wrapper button.affirm').removeAttr('disabled');
        });

        $('#adddress-suggestion .button-wrapper button.affirm').click(function (e) { // eslint-disable-line no-shadow
            e.preventDefault();
            if ($('.modal-dialog input:radio[name="option"]:checked')) {
                var address = $('.modal-dialog input:radio[name="option"]:checked').data('address');
                $('.shipping-address-block ').find('#shippingAddressOne').val(address.address1);
                $('.shipping-address-block ').find('#shippingAddressTwo').val(address.address2);
                $('#shippingState option[value="' + address.stateCode + '"]').prop('selected', true);
                $('#shippingState').trigger('change');
                $('#shippingCountry option[value="' + address.countryCode + '"]').prop('selected', true);
                $('.shipping-address-block ').find('#shippingAddressCity').val(address.city);
                $('.shipping-address-block ').find('#shippingZipCode').val(address.postalCode);
                $('body').find('.byPassAddressValidation').attr('value', 'true');
                if ($('#checkout-main').hasClass('multi-ship')) {
                    $('.shiptohome-shipment.save-shipment').trigger('click');
                } else {
                    $('.submit-shipping').trigger('click');
                }
                setTimeout(function () {
                    clearInputFocusForm();
                    $('input[name$="_cardNumber"]').data('cleave').setRawValue('');
                    $('body').find('.byPassAddressValidation').attr('value', 'false');
                }, 1000);
                $('div#adddress-suggestion').remove();
                $('body').removeClass('modal-open').removeAttr('style');
                $('.modal-backdrop').remove();
            }
        });
    });
}

/**
 * Perfect scroll bar & radio button background class
 */
function initEvents() {
    $(document).ajaxComplete(function () {
        if ($('.modal-body').hasClass('perfect-scrollbar')) {
            var resultsDiv = '.perfect-scrollbar';
            var perfectScrollbar = new PerfectScrollbar(resultsDiv);
            perfectScrollbar.update();
        }
    });

    $('body').on('change', '.selected-address-btn', function () {
        $('.suggested-address').removeClass('selected-address');
        if ($(this).is(':checked')) {
            $(this).closest('.suggested-address').addClass('selected-address');
        }
    });
}

base.initEvents = initEvents;
base.upsAddressValidation = upsAddressValidation;
base.createUPSModalHtmlString = createHtmlString;
base.createUPSModalBody = createModalBody;
module.exports = base;
